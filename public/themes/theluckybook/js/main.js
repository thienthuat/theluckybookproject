function addSeperator(nStr, dot) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? dot + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + dot + '$2');
    }
    return x1 + x2;
}

$(document).ready(function () {
    setInterval(function () {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function () {
            $(this).remove();
        });
    }, 5000);
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    jQuery.validator.addMethod("phoneno", function (phone_number, element) {
        phone_number = phone_number.replace(/\s+/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/(\+84|0)+([0-9]{9,10})\b/);
    }, "Số điện thoại không đúng định dạng.");
    jQuery.extend(jQuery.validator.messages, {
        required: "Vui lòng nhập đầy đủ vào ô bên trên.",
        remote: "Please fix this field.",
        email: "Vui lòng nhập địa chỉ email hợp lệ. Ví dụ abc@domain.com",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Mật khẩu không khớp nhau, vui lòng nhập lại.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Vui lòng nhập nhỏ hơn {0} ký tự."),
        minlength: jQuery.validator.format("Vui lòng nhập lớn hơn {0} ký tự."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Vui lòng nhập nhỏ hơn {0} ký tự."),
        min: jQuery.validator.format("Vui lòng nhập lớn hơn {0} ký tự.")
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.header-bottom').addClass("fix-header-fix");
        } else {
            $('.header-bottom').removeClass("fix-header-fix");
        }
    });
    $('.mobile-bar-icon').click(function () {
        $('.mobile-bar-content').addClass('open')
    })
    $('.mobile-bar-close').click(function () {
        $('.mobile-bar-content').removeClass('open')
    })
    $(".mobile-bar-content .tabs-mobile .item").click(function () {
        $(".mobile-bar-content .tabs-mobile .item").removeClass('active')
        $(this).addClass('active')
        if ($(".mobile-bar-content .tabs-mobile .item-menu").hasClass('active')) {
            $('.tabs-content-mobile.tabs-menu').css('display', 'block')
            $('.tabs-content-mobile.tabs-setting').css('display', 'none')
        } else {
            $('.tabs-content-mobile.tabs-setting').css('display', 'block')
            $('.tabs-content-mobile.tabs-menu').css('display', 'none')
        }
    })
    $('#tabs_category_product a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('#home_slider_carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        items: 1,
    });

    $('#home_news_carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            }
        }
    });

    $('#product_page_carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        cssEase: 'linear',
        arrows: true,
        asNavFor: '#thumb_product_page_carousel',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: true
                }
            }
        ]
    });
    $('#thumb_product_page_carousel').slick({
        asNavFor: '#product_page_carousel',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        slidesToShow: 5,
        slidesToScroll: 1
    });
    $('#thumb_product_page_carousel').on('afterChange', function () {
        var elm = $('#thumb_product_page_carousel'),
            getSlick = elm.slick('getSlick');

        if (getSlick.slideCount <= getSlick.options.slidesToShow) {
            elm.addClass('slick-no-slide');
        }
        else {
            elm.removeClass('slick-no-slide');
        }
    });
    $('#home_feature_product_carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    })
	var owl = $('.box-slider-category-product');
	owl.owlCarousel({
		onInitialize : function(element){
			owl.children().sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).each(function(){
				$(this).appendTo(owl);
			});
		},
		loop: false,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            400: {
                items: 2,
                nav: true
            },
            768: {
                items: 3,
                nav: true
            },
            992: {
                items: 4,
            }
        }
	});

    
    $('#login_form').validate({
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#form_create_account').validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#login_form_modal').validate({
        submitHandler: function (form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (response) {
                    $('body').removeClass('loading');
                    if (response.status === 404) {
                        var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert"';
                        html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                        html += '</button><span>' + response.message + '</span></div>';
                        $("#statusLogin").html(html);
                    } else {
                        location.reload(true);
                    }
                }
            });
        }
    });
    $('#form_birthday').validate({
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('.form-edit-account').validate({
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password_confirmation: {
                equalTo: "#password"
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#form_forgot_password').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#form_reset_password').validate({
        rules: {
            new_password: {
                required: true,
                minlength: 6,
            },
            confirm_new_password: {
                required: true,
                minlength: 6,
                equalTo: "#new_password"
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#box_check_change_password').change(function () {
        if ($(this).prop('checked') == true) {
            $("#box_show_change_password").show();
            $("#box_show_change_password").find('input').attr('required', true).attr('minlength', 6);
        } else {
            $("#box_show_change_password").hide();
            $("#box_show_change_password").find('input').attr('required', false).attr('minlength', 0);
        }
    });
    $("[data-fancybox]").fancybox();
    $('body').on('change', '#province_id', function () {
        var url = $(this).attr('data-url')
        var id = $(this).val();
        $.get(url + '/' + id, function (datas) {
            $("#district_id option").each(function () {
                if ($(this).val() != '') {
                    $(this).remove();
                }
            });
            $.each(datas.data, function (i, item) {
                $('#district_id').append($('<option>', {
                    value: item.id,
                    data_price: item.price_ship,
                    text: item.type + ' ' + item.name
                }));
            });
        });
    });
    $('.form-create-edit-address').validate({
        rules: {
            phone: {
                required: true,
                phoneno: true
            }
        },
        messages: {
            'fullname': 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'address': 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'province_id': 'Vui lòng chọn Tỉnh/Thành phố.',
            'district_id': 'Vui lòng chọn Quận/huyện.',
            'ward_id': 'Vui lòng chọn Phường, xã.',
            'phone.required': 'Vui lòng nhập đầy đủ vào ô bên trên.',
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('.form-checkout-no-login').validate({
        rules: {
            password_confirmation: {
                equalTo: "#password"
            },
            phone: {
                required: true,
                phoneno: true
            }
        },
        messages: {
            'fullname': 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'address': 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'province_id': 'Vui lòng chọn Tỉnh/Thành phố.',
            'district_id': 'Vui lòng chọn Quận/huyện.',
            'ward_id': 'Vui lòng chọn Phường, xã.',
            'phone.required': 'Vui lòng nhập đầy đủ vào ô bên trên.',
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#district_id').change(function () {
        var url = $(this).attr('data-url')
        var id = $(this).val();
        var data_price_ship = $('option:selected', this).attr('data_price');
        var data_sub_total = $('#checkout_sub_total_price').attr('data_price_ship');
        if ($('#checkout_ship_price').length > 0) {
            $('#checkout_ship_price').text(addSeperator(data_price_ship, ','));
            $('#checkout_total_price').text(addSeperator(parseInt(data_price_ship) + parseInt(data_sub_total), ','));
        }
        $.get(url + '/' + id, function (datas) {
            $("#ward_id option").each(function () {
                if ($(this).val() != '') {
                    $(this).remove();
                }
            });
            $.each(datas.data, function (i, item) {
                $('#ward_id').append($('<option>', {
                    value: item.id,
                    text: item.type + ' ' + item.name
                }));
            });
        });
    });
    $('.delete-address').click(function () {
        var url = $(this).attr('data-url');
        var _ = $(this);
        $.ajax({
            url: url,
            data: {_token: csrf_token},
            type: 'DELETE',
            success: function (result) {
                if (result.status === '401') {
                    location.reload(true);
                } else {
                    _.parents('.box-item-address').remove();
                    var html = '';
                    html += '<div class="alert alert-success alert-dismissible" role="alert">';
                    html += '<button type="button" class="close" data-dismiss="alert"';
                    html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                    html += '</button>' + result.message + '</div>';
                    $("#status").html(html);
                }
            }
        });
    });
    $('.fillter-category').change(function () {
        var id = '';
        $('input.fillter-category:checked').each(function (i, v) {
            id += $(v).val() + '-';
        })
        $("#id_category_product").val(id);
        $(this).parents('form').submit();
    });
    $('body').on('change', '.option-address-checkout', function () {
        $('.wrapper-address-option').removeClass('current');
        $(this).parents('.wrapper-address-option').addClass('current');
        var data_price_ship = $(this).attr('data_price_ship');
        if (data_price_ship !== undefined) {
            var data_sub_total = $('#checkout_sub_total_price').attr('data_price_ship');
            if ($('#checkout_ship_price').length > 0) {
                $('#checkout_ship_price').text(addSeperator(data_price_ship, ','));
                $('#checkout_total_price').text(addSeperator(parseInt(data_price_ship) + parseInt(data_sub_total), ','));
            }
        }
        if ($(this).val() == '0') {
            $('.box-form-add-address').show();
            $(".box-form-add-address").find('input,select').attr('required', true);
        } else {
            $('.box-form-add-address').hide();
            $(".box-form-add-address").find('input,select').attr('required', false);
        }
    });

    $('body').on('click', '.add-to-wishlist', function () {
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'get',
            success: function (result) {
                if (result.status === 405) {
                    window.location.href = result.url;
                } else {
                    $('.SectionContentModal').html(result.view_html);
                    $('#modalWishlist').modal('show');
                }
            }
        });
    })
    $('#newsletter-validate-detail').validate({
        submitHandler: function (form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (response) {
                    $('body').removeClass('loading');
                    alert(response.message);
                    location.reload(true);
                }
            });
        }
    });
    $('#frmContact').validate({
        submitHandler: function (form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (response) {
                    $('body').removeClass('loading');
                    alert(response.message);
                    location.reload(true);
                }
            });
        }
    });
    $('body').on('click', '.delete-item-favourite', function () {
        var id = $(this).attr('data-id');
        var _ = $(this);
        var url = $('#url_hidden_delete').val();
        $.ajax({
            url: url,
            data: {_token: csrf_token, id: id},
            type: 'DELETE',
            success: function (result) {
                if (result.status === 405) {
                    var html = '';
                    html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                    html += '<button type="button" class="close" data-dismiss="alert"';
                    html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                    html += '</button>' + result.message + '</div>';
                    $("#status").html(html);
                } else {
                    var html = '';
                    html += '<div class="alert alert-success alert-dismissible" role="alert">';
                    html += '<button type="button" class="close" data-dismiss="alert"';
                    html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                    html += '</button>' + result.message + '</div>';
                    $("#status").html(html);
                    _.parents('item').fadeOut();
                }
            }
        });
    })
    $('body').on('click', '.delete-birthday', function () {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var _ = $(this);
        $.ajax({
            url: url,
            data: {_token: csrf_token, id: id},
            type: 'DELETE',
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (result) {
                $('body').removeClass('loading');
                if (result.status === 405) {
                    var html = '';
                    html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                    html += '<button type="button" class="close" data-dismiss="alert"';
                    html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                    html += '</button>' + result.message + '</div>';
                    $("#status").html(html);
                } else {
                    var html = '';
                    html += '<div class="alert alert-success alert-dismissible" role="alert">';
                    html += '<button type="button" class="close" data-dismiss="alert"';
                    html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                    html += '</button>' + result.message + '</div>';
                    $("#status").html(html);
                    _.parents('tr').fadeOut();
                }
            }
        });
    })
    $('body').on('click', '.filter-size ul li input', function (e) {
        var title = '';
        e.stopPropagation();
        $.each($('.filter-size ul li'), function (i, e) {
            if ($(e).find('input').is(':checked')) {
                var v = $(e).find('input').val();
                title += '.' + v
            }
        })
        $("#filter_size").val(title)
        var form = $(this).parents('form');
        setTimeout(function () {
            form.submit();
        }, 2000);
    });
    $('body').on('click', '.filter-color ul li input', function (e) {
        e.stopPropagation();
        var title = '';
        $.each($('.filter-color ul li'), function (i, e) {
            if ($(e).find('input').is(':checked')) {
                var v = $(e).find('input').val();
                title += '.' + v
            }
        })
        $("#filter_color").val(title);
        var form = $(this).parents('form');
        setTimeout(function () {
            form.submit();
        }, 2000);
    });
});
(function () {
    function rangeInputChangeEventHandler(e) {
        var rangeGroup = $(this).attr('name'),
            minBtn = $(this).parent().children('.min'),
            maxBtn = $(this).parent().children('.max'),
            range_min = $(this).parent().children('.range_min'),
            range_max = $(this).parent().children('.range_max'),
            minVal = parseInt($(minBtn).val()),
            maxVal = parseInt($(maxBtn).val()),
            form = $(this).parents('form'),
            origin = $(this)[0].className;

        if (origin === 'min' && minVal > maxVal - 5) {
            $(minBtn).val(maxVal - 5);
        }
        var minVal = parseInt($(minBtn).val());
        $(range_min).html('<sup>đ</sup>' + addSeperator(minVal, '.'));


        if (origin === 'max' && maxVal - 5 < minVal) {
            $(maxBtn).val(5 + minVal);
        }
        var maxVal = parseInt($(maxBtn).val());
        $(range_max).html('<sup>đ</sup>' + addSeperator(maxVal, '.'));
        form.submit();
    }

    $('input[type="range"]').on('change', rangeInputChangeEventHandler);
})();

