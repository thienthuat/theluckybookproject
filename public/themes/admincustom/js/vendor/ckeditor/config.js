/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.allowedContent = true;
	config.filebrowserBrowseUrl =  Asgard.mediaGridCkEditor;
};
