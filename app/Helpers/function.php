<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 11/9/17
 * Time: 10:08 PM
 */
if (!function_exists('menuMultiple')) {
    function menuMultiple($menus, $id_parent = 0, $level = 1)
    {
        $menu_tmp = array();
        foreach ($menus as $key => $item) {
            if ((int)$item['parent_id'] == (int)$id_parent) {
                $menu_tmp[] = $item;
                unset($menus[$key]);
            }
        }
        if ($menu_tmp) {
            if ($level == 1) {
                foreach ($menu_tmp as $item) {
                    echo '<li class="multiple-menu has-sub-menu">';
                    echo '<a href="' . route('page.product.getProductCategory', $item['slug']) . '" title="' . $item['title'] . '" class="">' . $item['title'] . '</a>';
                    menuMultiple($menus, $item['id'], $level + 1);
                    echo '</li>';
                }
            } elseif ($level == 2) {
                echo '<ul class="sub-menu">';
                foreach ($menu_tmp as $item) {
                    echo "<ul class='num'>";
                    echo '<li class="item-menu-first">';
                    echo '<a href="' . route('page.product.getProductCategory', $item['slug']) . '">' . $item['title'] . '</a>';
                    echo '</li>';
                    menuMultiple($menus, $item['id'], $level + 1);
                    echo "</ul>";
                }
                echo '</ul>';
            } else {
                foreach ($menu_tmp as $item) {
                    echo '<li>';
                    echo '<a href="' . route('page.product.getProductCategory', $item['slug']) . '">' . $item['title'] . '</a>';
                    menuMultiple($menus, $item['id'], $level + 1);
                    echo '</li>';
                }
            }

        }
    }
}