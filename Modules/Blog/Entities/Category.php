<?php

namespace Modules\Blog\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;

    public $translatedAttributes = ['name', 'slug', 'meta_title', 'meta_keyword', 'meta_description'];
    protected $fillable = ['name', 'slug', 'meta_title', 'meta_keyword', 'meta_description'];
    protected $table = 'blog__categories';

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
