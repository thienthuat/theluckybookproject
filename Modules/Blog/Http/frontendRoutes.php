<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => trans('blog::blog.slug')], function (Router $router) {
    $router->get('/',[
        'uses' => 'PublicController@index',
        'as' => 'blog.index'
    ]);
    $router->get('/{slug}',[
        'uses' => 'PublicController@detail',
        'as' => 'blog.detail'
    ]);
    $router->get('tag/{slug}',[
        'uses' => 'PublicController@tag',
        'as' => 'blog.tag'
    ]);
});
