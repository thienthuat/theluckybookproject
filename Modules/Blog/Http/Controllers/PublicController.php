<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Support\Facades\App;
use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Blog\Repositories\TagRepository;
use Modules\Product\Repositories\ProductRepository;

class PublicController extends BasePublicController
{
    /**
     * @var PostRepository
     */
    private $post;
    private $tagRepository;
    private $productRepository;

    public function __construct(PostRepository $post, TagRepository $tagRepository, ProductRepository $productRepository)
    {
        parent::__construct();
        $this->post = $post;
        $this->tagRepository = $tagRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $posts = $this->post->paginate(6);

        return view('pages.blogs.index', compact('posts'));
    }

    public function detail($slug)
    {
        $post = $this->post->findBySlug($slug);
        if ($post) {
            $tags = $post->tags;
            return view('pages.blogs.detail', compact('post', 'tags'));
        } else {
            return redirect()->route('page.404');
        }
    }

    public function tag($slug)
    {
        $tag = $this->tagRepository->findBySlug($slug);
        if ($tag) {
            $posts = $tag->posts()->paginate(6);
            return view('pages.blogs.tag', compact('posts', 'tag'));
        } else {
            return redirect()->route('page.404');
        }
    }

    public function widgetLeft()
    {
        $posts = $this->post->latest(6);
        $products = $this->productRepository->getProductByAction();
        return view('partials.blogs.left', compact('posts','products'));
    }
}
