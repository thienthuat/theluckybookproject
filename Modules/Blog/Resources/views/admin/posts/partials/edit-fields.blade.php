<div class="box-body">
    <div class='form-group{{ $errors->has("$lang.title") ? ' has-error' : '' }}'>
        <?php $oldTitle = isset($post->translate($lang)->title) ? $post->translate($lang)->title : ''; ?>
        {!! Form::label("{$lang}[title]", trans('blog::post.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("$lang.title", $oldTitle), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('blog::post.form.title')]) !!}
        {!! $errors->first("$lang.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("$lang.slug") ? ' has-error' : '' }}'>
        <?php $oldSlug = isset($post->translate($lang)->slug) ? $post->translate($lang)->slug : ''; ?>
       {!! Form::label("{$lang}[slug]", trans('blog::post.form.slug')) !!}
       {!! Form::text("{$lang}[slug]", old("$lang.slug", $oldSlug), ['class' => 'form-control slug', 'data-slug' => 'target', 'placeholder' => trans('blog::post.form.slug')]) !!}
       {!! $errors->first("$lang.slug", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}[sumary]") ? ' has-error' : '' }}'>
        <?php $old = isset($post->translate($lang)->content) ? $post->translate($lang)->sumary : ''; ?>
        {!! Form::label("{$lang}[sumary]", trans('page::pages.form.sumary')) !!}
        <textarea class="form-control" name="{{$lang}}[sumary]" rows="10"
                  cols="80">{{ old("$lang.sumary",$old) }}</textarea>
        {!! $errors->first("{$lang}[sumary]", '<span class="help-block">:message</span>') !!}
    </div>
    <?php $old = isset($post->translate($lang)->content) ? $post->translate($lang)->content : ''; ?>
    @editor('content', trans('blog::post.form.content'), old("$lang.content", $old), $lang)

    <?php if (config('asgard.blog.config.post.partials.translatable.edit') !== []): ?>
        <?php foreach (config('asgard.blog.config.post.partials.translatable.edit') as $partial): ?>
        @include($partial)
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo-{{$lang}}">
                        {{ trans('page::pages.form.meta_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseTwo-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("{$lang}[meta_title]") ? ' has-error' : '' }}'>
                        <?php $old = $post->hasTranslation($locale) ? $post->translate($lang)->meta_title : '' ?>
                        {!! Form::label("{$lang}[meta_title]", trans('page::pages.form.meta_title')) !!}
                        {!! Form::text("{$lang}[meta_title]", old("$lang.meta_title",$old), ['class' => "form-control", 'placeholder' => trans('page::pages.form.meta_title')]) !!}
                        {!! $errors->first("{$lang}[meta_title]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}[meta_keyword]") ? ' has-error' : '' }}'>
                        <?php $old = $post->hasTranslation($locale) ? $post->translate($lang)->meta_keyword : '' ?>
                        {!! Form::label("{$lang}[meta_keyword]", trans('page::pages.form.meta_keyword')) !!}
                        {!! Form::text("{$lang}[meta_keyword]", old("$lang.meta_keyword",$old), ['class' => "form-control","data-role"=>"tagsinput" ,'placeholder' => trans('page::pages.form.meta_keyword')]) !!}
                        {!! $errors->first("{$lang}[meta_keyword]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}[meta_description]") ? ' has-error' : '' }}'>
                        <?php $old = $post->hasTranslation($locale) ? $post->translate($lang)->meta_description : '' ?>
                        {!! Form::label("{$lang}[meta_description]", trans('page::pages.form.meta_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[meta_description]" rows="10"
                                  cols="80">{{ old("$lang.meta_description",$old) }}</textarea>
                        {!! $errors->first("{$lang}[meta_description]", '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
