<?php

namespace Modules\Customer\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Customer\Events\Handlers\RegisterCustomerSidebar;

class CustomerServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    protected $middleware = [
        'auth.webclient' => \Modules\Customer\Http\Middleware\CheckCustomerLogin::class
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterCustomerSidebar::class);
    }

    public function boot()
    {
        $this->registerMiddleware();
        $this->publishConfig('customer', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    private function registerMiddleware()
    {
        foreach ($this->middleware as $name => $class) {
            $this->app['router']->aliasMiddleware($name, $class);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Customer\Repositories\CustomerRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentCustomerRepository(new \Modules\Customer\Entities\Customer());

                if (!config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CacheCustomerDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Customer\Repositories\FavouriteRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentFavouriteRepository(new \Modules\Customer\Entities\Favourite());

                if (!config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CacheFavouriteDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Customer\Repositories\AddressRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentAddressRepository(new \Modules\Customer\Entities\Address());

                if (!config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CacheAddressDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Customer\Repositories\keyRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentkeyRepository(new \Modules\Customer\Entities\key());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CachekeyDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Customer\Repositories\TypeRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentTypeRepository(new \Modules\Customer\Entities\Type());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CacheTypeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Customer\Repositories\BirthdaychildRepository',
            function () {
                $repository = new \Modules\Customer\Repositories\Eloquent\EloquentBirthdaychildRepository(new \Modules\Customer\Entities\Birthdaychild());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Customer\Repositories\Cache\CacheBirthdaychildDecorator($repository);
            }
        );
// add bindings





    }
}
