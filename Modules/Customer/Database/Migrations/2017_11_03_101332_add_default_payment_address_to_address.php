<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultPaymentAddressToAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer__addresses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->tinyInteger('default_payment_address')->after('default_shipping_address')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__addresses', function (Blueprint $table) {
            $table->dropColumn('default_payment_address');
        });
    }
}
