<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerkeyTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer__key_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('key_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['key_id', 'locale']);
            $table->foreign('key_id')->references('id')->on('customer__keys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__key_translations', function (Blueprint $table) {
            $table->dropForeign(['key_id']);
        });
        Schema::dropIfExists('customer__key_translations');
    }
}
