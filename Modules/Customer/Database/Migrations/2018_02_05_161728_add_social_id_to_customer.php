<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialIdToCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer__customers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('social_id')->after('sex')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__customers', function (Blueprint $table) {
            $table->dropColumn('social_id');
        });
    }
}
