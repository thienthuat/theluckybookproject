<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeCustomerToCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer__customers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('type_id')->after('sex')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__customers', function (Blueprint $table) {
            $table->dropColumn('type_id');
        });
    }
}
