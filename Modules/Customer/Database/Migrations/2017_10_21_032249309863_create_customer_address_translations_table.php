<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAddressTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer__address_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('address_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['address_id', 'locale']);
            $table->foreign('address_id')->references('id')->on('customer__addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__address_translations', function (Blueprint $table) {
            $table->dropForeign(['address_id']);
        });
        Schema::dropIfExists('customer__address_translations');
    }
}
