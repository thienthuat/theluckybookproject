<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerBirthdaychildTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('birthdaychild_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('birthdaychild_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['birthdaychild_id', 'locale']);
            $table->foreign('birthdaychild_id')->references('id')->on('customer__birthdaychildren')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('birthdaychild_translations', function (Blueprint $table) {
            $table->dropForeign(['birthdaychild_id']);
        });
        Schema::dropIfExists('birthdaychild_translations');
    }
}
