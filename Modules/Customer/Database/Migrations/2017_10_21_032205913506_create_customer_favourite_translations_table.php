<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerFavouriteTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer__favourite_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('favourite_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['favourite_id', 'locale']);
            $table->foreign('favourite_id')->references('id')->on('customer__favourites')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer__favourite_translations', function (Blueprint $table) {
            $table->dropForeign(['favourite_id']);
        });
        Schema::dropIfExists('customer__favourite_translations');
    }
}
