<?php

namespace Modules\Customer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Customer\Entities\Favourite;
use Modules\Customer\Http\Requests\CreateFavouriteRequest;
use Modules\Customer\Http\Requests\UpdateFavouriteRequest;
use Modules\Customer\Repositories\FavouriteRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class FavouriteController extends AdminBaseController
{
    /**
     * @var FavouriteRepository
     */
    private $favourite;

    public function __construct(FavouriteRepository $favourite)
    {
        parent::__construct();

        $this->favourite = $favourite;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$favourites = $this->favourite->all();

        return view('customer::admin.favourites.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer::admin.favourites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFavouriteRequest $request
     * @return Response
     */
    public function store(CreateFavouriteRequest $request)
    {
        $this->favourite->create($request->all());

        return redirect()->route('admin.customer.favourite.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('customer::favourites.title.favourites')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Favourite $favourite
     * @return Response
     */
    public function edit(Favourite $favourite)
    {
        return view('customer::admin.favourites.edit', compact('favourite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Favourite $favourite
     * @param  UpdateFavouriteRequest $request
     * @return Response
     */
    public function update(Favourite $favourite, UpdateFavouriteRequest $request)
    {
        $this->favourite->update($favourite, $request->all());

        return redirect()->route('admin.customer.favourite.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('customer::favourites.title.favourites')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Favourite $favourite
     * @return Response
     */
    public function destroy(Favourite $favourite)
    {
        $this->favourite->destroy($favourite);

        return redirect()->route('admin.customer.favourite.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('customer::favourites.title.favourites')]));
    }
}
