<?php

namespace Modules\Customer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Customer\Entities\key;
use Modules\Customer\Http\Requests\CreatekeyRequest;
use Modules\Customer\Http\Requests\UpdatekeyRequest;
use Modules\Customer\Repositories\keyRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class keyController extends AdminBaseController
{
    /**
     * @var keyRepository
     */
    private $key;

    public function __construct(keyRepository $key)
    {
        parent::__construct();

        $this->key = $key;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$keys = $this->key->all();

        return view('customer::admin.keys.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer::admin.keys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatekeyRequest $request
     * @return Response
     */
    public function store(CreatekeyRequest $request)
    {
        $this->key->create($request->all());

        return redirect()->route('admin.customer.key.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('customer::keys.title.keys')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  key $key
     * @return Response
     */
    public function edit(key $key)
    {
        return view('customer::admin.keys.edit', compact('key'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  key $key
     * @param  UpdatekeyRequest $request
     * @return Response
     */
    public function update(key $key, UpdatekeyRequest $request)
    {
        $this->key->update($key, $request->all());

        return redirect()->route('admin.customer.key.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('customer::keys.title.keys')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  key $key
     * @return Response
     */
    public function destroy(key $key)
    {
        $this->key->destroy($key);

        return redirect()->route('admin.customer.key.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('customer::keys.title.keys')]));
    }
}
