<?php

namespace Modules\Customer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Http\Requests\CreateCustomerRequest;
use Modules\Customer\Http\Requests\UpdateCustomerRequest;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Location\Repositories\WardRepository;
use Modules\Customer\Repositories\TypeRepository;
use Modules\Customer\Repositories\AddressRepository;
use Hash;
use Hashids\Hashids;

class CustomerController extends AdminBaseController
{
    /**
     * @var CustomerRepository
     */
    private $customer;
    private $provinceRepository;
    private $wardRepository;
    private $districtRepository;
    private $typeRepository;
    private $addressRepository;
    private $hashids;

    public function __construct(CustomerRepository $customer,
                                ProvinceRepository $provinceRepository,
                                DistrictRepository $districtRepository,
                                AddressRepository $addressRepository,
                                TypeRepository $typeRepository,
                                WardRepository $wardRepository)
    {
        parent::__construct();

        $this->customer = $customer;
        $this->provinceRepository = $provinceRepository;
        $this->districtRepository = $districtRepository;
        $this->wardRepository = $wardRepository;
        $this->typeRepository = $typeRepository;
        $this->addressRepository = $addressRepository;
        $this->hashids = new Hashids('', 10, '123456789QƯERTYUIOPASDFGHJKLZXCVBNM');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $customers = $this->customer->all();

        return view('customer::admin.customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $provinces = $this->provinceRepository->all();
        $types = $this->typeRepository->all();
        return view('customer::admin.customers.create', compact('provinces', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $data = $request->all();
        $this->hashPassword($data);
        $customer = $this->customer->create($data);
        $customer->code = $this->hashids->encode($customer->id);
        $customer->save();

        return redirect()->route('admin.customer.customer.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('customer::customers.title.customers')]));
    }

    private function hashPassword(array &$data)
    {
        $data['password'] = Hash::make($data['password']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Customer $customer
     * @return Response
     */
    public function edit(Customer $customer)
    {
        $types = $this->typeRepository->all();
        $provinces = $this->provinceRepository->all();
        $district = $this->districtRepository->find($customer->district);
        $ward = $this->wardRepository->find($customer->ward);
        $address_ship_default = $this->addressRepository->getShipDefault($customer->id);
        $address_payment_default = $this->addressRepository->getPaymentDefault($customer->id);
        return view('customer::admin.customers.edit', compact('customer', 'provinces', 'district', 'ward', 'types', 'address_ship_default', 'address_payment_default'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Customer $customer
     * @param  UpdateCustomerRequest $request
     * @return Response
     */
    public function update(Customer $customer, UpdateCustomerRequest $request)
    {
        $data = $request->all();
        if ($data['password'] != "") {
            $this->hashPassword($data);
        } else {
            unset($data['password']);
        }
        unset($data['email']);
        $this->customer->update($customer, $data);
        $this->customer->updateBithday($customer, $data);

        return redirect()->route('admin.customer.customer.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('customer::customers.title.customers')]));
    }

    public function addressShip(Customer $customer, $address_id)
    {
        $address = $this->addressRepository->find($address_id);
        $provinces = $this->provinceRepository->all();
        $district = '';
        $ward = '';
        if ($address) {
            $district = $this->districtRepository->find($address->district);
            $ward = $this->wardRepository->find($address->wards);

        }
        return view('customer::admin.customers.ship', compact('customer', 'provinces', 'district', 'ward', 'address'));
    }

    public function postAddressShip(Request $request, $address_id)
    {
        $this->addressRepository->createOrUpdateShip($request->all(), $address_id);
        return redirect()->route('admin.customer.customer.edit', $request->customer_id)
            ->withSuccess('Create Or Update Success Address Ship');;
    }

    public function addressPayment(Customer $customer, $address_id)
    {
        $address = $this->addressRepository->find($address_id);
        $provinces = $this->provinceRepository->all();
        $district = '';
        $ward = '';
        if ($address) {
            $district = $this->districtRepository->find($address->district);
            $ward = $this->wardRepository->find($address->wards);

        }
        return view('customer::admin.customers.payment', compact('customer', 'provinces', 'district', 'ward', 'address'));
    }

    public function postAddressPayment(Request $request, $address_id)
    {
        $this->addressRepository->createOrUpdatePayment($request->all(), $address_id);
        return redirect()->route('admin.customer.customer.edit', $request->customer_id)
            ->withSuccess('Create Or Update Success Address Payment');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $customer
     * @return Response
     */
    public function destroy(Customer $customer)
    {
        $this->customer->destroy($customer);

        return redirect()->route('admin.customer.customer.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('customer::customers.title.customers')]));
    }
}
