<?php

namespace Modules\Customer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Customer\Entities\Birthdaychild;
use Modules\Customer\Http\Requests\CreateBirthdaychildRequest;
use Modules\Customer\Http\Requests\UpdateBirthdaychildRequest;
use Modules\Customer\Repositories\BirthdaychildRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class BirthdaychildController extends AdminBaseController
{
    /**
     * @var BirthdaychildRepository
     */
    private $birthdaychild;

    public function __construct(BirthdaychildRepository $birthdaychild, Request $request)
    {
        parent::__construct();

        $this->birthdaychild = $birthdaychild;
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $month = $this->request->get('month') != null ? $this->request->get('month') : '';
        $birthdaychildren = $this->birthdaychild->getDataChild($month);

        return view('customer::admin.birthdaychildren.index', compact('birthdaychildren', 'month'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer::admin.birthdaychildren.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBirthdaychildRequest $request
     * @return Response
     */
    public function store(CreateBirthdaychildRequest $request)
    {
        $this->birthdaychild->create($request->all());

        return redirect()->route('admin.customer.birthdaychild.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('customer::birthdaychildren.title.birthdaychildren')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Birthdaychild $birthdaychild
     * @return Response
     */
    public function edit(Birthdaychild $birthdaychild)
    {
        return view('customer::admin.birthdaychildren.edit', compact('birthdaychild'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Birthdaychild $birthdaychild
     * @param  UpdateBirthdaychildRequest $request
     * @return Response
     */
    public function update(Birthdaychild $birthdaychild, UpdateBirthdaychildRequest $request)
    {
        $this->birthdaychild->update($birthdaychild, $request->all());

        return redirect()->route('admin.customer.birthdaychild.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('customer::birthdaychildren.title.birthdaychildren')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Birthdaychild $birthdaychild
     * @return Response
     */
    public function destroy(Birthdaychild $birthdaychild)
    {
        $this->birthdaychild->destroy($birthdaychild);

        return redirect()->route('admin.customer.birthdaychild.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('customer::birthdaychildren.title.birthdaychildren')]));
    }

    public function delete($id)
    {
        $birthdaychild = $this->birthdaychild->find($id);
        $this->birthdaychild->destroy($birthdaychild);
    }
}
