<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Location\Repositories\WardRepository;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Customer\Repositories\AddressRepository;
use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Customer\Entities\Favourite;
use Modules\Product\Repositories\ProductRepository;
use Modules\Customer\Repositories\BirthdaychildRepository;
use Validator;
use Auth;
use Session;
use Hash;
use Hashids\Hashids;

class PublicController extends BasePublicController
{
    protected $app;
    protected $customerRepository;
    protected $provinceRepository;
    protected $districtRepository;
    protected $wardRepository;
    protected $addressRepository;
    protected $invoiceRepository;
    protected $productRepository;
    protected $birthdaychildRepository;
    protected $hashids;

    public function __construct(
        Application $app,
        CustomerRepository $customerRepository,
        ProvinceRepository $provinceRepository,
        BirthdaychildRepository $birthdaychildRepository,
        WardRepository $wardRepository,
        AddressRepository $addressRepository,
        InvoiceRepository $invoiceRepository,
        ProductRepository $productRepository,
        DistrictRepository $districtRepository
    ) {
        $this->app = $app;
        $this->customerRepository = $customerRepository;
        $this->provinceRepository = $provinceRepository;
        $this->wardRepository = $wardRepository;
        $this->districtRepository = $districtRepository;
        $this->addressRepository = $addressRepository;
        $this->productRepository = $productRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->birthdaychildRepository = $birthdaychildRepository;
        $this->hashids = new Hashids('', 10, '123456789QƯERTYUIOPASDFGHJKLZXCVBNM');
    }

    public function getAccount()
    {
        $user = Auth::guard('customer')->user();
        $address_ship_default = $this->addressRepository->getShipDefault($user->id);
        $address_payment_default = $this->addressRepository->getPaymentDefault($user->id);
        return view('pages.accounts.account', compact('address_payment_default', 'address_ship_default'));
    }

    public function editAccount()
    {
        return view('pages.accounts.edit-account');
    }

    public function postAccountEdit(Request $request)
    {
        $user = Auth::guard('customer')->user();
        if ($user) {
            $user_check = $this->customerRepository->model()->where('email', strip_tags($request->email))->where('id', '!=', $user->id)->first();
            if ($user_check) {
                Session::flash('error', "Email đã tồn tại.");
                return redirect()->back()->withInput();
            } else {
                $user->email = strip_tags($request->email);
                $user->first_name = strip_tags($request->firstname);
                $user->last_name = strip_tags($request->lastname);
                $user->sex = strip_tags($request->sex);
                $user->birthday = strip_tags($request->birthday);
                $user->save();
                if (strip_tags($request->change_password) != '') {
                    if (Hash::check(strip_tags($request->current_password), $user->password)) {
                        $user->password = Hash::make(strip_tags($request->password));
                        $user->save();
                        Session::flash('success', "Cập nhật thành công.");
                        return redirect()->back();
                    } else {
                        Session::flash('error', "Mật khẩu hiện tại không đúng.");
                        return redirect()->back()->withInput();
                    }
                } else {
                    Session::flash('success', "Cập nhật thành công.");
                    return redirect()->back();
                }
            }
        } else {
            return redirect()->route('page.login');
        }
    }

    public function manageAddress()
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->model()->where('customer_id', $user->id)->get();
        $address_ship_default = $this->addressRepository->getShipDefault($user->id);
        $address_payment_default = $this->addressRepository->getPaymentDefault($user->id);
        return view('pages.accounts.address.index', compact('address', 'address_ship_default', 'address_payment_default'));
    }

    public function addMoreAddress()
    {
        $provinces = $this->provinceRepository->all();
        return view('pages.accounts.address.add', compact('provinces'));
    }

    public function postAddMoreAddress(Request $request)
    {
        $rules = [
            'fullname' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'phone' => 'required'
        ];
        $messages = [
            'fullname.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'address.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
            'province_id.required' => 'Vui lòng chọn Tỉnh/Thành phố.',
            'district_id.required' => 'Vui lòng chọn Quận/huyện.',
            'ward_id.required' => 'Vui lòng chọn Phường, xã.',
            'phone.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = Auth::guard('customer')->user();
            $data = [
                'full_name' => strip_tags($request->fullname),
                'city' => strip_tags($request->province_id),
                'district' => strip_tags($request->district_id),
                'wards' => strip_tags($request->ward_id),
                'address' => strip_tags($request->address),
                'phone' => strip_tags($request->phone),
                'customer_id' => strip_tags($user->id)
            ];
            $this->addressRepository->create($data);
            Session::flash('success', "Tạo mới địa chỉ thành công.");
            return redirect()->route('page.customer.manageAddress');
        }
    }

    public function ajaxDistrict($id)
    {
        $data = $this->districtRepository->getDataByIdProvince(strip_tags($id));
        return \response()->json(['data' => $data])->header('Author', 'linhdev92@gmail.om');
    }

    public function ajaxWard($id)
    {
        $data = $this->wardRepository->getDataByIdDistrict(strip_tags($id));
        return \response()->json(['data' => $data])->header('Author', 'linhdev92@gmail.om');
    }

    public function deleteMoreAddress($id, Request $request)
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->find(strip_tags($id));
        if ($address) {
            if ($address->customer_id == $user->id) {
                if ($address->default_shipping_address == 1 || $address->default_payment_address) {
                    return response()->json(['status' => '401']);
                } else {
                    $this->addressRepository->destroy($address);
                    return response()->json(['status' => '200', 'message' => 'Xoá địa chỉ thành công.']);
                }
            } else {
                return response()->json(['status' => '401']);
            }
        } else {
            return response()->json(['status' => '401']);
        }
    }

    public function editMoreAddress($id)
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->find(strip_tags($id));
        if ($address) {
            if ($address->customer_id == $user->id) {
                $provinces = $this->provinceRepository->all();
                $districts = $this->districtRepository->getDataByIdProvince($address->city);
                $wards = $this->wardRepository->getDataByIdDistrict($address->district);
                return view('pages.accounts.address.edit', compact('provinces', 'address', 'districts', 'wards'));
            } else {
                return view('error.404');
            }
        } else {
            return view('error.404');
        }
    }

    public function setDefaultShip($id)
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->find(strip_tags($id));
        if ($address) {
            if ($address->customer_id == $user->id) {
                $address_user = $user->addressre;
                foreach ($address_user as $item) {
                    if ($address->id == $item->id) {
                        $item->default_shipping_address = 1;
                        $item->save();
                    } else {
                        $item->default_shipping_address = 0;
                        $item->save();
                    }
                }
                Session::flash('success', "Cập nhật địa chỉ thành công.");
                return redirect()->route('page.customer.manageAddress');
            } else {
                return view('error.404');
            }
        } else {
            return view('error.404');
        }
    }

    public function setDefaultPayment($id)
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->find(strip_tags($id));
        if ($address) {
            if ($address->customer_id == $user->id) {
                $address_user = $user->addressre;
                foreach ($address_user as $item) {
                    if ($address->id == $item->id) {
                        $item->default_payment_address = 1;
                        $item->save();
                    } else {
                        $item->default_payment_address = 0;
                        $item->save();
                    }
                }
                Session::flash('success', "Cập nhật địa chỉ thành công.");
                return redirect()->route('page.customer.manageAddress');
            } else {
                return view('error.404');
            }
        } else {
            return view('error.404');
        }
    }

    public function postEditMoreAddress($id, Request $request)
    {
        $user = Auth::guard('customer')->user();
        $address = $this->addressRepository->find(strip_tags($id));
        if ($address) {
            if ($address->customer_id == $user->id) {
                $rules = [
                    'fullname' => 'required',
                    'address' => 'required',
                    'province_id' => 'required',
                    'district_id' => 'required',
                    'ward_id' => 'required',
                    'phone' => 'required'
                ];
                $messages = [
                    'fullname.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                    'address.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                    'province_id.required' => 'Vui lòng chọn Tỉnh/Thành phố.',
                    'district_id.required' => 'Vui lòng chọn Quận/huyện.',
                    'ward_id.required' => 'Vui lòng chọn Phường, xã.',
                    'phone.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                ];
                $validator = Validator::make($request->all(), $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $user = Auth::guard('customer')->user();
                    $data = [
                        'full_name' => strip_tags($request->fullname),
                        'city' => strip_tags($request->province_id),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => strip_tags($user->id)
                    ];
                    $this->addressRepository->update($address, $data);
                    Session::flash('success', "Cập nhật địa chỉ thành công.");
                    return redirect()->route('page.customer.manageAddress');
                }
            } else {
                return view('error.404');
            }
        } else {
            return view('error.404');
        }
    }

    public function myOrder()
    {
        $user = Auth::guard('customer')->user();
        if ($user) {
            $invoices = $this->invoiceRepository->getInvoiceByUser($user->id);
            return view('pages.accounts.my-order', compact('invoices'));
        } else {
            return view('error.404');
        }
    }

    public function detailOrder($code)
    {
        $invoice = $this->invoiceRepository->findByAttributes(['code' => $code]);
        if ($invoice) {
            return view('pages.accounts.detail-order', compact('invoice'));
        } else {
            return view('error.404');
        }
    }

    public function myWishList()
    {
        $user = Auth::guard('customer')->user();
        $all_favourite = Favourite::where('customer_id', $user->id)->get();
        return view('pages.accounts.my-wishlist', compact('all_favourite'));
    }

    public function deleteMyWishList(Request $request)
    {
        $user = Auth::guard('customer')->user();
        $favourite = Favourite::where('id', $request->id)->where('customer_id', $user->id)->first();
        if ($favourite) {
            Favourite::destroy($favourite->id);
            return response()->json(['status' => 200, 'message' => 'Xoá sản phẩm yêu thích thành công.']);
        } else {
            return response()->json(['status' => 405, 'message' => 'Bạn không thể xoá sản phẩm này.']);
        }
    }

    public function addToWishList($id)
    {
        $user = Auth::guard('customer')->user();
        if ($user) {
            Favourite::updateOrCreate(['customer_id' => $user->id, 'product_id' => $id]);
            $product = $this->productRepository->find($id);
            return response()->json(['status' => 200, 'view_html' => view('partials.cart.modal-wishlist', compact('product'))->render()]);
        } else {
            session(['IDProductAddToWishList' => $id]);
            return response()->json(['status' => 405, 'url' => route('page.login')]);
        }
    }

    public function myBirthdayChild()
    {
        return view('pages.accounts.my-birthday');
    }

    public function postAddBirthday(Request $request)
    {
        $user = Auth::guard('customer')->user();
        $full_name = $request->full_name;
        $birthday_child = $request->birthday_child;
        $sex_child = $request->sex_child;
        $note_child = $request->note_child;
        $this->birthdaychildRepository->create([
            'customer_id' => $user->id, 'full_name' => $full_name,
            'birthday_child' => $birthday_child, 'sex_child' => $sex_child, 'note_child' => $note_child
        ]);
        Session::flash('success', "Cập nhật ngày sinh thành công.");
        return redirect()->back();
    }

    public function deleteBirthday(Request $request)
    {
        $user = Auth::guard('customer')->user();
        $id = $request->id;
        $birthday = $this->birthdaychildRepository->find($id);
        if ($birthday && $birthday->customer_id == $user->id) {
            $this->birthdaychildRepository->destroy($birthday);
            return response()->json(['status' => 200, 'message' => 'Xoá ngày sinh thành công']);
        }
    }

    public function myPointAccumulated()
    {
        return view('pages.accounts.my-point');
    }
}
