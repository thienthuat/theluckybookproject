<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 10/30/17
 * Time: 4:25 PM
 */
use Illuminate\Routing\Router;

/** @var Router $router */

$router->group(['prefix' => '/' . trans('customer::url.customer')], function (Router $router) {
    $router->get(trans('customer::url.my_customer'), [
        'as' => 'page.customer.account',
        'uses' => 'PublicController@getAccount',
        'middleware' => 'auth.webclient'
    ]);
    $router->get(trans('customer::url.my_customer_edit'), [
        'as' => 'page.customer.accountEdit',
        'uses' => 'PublicController@editAccount',
        'middleware' => 'auth.webclient'
    ]);

    $router->get(trans('customer::url.my_point_accumulated'), [
        'as' => 'page.customer.myPointAccumulated',
        'uses' => 'PublicController@myPointAccumulated',
        'middleware' => 'auth.webclient'
    ]);

    $router->post(trans('customer::url.my_customer_edit'), [
        'as' => 'page.customer.postAccountEdit',
        'uses' => 'PublicController@postAccountEdit',
        'middleware' => 'auth.webclient'
    ]);
    $router->post('/add-birthday-customer', [
        'as' => 'page.customer.postAddBirthday',
        'uses' => 'PublicController@postAddBirthday',
        'middleware' => 'auth.webclient'
    ]);
    $router->delete('/delete-birthday-customer', [
        'as' => 'page.customer.deleteBirthday',
        'uses' => 'PublicController@deleteBirthday',
        'middleware' => 'auth.webclient'
    ]);
    $router->get('/danh-sach-sinh-nhat', [
        'as' => 'page.customer.myBirthdayChild',
        'uses' => 'PublicController@myBirthdayChild'
    ]);
    $router->group(['prefix' => '/' . trans('customer::url.my_manage_address'), 'middleware' => 'auth.webclient'], function (Router $router) {
        $router->get('/', [
            'as' => 'page.customer.manageAddress',
            'uses' => 'PublicController@manageAddress'
        ]);
        $router->get('/them-dia-chi-moi', [
            'as' => 'page.customer.addMoreAddress',
            'uses' => 'PublicController@addMoreAddress'
        ]);
        $router->post('/them-dia-chi-moi', [
            'as' => 'page.customer.postAddMoreAddress',
            'uses' => 'PublicController@postAddMoreAddress'
        ]);
        $router->delete('/xoa-dia-chi/{id?}', [
            'as' => 'page.customer.deleteMoreAddress',
            'uses' => 'PublicController@deleteMoreAddress'
        ]);
        $router->get('/chinh-sua-dia-chi/{id?}', [
            'as' => 'page.customer.editMoreAddress',
            'uses' => 'PublicController@editMoreAddress'
        ]);
        $router->get('/cai-dat-dia-chi-giao-hang-co-dinh/{id?}', [
            'as' => 'page.customer.setDefaultShip',
            'uses' => 'PublicController@setDefaultShip'
        ]);
        $router->get('/cai-dat-dia-chi-thanh-toan-co-dinh/{id?}', [
            'as' => 'page.customer.setDefaultPayment',
            'uses' => 'PublicController@setDefaultPayment'
        ]);
        $router->put('/chinh-sua-dia-chi/{id?}', [
            'as' => 'page.customer.postEditMoreAddress',
            'uses' => 'PublicController@postEditMoreAddress'
        ]);
    });
    $router->get(trans('customer::url.my_order'), [
        'as' => 'page.customer.myOrder',
        'uses' => 'PublicController@myOrder',
        'middleware' => 'auth.webclient'
    ]);
    $router->get(trans('customer::url.my_order').'/{code?}', [
        'as' => 'page.customer.detailOrder',
        'uses' => 'PublicController@detailOrder',
        'middleware' => 'auth.webclient'
    ]);
    $router->get(trans('customer::url.my_wish_list'), [
        'as' => 'page.customer.myWishList',
        'uses' => 'PublicController@myWishList',
        'middleware' => 'auth.webclient'
    ]);
    $router->delete(trans('customer::url.my_wish_list'), [
        'as' => 'page.customer.deleteMyWishList',
        'uses' => 'PublicController@deleteMyWishList',
        'middleware' => 'auth.webclient'
    ]);
});
$router->group(['prefix' => '/ajax'], function (Router $router) {
    $router->get('/ajaxDistrict/{provice_id?}', [
        'as' => 'page.location.ajaxDistrict',
        'uses' => 'PublicController@ajaxDistrict'
    ]);
    $router->get('/ajaxWard/{district_id?}', [
        'as' => 'page.location.ajaxWard',
        'uses' => 'PublicController@ajaxWard'
    ]);
    $router->get('/add-to-wishlist/{id?}',[
        'as'=>'page.addToWishList',
        'uses' => 'PublicController@addToWishList'
    ]);
});