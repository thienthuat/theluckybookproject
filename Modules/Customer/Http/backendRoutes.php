<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/customer'], function (Router $router) {
    $router->bind('customer', function ($id) {
        return app('Modules\Customer\Repositories\CustomerRepository')->find($id);
    });
    $router->get('customers', [
        'as' => 'admin.customer.customer.index',
        'uses' => 'CustomerController@index',
        'middleware' => 'can:customer.customers.index'
    ]);
    $router->get('customers/create', [
        'as' => 'admin.customer.customer.create',
        'uses' => 'CustomerController@create',
        'middleware' => 'can:customer.customers.create'
    ]);
    $router->post('customers', [
        'as' => 'admin.customer.customer.store',
        'uses' => 'CustomerController@store',
        'middleware' => 'can:customer.customers.create'
    ]);
    $router->get('customers/{customer}/edit', [
        'as' => 'admin.customer.customer.edit',
        'uses' => 'CustomerController@edit',
        'middleware' => 'can:customer.customers.edit'
    ]);
    $router->get('customers/{customer}/{address_id}/ship', [
        'as' => 'admin.customer.customer.addressShip',
        'uses' => 'CustomerController@addressShip',
        'middleware' => 'can:customer.customers.edit'
    ]);
    $router->post('customers/{address_id}/ship', [
        'as' => 'admin.customer.customer.postAddressShip',
        'uses' => 'CustomerController@postAddressShip',
        'middleware' => 'can:customer.customers.edit'
    ]);

    $router->get('customers/{customer}/{address_id}/payment', [
        'as' => 'admin.customer.customer.addressPayment',
        'uses' => 'CustomerController@addressPayment',
        'middleware' => 'can:customer.customers.edit'
    ]);
    $router->post('customers/{address_id}/payment', [
        'as' => 'admin.customer.customer.postAddressPayment',
        'uses' => 'CustomerController@postAddressPayment',
        'middleware' => 'can:customer.customers.edit'
    ]);
    $router->put('customers/{customer}', [
        'as' => 'admin.customer.customer.update',
        'uses' => 'CustomerController@update',
        'middleware' => 'can:customer.customers.edit'
    ]);
    $router->delete('customers/{customer}', [
        'as' => 'admin.customer.customer.destroy',
        'uses' => 'CustomerController@destroy',
        'middleware' => 'can:customer.customers.destroy'
    ]);
    $router->bind('favourite', function ($id) {
        return app('Modules\Customer\Repositories\FavouriteRepository')->find($id);
    });
    $router->get('favourites', [
        'as' => 'admin.customer.favourite.index',
        'uses' => 'FavouriteController@index',
        'middleware' => 'can:customer.favourites.index'
    ]);
    $router->get('favourites/create', [
        'as' => 'admin.customer.favourite.create',
        'uses' => 'FavouriteController@create',
        'middleware' => 'can:customer.favourites.create'
    ]);
    $router->post('favourites', [
        'as' => 'admin.customer.favourite.store',
        'uses' => 'FavouriteController@store',
        'middleware' => 'can:customer.favourites.create'
    ]);
    $router->get('favourites/{favourite}/edit', [
        'as' => 'admin.customer.favourite.edit',
        'uses' => 'FavouriteController@edit',
        'middleware' => 'can:customer.favourites.edit'
    ]);
    $router->put('favourites/{favourite}', [
        'as' => 'admin.customer.favourite.update',
        'uses' => 'FavouriteController@update',
        'middleware' => 'can:customer.favourites.edit'
    ]);
    $router->delete('favourites/{favourite}', [
        'as' => 'admin.customer.favourite.destroy',
        'uses' => 'FavouriteController@destroy',
        'middleware' => 'can:customer.favourites.destroy'
    ]);
    $router->bind('address', function ($id) {
        return app('Modules\Customer\Repositories\AddressRepository')->find($id);
    });
    $router->get('addresses', [
        'as' => 'admin.customer.address.index',
        'uses' => 'AddressController@index',
        'middleware' => 'can:customer.addresses.index'
    ]);
    $router->get('addresses/create', [
        'as' => 'admin.customer.address.create',
        'uses' => 'AddressController@create',
        'middleware' => 'can:customer.addresses.create'
    ]);
    $router->post('addresses', [
        'as' => 'admin.customer.address.store',
        'uses' => 'AddressController@store',
        'middleware' => 'can:customer.addresses.create'
    ]);
    $router->get('addresses/{address}/edit', [
        'as' => 'admin.customer.address.edit',
        'uses' => 'AddressController@edit',
        'middleware' => 'can:customer.addresses.edit'
    ]);
    $router->put('addresses/{address}', [
        'as' => 'admin.customer.address.update',
        'uses' => 'AddressController@update',
        'middleware' => 'can:customer.addresses.edit'
    ]);
    $router->delete('addresses/{address}', [
        'as' => 'admin.customer.address.destroy',
        'uses' => 'AddressController@destroy',
        'middleware' => 'can:customer.addresses.destroy'
    ]);
    $router->bind('key', function ($id) {
        return app('Modules\Customer\Repositories\keyRepository')->find($id);
    });
    $router->get('keys', [
        'as' => 'admin.customer.key.index',
        'uses' => 'keyController@index',
        'middleware' => 'can:customer.keys.index'
    ]);
    $router->get('keys/create', [
        'as' => 'admin.customer.key.create',
        'uses' => 'keyController@create',
        'middleware' => 'can:customer.keys.create'
    ]);
    $router->post('keys', [
        'as' => 'admin.customer.key.store',
        'uses' => 'keyController@store',
        'middleware' => 'can:customer.keys.create'
    ]);
    $router->get('keys/{key}/edit', [
        'as' => 'admin.customer.key.edit',
        'uses' => 'keyController@edit',
        'middleware' => 'can:customer.keys.edit'
    ]);
    $router->put('keys/{key}', [
        'as' => 'admin.customer.key.update',
        'uses' => 'keyController@update',
        'middleware' => 'can:customer.keys.edit'
    ]);
    $router->delete('keys/{key}', [
        'as' => 'admin.customer.key.destroy',
        'uses' => 'keyController@destroy',
        'middleware' => 'can:customer.keys.destroy'
    ]);
    $router->bind('type', function ($id) {
        return app('Modules\Customer\Repositories\TypeRepository')->find($id);
    });
    $router->get('types', [
        'as' => 'admin.customer.type.index',
        'uses' => 'TypeController@index',
        'middleware' => 'can:customer.types.index'
    ]);
    $router->get('types/create', [
        'as' => 'admin.customer.type.create',
        'uses' => 'TypeController@create',
        'middleware' => 'can:customer.types.create'
    ]);
    $router->post('types', [
        'as' => 'admin.customer.type.store',
        'uses' => 'TypeController@store',
        'middleware' => 'can:customer.types.create'
    ]);
    $router->get('types/{type}/edit', [
        'as' => 'admin.customer.type.edit',
        'uses' => 'TypeController@edit',
        'middleware' => 'can:customer.types.edit'
    ]);
    $router->put('types/{type}', [
        'as' => 'admin.customer.type.update',
        'uses' => 'TypeController@update',
        'middleware' => 'can:customer.types.edit'
    ]);
    $router->delete('types/{type}', [
        'as' => 'admin.customer.type.destroy',
        'uses' => 'TypeController@destroy',
        'middleware' => 'can:customer.types.destroy'
    ]);
    $router->bind('birthdaychild', function ($id) {
        return app('Modules\Customer\Repositories\BirthdaychildRepository')->find($id);
    });
    $router->get('birthdaychildren', [
        'as' => 'admin.customer.birthdaychild.index',
        'uses' => 'BirthdaychildController@index',
        'middleware' => 'can:customer.birthdaychildren.index'
    ]);
    $router->get('birthdaychildren/create', [
        'as' => 'admin.customer.birthdaychild.create',
        'uses' => 'BirthdaychildController@create',
        'middleware' => 'can:customer.birthdaychildren.create'
    ]);
    $router->post('birthdaychildren', [
        'as' => 'admin.customer.birthdaychild.store',
        'uses' => 'BirthdaychildController@store',
        'middleware' => 'can:customer.birthdaychildren.create'
    ]);
    $router->get('birthdaychildren/{birthdaychild}/edit', [
        'as' => 'admin.customer.birthdaychild.edit',
        'uses' => 'BirthdaychildController@edit',
        'middleware' => 'can:customer.birthdaychildren.edit'
    ]);
    $router->put('birthdaychildren/{birthdaychild}', [
        'as' => 'admin.customer.birthdaychild.update',
        'uses' => 'BirthdaychildController@update',
        'middleware' => 'can:customer.birthdaychildren.edit'
    ]);
    $router->delete('birthdaychildren/{birthdaychild}', [
        'as' => 'admin.customer.birthdaychild.destroy',
        'uses' => 'BirthdaychildController@destroy',
        'middleware' => 'can:customer.birthdaychildren.destroy'
    ]);
    $router->get('birthdaychildren/{id}', [
        'as' => 'admin.customer.birthdaychild.delete',
        'uses' => 'BirthdaychildController@delete',
        'middleware' => 'can:customer.birthdaychildren.destroy'
    ]);
// append








});
