<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\FavouriteRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFavouriteRepository extends EloquentBaseRepository implements FavouriteRepository
{
}
