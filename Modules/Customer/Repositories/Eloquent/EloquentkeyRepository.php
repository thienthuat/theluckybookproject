<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\keyRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentkeyRepository extends EloquentBaseRepository implements keyRepository
{
}
