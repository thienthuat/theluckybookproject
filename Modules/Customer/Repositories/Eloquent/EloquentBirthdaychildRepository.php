<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\BirthdaychildRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBirthdaychildRepository extends EloquentBaseRepository implements BirthdaychildRepository
{
    public function getDataChild($month = '')
    {
        return $this->model->when($month != '', function ($query) use ($month) {
            $query->whereMonth('birthday_child', '=', $month);
        })->get();
    }
}
