<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\CustomerRepository;
use Modules\Customer\Entities\Address;
use Modules\Customer\Entities\Birthdaychild;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use DateTime;
use Carbon\Carbon;

class EloquentCustomerRepository extends EloquentBaseRepository implements CustomerRepository
{
    public function model()
    {
        return $this->model;
    }

    public function create($data)
    {
        $customer = $this->model->create($data);
        if ($customer) {
            $births = [];
            $birth = $this->convertData($data);
            foreach ($birth as $key => $item) {
                $births[$key] = new Birthdaychild($item);
            }
            $customer->birthday_customer()->saveMany($births);
            if ($customer->city != 0 && $customer->district != 0 && $customer->ward != 0) {
                $address = new Address(
                    [
                        'full_name' => strip_tags($customer->last_name) . ' ' . strip_tags($customer->first_name),
                        'city' => strip_tags($customer->city),
                        'district' => strip_tags($customer->district),
                        'wards' => strip_tags($customer->ward),
                        'address' => strip_tags($customer->address),
                        'phone' => strip_tags($customer->phone),
                        'customer_id' => strip_tags($customer->id),
                        'default_shipping_address' => 1
                    ]
                );
                $customer->addressre()->save($address);
            }
            return $customer;
        }
    }

    private function convertData($data, $id_birthday = [])
    {
        $arr = [];
        foreach ($data as $key => $datum) {
            if (is_array($datum)) {
                foreach ($datum as $k => $item) {
                    $arr[$k][$key] = $item != '' ? $item : '';
                }
            }
        }
        return $arr;
    }

    public function updateBithday($customer, $data)
    {
        $data_convert = $this->convertData($data);
        foreach ($data_convert as $item) {
            $birthday = Birthdaychild::find(isset($item['birthday_id'])?$item['birthday_id']:0);
            if ($birthday) {
                $birthday->full_name = $item['full_name'];
                $birthday->birthday_child = $item['birthday_child'];
                $birthday->sex_child = $item['sex_child'];
                $birthday->note_child = $item['note_child'];

                $birthday->save();
            } else {
                $births = new Birthdaychild($item);
                $customer->birthday_customer()->save($births);
            }
        }
    }

    public function getCustomerHasBirthday()
    {
        $dayAfter = (new DateTime('today'))->modify('+30 day')->format('m-d');
        $dayBefore = (new DateTime('today'))->format('m-d');
        return $this->model->whereRaw('DAYOFYEAR(curdate()) <= DAYOFYEAR(birthday) AND DAYOFYEAR(curdate()) + 30 >=  dayofyear(birthday)')
            ->orderByRaw('DAYOFYEAR(birthday)')
            ->get();
    }
}
