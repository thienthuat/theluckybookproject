<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\TypeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTypeRepository extends EloquentBaseRepository implements TypeRepository
{
}
