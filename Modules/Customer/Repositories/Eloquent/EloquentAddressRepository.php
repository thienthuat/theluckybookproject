<?php

namespace Modules\Customer\Repositories\Eloquent;

use Modules\Customer\Repositories\AddressRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAddressRepository extends EloquentBaseRepository implements AddressRepository
{
    public function getShipDefault($customer_id)
    {
        return $this->model->where('customer_id', $customer_id)->where('default_shipping_address', 1)->first();
    }

    public function getPaymentDefault($customer_id)
    {
        return $this->model->where('customer_id', $customer_id)->where('default_payment_address', 1)->first();
    }

    public function model(){
        return $this->model;
    }

    public function createOrUpdateShip($data, $address_id)
    {
        if ($address_id != 0) {
            $address = $this->find($address_id);
            $data = [
                'full_name' => $data['full_name'],
                'city' => $data['city'],
                'district' => $data['district'],
                'wards' => $data['ward'],
                'address' => $data['address'],
                'phone' => $data['phone'],
            ];
            $this->update($address, $data);
        } else {
            $data = [
                'full_name' => $data['full_name'],
                'city' => $data['city'],
                'district' => $data['district'],
                'wards' => $data['ward'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'customer_id' => $data['customer_id'],
                'default_shipping_address' => 1
            ];
            $this->model->create($data);
        }
    }

    public function createOrUpdatePayment($data, $address_id)
    {
        if ($address_id != 0) {
            $address = $this->find($address_id);
            $data = [
                'full_name' => $data['full_name'],
                'city' => $data['city'],
                'district' => $data['district'],
                'wards' => $data['ward'],
                'address' => $data['address'],
                'phone' => $data['phone'],
            ];
            $this->update($address, $data);
        } else {
            $data = [
                'full_name' => $data['full_name'],
                'city' => $data['city'],
                'district' => $data['district'],
                'wards' => $data['ward'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'customer_id' => $data['customer_id'],
                'default_payment_address' => 1
            ];
            $this->model->create($data);
        }
    }
}
