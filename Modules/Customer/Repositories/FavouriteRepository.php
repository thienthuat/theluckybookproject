<?php

namespace Modules\Customer\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface FavouriteRepository extends BaseRepository
{
}
