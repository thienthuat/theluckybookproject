<?php

namespace Modules\Customer\Repositories\Cache;

use Modules\Customer\Repositories\FavouriteRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFavouriteDecorator extends BaseCacheDecorator implements FavouriteRepository
{
    public function __construct(FavouriteRepository $favourite)
    {
        parent::__construct();
        $this->entityName = 'customer.favourites';
        $this->repository = $favourite;
    }
}
