<?php

namespace Modules\Customer\Repositories\Cache;

use Modules\Customer\Repositories\TypeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTypeDecorator extends BaseCacheDecorator implements TypeRepository
{
    public function __construct(TypeRepository $type)
    {
        parent::__construct();
        $this->entityName = 'customer.types';
        $this->repository = $type;
    }
}
