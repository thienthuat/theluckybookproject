<?php

namespace Modules\Customer\Repositories\Cache;

use Modules\Customer\Repositories\BirthdaychildRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBirthdaychildDecorator extends BaseCacheDecorator implements BirthdaychildRepository
{
    public function __construct(BirthdaychildRepository $birthdaychild)
    {
        parent::__construct();
        $this->entityName = 'customer.birthdaychildren';
        $this->repository = $birthdaychild;
    }
}
