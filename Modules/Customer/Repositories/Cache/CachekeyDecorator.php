<?php

namespace Modules\Customer\Repositories\Cache;

use Modules\Customer\Repositories\keyRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachekeyDecorator extends BaseCacheDecorator implements keyRepository
{
    public function __construct(keyRepository $key)
    {
        parent::__construct();
        $this->entityName = 'customer.keys';
        $this->repository = $key;
    }
}
