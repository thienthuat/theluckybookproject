<?php

namespace Modules\Customer\Repositories\Cache;

use Modules\Customer\Repositories\AddressRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAddressDecorator extends BaseCacheDecorator implements AddressRepository
{
    public function __construct(AddressRepository $address)
    {
        parent::__construct();
        $this->entityName = 'customer.addresses';
        $this->repository = $address;
    }
}
