<?php

return [
    'list resource' => 'List favourites',
    'create resource' => 'Create favourites',
    'edit resource' => 'Edit favourites',
    'destroy resource' => 'Destroy favourites',
    'title' => [
        'favourites' => 'Favourite',
        'create favourite' => 'Create a favourite',
        'edit favourite' => 'Edit a favourite',
    ],
    'button' => [
        'create favourite' => 'Create a favourite',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
