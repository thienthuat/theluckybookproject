<?php

return [
    'list resource' => 'List birthdaychildren',
    'create resource' => 'Create birthdaychildren',
    'edit resource' => 'Edit birthdaychildren',
    'destroy resource' => 'Destroy birthdaychildren',
    'title' => [
        'birthdaychildren' => 'Birthdaychild',
        'create birthdaychild' => 'Create a birthdaychild',
        'edit birthdaychild' => 'Edit a birthdaychild',
    ],
    'button' => [
        'create birthdaychild' => 'Create a birthdaychild',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
