<?php

return [
    'list resource' => 'List customers',
    'create resource' => 'Create customers',
    'edit resource' => 'Edit customers',
    'destroy resource' => 'Destroy customers',
    'title' => [
        'customers' => 'Customer',
        'create customer' => 'Create a customer',
        'edit customer' => 'Edit a customer',
    ],
    'button' => [
        'create customer' => 'Create a customer',
    ],
    'table' => [
        'code' => 'Code',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'phone' => 'Phone number',
        'birthday' => 'Birthday',
        'address' => 'Address',
        'sex' => 'Sex',
        'city' => 'City',
        'district' => 'District',
        'ward' => 'Ward',
        'not_verify' => 'Not Verify',
        'lock' => 'Lock',
        'type' => 'Type Customer',
        'unlock' => 'unLock',
    ],
    'form' => [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'password' => 'Password',
        'phone' => 'Phone number',
        'birthday' => 'Birthday',
        'address' => 'Address',
        'sex' => 'Sex',
        'city' => 'City',
        'district' => 'District',
        'ward' => 'Ward',
        'not_verify' => 'Not Verify',
        'lock' => 'Lock',
        'type' => 'Type Customer',
        'unlock' => 'unLock',
        'full_name' => 'Full Name'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
