<?php

return [
    'list resource' => 'List keys',
    'create resource' => 'Create keys',
    'edit resource' => 'Edit keys',
    'destroy resource' => 'Destroy keys',
    'title' => [
        'keys' => 'key',
        'create key' => 'Create a key',
        'edit key' => 'Edit a key',
    ],
    'button' => [
        'create key' => 'Create a key',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
