<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 10/30/17
 * Time: 4:26 PM
 */
return [
    'customer' => 'khach-hang',
    'my_customer' => 'tai-khoan',
    'my_customer_edit' => 'chinh-sua-tai-khoan',
    'my_manage_address' => 'quan-ly-dia-chi',
    'my_order' => 'don-hang-cua-toi',
    'my_wish_list' => 'san-pham-yeu-thich',
];