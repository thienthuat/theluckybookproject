<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Customer has birthday</h3>

        <div class="box-tools pull-right">
            <span class="label label-danger">{{$customers->count()}} Customer</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin data-table">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Birthday</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td>
                            <a href="{{route('admin.customer.customer.edit',$customer->id)}}">{{$customer->code}}</a>
                        </td>
                        <td>{{$customer->last_name}} {{$customer->first_name}}</td>
                        <td>{{$customer->email}}</td>
                        <td>
                            {{date('d-m-Y',strtotime($customer->birthday))}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.users-list -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <a href="{{route('admin.customer.customer.index')}}" class="uppercase">View All Users</a>
    </div>
    <!-- /.box-footer -->
</div>