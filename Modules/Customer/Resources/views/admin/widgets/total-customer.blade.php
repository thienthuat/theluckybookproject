<div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fa fa-users" aria-hidden="true"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Total Customer</span>
        <span class="info-box-number">{{$countCustomer}}</span>
    </div>
    <!-- /.info-box-content -->
</div>