@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('customer::types.title.create type') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.customer.type.index') }}">{{ trans('customer::types.title.types') }}</a></li>
        <li class="active">{{ trans('customer::types.title.create type') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.customer.type.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has("title") ? ' has-error' : '' }}">
                                <label for="first_name">{{trans('customer::types.form.title')}}</label>
                                <input type="text" class="form-control" name="title" id="title"
                                       placeholder="{{trans('customer::types.form.title')}}"
                                       value="{{old('title')}}" required>
                                {!! $errors->first("title", '<span class="help-block">:message</span>') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label("is_default_customer", trans('customer::types.table.is_default_customer')) !!}
                                <div class="checkbox">
                                    <label for="is_default_customer">
                                        <input id="is_default_customer"
                                               name="is_default_customer"
                                               type="checkbox"
                                               class="flat-blue"
                                               value="1"/>
                                        {{ trans('customer::types.table.is_default_customer') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            {{--@include('customer::admin.types.partials.create-fields', ['lang' => $locale])--}}
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.customer.type.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.customer.type.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
