@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('customer::customers.title.create customer') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i>
            {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li>
        <a href="{{ route('admin.customer.customer.index') }}">{{ trans('customer::customers.title.customers') }}</a>
    </li>
    <li class="active">{{ trans('customer::customers.title.create customer') }}</li>
</ol>
@stop

@section('content')
{!! Form::open(['route' => ['admin.customer.customer.store'], 'method' => 'post','id'=>'CUSTOMER']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            @include('partials.form-tab-headers')
            <div class="tab-content">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("first_name") ? ' has-error' : '' }}">
                                <label for="first_name">{{trans('customer::customers.form.first_name')}}</label>
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                    placeholder="{{trans('customer::customers.form.first_name')}}"
                                    value="{{old('first_name')}}" required>
                                {!! $errors->first("first_name", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("last_name") ? ' has-error' : '' }}">
                                <label for="last_name">{{trans('customer::customers.form.last_name')}}</label>
                                <input type="text" class="form-control" name="last_name"
                                    placeholder="{{trans('customer::customers.form.last_name')}}" id="last_name"
                                    value="{{old('last_name')}}" required>
                                {!! $errors->first("last_name", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("email") ? ' has-error' : '' }}">
                                <label for="email">{{trans('customer::customers.form.email')}}</label>
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="{{trans('customer::customers.form.email')}}" value="{{old('email')}}"
                                    required>
                                {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("password") ? ' has-error' : '' }}">
                                <label for="password">{{trans('customer::customers.form.password')}}</label>
                                <input type="password" class="form-control" name="password"
                                    placeholder="{{trans('customer::customers.form.password')}}" id="password"
                                    value="{{old('password')}}" required>
                                {!! $errors->first("password", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("birthday") ? ' has-error' : '' }}">
                                <label for="birthday">{{trans('customer::customers.form.birthday')}}</label>
                                <input type="date" class="form-control" name="birthday" id="birthday"
                                    placeholder="{{trans('customer::customers.form.birthday')}}"
                                    value="{{old('birthday')}}" required>
                                {!! $errors->first("birthday", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("phone") ? ' has-error' : '' }}">
                                <label for="phone">{{trans('customer::customers.form.phone')}}</label>
                                <input type="text" class="form-control" name="phone"
                                    placeholder="{{trans('customer::customers.form.phone')}}" id="phone"
                                    value="{{old('phone')}}" required>
                                {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("sex") ? ' has-error' : '' }}">
                                <label for="sex">{{trans('customer::customers.form.sex')}}</label>
                                <select name="sex" id="sex" class="form-control">
                                    <option value="">--Choose {{trans('customer::customers.form.sex')}}--</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Other</option>
                                </select>
                                {!! $errors->first("sex", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("type_id") ? ' has-error' : '' }}">
                                <label for="type_id">{{trans('customer::customers.form.type')}}</label>
                                <select name="type_id" id="type_id" class="form-control" required>
                                    <option value="">--Choose {{trans('customer::customers.form.type')}}--</option>
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->title}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first("type_id", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("point") ? ' has-error' : '' }}">
                                <label for="point">Points accumulated</label>
                                <input type="text" class="form-control" name="point" placeholder="Points accumulated"
                                    id="point" value="{{old('point',0)}}" required>
                                {!! $errors->first("point", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has("address") ? ' has-error' : '' }}">
                                <label for="address">{{trans('customer::customers.form.address')}}</label>
                                <input type="text" class="form-control" name="address"
                                    placeholder="{{trans('customer::customers.form.address')}}" id="address"
                                    value="{{old('address')}}">
                                {!! $errors->first("address", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("city") ? ' has-error' : '' }}">
                                <label for="city">{{trans('customer::customers.form.city')}}</label>
                                <select name="city" id="city" class="form-control select2 city_onchange" required
                                    data-url="{{route('admin.location.ajax.district')}}">
                                    <option value="">--Choose {{trans('customer::customers.form.city')}}--</option>
                                    @foreach($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach

                                </select>
                                {!! $errors->first("city", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("district") ? ' has-error' : '' }}">
                                <label for="district">{{trans('customer::customers.form.district')}}</label>
                                <select name="district" id="district" class="form-control select2 district_onchange"
                                    required data-url="{{route('admin.location.ajax.ward')}}">
                                    <option value="">--Choose {{trans('customer::customers.form.district')}}--
                                    </option>
                                </select>
                                {!! $errors->first("district", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("ward") ? ' has-error' : '' }}">
                                <label for="ward">{{trans('customer::customers.form.ward')}}</label>
                                <select name="ward" id="ward" class="form-control select2 ward_onchange" required>
                                    <option value="">--Choose {{trans('customer::customers.form.ward')}}--</option>
                                </select>
                                {!! $errors->first("ward", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Birthday Child</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="box-wrapper-birthday box-multi-add">
                                        <div class="item-detail box-input-add">
                                            <a href="javascript:void(0)" class="remove-item">x</a>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="full_name">Full Name</label>
                                                    <input type="text" class="form-control" name="full_name[]"
                                                        placeholder="Full Name" id="full_name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="birthday_child">Birthday</label>
                                                    <input type="date" class="form-control" name="birthday_child[]"
                                                        placeholder="Birthday" id="birthday_child" required>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        for="sex_child">{{trans('customer::customers.form.sex')}}</label>
                                                    <select name="sex_child[]" id="sex_child" class="form-control"
                                                        required>
                                                        <option value="">--Choose
                                                            {{trans('customer::customers.form.sex')}}--</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                        <option value="3">Other</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="note_child">Note</label>
                                                    <textarea class="form-control" name="note_child[]"
                                                        placeholder="Note" id="note_child"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-btn-add">
                                            <button type="button" class="btn btn-default">Add New</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("Status", 'Status') !!}
                        <div class="checkbox">
                            <label for="status_0">
                                <input id="status_0" name="status" type="radio" class="flat-blue" checked value="0" />
                                {{ trans('customer::customers.form.not_verify') }}
                            </label>
                            <label for="status_1">
                                <input id="status_1" name="status" type="radio" class="flat-blue" value="1" />
                                {{ trans('customer::customers.form.lock') }}
                            </label>
                            <label for="status_2">
                                <input id="status_2" name="status" type="radio" class="flat-blue" value="2" />
                                {{ trans('customer::customers.form.unlock') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit"
                        class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.customer.customer.index')}}"><i
                            class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div> {{-- end nav-tabs-custom --}}
    </div>
</div>
{!! Form::close() !!}
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
    <dt><code>b</code></dt>
    <dd>{{ trans('core::core.back to index') }}</dd>
</dl>
@stop

@push('js-stack')
<script type="text/javascript">
    $(document).ready(function () {
            $('.city_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".district_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.district_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('.district_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".ward_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.ward_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('.select2').select2();
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.customer.customer.index') ?>"}
                ]
            });
        });
</script>
<script>
    $(document).ready(function () {
            $("#CUSTOMER").validate({
                submitHandler: function (form) {
                    form.submit();
                }
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
</script>
@endpush