@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('customer::customers.title.edit customer') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i>
            {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li>
        <a href="{{ route('admin.customer.customer.index') }}">{{ trans('customer::customers.title.customers') }}</a>
    </li>
    <li class="active">{{ trans('customer::customers.title.edit customer') }}</li>
</ol>
@stop

@section('content')
{!! Form::open(['route' => ['admin.customer.customer.update', $customer->id], 'method' => 'put']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            @include('partials.form-tab-headers')
            <div class="tab-content">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("first_name") ? ' has-error' : '' }}">
                                <label for="first_name">{{trans('customer::customers.form.first_name')}}</label>
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                    placeholder="{{trans('customer::customers.form.first_name')}}"
                                    value="{{old('first_name',$customer->first_name)}}">
                                {!! $errors->first("first_name", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("last_name") ? ' has-error' : '' }}">
                                <label for="last_name">{{trans('customer::customers.form.last_name')}}</label>
                                <input type="text" class="form-control" name="last_name"
                                    placeholder="{{trans('customer::customers.form.last_name')}}" id="last_name"
                                    value="{{old('last_name',$customer->last_name)}}">
                                {!! $errors->first("last_name", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("email") ? ' has-error' : '' }}">
                                <label for="email">{{trans('customer::customers.form.email')}}</label>
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="{{trans('customer::customers.form.email')}}"
                                    value="{{old('email',$customer->email)}}" readonly>
                                {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("password") ? ' has-error' : '' }}">
                                <label for="password">{{trans('customer::customers.form.password')}}</label>
                                <input type="password" class="form-control" name="password"
                                    placeholder="{{trans('customer::customers.form.password')}}" id="password"
                                    value="{{old('password')}}">
                                {!! $errors->first("password", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("birthday") ? ' has-error' : '' }}">
                                <label for="birthday">{{trans('customer::customers.form.birthday')}}</label>
                                <input type="date" class="form-control" name="birthday" id="birthday"
                                    placeholder="{{trans('customer::customers.form.birthday')}}"
                                    value="{{old('birthday', date('Y-m-d',strtotime($customer->birthday)))}}">
                                {!! $errors->first("birthday", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("phone") ? ' has-error' : '' }}">
                                <label for="phone">{{trans('customer::customers.form.phone')}}</label>
                                <input type="text" class="form-control" name="phone"
                                    placeholder="{{trans('customer::customers.form.phone')}}" id="phone"
                                    value="{{old('phone',$customer->phone)}}">
                                {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("point") ? ' has-error' : '' }}">
                                <label for="point">Points accumulated</label>
                                <input type="number" class="form-control" name="point" placeholder="Points accumulated"
                                    id="point" value="{{old('point',$customer->point)}}" required>
                                {!! $errors->first("point", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("sex") ? ' has-error' : '' }}">
                                <label for="sex">{{trans('customer::customers.form.sex')}}</label>
                                <select name="sex" id="sex" class="form-control">
                                    <option value="">--Choose {{trans('customer::customers.form.sex')}}--</option>
                                    <option value="1" {{$customer->sex == 1 ? 'selected':''}}>Male</option>
                                    <option value="2" {{$customer->sex == 2 ? 'selected':''}}>Female</option>
                                    <option value="3" {{$customer->sex == 3 ? 'selected':''}}>Other</option>
                                </select>
                                {!! $errors->first("sex", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has("type_id") ? ' has-error' : '' }}">
                                <label for="type_id">{{trans('customer::customers.form.type')}}</label>
                                <select name="type_id" id="type_id" class="form-control" required>
                                    <option value="">--Choose {{trans('customer::customers.form.type')}}--</option>
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}" {{$customer->type_id == $type->id ? 'selected':''}}>
                                        {{$type->title}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first("sex", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has("address") ? ' has-error' : '' }}">
                                <label for="address">{{trans('customer::customers.form.address')}}</label>
                                <input type="text" class="form-control" name="address"
                                    placeholder="{{trans('customer::customers.form.address')}}" id="address"
                                    value="{{old('address',$customer->address)}}">
                                {!! $errors->first("address", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("city") ? ' has-error' : '' }}">
                                <label for="city">{{trans('customer::customers.form.city')}}</label>
                                <select name="city" id="city" class="form-control select2 city_onchange" required
                                    data-url="{{route('admin.location.ajax.district')}}">
                                    <option value="">--Choose {{trans('customer::customers.form.city')}}--</option>
                                    @foreach($provinces as $province)
                                    <option value="{{$province->id}}"
                                        {{$province->id == $customer->city ?'selected':''}}>{{$province->name}}</option>
                                    @endforeach

                                </select>
                                {!! $errors->first("city", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("district") ? ' has-error' : '' }}">
                                <label for="district">{{trans('customer::customers.form.district')}}</label>
                                <select name="district" id="district" class="form-control select2 district_onchange"
                                    required data-url="{{route('admin.location.ajax.ward')}}">
                                    <option value="">--Choose {{trans('customer::customers.form.district')}}--
                                    </option>
                                    @if($district != null)
                                    <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                    @endif
                                </select>
                                {!! $errors->first("district", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has("ward") ? ' has-error' : '' }}">
                                <label for="ward">{{trans('customer::customers.form.ward')}}</label>
                                <select name="ward" id="ward" class="form-control select2 ward_onchange" required>
                                    <option value="">--Choose {{trans('customer::customers.form.ward')}}--</option>
                                    @if($ward != null)
                                    <option value="{{$ward->id}}" selected>{{$ward->type}} {{$ward->name}}</option>
                                    @endif
                                </select>
                                {!! $errors->first("ward", '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Birthday Child</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="box-wrapper-birthday box-multi-add">
                                        @if($customer->birthday_customer()->count() >0)
                                        @foreach($customer->birthday_customer as $item)
                                        <div class="item-detail box-input-add">
                                            <input type="hidden" name="birthday_id[]" value="{{$item->id}}">
                                            <a href="javascript:void(0)" class="remove-item"
                                                data-url="{{route('admin.customer.birthdaychild.delete',$item->id)}}">x</a>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="full_name">Full Name</label>
                                                    <input type="text" class="form-control" name="full_name[]"
                                                        placeholder="Full Name" id="full_name"
                                                        value="{{$item->full_name}}" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="birthday_child">Birthday</label>
                                                    <input type="date" class="form-control" name="birthday_child[]"
                                                        placeholder="Birthday" id="birthday_child"
                                                        value="{{date('Y-m-d',strtotime($item->birthday_child))}}"
                                                        required>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        for="sex_child">{{trans('customer::customers.form.sex')}}</label>
                                                    <select name="sex_child[]" id="sex_child" class="form-control"
                                                        required>
                                                        <option value="">
                                                            --Choose {{trans('customer::customers.form.sex')}}
                                                            --
                                                        </option>
                                                        <option value="1" {{$item->sex_child == 1 ? 'selected':''}}>
                                                            Male
                                                        </option>
                                                        <option value="2" {{$item->sex_child == 2 ? 'selected':''}}>
                                                            Female
                                                        </option>
                                                        <option value="3" {{$item->sex_child == 3 ? 'selected':''}}>
                                                            Other
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="note_child">Note</label>
                                                    <textarea class="form-control" name="note_child[]"
                                                        placeholder="Note"
                                                        id="note_child">{{$item->note_child}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="item-detail box-input-add">
                                            <a href="javascript:void(0)" class="remove-item">x</a>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="full_name">Full Name</label>
                                                    <input type="text" class="form-control" name="full_name[]"
                                                        placeholder="Full Name" id="full_name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="birthday_child">Birthday</label>
                                                    <input type="date" class="form-control" name="birthday_child[]"
                                                        placeholder="Birthday" id="birthday_child" required>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        for="sex_child">{{trans('customer::customers.form.sex')}}</label>
                                                    <select name="sex_child[]" id="sex_child" class="form-control"
                                                        required>
                                                        <option value="">
                                                            --Choose {{trans('customer::customers.form.sex')}}--
                                                        </option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                        <option value="3">Other</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="note_child">Note</label>
                                                    <textarea class="form-control" name="note_child[]"
                                                        placeholder="Note" id="note_child"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="box-btn-add">
                                            <button type="button" class="btn btn-default">Add New</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Địa chỉ giao hàng mặc định</h3>
                                </div>
                                <div class="panel-body">
                                    @if($address_ship_default)
                                    <h5>{{$address_ship_default->full_name}}</h5>
                                    <p>{{$address_ship_default->address}}</p>
                                    <p>
                                        {{$address_ship_default->citys?$address_ship_default->citys->name:''}}
                                        ,
                                        @if($address_ship_default->districts){{$address_ship_default->districts->type}}
                                        {{$address_ship_default->districts->name}}@endif
                                        , @if($address_ship_default->ward){{$address_ship_default->ward->type}}
                                        {{$address_ship_default->ward->name}}@endif
                                    </p>
                                    <p>{{$address_ship_default->phone}}</p>
                                    @endif
                                    @if($address_ship_default)
                                    <a href="{{route('admin.customer.customer.addressShip',[$customer->id,$address_ship_default->id])}}"
                                        class="btn btn-primary btn-flat">Edit</a>
                                    @else
                                    <a href="{{route('admin.customer.customer.addressShip',[$customer->id,0])}}"
                                        class="btn btn-primary btn-flat">Add new</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Địa chỉ thanh toán mặc định</h3>
                                </div>
                                <div class="panel-body">
                                    @if($address_payment_default)
                                    <h5>{{$address_payment_default->full_name}}</h5>
                                    <p>{{$address_payment_default->address}}</p>
                                    <p>
                                        {{$address_payment_default->citys?$address_payment_default->citys->name:''}}
                                        ,
                                        @if($address_payment_default->districts){{$address_payment_default->districts->type}}
                                        {{$address_payment_default->districts->name}}@endif
                                        , @if($address_payment_default->ward){{$address_payment_default->ward->type}}
                                        {{$address_payment_default->ward->name}}@endif
                                    </p>
                                    <p>{{$address_payment_default->phone}}</p>
                                    @endif
                                    @if($address_payment_default)
                                    <a href="{{route('admin.customer.customer.addressPayment',[$customer->id,$address_payment_default->id])}}"
                                        class="btn btn-primary btn-flat" data-type="1"
                                        data-customer="{{$customer->id}}">Edit</a>
                                    @else
                                    <a href="{{route('admin.customer.customer.addressPayment',[$customer->id,0])}}"
                                        class="btn btn-primary btn-flat" data-type="1"
                                        data-customer="{{$customer->id}}">Add new</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("Status", 'Status') !!}
                        <div class="checkbox">
                            <label for="status_0">
                                <input id="status_0" name="status" type="radio" class="flat-blue"
                                    {{$customer->status == 0 ?'checked':''}} value="0" />
                                {{ trans('customer::customers.form.not_verify') }}
                            </label>
                            <label for="status_1">
                                <input id="status_1" name="status" type="radio" class="flat-blue"
                                    {{$customer->status == 1 ?'checked':''}} value="1" />
                                {{ trans('customer::customers.form.lock') }}
                            </label>
                            <label for="status_2">
                                <input id="status_2" name="status" type="radio" class="flat-blue"
                                    {{$customer->status == 2 ?'checked':''}} value="2" />
                                {{ trans('customer::customers.form.unlock') }}
                            </label>
                        </div>
                    </div>
                </div>
                <?php $i = 0; ?>
                @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                <?php $i++; ?>
                <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                    @include('customer::admin.customers.partials.edit-fields', ['lang' => $locale])
                </div>
                @endforeach

                <div class="box-footer">
                    <button type="submit"
                        class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.customer.customer.index')}}"><i
                            class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div> {{-- end nav-tabs-custom --}}
    </div>
</div>
{!! Form::close() !!}
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
    <dt><code>b</code></dt>
    <dd>{{ trans('core::core.back to index') }}</dd>
</dl>
@stop

@push('js-stack')
<script type="text/javascript">
    $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.customer.customer.index') ?>"}
                ]
            });
        });
</script>
<script>
    $(document).ready(function () {
            $('.city_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".district_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.district_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('.district_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".ward_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.ward_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
</script>
@endpush