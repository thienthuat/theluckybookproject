@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('customer::customers.title.edit customer') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li>
            <a href="{{ route('admin.customer.customer.index') }}">{{ trans('customer::customers.title.customers') }}</a>
        </li>
        <li class="active">{{ trans('customer::customers.title.edit customer') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.customer.customer.postAddressPayment',$address?$address->id:0], 'method' => 'post']) !!}
    <input type="hidden" name="customer_id" value="{{$customer->id}}">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has("full_name") ? ' has-error' : '' }}">
                                    <label for="full_name">{{trans('customer::customers.form.full_name')}}</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name"
                                           placeholder="{{trans('customer::customers.form.full_name')}}"
                                           value="{{old('full_name',$address ?$address->full_name:'')}}">
                                    {!! $errors->first("full_name", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has("address") ? ' has-error' : '' }}">
                                    <label for="address">{{trans('customer::customers.form.address')}}</label>
                                    <input type="text" class="form-control" name="address"
                                           placeholder="{{trans('customer::customers.form.address')}}" id="address"
                                           value="{{old('address',$address?$address->address:'')}}">
                                    {!! $errors->first("address", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has("phone") ? ' has-error' : '' }}">
                                    <label for="phone">{{trans('customer::customers.form.phone')}}</label>
                                    <input type="text" class="form-control" name="phone"
                                           placeholder="{{trans('customer::customers.form.phone')}}" id="phone"
                                           value="{{old('phone',$address ?$address->phone:'')}}">
                                    {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has("city") ? ' has-error' : '' }}">
                                    <label for="city">{{trans('customer::customers.form.city')}}</label>
                                    <select name="city" id="city" class="form-control select2 city_onchange" required
                                            data-url="{{route('admin.location.ajax.district')}}">
                                        <option value="">--Choose {{trans('customer::customers.form.city')}}--</option>
                                        @foreach($provinces as $province)
                                            <option value="{{$province->id}}" {{$province->id == ($address?$address->city:0) ?'selected':''}}>{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first("city", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has("district") ? ' has-error' : '' }}">
                                    <label for="district">{{trans('customer::customers.form.district')}}</label>
                                    <select name="district" id="district" class="form-control select2 district_onchange"
                                            required data-url="{{route('admin.location.ajax.ward')}}">
                                        <option value="">--Choose {{trans('customer::customers.form.district')}}--
                                        </option>
                                        @if($district != '')
                                            <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                        @endif
                                    </select>
                                    {!! $errors->first("district", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has("ward") ? ' has-error' : '' }}">
                                    <label for="ward">{{trans('customer::customers.form.ward')}}</label>
                                    <select name="ward" id="ward" class="form-control select2 ward_onchange" required>
                                        <option value="">--Choose {{trans('customer::customers.form.ward')}}--</option>
                                        @if($ward !='')
                                            <option value="{{$ward->id}}"
                                                    selected>{{$ward->type}} {{$ward->name}}</option>
                                        @endif
                                    </select>
                                    {!! $errors->first("ward", '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat"
                           href="{{ route('admin.customer.customer.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.customer.customer.index') ?>"}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.city_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".district_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.district_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                        $(".ward_onchange option").each(function () {
                            if ($(this).val() != '') {
                                $(this).remove();
                            }
                        });
                    });
                });
            });
            $('.district_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".ward_onchange option").each(function () {
                        if ($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.ward_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
