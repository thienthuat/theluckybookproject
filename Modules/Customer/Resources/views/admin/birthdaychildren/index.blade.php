@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('customer::birthdaychildren.title.birthdaychildren') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('customer::birthdaychildren.title.birthdaychildren') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <form action="{{route('admin.customer.birthdaychild.index')}}" method="get">
                        <select name="month" id="" class="form-control" style="min-width: 100px"
                                onchange="this.form.submit()">
                            <option value="">--Choose month--</option>
                            @for($i =1; $i <= 12; $i++)
                                <option value="{{$i}}" {{$month == $i ? 'selected':''}}>{{$i}}</option>
                            @endfor
                        </select>
                    </form>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name Child</th>
                                <th>Birthday Child</th>
                                <th>Sex</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($birthdaychildren)): ?>
                            <?php foreach ($birthdaychildren as $birthdaychild): ?>
                            <tr>
                                <td>
                                    {{ $birthdaychild->id }}
                                </td>
                                <td>
                                    {{ $birthdaychild->full_name }}
                                </td>
                                <td>
                                    {{ date('d-m-Y',strtotime($birthdaychild->birthday_child)) }}
                                </td>
                                <td>
                                    @if($birthdaychild->sex_child == 1)
                                        Male
                                    @elseif($birthdaychild->sex_child == 2)
                                        Female
                                    @else
                                        Other
                                    @endif

                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.customer.customer.edit', [$birthdaychild->customer_id]) }}"
                                           class="btn btn-default btn-flat">
                                            <i class="fa fa-eye"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal"
                                                data-target="#modal-delete-confirmation"
                                                data-action-target="{{ route('admin.customer.birthdaychild.destroy', [$birthdaychild->id]) }}">
                                            <i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Full Name Child</th>
                                <th>Birthday Child</th>
                                <th>Sex</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('customer::birthdaychildren.title.create birthdaychild') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'c', route: "<?= route('admin.customer.birthdaychild.create') ?>"}
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[0, "desc"]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
