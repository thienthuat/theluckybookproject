<?php

namespace Modules\Customer\Widgets;

use Modules\Customer\Repositories\CustomerRepository;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;

class CustomerWidget extends BaseWidget
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
        return 'CustomerWidget';
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
        return 'customer::admin.widgets.customer';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        return ['customers' => $this->customerRepository->getCustomerHasBirthday()];
    }

    /**
     * Get the widget type
     * @return string
     */
    protected function options()
    {
        return [
            'width' => '6',
            'height' => '4',
            'x' => '6',
            'y' => '2'
        ];
    }
}
