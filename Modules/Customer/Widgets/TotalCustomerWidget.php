<?php

namespace Modules\Customer\Widgets;

use Modules\Customer\Repositories\CustomerRepository;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;

class TotalCustomerWidget extends BaseWidget
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
        return 'TotalCustomerWidget';
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
        return 'customer::admin.widgets.total-customer';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        return ['countCustomer' => $this->customerRepository->all()->count()];
    }

    /**
     * Get the widget type
     * @return string
     */
    protected function options()
    {
        return [
            'width' => '3',
            'height' => '2',
            'x' => '9',
            'y' => '0'
        ];
    }
}
