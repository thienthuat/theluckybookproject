<?php

namespace Modules\Customer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Birthdaychild extends Model
{
    use Translatable;

    protected $table = 'customer__birthdaychildren';
    public $translatedAttributes = [];
    protected $fillable = ['full_name', 'birthday_child', 'note_child', 'customer_id', 'sex_child'];
}
