<?php

namespace Modules\Customer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Location\Entities\Province;
use Modules\Location\Entities\District;
use Modules\Location\Entities\Ward;

class Address extends Model
{
    use Translatable;

    protected $table = 'customer__addresses';
    public $translatedAttributes = [];
    protected $fillable = ['full_name', 'company', 'city', 'district', 'wards', 'address', 'phone', 'default_shipping_address', 'default_payment_address', 'customer_id'];

    public function citys()
    {
        return $this->belongsTo(Province::class, 'city');
    }

    public function districts()
    {
        return $this->belongsTo(District::class, 'district');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'wards');
    }
}
