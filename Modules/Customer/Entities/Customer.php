<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Media\Support\Traits\MediaRelation;

class Customer extends Authenticatable
{
    use  MediaRelation;

    protected $table = 'customer__customers';
    protected $hidden = ['password', 'remember_token'];
    protected $fillable = ['first_name', 'remember_token', 'last_name', 'code', 'email', 'facebook_id',
        'google_id', 'token', 'password', 'birthday', 'type_id', 'city', 'district', 'ward', 'phone', 'sex', 'status', 'address', 'social_id','point'];

    public function key()
    {
        return $this->hasMany(Key::class, 'customer_id');
    }

    public function addressre()
    {
        return $this->hasMany(Address::class, 'customer_id');
    }

    public function birthday_customer()
    {
        return $this->hasMany(Birthdaychild::class, 'customer_id');
    }

    public function addressShipDefault()
    {
        return $this->hasMany(Address::class, 'customer_id')->where('default_shipping_address', 1)->first();
    }

    public function favourite()
    {
        return $this->hasMany(Favourite::class, 'customer_id');
    }
}
