<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class TypeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'customer__type_translations';
}
