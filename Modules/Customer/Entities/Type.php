<?php

namespace Modules\Customer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use Translatable;

    protected $table = 'customer__types';
    public $translatedAttributes = [];
    protected $fillable = ['title','is_default_customer'];
}
