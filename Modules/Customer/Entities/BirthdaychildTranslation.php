<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class BirthdaychildTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'birthdaychild_translations';
}
