<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class FavouriteTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'customer__favourite_translations';
}
