<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class keyTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'customer__key_translations';
}
