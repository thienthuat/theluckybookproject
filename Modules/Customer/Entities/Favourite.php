<?php

namespace Modules\Customer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class Favourite extends Model
{
    use Translatable;

    protected $table = 'customer__favourites';
    public $translatedAttributes = [];
    protected $fillable = ['product_id', 'customer_id'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
