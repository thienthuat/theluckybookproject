<?php

namespace Modules\Customer\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class key extends Model
{
    use Translatable;

    protected $table = 'customer__keys';
    public $translatedAttributes = [];
    protected $fillable = ['key','type','customer_id'];
}
