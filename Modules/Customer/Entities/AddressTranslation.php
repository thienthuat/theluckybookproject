<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class AddressTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'customer__address_translations';
}
