<?php

return [
    'customer.customers' => [
        'index' => 'customer::customers.list resource',
        'create' => 'customer::customers.create resource',
        'edit' => 'customer::customers.edit resource',
        'destroy' => 'customer::customers.destroy resource',
    ],
    'customer.types' => [
        'index' => 'customer::types.list resource',
        'create' => 'customer::types.create resource',
        'edit' => 'customer::types.edit resource',
        'destroy' => 'customer::types.destroy resource',
    ],
    'customer.birthdaychildren' => [
        'destroy' => 'customer::birthdaychildren.destroy resource',
        'index' => 'customer::birthdaychildren.list resource',
    ],
// append








];
