<?php

return [
    'address' => [
        'description' => 'Address',
        'view' => 'text',
        'translatable' => false,
    ], 'address_multiple' => [
        'description' => 'Address Multiple',
        'view' => 'wysiwyg',
        'translatable' => false,
    ],
    'hotline' => [
        'description' => 'Hotline',
        'view' => 'text',
        'translatable' => false,
    ], 'phone' => [
        'description' => 'Phone',
        'view' => 'text',
        'translatable' => false,
    ],
    'email' => [
        'description' => 'Email',
        'view' => 'text',
        'translatable' => false,
    ], 'email_revice' => [
        'description' => 'Email Revice',
        'view' => 'text',
        'translatable' => false,
    ],
    'linked_in' => [
        'description' => 'Linked In link',
        'view' => 'text',
        'translatable' => false,
    ], 'youtube' => [
        'description' => 'Youtube',
        'view' => 'text',
        'translatable' => false,
    ], 'g_plus' => [
        'description' => 'goolge Plus',
        'view' => 'text',
        'translatable' => false,
    ], 'twitter' => [
        'description' => 'Twitter',
        'view' => 'text',
        'translatable' => false,
    ],
    'facebook' => [
        'description' => 'Facebook link',
        'view' => 'text',
        'translatable' => false,
    ], 'facebook_app_id' => [
        'description' => 'Facebook App Id',
        'view' => 'text',
        'translatable' => false,
    ],
    'gmaps_api_key' => [
        'description' => 'Google Maps API key',
        'view' => 'text',
        'translatable' => false,
    ],
];
