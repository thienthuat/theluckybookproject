<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingMethodTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping__method_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('method_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['method_id', 'locale']);
            $table->foreign('method_id')->references('id')->on('shopping__methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping__method_translations', function (Blueprint $table) {
            $table->dropForeign(['method_id']);
        });
        Schema::dropIfExists('shopping__method_translations');
    }
}
