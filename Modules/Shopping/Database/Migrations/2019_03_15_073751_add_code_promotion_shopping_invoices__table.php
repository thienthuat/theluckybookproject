<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodePromotionShoppingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->string('id_promotion')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->dropColumn('id_promotion');
        });
    }
}
