<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledToShoppingInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('full_name')->after('id');
            $table->string('phone')->after('full_name');
            $table->integer('city')->after('phone');
            $table->integer('district')->after('city');
            $table->integer('wards')->after('district');
            $table->string('address')->after('wards');
            $table->integer('customer_id')->after('address');
            $table->double('total')->after('customer_id');
            $table->double('sub_total')->after('total');
            $table->tinyInteger('payment')->after('sub_total');
            $table->integer('qty')->after('payment');
            $table->tinyInteger('status')->after('qty')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->dropColumn('full_name');
            $table->dropColumn('phone');
            $table->dropColumn('city');
            $table->dropColumn('district');
            $table->dropColumn('wards');
            $table->dropColumn('address');
            $table->dropColumn('customer_id');
            $table->dropColumn('total');
            $table->dropColumn('sub_total');
            $table->dropColumn('payment');
            $table->dropColumn('qty');
            $table->dropColumn('status');
        });
    }
}
