<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledToShoppingInvoiceitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping__invoiceitems', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->after('id');
            $table->string('title')->after('product_id');
            $table->string('slug')->after('title');
            $table->double('price')->after('slug');
            $table->integer('qty')->after('price');
            $table->integer('size_id')->after('qty');
            $table->string('size')->after('size_id');
            $table->integer('color_id')->after('size');
            $table->string('color')->after('color_id');
            $table->integer('invoice_id')->after('color')->unsigned();
            $table->foreign('invoice_id')->references('id')->on('shopping__invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping__invoiceitems', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('slug');
            $table->dropColumn('price');
            $table->dropColumn('qty');
            $table->dropColumn('size_id');
            $table->dropColumn('size');
            $table->dropColumn('color_id');
            $table->dropColumn('color');
        });
    }
}
