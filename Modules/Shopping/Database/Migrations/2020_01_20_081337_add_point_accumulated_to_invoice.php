<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointAccumulatedToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->float('point')->default(0)->nullable()->after('id_promotion');
            $table->float('point_request')->default(0)->nullable()->after('id_promotion');
            $table->float('total_promo')->default(0)->nullable()->after('id_promotion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping__invoices', function (Blueprint $table) {
            $table->dropColumn('point');
            $table->dropColumn('point_request');
            $table->dropColumn('total_promo');
        });
    }
}
