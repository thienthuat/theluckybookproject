<?php

return [
    'name' => 'Shopping',
    'status_invoice' => [
        0 => 'Pending',
        1 => 'Payment received',
        2 => 'Processing',
        3 => 'Order shipped',
        4 => 'Paid',
        5 => 'Canceled',
    ]
];
