<?php

namespace Modules\Shopping\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shopping\Entities\Invoiceitem;
use Modules\Shopping\Http\Requests\CreateInvoiceitemRequest;
use Modules\Shopping\Http\Requests\UpdateInvoiceitemRequest;
use Modules\Shopping\Repositories\InvoiceitemRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Product\Repositories\ProductRepository;

class InvoiceitemController extends AdminBaseController
{
    /**
     * @var InvoiceitemRepository
     */
    private $invoiceitem;
    private $invoiceRepository;
    private $productRepository;

    public function __construct(InvoiceitemRepository $invoiceitem, InvoiceRepository $invoiceRepository, ProductRepository $productRepository)
    {
        parent::__construct();

        $this->invoiceitem = $invoiceitem;
        $this->invoiceRepository = $invoiceRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$invoiceitems = $this->invoiceitem->all();

        return view('shopping::admin.invoiceitems.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('shopping::admin.invoiceitems.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInvoiceitemRequest $request
     * @return Response
     */
    public function store(CreateInvoiceitemRequest $request)
    {
        $this->invoiceitem->create($request->all());

        return redirect()->route('admin.shopping.invoiceitem.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('shopping::invoiceitems.title.invoiceitems')]));
    }

    public function storeItem(CreateInvoiceitemRequest $request)
    {
        $invoice = $this->invoiceRepository->find($request->invoice);
        $product = $this->productRepository->find($request->product_id);
        $price = $request->price;
        $qty = $request->qty;
        $type_color = $request->type_color != null ? $request->type_color : 0;
        $type_color_title = $request->type_color_title != null ? $request->type_color_title : '';
        $type_size = $request->type_size != null ? $request->type_size : 0;
        $type_size_title = $request->type_size_title != null ? $request->type_size_title : '';
        $type_size_price = $request->type_size_price != null ? $request->type_size_price : $price;
        $data_price = $type_size_price * $qty;
        $item_invoice = [
            'product_id' => $product->id,
            'title' => $product->title,
            'slug' => $product->slug,
            'price' => $price,
            'qty' => $qty,
            'size_id' => $type_size,
            'size' => $type_size_title,
            'color' => $type_color_title,
            'color_id' => $type_color,
            'invoice_id' => $invoice->id,
        ];
        $this->invoiceitem->create($item_invoice);
        $this->invoiceRepository->update($invoice, ['total' => $invoice->total + $data_price, 'sub_total' => $invoice->sub_total + $data_price]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Invoiceitem $invoiceitem
     * @return Response
     */
    public function edit(Invoiceitem $invoiceitem)
    {
        return view('shopping::admin.invoiceitems.edit', compact('invoiceitem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Invoiceitem $invoiceitem
     * @param  UpdateInvoiceitemRequest $request
     * @return Response
     */
    public function update(Invoiceitem $invoiceitem, UpdateInvoiceitemRequest $request)
    {
        $this->invoiceitem->update($invoiceitem, $request->all());

        return redirect()->route('admin.shopping.invoiceitem.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('shopping::invoiceitems.title.invoiceitems')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Invoiceitem $invoiceitem
     * @return Response
     */
    public function destroy(Invoiceitem $invoiceitem)
    {
        $this->invoiceitem->destroy($invoiceitem);

        return redirect()->route('admin.shopping.invoiceitem.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shopping::invoiceitems.title.invoiceitems')]));
    }

    public function destroyByID($id)
    {
        $item = $this->invoiceitem->find($id);
        $invoice = $this->invoiceRepository->find($item->invoice_id);
        $price_item = $item->qty * $item->price;
        $this->invoiceRepository->update($invoice, ['total' => $invoice->total - $price_item, 'sub_total' => $invoice->sub_total - $price_item]);
        $this->invoiceitem->destroy($item);
    }
}
