<?php

namespace Modules\Shopping\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shopping\Entities\Method;
use Modules\Shopping\Http\Requests\CreateMethodRequest;
use Modules\Shopping\Http\Requests\UpdateMethodRequest;
use Modules\Shopping\Repositories\MethodRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class MethodController extends AdminBaseController
{
    /**
     * @var MethodRepository
     */
    private $method;

    public function __construct(MethodRepository $method)
    {
        parent::__construct();

        $this->method = $method;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $methods = $this->method->all();

        return view('shopping::admin.methods.index', compact('methods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('shopping::admin.methods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMethodRequest $request
     * @return Response
     */
    public function store(CreateMethodRequest $request)
    {
        $this->method->create($request->all());

        return redirect()->route('admin.shopping.method.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('shopping::methods.title.methods')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Method $method
     * @return Response
     */
    public function edit(Method $method)
    {
        return view('shopping::admin.methods.edit', compact('method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Method $method
     * @param  UpdateMethodRequest $request
     * @return Response
     */
    public function update(Method $method, UpdateMethodRequest $request)
    {
        $this->method->update($method, $request->all());

        return redirect()->route('admin.shopping.method.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('shopping::methods.title.methods')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Method $method
     * @return Response
     */
    public function destroy(Method $method)
    {
        $this->method->destroy($method);

        return redirect()->route('admin.shopping.method.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shopping::methods.title.methods')]));
    }
}
