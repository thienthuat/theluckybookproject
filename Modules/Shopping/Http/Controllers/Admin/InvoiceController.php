<?php

namespace Modules\Shopping\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Shopping\Entities\Invoice;
use Modules\Shopping\Http\Requests\CreateInvoiceRequest;
use Modules\Shopping\Http\Requests\UpdateInvoiceRequest;
use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Repositories\ColorRepository;
use Modules\Product\Repositories\SizeRepository;
use Modules\Shopping\Repositories\MethodRepository;
use Modules\Shopping\Repositories\InvoiceitemRepository;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Location\Repositories\WardRepository;
use Auth;
use Session;
use Hash;
use Mail;
use Hashids\Hashids;
use Cart;
use Imagy;
use Theme;
use Nwidart\Modules\Facades\Module;
use Modules\Setting\Contracts\Setting;

class InvoiceController extends AdminBaseController
{
    /**
     * @var InvoiceRepository
     */
    private $invoice;
    private $customerRepository;
    private $productRepository;
    private $colorRepository;
    private $sizeRepository;
    private $methodRepository;
    private $invoiceitemRepository;
    private $districtRepository;
    private $wardRepository;
    private $provinceRepository;
    private $hashids;
    private $setting;

    public function __construct(
        InvoiceRepository $invoice,
        ProductRepository $productRepository,
        ColorRepository $colorRepository,
        SizeRepository $sizeRepository,
        InvoiceitemRepository $invoiceitemRepository,
        MethodRepository $methodRepository,
        DistrictRepository $districtRepository,
        WardRepository $wardRepository,
        ProvinceRepository $provinceRepository,
        CustomerRepository $customerRepository,
        Setting $setting
    ) {
        parent::__construct();

        $this->invoice = $invoice;
        $this->customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
        $this->colorRepository = $colorRepository;
        $this->sizeRepository = $sizeRepository;
        $this->methodRepository = $methodRepository;
        $this->districtRepository = $districtRepository;
        $this->provinceRepository = $provinceRepository;
        $this->invoiceitemRepository = $invoiceitemRepository;
        $this->wardRepository = $wardRepository;
        $this->setting = $setting;
        $this->hashids = new Hashids('', 10, '123456789QƯERTYUIOPASDFGHJKLZXCVBNM');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $invoices = $this->invoice->all();

        return view('shopping::admin.invoices.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $customers = $this->customerRepository->all();
        $products = $this->productRepository->all();
        return view('shopping::admin.invoices.create', compact('customers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInvoiceRequest $request
     * @return Response
     */
    public function store(CreateInvoiceRequest $request)
    {
        $customer = $this->customerRepository->find($request->customer_id);
        $data_invoice = [
            'full_name' => $customer->addressShipDefault()->full_name,
            'city' => $customer->addressShipDefault()->citys->id,
            'email' => $customer->email,
            'district' => $customer->addressShipDefault()->districts->id,
            'wards' => $customer->addressShipDefault()->ward->id,
            'address' => $customer->addressShipDefault()->address,
            'phone' => $customer->addressShipDefault()->phone,
            'customer_id' => $customer->id,
            'total' => Cart::total(0, '.', ''),
            'sub_total' => Cart::subtotal(0, '.', ''),
            'payment' => $request->payment_method,
            'note' => $request->note,
            'qty' => Cart::count()
        ];

        $invoice = $this->invoice->create($data_invoice);
        if ($invoice) {
            $invoice->code = $this->hashids->encode($invoice->id);
            $invoice->save();
            foreach (Cart::content() as $item) {
                $item_invoice = [
                    'product_id' => $item->id,
                    'title' => $item->name,
                    'slug' => $item->options->has('slug') ? $item->options->slug : '',
                    'price' => $item->price,
                    'qty' => $item->qty,
                    'size_id' => $item->options->has('size_id') ? $item->options->size_id : '',
                    'size' => $item->options->has('size') ? $item->options->size : '',
                    'color' => $item->options->has('color') ? $item->options->color : '',
                    'color_id' => $item->options->has('color_id') ? $item->options->color_id : '',
                    'invoice_id' => $invoice->id,
                ];
                $this->invoiceitemRepository->create($item_invoice);
            }
            Cart::destroy();
            return redirect()->route('admin.shopping.invoice.index')
                ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('shopping::invoices.title.invoices')]));
        }
    }

    public function invoiceReview(Request $request)
    {
        $customer = $this->customerRepository->find($request->customer_id);
        $payment_methods = $this->methodRepository->all();
        return view('shopping::admin.invoices.review', compact('customer', 'payment_methods'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Invoice $invoice
     * @return Response
     */
    public function edit(Invoice $invoice)
    {
        $this->requireAssets();
        $methods = $this->methodRepository->all();
        $provinces = $this->provinceRepository->all();
        $districts = $this->districtRepository->getDataByIdProvince($invoice->city);
        $wards = $this->wardRepository->getDataByIdDistrict($invoice->district);
        $colors = $this->colorRepository->all();
        $sizes = $this->sizeRepository->all();
        $products = $this->productRepository->all();
        return view('shopping::admin.invoices.edit', compact('invoice', 'methods', 'provinces', 'districts', 'wards', 'colors', 'sizes', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Invoice $invoice
     * @param  UpdateInvoiceRequest $request
     * @return Response
     */
    public function update(Invoice $invoice, UpdateInvoiceRequest $request)
    {
        $data = $request->all();

        $this->invoice->update($invoice, $data);
        if ($request->status == "4") {
            $total = $invoice->total;
            $customer = $invoice->customer;
            if ($customer) {
                $this->updatePointUser($customer, $invoice, $total);
            }
        }
        $colors = isset($data['color']) ? $data['color'] : "";
        $sizes = isset($data['size']) ? $data['size'] : "";
        if (is_array($colors)) {
            foreach ($colors as $key => $v) {
                $item = $this->invoiceitemRepository->find($key);
                $color = $this->colorRepository->find($v);
                $this->invoiceitemRepository->update($item, ['color_id' => $color ? $color->id : 0, 'color' => $color ? $color->title : ""]);
            }
        }
        if (is_array($sizes)) {
            foreach ($sizes as $key => $v) {
                $item = $this->invoiceitemRepository->find($key);
                $size = $this->sizeRepository->find($v);
                $this->invoiceitemRepository->update($item, ['size_id' => $size ? $size->id : 0, 'size' => $size ? $size->title : ""]);
            }
        }
        if ($request->get('button') === 'index') {
            return redirect()->route('admin.shopping.invoice.index')
                ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('shopping::invoices.title.invoices')]));
        }
        return redirect()->back()
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('shopping::invoices.title.invoices')]));
    }

    private function updatePointUser($customer, $invoice, $totalCart)
    {
		if($invoice->point == 0){
			$point = $this->getPointAccumulated($totalCart);
			$customer->point = $point+$customer->point;
			$customer->save();
			$invoice->point = $point;
			$invoice->save();
		}
    }

    private function getPointAccumulated($totalCart)
    {
        $totalCart = (int) $totalCart;
        $per_cent_point = $this->setting->get('core::per_cent_point') ?? 1;
        return (($totalCart / 1000) * $per_cent_point) / 100;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Invoice $invoice
     * @return Response
     */
    public function destroy(Invoice $invoice)
    {
        $this->invoice->destroy($invoice);

        return redirect()->route('admin.shopping.invoice.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('shopping::invoices.title.invoices')]));
    }

    public function invoiceAddToCart(Request $request)
    {
        if ($request->ajax()) {
            $product_id = strip_tags($request->product_id);
            $price = strip_tags($request->price);
            $qty = strip_tags($request->qty);
            $type_color = strip_tags($request->type_color);
            $type_size = strip_tags($request->type_size);
            $product = $this->productRepository->find($product_id);
            if ($product) {
                $color = $this->colorRepository->find($type_color);
                $size = $this->sizeRepository->find($type_size);
                $image = $product->getThumbnailAttribute() != '' ? Imagy::getThumbnail($product->getThumbnailAttribute()->path, 'smallThumb') : Theme::url('images/img404.png');
                Cart::add($product->id, $product->title, $qty, $price, [
                    'size' => $size ? $size->title : '',
                    'size_id' => $size ? $size->id : '',
                    'color' => $color ? $color->title : '',
                    'color_id' => $color ? $color->id : '',
                    'avatar' => $image,
                    'slug' => $product->slug
                ]);
                return response()->json([
                    'status' => '200',
                    'count_cart' => Cart::count(),
                    'subtotal_cart' => Cart::subtotal(),
                    'view_page_cart' => view('shopping::admin.invoices.partials.cart')->render()
                ]);
            } else {
                return response()->json(['status' => '404']);
            }
        }
    }

    public function invoiceDeleteToCart($id, Request $request)
    {
        if ($request->ajax()) {
            $cart_id = strip_tags($id);
            Cart::remove($cart_id);
            return response()->json([
                'status' => '200',
                'count_cart' => Cart::count(),
                'subtotal_cart' => Cart::subtotal(),
                'view_page_cart' => view('shopping::admin.invoices.partials.cart')->render()
            ]);
        }
    }

    public function invoiceUpdateToCart(Request $request)
    {
        if ($request->ajax()) {
            $cart_id = strip_tags($request->cart_id);
            $qty = strip_tags($request->qty);
            Cart::update($cart_id, $qty);
            return response()->json([
                'status' => '200',
                'count_cart' => Cart::count(),
                'subtotal_cart' => Cart::subtotal(),
                'view_page_cart' => view('shopping::admin.invoices.partials.cart')->render()
            ]);
        }
    }

    private function requireAssets()
    {
        $this->assetManager->addAsset('bootstrap-editable.css', Module::asset('translation:vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'));
        $this->assetManager->addAsset('bootstrap-editable.js', Module::asset('translation:vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js'));
        $this->assetPipeline->requireJs('bootstrap-editable.js');
        $this->assetPipeline->requireCss('bootstrap-editable.css');
    }

    public function updateItemInvoice(Request $request)
    {
        $itemInvoice = $this->invoiceitemRepository->find($request->id);
        $this->invoiceitemRepository->update($itemInvoice, [$request->key => $request->value]);
        $invoice = $this->invoice->find($itemInvoice->invoice_id);


        $total = $this->invoiceitemRepository->calTotal($invoice, null);
        $sub_total = $this->invoiceitemRepository->calTotal($invoice, 0);
        $this->invoice->update($invoice, [
            'total' => $total,
            'sub_total' => $sub_total,
            'qty' => $invoice->item_invoice()->sum('qty'),
            'price_ship' => $sub_total > 500000 ? 0 : 30000
        ]);
        return \response()->json(['status' => 200]);
    }
}
