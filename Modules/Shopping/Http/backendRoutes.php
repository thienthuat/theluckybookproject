<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/shopping'], function (Router $router) {
    $router->bind('invoice', function ($id) {
        return app('Modules\Shopping\Repositories\InvoiceRepository')->find($id);
    });
    $router->get('invoices', [
        'as' => 'admin.shopping.invoice.index',
        'uses' => 'InvoiceController@index',
        'middleware' => 'can:shopping.invoices.index'
    ]);
    $router->get('invoices/create', [
        'as' => 'admin.shopping.invoice.create',
        'uses' => 'InvoiceController@create',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->post('invoices', [
        'as' => 'admin.shopping.invoice.store',
        'uses' => 'InvoiceController@store',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->post('invoices-review', [
        'as' => 'admin.shopping.invoice.invoiceReview',
        'uses' => 'InvoiceController@invoiceReview',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->post('update-item-cart', [
        'as' => 'admin.shopping.invoice.updateItemInvoice',
        'uses' => 'InvoiceController@updateItemInvoice',
        'middleware' => 'can:shopping.invoices.edit'
    ]);
    $router->post('invoices-add-to-cart', [
        'as' => 'admin.shopping.invoice.invoiceAddToCart',
        'uses' => 'InvoiceController@invoiceAddToCart',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->get('invoices-delete-to-cart/{id?}', [
        'as' => 'admin.shopping.invoice.invoiceDeleteToCart',
        'uses' => 'InvoiceController@invoiceDeleteToCart',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->post('invoices-update-to-cart', [
        'as' => 'admin.shopping.invoice.invoiceUpdateToCart',
        'uses' => 'InvoiceController@invoiceUpdateToCart',
        'middleware' => 'can:shopping.invoices.create'
    ]);
    $router->get('invoices/{invoice}/edit', [
        'as' => 'admin.shopping.invoice.edit',
        'uses' => 'InvoiceController@edit',
        'middleware' => 'can:shopping.invoices.edit'
    ]);
    $router->put('invoices/{invoice}', [
        'as' => 'admin.shopping.invoice.update',
        'uses' => 'InvoiceController@update',
        'middleware' => 'can:shopping.invoices.edit'
    ]);
    $router->delete('invoices/{invoice}', [
        'as' => 'admin.shopping.invoice.destroy',
        'uses' => 'InvoiceController@destroy',
        'middleware' => 'can:shopping.invoices.destroy'
    ]);
    $router->bind('invoiceitem', function ($id) {
        return app('Modules\Shopping\Repositories\InvoiceitemRepository')->find($id);
    });
    $router->get('invoiceitems', [
        'as' => 'admin.shopping.invoiceitem.index',
        'uses' => 'InvoiceitemController@index',
        'middleware' => 'can:shopping.invoiceitems.index'
    ]);
    $router->get('invoiceitems/create', [
        'as' => 'admin.shopping.invoiceitem.create',
        'uses' => 'InvoiceitemController@create',
        'middleware' => 'can:shopping.invoiceitems.create'
    ]);
    $router->post('invoiceitems', [
        'as' => 'admin.shopping.invoiceitem.store',
        'uses' => 'InvoiceitemController@store',
        'middleware' => 'can:shopping.invoiceitems.create'
    ]);
    $router->post('invoiceitems-ajax', [
        'as' => 'admin.shopping.invoiceitem.storeItem',
        'uses' => 'InvoiceitemController@storeItem',
        'middleware' => 'can:shopping.invoiceitems.create'
    ]);
    $router->get('invoiceitems/{invoiceitem}/edit', [
        'as' => 'admin.shopping.invoiceitem.edit',
        'uses' => 'InvoiceitemController@edit',
        'middleware' => 'can:shopping.invoiceitems.edit'
    ]);
    $router->put('invoiceitems/{invoiceitem}', [
        'as' => 'admin.shopping.invoiceitem.update',
        'uses' => 'InvoiceitemController@update',
        'middleware' => 'can:shopping.invoiceitems.edit'
    ]);
    $router->delete('invoiceitems/{invoiceitem}', [
        'as' => 'admin.shopping.invoiceitem.destroy',
        'uses' => 'InvoiceitemController@destroy',
        'middleware' => 'can:shopping.invoiceitems.destroy'
    ]);
    $router->get('delete-invoiceitems/{id}', [
        'as' => 'admin.shopping.invoiceitem.destroyByID',
        'uses' => 'InvoiceitemController@destroyByID',
        'middleware' => 'can:shopping.invoiceitems.destroy'
    ]);
    $router->bind('method', function ($id) {
        return app('Modules\Shopping\Repositories\MethodRepository')->find($id);
    });
    $router->get('methods', [
        'as' => 'admin.shopping.method.index',
        'uses' => 'MethodController@index',
        'middleware' => 'can:shopping.methods.index'
    ]);
    $router->get('methods/create', [
        'as' => 'admin.shopping.method.create',
        'uses' => 'MethodController@create',
        'middleware' => 'can:shopping.methods.create'
    ]);
    $router->post('methods', [
        'as' => 'admin.shopping.method.store',
        'uses' => 'MethodController@store',
        'middleware' => 'can:shopping.methods.create'
    ]);
    $router->get('methods/{method}/edit', [
        'as' => 'admin.shopping.method.edit',
        'uses' => 'MethodController@edit',
        'middleware' => 'can:shopping.methods.edit'
    ]);
    $router->put('methods/{method}', [
        'as' => 'admin.shopping.method.update',
        'uses' => 'MethodController@update',
        'middleware' => 'can:shopping.methods.edit'
    ]);
    $router->delete('methods/{method}', [
        'as' => 'admin.shopping.method.destroy',
        'uses' => 'MethodController@destroy',
        'middleware' => 'can:shopping.methods.destroy'
    ]);
// append




});
