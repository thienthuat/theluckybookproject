<?php

namespace Modules\Shopping\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterShoppingSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('shopping::shoppings.title.shoppings'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('shopping::invoices.title.invoices'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.shopping.invoice.create');
                    $item->route('admin.shopping.invoice.index');
                    $item->authorize(
                        $this->auth->hasAccess('shopping.invoices.index')
                    );
                });
//                $item->item(trans('shopping::invoiceitems.title.invoiceitems'), function (Item $item) {
//                    $item->icon('fa fa-copy');
//                    $item->weight(0);
//                    $item->append('admin.shopping.invoiceitem.create');
//                    $item->route('admin.shopping.invoiceitem.index');
//                    $item->authorize(
//                        $this->auth->hasAccess('shopping.invoiceitems.index')
//                    );
//                });
                $item->item(trans('shopping::methods.title.methods'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.shopping.method.create');
                    $item->route('admin.shopping.method.index');
                    $item->authorize(
                        $this->auth->hasAccess('shopping.methods.index')
                    );
                });
// append




            });
        });

        return $menu;
    }
}
