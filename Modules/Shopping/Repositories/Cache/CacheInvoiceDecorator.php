<?php

namespace Modules\Shopping\Repositories\Cache;

use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInvoiceDecorator extends BaseCacheDecorator implements InvoiceRepository
{
    public function __construct(InvoiceRepository $invoice)
    {
        parent::__construct();
        $this->entityName = 'shopping.invoices';
        $this->repository = $invoice;
    }
}
