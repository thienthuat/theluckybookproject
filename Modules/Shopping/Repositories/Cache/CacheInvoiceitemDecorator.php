<?php

namespace Modules\Shopping\Repositories\Cache;

use Modules\Shopping\Repositories\InvoiceitemRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInvoiceitemDecorator extends BaseCacheDecorator implements InvoiceitemRepository
{
    public function __construct(InvoiceitemRepository $invoiceitem)
    {
        parent::__construct();
        $this->entityName = 'shopping.invoiceitems';
        $this->repository = $invoiceitem;
    }
}
