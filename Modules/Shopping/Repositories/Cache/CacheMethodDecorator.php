<?php

namespace Modules\Shopping\Repositories\Cache;

use Modules\Shopping\Repositories\MethodRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheMethodDecorator extends BaseCacheDecorator implements MethodRepository
{
    public function __construct(MethodRepository $method)
    {
        parent::__construct();
        $this->entityName = 'shopping.methods';
        $this->repository = $method;
    }
}
