<?php

namespace Modules\Shopping\Repositories\Eloquent;

use Modules\Shopping\Repositories\MethodRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentMethodRepository extends EloquentBaseRepository implements MethodRepository
{
}
