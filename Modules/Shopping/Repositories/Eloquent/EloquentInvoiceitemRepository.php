<?php

namespace Modules\Shopping\Repositories\Eloquent;

use Modules\Shopping\Repositories\InvoiceitemRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInvoiceitemRepository extends EloquentBaseRepository implements InvoiceitemRepository
{
    public function calTotal($invoice, $ship)
    {
        $itemInvoices = $this->model->where('invoice_id', $invoice->id)->get();
        $total = 0;
        foreach ($itemInvoices as $itemInvoice) {
            $total = $total + (intval($itemInvoice->price) * intval($itemInvoice->qty));
        }

        if ($ship == null) {
            if ($total < 500000) {
                $total = $total + 30000 - $invoice->total_promo;
            }
        }
        return $total;
    }
}
