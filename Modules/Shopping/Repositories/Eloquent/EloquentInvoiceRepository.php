<?php

namespace Modules\Shopping\Repositories\Eloquent;

use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInvoiceRepository extends EloquentBaseRepository implements InvoiceRepository
{
    public function getInvoiceByUser($user_id, $take = 5)
    {
        return $this->model->where('customer_id', $user_id)->orderBy('created_at', 'DESC')->take($take)->get();
    }

    public function getInvoiceStatus()
    {
        return $this->model->where('status', 0)->orderBy('created_at', 'ASC')->get();
    }
}
