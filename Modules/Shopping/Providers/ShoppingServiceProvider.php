<?php

namespace Modules\Shopping\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Shopping\Events\Handlers\RegisterShoppingSidebar;

class ShoppingServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterShoppingSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('shopping', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Shopping\Repositories\InvoiceRepository',
            function () {
                $repository = new \Modules\Shopping\Repositories\Eloquent\EloquentInvoiceRepository(new \Modules\Shopping\Entities\Invoice());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Shopping\Repositories\Cache\CacheInvoiceDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Shopping\Repositories\InvoiceitemRepository',
            function () {
                $repository = new \Modules\Shopping\Repositories\Eloquent\EloquentInvoiceitemRepository(new \Modules\Shopping\Entities\Invoiceitem());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Shopping\Repositories\Cache\CacheInvoiceitemDecorator($repository);
            }
        );

        $this->app->bind(
            'Modules\Shopping\Repositories\MethodRepository',
            function () {
                $repository = new \Modules\Shopping\Repositories\Eloquent\EloquentMethodRepository(new \Modules\Shopping\Entities\Method());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Shopping\Repositories\Cache\CacheMethodDecorator($repository);
            }
        );
// add bindings




    }
}
