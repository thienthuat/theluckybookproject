<?php

namespace Modules\Shopping\Widgets;

use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;

class TotalOrderWidget extends BaseWidget
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
        return 'TotalOrderWidget';
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
        return 'shopping::admin.widgets.total-order';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        return ['countOrder'=>$this->invoiceRepository->all()->count()];
    }

    /**
     * Get the widget type
     * @return string
     */
    protected function options()
    {
        return [
            'width' => '3',
            'height' => '2',
            'x' => '6',
            'y' => '0'
        ];
    }
}
