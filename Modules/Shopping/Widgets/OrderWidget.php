<?php

namespace Modules\Shopping\Widgets;

use Modules\Customer\Repositories\CustomerRepository;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;
use Modules\Shopping\Repositories\InvoiceRepository;

class OrderWidget extends BaseWidget
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;
    private $invoiceRepository;

    public function __construct(CustomerRepository $customerRepository, InvoiceRepository $invoiceRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
        return 'OrderWidget';
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
        return 'shopping::admin.widgets.order';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        return ['invoices' => $this->invoiceRepository->getInvoiceStatus()];
    }

    /**
     * Get the widget type
     * @return string
     */
    protected function options()
    {
        return [
            'width' => '6',
            'height' => '4',
            'x' => '0',
            'y' => '2'
        ];
    }
}
