<div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Total Invoice</span>
        <span class="info-box-number">{{$countOrder}}</span>
    </div>
    <!-- /.info-box-content -->
</div>