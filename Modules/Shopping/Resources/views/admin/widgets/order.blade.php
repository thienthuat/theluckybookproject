<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Orders Pendding</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin data-table">
                <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Payment Method</th>
                    <th>Total</th>
                    <th>Create at</th>
                </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                <tr>
                    <td><a href="{{route('admin.shopping.invoice.edit',$invoice->id)}}">{{$invoice->code}}</a></td>
                    <td><strong>{{$invoice->method_payment->title}}</strong></td>
                    <td><span class="label label-success">
                            <span>{{number_format($invoice->total)}}
                                <sup>đ</sup></span>
                        </span></td>
                    <td>
                        {{date('d-m-Y H:i',strtotime($invoice->created_at))}}
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <a href="{{route('admin.shopping.invoice.index')}}" class="uppercase">View All Invoice</a>
    </div>
    <!-- /.box-footer -->
</div>
<script type="text/javascript">
    $(function () {
        $('.data-table').dataTable({
            "paginate": true,
            "lengthChange": true,
            "filter": true,
            "sort": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]]
        });
    });
</script>