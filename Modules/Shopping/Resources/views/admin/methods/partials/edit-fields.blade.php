<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        <?php $old = $method->hasTranslation($lang) ? $method->translate($lang)->title : '' ?>
        {!! Form::label("{$lang}[title]", trans('shopping::methods.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}[title]",$old), ['class' => 'form-control', 'placeholder' => trans('shopping::methods.form.title')]) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class="alert alert-warning">
        <p>%invoice_number% : Sẻ thay đổi thành mã đơn hàng</p>
    </div>
    <?php $old = $method->hasTranslation($lang) ? $method->translate($lang)->description : '' ?>
    @editor('description', trans('shopping::methods.form.description'), old("{$lang}.description",$old), $lang)
</div>
