<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[title]", trans('shopping::methods.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}[title]"), ['class' => 'form-control', 'placeholder' =>
        trans('shopping::methods.form.title')]) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class="alert alert-warning">
        <p>%invoice_number% : Sẻ thay đổi thành mã đơn hàng</p>
    </div>
    @editor('description', trans('shopping::methods.form.description'), old("{$lang}.description"), $lang)
</div>