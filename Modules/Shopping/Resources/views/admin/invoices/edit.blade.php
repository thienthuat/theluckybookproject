@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('shopping::invoices.title.edit invoice') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i>
            {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li><a href="{{ route('admin.shopping.invoice.index') }}">{{ trans('shopping::invoices.title.invoices') }}</a>
    </li>
    <li class="active">{{ trans('shopping::invoices.title.edit invoice') }}</li>
</ol>
@stop

@section('content')
{!! Form::open(['route' => ['admin.shopping.invoice.update', $invoice->id], 'method' => 'put']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            @include('partials.form-tab-headers')
            <div class="tab-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="item-order">
                            <div id="box_main_cart" class="mt-5">

                                <div class="col-md-8">
                                    <div class="block block-cart-info">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>Temporarily charged</th>
                                            </tr>
                                            @foreach($invoice->item_invoice as $item)
                                            <tr>
                                                <td>
                                                    <h3>{{$item->title}}</h3>
                                                    <p class="option-item-cart">
                                                        <i><a href="javascript:void(0)" class="delete-header-cart"
                                                                data-url="{{route('admin.shopping.invoiceitem.destroyByID',$item->id)}}">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                                <span>Delete Item</span>
                                                            </a></i></p>
                                                </td>
                                                <td>
                                                    <span><a href="#" class="item"
                                                            data-pk="price__-__{{$item->id}}">{{number_format($item->price)}}</a>
                                                        <sup>đ</sup></span>
                                                </td>
                                                <td>
                                                    <div>
                                                        <a href="#" class="item"
                                                            data-pk="qty__-__{{$item->id}}">{{$item->qty}}</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <span>{{number_format($item->price*$item->qty)}}
                                                        <sup>đ</sup></span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        <div class="box-btn-add-item" style="margin-bottom: 20px">
                                            <button type="button" class="btn btn-info" id="btn_add_item_product">Add
                                                Invoice Item
                                            </button>
                                            <div class="box-product box-product-add-form" style="display: none">
                                                <div id="status"></div>
                                                <div class="table-responsive">
                                                    <table class="data-table table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>SKU</th>
                                                                <th>{{trans('shopping::invoices.form.product_name')}}
                                                                </th>
                                                                <th>{{trans('shopping::invoices.form.price')}}</th>
                                                                <th>{{trans('shopping::invoices.form.qty')}}</th>
                                                                <th data-sortable="false">
                                                                    {{ trans('core::core.table.actions') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if (isset($products)): ?>
                                                            <?php foreach ($products as $product): ?>
                                                            <tr>
                                                                <td>{{$product->SKU}}</td>
                                                                <td>{{$product->title}}</td>
                                                                <td>
                                                                    @if($product->price_sale > 0)
                                                                    <div class="special-price">
                                                                        <div class="price-container">
                                                                            <div class="price-wrapper ">
                                                                                <div class="price">
                                                                                    <sup>đ</sup>
                                                                                    <span
                                                                                        class="price_change">{{number_format($product->price_sale)}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="old-price">
                                                                        <div class="price-container">
                                                                            <div class="price-wrapper ">
                                                                                <div class="price">
                                                                                    <sup>đ</sup>{{number_format($product->price)}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="special-price">
                                                                        <div class="price-container">
                                                                            <div class="price-wrapper ">
                                                                                <div class="price"><sup>đ</sup>
                                                                                    <span class="price_change">
                                                                                        {{number_format($product->price)}}
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <input type="number" name="qty" id="qty"
                                                                        maxlength="12" value="1" title="Qty"
                                                                        class="input-text product_qty">
                                                                </td>
                                                                <td>
                                                                    <div class="btn-group">
                                                                        <button
                                                                            class="btn btn-danger btn-flat add-item-to-invoice"
                                                                            type="button"
                                                                            data-url="{{route('admin.shopping.invoiceitem.storeItem')}}"
                                                                            data-id="{{$product->id}}"
                                                                            data-invoice="{{$invoice->id}}"
                                                                            data-price="{{$product->price_sale >0 ?$product->price_sale:$product->price}}">
                                                                            <i class="fa fa-cart-plus"></i></button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th>SKU</th>
                                                                <th>{{trans('shopping::invoices.form.product_name')}}
                                                                </th>
                                                                <th>{{trans('shopping::invoices.form.price')}}</th>
                                                                <th>{{trans('shopping::invoices.form.qty')}}</th>
                                                                <th>{{ trans('core::core.table.actions') }}</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <!-- /.box-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block block-dashboard-addresses">
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Client Information</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("full_name") ? ' has-error' : '' }}">
                                                            <label
                                                                for="first_name">{{trans('customer::customers.form.full_name')}}</label>
                                                            <input type="text" class="form-control" name="full_name"
                                                                id="full_name"
                                                                placeholder="{{trans('customer::customers.form.first_name')}}"
                                                                value="{{old('full_name',$invoice->full_name)}}">
                                                            {!! $errors->first("full_name", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("address") ? ' has-error' : '' }}">
                                                            <label
                                                                for="address">{{trans('customer::customers.form.address')}}</label>
                                                            <input type="text" class="form-control" name="address"
                                                                placeholder="{{trans('customer::customers.form.address')}}"
                                                                id="address"
                                                                value="{{old('address',$invoice->address)}}">
                                                            {!! $errors->first("address", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("phone") ? ' has-error' : '' }}">
                                                            <label
                                                                for="phone">{{trans('customer::customers.form.phone')}}</label>
                                                            <input type="text" class="form-control" name="phone"
                                                                placeholder="{{trans('customer::customers.form.phone')}}"
                                                                id="phone" value="{{old('phone',$invoice->phone)}}">
                                                            {!! $errors->first("phone", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("city") ? ' has-error' : '' }}">
                                                            <label
                                                                for="city">{{trans('customer::customers.form.city')}}</label>
                                                            <select name="city" id="city"
                                                                class="form-control select2 city_onchange" required
                                                                data-url="{{route('admin.location.ajax.district')}}">
                                                                <option value="">
                                                                    --Choose {{trans('customer::customers.form.city')}}
                                                                    --
                                                                </option>
                                                                @foreach($provinces as $province)
                                                                <option value="{{$province->id}}"
                                                                    {{$province->id == $invoice->city ?'selected':''}}>
                                                                    {{$province->name}}</option>
                                                                @endforeach

                                                            </select>
                                                            {!! $errors->first("city", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("district") ? ' has-error' : '' }}">
                                                            <label
                                                                for="district">{{trans('customer::customers.form.district')}}</label>
                                                            <select name="district" id="district"
                                                                class="form-control select2 district_onchange" required
                                                                data-url="{{route('admin.location.ajax.ward')}}">
                                                                <option value="">
                                                                    --Choose
                                                                    {{trans('customer::customers.form.district')}}
                                                                    --
                                                                </option>
                                                                @foreach($districts as $district)
                                                                <option value="{{$district->id}}"
                                                                    {{$district->id == $invoice->district ?'selected':''}}>
                                                                    {{$district->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            {!! $errors->first("district", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div
                                                            class="form-group {{ $errors->has("ward") ? ' has-error' : '' }}">
                                                            <label
                                                                for="ward">{{trans('customer::customers.form.ward')}}</label>
                                                            <select name="wards" id="ward"
                                                                class="form-control select2 ward_onchange" required>
                                                                <option value="">
                                                                    --Choose {{trans('customer::customers.form.ward')}}
                                                                    --
                                                                </option>
                                                                @foreach($wards as $ward)
                                                                <option value="{{$ward->id}}"
                                                                    {{$ward->id == $invoice->wards ?'selected':''}}>
                                                                    {{$ward->type}} {{$ward->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            {!! $errors->first("ward", '<span
                                                                class="help-block">:message</span>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="note">Note</label>
                                                            <textarea class="form-control" name="note"
                                                                id="note">{{old('note',$invoice->note)}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="block block-collapsible-nav block-item-nav-cart-first">
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Order Information</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Mã đơn hàng:</span></strong>
                                                    </span>
                                                    <span>
                                                        <span>{{$invoice->code}}</span>
                                                    </span>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Tạm tính:</span></strong>
                                                    </span>
                                                    <span>
                                                        <span>{{number_format($invoice->sub_total)}}
                                                            <sup>đ</sup></span>
                                                    </span>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Giảm giá:</span></strong>
                                                    </span>
                                                    <span>
                                                        <span>{{number_format($invoice->total_promo)}}
                                                            <sup>đ</sup></span>
                                                    </span>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Chi phí vận chuyển:</span></strong>
                                                    </span>
                                                    <span>
                                                        <span>{{number_format($invoice->price_ship)}}
                                                            <sup>đ</sup></span>
                                                    </span>
                                                </div>
                                                @if($invoice->promotion)
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Mã Khuyến Mãi:</span></strong>
                                                    </span>
                                                    <span>
                                                        <a
                                                            href="{{ route('admin.product.promotion.edit', [$invoice->promotion->id]) }}">
                                                            {{$invoice->promotion->code}}</a>
                                                    </span>
                                                </div>
                                                @endif
                                                <div class="d-flex justify-content-between">
                                                    <span>
                                                        <strong><span>Total:</span></strong>
                                                    </span>
                                                    <span>
                                                        <span>{{number_format($invoice->total)}}
                                                            <sup>đ</sup></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block block-collapsible-nav">
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Payment</h3>
                                            </div>
                                            <div class="box-body">
                                                @foreach($methods as $key=>$item)
                                                <div class="checkbox">
                                                    <label for="method{{$key}}">
                                                        <input id="method{{$key}}" name="payment" type="radio"
                                                            class="flat-blue"
                                                            {{$invoice->payment==$item->id?'checked':''}}
                                                            value="{{$item->id}}" />
                                                        {{ $item->title }}
                                                    </label>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block block-collapsible-nav">
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Status</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    @php($status_invoices =
                                                    Config::get('asgard.shopping.config.status_invoice'))
                                                    @foreach($status_invoices as $key=>$item)
                                                    <div class="checkbox">
                                                        <label for="status{{$key}}">
                                                            <input id="status{{$key}}" name="status" type="radio"
                                                                class="flat-blue"
                                                                {{$invoice->status==$key?'checked':''}}
                                                                value="{{$key}}" />
                                                            {{ $item }}
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat" name="button" value="index">
                        <i class="fa fa-angle-left"></i>
                        {{ trans('core::core.button.update and back') }}
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat">
                        {{ trans('core::core.button.update') }}
                    </button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.shopping.invoice.index')}}"><i
                            class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
        </div> {{-- end nav-tabs-custom --}}
    </div>
</div>
{!! Form::close() !!}
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
    <dt><code>b</code></dt>
    <dd>{{ trans('core::core.back to index') }}</dd>
</dl>
@stop

@push('js-stack')
<script type="text/javascript">
    $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.shopping.invoice.index') ?>"}
                ]
            });

            $('.delete-header-cart').click(function () {
                var url = $(this).attr('data-url')
                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function (res) {
                        location.reload(true);
                    }
                })
            });

            $('.add-item-to-invoice').click(function () {
                var url = $(this).attr('data-url');
                var price = $(this).attr('data-price');
                var product_id = $(this).attr('data-id');
                var invoice = $(this).attr('data-invoice');
                var qty = $(this).parents('tr').find('.product_qty').val();
                var type_color = $(this).parents('tr').find('.type_color:checked').val();
                var type_color_title = $(this).parents('tr').find('.type_color:checked').attr('data-title');
                var type_size = $(this).parents('tr').find('.type_size:checked').val();
                var type_size_title = $(this).parents('tr').find('.type_size:checked').attr('data-title');
                var type_size_price = $(this).parents('tr').find('.type_size:checked ~ .choose-size').attr('data-price');
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        price: price,
                        product_id: product_id,
                        invoice: invoice,
                        qty: qty,
                        type_color: type_color,
                        type_size: type_size,
                        type_size_title: type_size_title,
                        type_color_title: type_color_title,
                        type_size_price: type_size_price,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (res) {
                        location.reload(true);
                    }
                })
            });
        });
        $(function () {
            $('a.item').editable({
                url: function (params) {
                    var splitKey = params.pk.split("__-__");
                    var id = splitKey[1];
                    var key = splitKey[0]
                    var value = params.value;
                    if(!id || !splitKey) {
                        return false;
                    }

                    $.ajax({
                        url: '{{ route("admin.shopping.invoice.updateItemInvoice") }}',
                        method: 'POST',
                        data: {
                            key: key,
                            value: value,
                            id: id,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function (res) {
                            location.reload(true);
                        }
                    })
                },
                type: 'text',
                mode: 'inline',
                send: 'always', /* Always send, because we have no 'pk' which editable expects */
                inputclass: 'item_input'
            });
        });
</script>
<script>
    $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[0, "desc"]]
            });
        });
        $(document).ready(function () {
            $('#btn_add_item_product').click(function () {
                $('.box-product-add-form').slideToggle();
            });
            $('.city_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".district_onchange option").each(function () {
                        if($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $(".ward_onchange option").each(function () {
                        if($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.district_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('.district_onchange').change(function () {
                var url = $(this).attr('data-url')
                var id = $(this).val();
                $.get(url + '/' + id, function (datas) {
                    $(".ward_onchange option").each(function () {
                        if($(this).val() != '') {
                            $(this).remove();
                        }
                    });
                    $.each(datas.data, function (i, item) {
                        $('.ward_onchange').append($('<option>', {
                            value: item.id,
                            text: item.type + ' ' + item.name
                        }));
                    });
                });
            });
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
</script>
@endpush