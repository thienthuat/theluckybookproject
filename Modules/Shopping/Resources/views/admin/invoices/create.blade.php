@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('shopping::invoices.title.create invoice') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.shopping.invoice.index') }}">{{ trans('shopping::invoices.title.invoices') }}</a>
        </li>
        <li class="active">{{ trans('shopping::invoices.title.create invoice') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.shopping.invoice.invoiceReview'], 'method' => 'post','id'=>'CREATECUSTOMER']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group {{ $errors->has("sex") ? ' has-error' : '' }}">
                                        <label for="customer_id">{{trans('shopping::invoices.form.customer')}}</label>
                                        <select name="customer_id" id="customer_id" class="form-control select2" required>
                                            <option value="">--Choose {{trans('shopping::invoices.form.customer')}}--</option>
                                            @foreach($customers as $customer)
                                                <option value="{{$customer->id}}">{{$customer->first_name}} {{$customer->first_name}}
                                                    ({{$customer->phone}}) ({{$customer->email}})
                                                </option>
                                            @endforeach
                                        </select>
                                        {!! $errors->first("customer_id", '<span class="help-block">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('admin.customer.customer.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px; margin-top: 2.5rem">
                                        <i class="fa fa-user"></i> {{ trans('customer::customers.button.create customer') }}
                                    </a>
                                </div>
                            </div>
                            <div class="box-product box-product-add-form">
                                <div id="status"></div>
                                <div class="table-responsive">
                                    <table class="data-table table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SKU</th>
                                            <th>{{trans('shopping::invoices.form.product_name')}}</th>
                                            <th>{{trans('shopping::invoices.form.price')}}</th>
                                            <th>{{trans('shopping::invoices.form.color')}}</th>
                                            <th>{{trans('shopping::invoices.form.size')}}</th>
                                            <th>{{trans('shopping::invoices.form.qty')}}</th>
                                            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (isset($products)): ?>
                                        <?php foreach ($products as $product): ?>
                                        <tr>
                                            <td>{{$product->SKU}}</td>
                                            <td>{{$product->title}}</td>
                                            <td>
                                                @if($product->price_sale > 0)
                                                    <div class="special-price">
                                                        <div class="price-container">
                                                            <div class="price-wrapper ">
                                                                <div class="price">
                                                                    <sup>đ</sup>
                                                                    <span class="price_change">{{number_format($product->price_sale)}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="old-price">
                                                        <div class="price-container">
                                                            <div class="price-wrapper ">
                                                                <div class="price">
                                                                    <sup>đ</sup>{{number_format($product->price)}}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="special-price">
                                                        <div class="price-container">
                                                            <div class="price-wrapper ">
                                                                <div class="price"><sup>đ</sup>
                                                                    <span class="price_change">
                                                                        {{number_format($product->price)}}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                @php($colors= $product->colors)
                                                @if($colors->count() >0 )
                                                    <div class="box-color-attr">
                                                        @foreach($colors as $color)
                                                            <label for="color_{{$color->id}}{{$product->id}}">
                                                                <input type="radio" name="type_color_{{$product->id}}"
                                                                       value="{{$color->id}}"
                                                                       id="color_{{$color->id}}{{$product->id}}" class="type_color">
                                                                <div class="choose"
                                                                     style="background-color: {{$color->code}}"></div>
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                @php($sizes= $product->sizes)
                                                @if($sizes->count() >0 )
                                                    <div class="box-size-attr">
                                                        @foreach($sizes as $size)
                                                            @php($price = $size->getPriceById($product->id,$size->id))
                                                            <label for="size_{{$size->id}}{{$product->id}}">
                                                                <input type="radio" name="type_size_{{$product->id}}"
                                                                       value="{{$size->id}}"
                                                                       id="size_{{$size->id}}{{$product->id}}" class="type_size">
                                                                <div class="choose-size" data-price="{{$price['price']}}"
                                                                     data-price-format="{{number_format($price['price'])}}">{{$size->title}}</div>
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                <input type="number" name="qty" id="qty" maxlength="12" value="1"
                                                       title="Qty"
                                                       class="input-text product_qty">
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-danger btn-flat add-to-cart"
                                                            type="button"
                                                            data-url="{{route('admin.shopping.invoice.invoiceAddToCart')}}"
                                                            data-id="{{$product->id}}"
                                                            data-price="{{$product->price_sale >0 ?$product->price_sale:$product->price}}">
                                                        <i class="fa fa-cart-plus"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SKU</th>
                                            <th>{{trans('shopping::invoices.form.product_name')}}</th>
                                            <th>{{trans('shopping::invoices.form.price')}}</th>
                                            <th>{{trans('shopping::invoices.form.color')}}</th>
                                            <th>{{trans('shopping::invoices.form.size')}}</th>
                                            <th>{{trans('shopping::invoices.form.qty')}}</th>
                                            <th>{{ trans('core::core.table.actions') }}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <div id="box_main_cart">
                                @if(Cart::content()->count() > 0)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <table class="table table-striped">
                                                <tr>
                                                    <th width="10rem">Image</th>
                                                    <th>Product Name</th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                </tr>
                                                @foreach(Cart::content() as $item)
                                                    <tr>
                                                        <td>
                                                            <img src="{{$item->options->avatar}}"
                                                                 alt="{{$item->name}}" class="img-responsive">
                                                        </td>
                                                        <td>
                                                            <h3>{{$item->name}}</h3>
                                                            @if($item->options->size !='' ||$item->options->size)
                                                                <p class="option-item-cart">
                                                                    <i>( {{$item->options->size !='' ? 'Kích thước:'.$item->options->size:''}} {{$item->options->color !='' ? 'Màu sắc:'.$item->options->color:''}}
                                                                        )</i></p>
                                                            @endif
                                                            <a href="javascript:void(0)" class="delete-item-cart"
                                                               data-url="{{route('admin.shopping.invoice.invoiceDeleteToCart',$item->rowId)}}"><i>Delete</i></a>
                                                        </td>
                                                        <td>
                                                            <span>{{number_format($item->price)}} <sup>đ</sup></span>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <input type="number" name="qty" value="{{$item->qty}}"
                                                                       class="input-qty" data-id="{{$item->rowId}}"
                                                                       data-url="{{route('admin.shopping.invoice.invoiceUpdateToCart')}}">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="block block-collapsible-nav">
                                                <div class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Order Information</h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <di class="d-flex justify-content-between">
                                                            <span>
                                                                <strong><span>Temporarily charged:</span></strong>
                                                            </span>
                                                            <span>
                                                                <span>{{Cart::subtotal()}} <sup>đ</sup></span>
                                                            </span>
                                                        </di>
                                                        <div class="total-cart">
                                                            <di class="d-flex justify-content-between">
                                                                <span>
                                                                    <strong><span>Total:</span></strong>
                                                                </span>
                                                                <span>
                                                                    <span>{{Cart::total()}} <sup>đ</sup></span>
                                                                </span>
                                                            </di>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.shopping.invoice.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#CREATECUSTOMER').validate({
                submitHandler : function (form) {
                    $(form).submit();
                }
            });
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.shopping.invoice.index') ?>"}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[0, "desc"]]
            });
        });
    </script>
@endpush
