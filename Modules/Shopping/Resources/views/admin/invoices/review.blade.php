@extends('layouts.master')

@section('content-header')
    <h1>
        Review Invoice
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.shopping.invoice.index') }}">{{ trans('shopping::invoices.title.invoices') }}</a>
        </li>
        <li class="active">Review Invoice</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.shopping.invoice.store'], 'method' => 'post']) !!}
    <input type="hidden" name="customer_id" value="{{$customer->id}}">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="box_main_cart">
                                @if(Cart::content()->count() > 0)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <table class="table table-striped">
                                                <tr>
                                                    <th width="5rem">Image</th>
                                                    <th>Product Name</th>
                                                    <th>Price</th>
                                                    <th>Qty</th>
                                                </tr>
                                                @foreach(Cart::content() as $item)
                                                    <tr>
                                                        <td>
                                                            <img src="{{$item->options->avatar}}"
                                                                 alt="{{$item->name}}" class="img-responsive">
                                                        </td>
                                                        <td>
                                                            <h3>{{$item->name}}</h3>
                                                        </td>
                                                        <td>
                                                            <span>{{number_format($item->price)}} <sup>đ</sup></span>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                <input type="number" name="qty" value="{{$item->qty}}"
                                                                       disabled
                                                                       class="input-qty" data-id="{{$item->rowId}}"
                                                                       data-url="{{route('admin.shopping.invoice.invoiceUpdateToCart')}}">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="note">Note</label>
                                                        <textarea class="form-control" name="note" id="note" rows="5">{{old('note')}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="block block-collapsible-nav">
                                                <div class="block block-collapsible-nav">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Order Information</h3>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="d-flex justify-content-between">
                                                        <span>
                                                            <strong><span>Temporarily charged:</span></strong>
                                                        </span>
                                                                <span>
                                                            <span>{{Cart::subtotal()}} <sup>đ</sup></span>
                                                        </span>
                                                            </div>
                                                            <div class="d-flex justify-content-between">
                                                        <span>
                                                            <strong><span>Ship Fee:</span></strong>
                                                        </span>
                                                                <span>
                                                            <span>{{number_format($customer->addressShipDefault()->districts->price_ship)}}
                                                                <sup>đ</sup></span>
                                                        </span>
                                                            </div>
                                                            <div class="total-cart">
                                                                <div class="d-flex justify-content-between">
                                                            <span>
                                                                <strong><span>Total:</span></strong>
                                                            </span>
                                                                    <span>
                                                                <span>{{number_format(Cart::total(0,'.','')+$customer->addressShipDefault()->districts->price_ship)}}
                                                                    <sup>đ</sup></span>
                                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block block-collapsible-nav">
                                                <div class="block block-collapsible-nav">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Client Information</h3>
                                                        </div>
                                                        <div class="box-body">
                                                            <address>
                                                                <div class="box-info-address">
                                                                    <p>
                                                                        <strong>{{$customer->addressShipDefault()->full_name}}</strong>
                                                                    </p>
                                                                    <p>{{$customer->addressShipDefault()->address}}</p>
                                                                    <p>
                                                                        {{$customer->addressShipDefault()->citys->name}}
                                                                        , {{$customer->addressShipDefault()->districts->type}} {{$customer->addressShipDefault()->districts->name}}
                                                                        , {{$customer->addressShipDefault()->ward->type}} {{$customer->addressShipDefault()->ward->name}}
                                                                    </p>
                                                                    <p>{{$customer->addressShipDefault()->phone}}</p>
                                                                </div>
                                                            </address>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block block-collapsible-nav">
                                                <div class="block block-collapsible-nav">
                                                    <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Payment</h3>
                                                        </div>
                                                        <div class="box-body">
                                                            @foreach($payment_methods as $key=>$method)
                                                                <label class="custom-control custom-radio">
                                                                    <input id="payment_method{{$method->id}}"
                                                                           value="{{$method->id}}"
                                                                           name="payment_method" type="radio"
                                                                           class="custom-control-input" {{$key==0 ?'checked':''}}>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">{{$method->title}}</span>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.shopping.invoice.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.shopping.invoice.index') ?>"}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
