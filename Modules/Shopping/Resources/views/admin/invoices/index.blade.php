@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('shopping::invoices.title.invoices') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('shopping::invoices.title.invoices') }}</li>
    </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.shopping.invoice.create') }}" class="btn btn-primary btn-flat"
                       style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('shopping::invoices.button.create invoice') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('shopping::invoices.table.code') }}</th>
                                <th>{{ trans('shopping::invoices.table.method') }}</th>
                                <th>{{ trans('shopping::invoices.table.total') }}</th>
                                <th>{{ trans('shopping::invoices.table.status') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($invoices)): ?>
                            <?php foreach ($invoices as $invoice): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.shopping.invoice.edit', [$invoice->id]) }}">
                                        {{ $invoice->id }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.shopping.invoice.edit', [$invoice->id]) }}">
                                        {{ $invoice->code }}
                                    </a>
                                </td>
                                <td>
                                    <strong>{{$invoice->method_payment->title}}</strong>
                                </td>
                                <td><span>{{number_format($invoice->total)}}
                                        <sup>đ</sup></span></td>
                                <td>
                                    @if($invoice->status == 0)
                                        <span class="label label-danger">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                                    @endif
                                    @if($invoice->status == 1)
                                        <span class="label label-warning">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                                    @endif
                                    @if($invoice->status == 2)
                                        <span class="label label-default">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                                    @endif
                                    @if($invoice->status == 3)
                                        <span class="label label-info">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                                    @endif
                                    @if($invoice->status == 4)
                                        <span class="label label-success">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                                    @endif
                                </td>
                                <td>{{date('d-m-Y H:i',strtotime($invoice->created_at))}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.shopping.invoice.edit', [$invoice->id]) }}"
                                           class="btn btn-default btn-flat"><i class="fa fa-eye-slash"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal"
                                                data-target="#modal-delete-confirmation"
                                                data-action-target="{{ route('admin.shopping.invoice.destroy', [$invoice->id]) }}">
                                            <i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('shopping::invoices.table.code') }}</th>
                                <th>{{ trans('shopping::invoices.table.method') }}</th>
                                <th>{{ trans('shopping::invoices.table.total') }}</th>
                                <th>{{ trans('shopping::invoices.table.status') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('shopping::invoices.title.create invoice') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'c', route: "<?= route('admin.shopping.invoice.create') ?>"}
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[0, "desc"]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
