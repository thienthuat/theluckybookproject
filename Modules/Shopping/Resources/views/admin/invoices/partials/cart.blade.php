@if(Cart::content()->count() > 0)
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped">
                <tr>
                    <th width="10rem">Image</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Qty</th>
                </tr>
                @foreach(Cart::content() as $item)
                    <tr>
                        <td>
                            <img src="{{$item->options->avatar}}"
                                 alt="{{$item->name}}" class="img-responsive">
                        </td>
                        <td>
                            <h3>{{$item->name}}</h3>
                            @if($item->options->size !='' ||$item->options->size)
                                <p class="option-item-cart">
                                    <i>( {{$item->options->size !='' ? 'Kích thước:'.$item->options->size:''}} {{$item->options->color !='' ? 'Màu sắc:'.$item->options->color:''}}
                                        )</i></p>
                            @endif
                            <a href="javascript:void(0)" class="delete-item-cart"
                               data-url="{{route('admin.shopping.invoice.invoiceDeleteToCart',$item->rowId)}}"><i>Delete</i></a>
                        </td>
                        <td>
                            <span>{{number_format($item->price)}} <sup>đ</sup></span>
                        </td>
                        <td>
                            <div>
                                <input type="number" name="qty" value="{{$item->qty}}" class="input-qty"
                                       data-id="{{$item->rowId}}"
                                       data-url="{{route('admin.shopping.invoice.invoiceUpdateToCart')}}">
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-4">
            <div class="block block-collapsible-nav">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Order Information</h3>
                    </div>
                    <div class="box-body">
                        <di class="d-flex justify-content-between">
                                                            <span>
                                                                <strong><span>Temporarily charged:</span></strong>
                                                            </span>
                            <span>
                                                                <span>{{Cart::subtotal()}} <sup>đ</sup></span>
                                                            </span>
                        </di>
                        <div class="total-cart">
                            <di class="d-flex justify-content-between">
                                                                <span>
                                                                    <strong><span>Total:</span></strong>
                                                                </span>
                                <span>
                                                                    <span>{{Cart::total()}} <sup>đ</sup></span>
                                                                </span>
                            </di>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif