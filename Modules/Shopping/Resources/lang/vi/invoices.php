<?php

return [
    'list resource' => 'List invoices',
    'create resource' => 'Create invoices',
    'edit resource' => 'View invoices',
    'destroy resource' => 'Destroy invoices',
    'title' => [
        'invoices' => 'Invoice',
        'create invoice' => 'Create a invoice',
        'edit invoice' => 'View a invoice',
    ],
    'button' => [
        'create invoice' => 'Create a invoice',
    ],
    'table' => [
        'title' => 'Title',
        'code' => 'Order Code',
        'method' => 'Payment Method',
        'total' => 'Total',
        'status' => 'status'
    ],
    'form' => [
        'customer' => 'Customer',
        'product_name' => 'Product Name',
        'price' => 'Price',
        'color' => 'Color',
        'size' => 'Size',
        'qty' => 'Qty',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
