<?php

return [
    'list resource' => 'List methods',
    'create resource' => 'Create methods',
    'edit resource' => 'Edit methods',
    'destroy resource' => 'Destroy methods',
    'title' => [
        'methods' => 'Method',
        'create method' => 'Create a method',
        'edit method' => 'Edit a method',
    ],
    'button' => [
        'create method' => 'Create a method',
    ],
    'table' => [
    ],
    'form' => [
        'title'=>'Title',
        'description'=>'Description',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
