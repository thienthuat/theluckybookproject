<?php

return [
    'list resource' => 'List invoiceitems',
    'create resource' => 'Create invoiceitems',
    'edit resource' => 'Edit invoiceitems',
    'destroy resource' => 'Destroy invoiceitems',
    'title' => [
        'invoiceitems' => 'Invoiceitem',
        'create invoiceitem' => 'Create a invoiceitem',
        'edit invoiceitem' => 'Edit a invoiceitem',
    ],
    'button' => [
        'create invoiceitem' => 'Create a invoiceitem',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
