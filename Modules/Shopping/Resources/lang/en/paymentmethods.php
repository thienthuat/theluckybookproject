<?php

return [
    'list resource' => 'List paymentmethods',
    'create resource' => 'Create paymentmethods',
    'edit resource' => 'Edit paymentmethods',
    'destroy resource' => 'Destroy paymentmethods',
    'title' => [
        'paymentmethods' => 'Paymentmethod',
        'create paymentmethod' => 'Create a paymentmethod',
        'edit paymentmethod' => 'Edit a paymentmethod',
    ],
    'button' => [
        'create paymentmethod' => 'Create a paymentmethod',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
