<?php

namespace Modules\Shopping\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Location\Entities\Province;
use Modules\Location\Entities\District;
use Modules\Location\Entities\Ward;
use Modules\Product\Entities\promotion;
use Modules\Customer\Entities\Customer;

class Invoice extends Model
{
    protected $table = 'shopping__invoices';
    protected $fillable = ['full_name', 'point', 'total_promo', 'point_request', 'id_promotion', 'address', 'price_ship', 'phone', 'district', 'city', 'wards', 'customer_id', 'total', 'sub_total', 'payment', 'status', 'qty', 'code', 'email', 'note'];

    public function item_invoice()
    {
        return $this->hasMany(Invoiceitem::class, 'invoice_id');
    }

    public function method_payment()
    {
        return $this->belongsTo(Method::class, 'payment');
    }

    public function citys()
    {
        return $this->belongsTo(Province::class, 'city');
    }

    public function districts()
    {
        return $this->belongsTo(District::class, 'district');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'wards');
    }

    public function promotion()
    {
        return $this->belongsTo(promotion::class, 'id_promotion');
    }
	
	 public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
