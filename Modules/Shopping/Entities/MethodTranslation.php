<?php

namespace Modules\Shopping\Entities;

use Illuminate\Database\Eloquent\Model;

class MethodTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','description'];
    protected $table = 'shopping__method_translations';
}
