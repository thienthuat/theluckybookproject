<?php

namespace Modules\Shopping\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;

class Invoiceitem extends Model
{
    protected $table = 'shopping__invoiceitems';
    protected $fillable = ['product_id', 'title', 'slug', 'price', 'qty', 'size_id', 'size', 'color', 'color_id', 'invoice_id'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
