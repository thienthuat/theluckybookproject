<?php

namespace Modules\Shopping\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    use Translatable;

    protected $table = 'shopping__methods';
    public $translatedAttributes = ['title','description'];
    protected $fillable = ['title','description'];
}
