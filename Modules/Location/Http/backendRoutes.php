<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/location'], function (Router $router) {
    $router->bind('province', function ($id) {
        return app('Modules\Location\Repositories\ProvinceRepository')->find($id);
    });
    $router->get('provinces', [
        'as' => 'admin.location.province.index',
        'uses' => 'ProvinceController@index',
        'middleware' => 'can:location.provinces.index'
    ]);
    $router->get('provinces/create', [
        'as' => 'admin.location.province.create',
        'uses' => 'ProvinceController@create',
        'middleware' => 'can:location.provinces.create'
    ]);
    $router->post('provinces', [
        'as' => 'admin.location.province.store',
        'uses' => 'ProvinceController@store',
        'middleware' => 'can:location.provinces.create'
    ]);
    $router->get('provinces/{province}/edit', [
        'as' => 'admin.location.province.edit',
        'uses' => 'ProvinceController@edit',
        'middleware' => 'can:location.provinces.edit'
    ]);
    $router->put('provinces/{province}', [
        'as' => 'admin.location.province.update',
        'uses' => 'ProvinceController@update',
        'middleware' => 'can:location.provinces.edit'
    ]);
    $router->delete('provinces/{province}', [
        'as' => 'admin.location.province.destroy',
        'uses' => 'ProvinceController@destroy',
        'middleware' => 'can:location.provinces.destroy'
    ]);
    $router->bind('district', function ($id) {
        return app('Modules\Location\Repositories\DistrictRepository')->find($id);
    });
    $router->get('districts', [
        'as' => 'admin.location.district.index',
        'uses' => 'DistrictController@index',
        'middleware' => 'can:location.districts.index'
    ]);
    $router->get('districts/create', [
        'as' => 'admin.location.district.create',
        'uses' => 'DistrictController@create',
        'middleware' => 'can:location.districts.create'
    ]);
    $router->post('districts', [
        'as' => 'admin.location.district.store',
        'uses' => 'DistrictController@store',
        'middleware' => 'can:location.districts.create'
    ]);
    $router->get('districts/{district}/edit', [
        'as' => 'admin.location.district.edit',
        'uses' => 'DistrictController@edit',
        'middleware' => 'can:location.districts.edit'
    ]);
    $router->put('districts/{district}', [
        'as' => 'admin.location.district.update',
        'uses' => 'DistrictController@update',
        'middleware' => 'can:location.districts.edit'
    ]);
    $router->delete('districts/{district}', [
        'as' => 'admin.location.district.destroy',
        'uses' => 'DistrictController@destroy',
        'middleware' => 'can:location.districts.destroy'
    ]);
    $router->bind('ward', function ($id) {
        return app('Modules\Location\Repositories\WardRepository')->find($id);
    });
    $router->get('wards', [
        'as' => 'admin.location.ward.index',
        'uses' => 'WardController@index',
        'middleware' => 'can:location.wards.index'
    ]);
    $router->get('wards/create', [
        'as' => 'admin.location.ward.create',
        'uses' => 'WardController@create',
        'middleware' => 'can:location.wards.create'
    ]);
    $router->post('wards', [
        'as' => 'admin.location.ward.store',
        'uses' => 'WardController@store',
        'middleware' => 'can:location.wards.create'
    ]);
    $router->get('wards/{ward}/edit', [
        'as' => 'admin.location.ward.edit',
        'uses' => 'WardController@edit',
        'middleware' => 'can:location.wards.edit'
    ]);
    $router->put('wards/{ward}', [
        'as' => 'admin.location.ward.update',
        'uses' => 'WardController@update',
        'middleware' => 'can:location.wards.edit'
    ]);
    $router->get('wards/getWard',['as'=>'admin.location.ward.getWard','uses'=>'WardController@getWard']);
    $router->delete('wards/{ward}', [
        'as' => 'admin.location.ward.destroy',
        'uses' => 'WardController@destroy',
        'middleware' => 'can:location.wards.destroy'
    ]);
    $router->group(['prefix' =>'/ajax'], function (Router $router) {
        $router->get('district-by-id-province/{id?}', [
            'as' => 'admin.location.ajax.district',
            'uses' => 'DistrictController@ajaxDistrict'
        ]);
        $router->get('ward-by-id-district/{id?}', [
            'as' => 'admin.location.ajax.ward',
            'uses' => 'WardController@ajaxWard'
        ]);
    });
// append



});
