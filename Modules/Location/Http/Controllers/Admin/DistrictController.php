<?php

namespace Modules\Location\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Location\Entities\District;
use Modules\Location\Http\Requests\CreateDistrictRequest;
use Modules\Location\Http\Requests\UpdateDistrictRequest;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DistrictController extends AdminBaseController
{
    /**
     * @var DistrictRepository
     */
    private $district;
    private $provinceRepository;

    public function __construct(DistrictRepository $district, ProvinceRepository $provinceRepository)
    {
        parent::__construct();

        $this->district = $district;
        $this->provinceRepository = $provinceRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $districts = $this->district->all();

        return view('location::admin.districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $provinces = $this->provinceRepository->all();
        return view('location::admin.districts.create', compact('provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDistrictRequest $request
     * @return Response
     */
    public function store(CreateDistrictRequest $request)
    {
        $this->district->create($request->all());

        return redirect()->route('admin.location.district.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('location::districts.title.districts')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  District $district
     * @return Response
     */
    public function edit(District $district)
    {
        $provinces = $this->provinceRepository->all();
        return view('location::admin.districts.edit', compact('district','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  District $district
     * @param  UpdateDistrictRequest $request
     * @return Response
     */
    public function update(District $district, UpdateDistrictRequest $request)
    {
        $this->district->update($district, $request->all());

        return redirect()->route('admin.location.district.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('location::districts.title.districts')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  District $district
     * @return Response
     */
    public function destroy(District $district)
    {
        $this->district->destroy($district);

        return redirect()->route('admin.location.district.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('location::districts.title.districts')]));
    }
    public function ajaxDistrict($id){
        $data = $this->district->getDataByIdProvince(strip_tags($id));
        return \response()->json(['data'=>$data]) ->header('Author', 'linhdev92@gmail.om');
    }
}
