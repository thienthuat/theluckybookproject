<?php

namespace Modules\Location\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Location\Entities\Ward;
use Modules\Location\Http\Requests\CreateWardRequest;
use Modules\Location\Http\Requests\UpdateWardRequest;
use Modules\Location\Repositories\WardRepository;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class WardController extends AdminBaseController
{
    /**
     * @var WardRepository
     */
    private $ward;
    private $districtRepository;

    public function __construct(WardRepository $ward, DistrictRepository $districtRepository)
    {
        parent::__construct();

        $this->ward = $ward;
        $this->districtRepository = $districtRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$wards = $this->ward->all();

        return view('location::admin.wards.index');
    }

    public function getWard(Request $request)
    {
        $value = session('count_ward');
        if ($value > 0) {
            $count = $value;
        } else {
            $count = $this->ward->all()->count();
        }
        session(['count_ward' => $count]);
        $take = $request->iDisplayLength;
        $skip = $request->iDisplayStart;
        $draw = $request->sEcho;
        $search = $request->sSearch;
        $data = $this->ward->getData($take, $skip, $search);
        return response()->json(["draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $districts = $this->districtRepository->all();
        return view('location::admin.wards.create',compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWardRequest $request
     * @return Response
     */
    public function store(CreateWardRequest $request)
    {
        $this->ward->create($request->all());

        return redirect()->route('admin.location.ward.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('location::wards.title.wards')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ward $ward
     * @return Response
     */
    public function edit(Ward $ward)
    {
        $districts = $this->districtRepository->all();
        return view('location::admin.wards.edit', compact('ward','districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Ward $ward
     * @param  UpdateWardRequest $request
     * @return Response
     */
    public function update(Ward $ward, UpdateWardRequest $request)
    {
        $this->ward->update($ward, $request->all());

        return redirect()->route('admin.location.ward.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('location::wards.title.wards')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ward $ward
     * @return Response
     */
    public function destroy(Ward $ward)
    {
        $this->ward->destroy($ward);

        return redirect()->route('admin.location.ward.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('location::wards.title.wards')]));
    }

    public function ajaxWard($id){
        $data = $this->ward->getDataByIdDistrict(strip_tags($id));
        return \response()->json(['data'=>$data]) ->header('Author', 'linhdev92@gmail.om');
    }
}
