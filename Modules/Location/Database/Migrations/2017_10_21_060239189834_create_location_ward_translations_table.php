<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationWardTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location__ward_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name');
            $table->string('type');
            $table->string('location')->nullable();
            $table->integer('ward_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['ward_id', 'locale']);
            $table->foreign('ward_id')->references('id')->on('location__wards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location__ward_translations', function (Blueprint $table) {
            $table->dropForeign(['ward_id']);
        });
        Schema::dropIfExists('location__ward_translations');
    }
}
