<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationDistrictTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //`name`, `type`, `location`, `provinceid`
        Schema::create('location__district_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields
            $table->string('name');
            $table->string('type');
            $table->string('location')->nullable();
            $table->integer('district_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['district_id', 'locale']);
            $table->foreign('district_id')->references('id')->on('location__districts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location__district_translations', function (Blueprint $table) {
            $table->dropForeign(['district_id']);
        });
        Schema::dropIfExists('location__district_translations');
    }
}
