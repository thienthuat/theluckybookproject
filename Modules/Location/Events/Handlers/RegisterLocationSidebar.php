<?php

namespace Modules\Location\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterLocationSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('location::locations.title.locations'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('location::provinces.title.provinces'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.location.province.create');
                    $item->route('admin.location.province.index');
                    $item->authorize(
                        $this->auth->hasAccess('location.provinces.index')
                    );
                });
                $item->item(trans('location::districts.title.districts'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.location.district.create');
                    $item->route('admin.location.district.index');
                    $item->authorize(
                        $this->auth->hasAccess('location.districts.index')
                    );
                });
                $item->item(trans('location::wards.title.wards'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.location.ward.create');
                    $item->route('admin.location.ward.index');
                    $item->authorize(
                        $this->auth->hasAccess('location.wards.index')
                    );
                });
// append



            });
        });

        return $menu;
    }
}
