<?php

return [
    'location.provinces' => [
        'index' => 'location::provinces.list resource',
        'create' => 'location::provinces.create resource',
        'edit' => 'location::provinces.edit resource',
        'destroy' => 'location::provinces.destroy resource',
    ],
    'location.districts' => [
        'index' => 'location::districts.list resource',
        'create' => 'location::districts.create resource',
        'edit' => 'location::districts.edit resource',
        'destroy' => 'location::districts.destroy resource',
    ],
    'location.wards' => [
        'index' => 'location::wards.list resource',
        'create' => 'location::wards.create resource',
        'edit' => 'location::wards.edit resource',
        'destroy' => 'location::wards.destroy resource',
    ],
// append



];
