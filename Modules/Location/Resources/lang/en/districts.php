<?php

return [
    'list resource' => 'List districts',
    'create resource' => 'Create districts',
    'edit resource' => 'Edit districts',
    'destroy resource' => 'Destroy districts',
    'title' => [
        'districts' => 'District',
        'create district' => 'Create a district',
        'edit district' => 'Edit a district',
    ],
    'button' => [
        'create district' => 'Create a district',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'type' => 'Type',
        'location' => 'Location'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
