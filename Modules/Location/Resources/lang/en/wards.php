<?php

return [
    'list resource' => 'List wards',
    'create resource' => 'Create wards',
    'edit resource' => 'Edit wards',
    'destroy resource' => 'Destroy wards',
    'title' => [
        'wards' => 'Ward',
        'create ward' => 'Create a ward',
        'edit ward' => 'Edit a ward',
    ],
    'button' => [
        'create ward' => 'Create a ward',
    ],
    'table' => [
    ],
    'form' => [
        'name' => 'Name',
        'type' => 'Type',
        'location' => 'Location'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
