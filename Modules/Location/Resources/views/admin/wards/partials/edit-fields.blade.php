<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        @php($old = $ward->hasTranslation($lang) ? $ward->translate($lang)->name : '')
        {!! Form::label("{$lang}[name]", trans('location::wards.form.name')) !!}
        {!! Form::text("{$lang}[name]", old("{$lang}[name]",$old), ['class' => 'form-control', 'placeholder' => trans('location::wards.form.name')]) !!}
        {!! $errors->first("{$lang}.name", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.type") ? ' has-error' : '' }}'>
        @php($old = $ward->hasTranslation($lang) ? $ward->translate($lang)->type : '')
        {!! Form::label("{$lang}[type]", trans('location::wards.form.type')) !!}
        {!! Form::text("{$lang}[type]", old("{$lang}[type]",$old), ['class' => 'form-control', 'placeholder' => trans('location::wards.form.type')]) !!}
        {!! $errors->first("{$lang}.type", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.location") ? ' has-error' : '' }}'>
        @php($old = $ward->hasTranslation($lang) ? $ward->translate($lang)->location : '')
        {!! Form::label("{$lang}[location]", trans('location::wards.form.location')) !!}
        {!! Form::text("{$lang}[location]", old("{$lang}[location]",$old), ['class' => 'form-control', 'placeholder' => trans('location::wards.form.location')]) !!}
        {!! $errors->first("{$lang}.location", '<span class="help-block">:message</span>') !!}
    </div>
</div>
