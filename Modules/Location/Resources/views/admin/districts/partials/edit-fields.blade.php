<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        @php($old = $district->hasTranslation($lang) ? $district->translate($lang)->name : '')
        {!! Form::label("{$lang}[name]", trans('location::districts.form.name')) !!}
        {!! Form::text("{$lang}[name]", old("{$lang}[name]",$old), ['class' => 'form-control', 'placeholder' => trans('location::districts.form.name')]) !!}
        {!! $errors->first("{$lang}.name", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.type") ? ' has-error' : '' }}'>
        @php($old = $district->hasTranslation($lang) ? $district->translate($lang)->type : '')
        {!! Form::label("{$lang}[type]", trans('location::districts.form.type')) !!}
        {!! Form::text("{$lang}[type]", old("{$lang}[type]",$old), ['class' => 'form-control', 'placeholder' => trans('location::districts.form.type')]) !!}
        {!! $errors->first("{$lang}.type", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.location") ? ' has-error' : '' }}'>
        @php($old = $district->hasTranslation($lang) ? $district->translate($lang)->location : '')
        {!! Form::label("{$lang}[location]", trans('location::districts.form.location')) !!}
        {!! Form::text("{$lang}[location]", old("{$lang}[location]",$old), ['class' => 'form-control', 'placeholder' => trans('location::districts.form.location')]) !!}
        {!! $errors->first("{$lang}.location", '<span class="help-block">:message</span>') !!}
    </div>
</div>
