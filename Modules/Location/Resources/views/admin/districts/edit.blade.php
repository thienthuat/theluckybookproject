@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('location::districts.title.edit district') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li>
            <a href="{{ route('admin.location.district.index') }}">{{ trans('location::districts.title.districts') }}</a>
        </li>
        <li class="active">{{ trans('location::districts.title.edit district') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.location.district.update', $district->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('location::admin.districts.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat"
                           href="{{ route('admin.location.district.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Province</h3>
                </div>
                <div class="box-body">
                    <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                        <label for="province_id">Province</label>
                        <select name="province_id" id="province_id" class="form-control select2">
                            <option value="">--Choose Province--</option>
                            @foreach($provinces as $province)
                                <option value="{{$province->id}}" {{$district->province_id == $province->id ?'selected':''}}>{{$province->name}}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('province_id', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('location::districts.form.price_ship')}}</h3>
                </div>
                <div class="box-body">
                    <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                        <label for="price_ship">{{trans('location::districts.form.price_ship')}}</label>
                        <input type="number" name="price_ship" class="form-control" id="price_ship" value="{{old('price_ship',$district->price_ship)}}">
                        {!! $errors->first('price_ship', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.location.district.index') ?>"}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
