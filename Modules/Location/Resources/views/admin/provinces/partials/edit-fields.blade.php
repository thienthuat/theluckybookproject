<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        @php($old = $province->hasTranslation($lang) ? $province->translate($lang)->name : '')
        {!! Form::label("{$lang}[name]", trans('location::provinces.form.name')) !!}
        {!! Form::text("{$lang}[name]", old("{$lang}[name]",$old), ['class' => 'form-control', 'placeholder' => trans('location::provinces.form.name')]) !!}
        {!! $errors->first("{$lang}.name", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.type") ? ' has-error' : '' }}'>
        @php($old = $province->hasTranslation($lang) ? $province->translate($lang)->type : '')
        {!! Form::label("{$lang}[type]", trans('location::provinces.form.type')) !!}
        {!! Form::text("{$lang}[type]", old("{$lang}[type]",$old), ['class' => 'form-control', 'placeholder' => trans('location::provinces.form.type')]) !!}
        {!! $errors->first("{$lang}.type", '<span class="help-block">:message</span>') !!}
    </div>
</div>
