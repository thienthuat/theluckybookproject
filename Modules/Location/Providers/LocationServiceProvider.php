<?php

namespace Modules\Location\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Location\Events\Handlers\RegisterLocationSidebar;

class LocationServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterLocationSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('location', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Location\Repositories\ProvinceRepository',
            function () {
                $repository = new \Modules\Location\Repositories\Eloquent\EloquentProvinceRepository(new \Modules\Location\Entities\Province());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Location\Repositories\Cache\CacheProvinceDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Location\Repositories\DistrictRepository',
            function () {
                $repository = new \Modules\Location\Repositories\Eloquent\EloquentDistrictRepository(new \Modules\Location\Entities\District());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Location\Repositories\Cache\CacheDistrictDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Location\Repositories\WardRepository',
            function () {
                $repository = new \Modules\Location\Repositories\Eloquent\EloquentWardRepository(new \Modules\Location\Entities\Ward());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Location\Repositories\Cache\CacheWardDecorator($repository);
            }
        );
// add bindings



    }
}
