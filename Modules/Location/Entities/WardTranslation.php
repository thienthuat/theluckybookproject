<?php

namespace Modules\Location\Entities;

use Illuminate\Database\Eloquent\Model;

class WardTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name','type','location'];
    protected $table = 'location__ward_translations';
}
