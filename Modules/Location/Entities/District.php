<?php

namespace Modules\Location\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use Translatable;

    protected $table = 'location__districts';
    public $translatedAttributes = ['name','type','location'];
    protected $fillable = ['id','province_id','name','type','location','price_ship'];
}
