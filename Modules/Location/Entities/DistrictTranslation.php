<?php

namespace Modules\Location\Entities;

use Illuminate\Database\Eloquent\Model;

class DistrictTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name','type','location'];
    protected $table = 'location__district_translations';
}
