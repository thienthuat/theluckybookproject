<?php

namespace Modules\Location\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    use Translatable;

    protected $table = 'location__wards';
    public $translatedAttributes = ['name','type','location'];
    protected $fillable = ['district_id','name','type','location'];
}
