<?php

namespace Modules\Location\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use Translatable;

    protected $table = 'location__provinces';
    public $translatedAttributes = ['name', 'type'];
    protected $fillable = ['id', 'name', 'type'];
}
