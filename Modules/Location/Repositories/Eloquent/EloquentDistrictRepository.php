<?php

namespace Modules\Location\Repositories\Eloquent;

use Modules\Location\Repositories\DistrictRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDistrictRepository extends EloquentBaseRepository implements DistrictRepository
{
    public function createMany($datas)
    {
        foreach ($datas as $data) {
            $this->model->create($data);
        }
    }

    public function getDataByIdProvince($id)
    {
        return $this->model->where('province_id', $id)->get();
    }

    public function model()
    {
        return $this->model;
    }
}
