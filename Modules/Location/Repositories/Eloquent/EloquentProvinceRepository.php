<?php

namespace Modules\Location\Repositories\Eloquent;

use Modules\Location\Repositories\ProvinceRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProvinceRepository extends EloquentBaseRepository implements ProvinceRepository
{
    public function createMany($datas){
        foreach ($datas as $data){
            $this->model->create($data);
        }
    }
}
