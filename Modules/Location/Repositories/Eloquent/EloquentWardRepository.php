<?php

namespace Modules\Location\Repositories\Eloquent;

use Modules\Location\Repositories\WardRepository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentWardRepository extends EloquentBaseRepository implements WardRepository
{
    public function createMany($datas)
    {
        foreach ($datas as $data) {
            $this->model->create($data);
        }
    }

    public function model()
    {
        return $this->model;
    }
    public function getDataByIdDistrict($id){
        return $this->model->where('district_id',$id)->get();
    }
    public function getData($take, $skip, $search)
    {
        $data = $this->model->whereHas('translations', function (Builder $q) use ($search) {
            if ($search != '') {
                $q->where('id', '=', $search)->orWhere('name', 'like', '%' . $search . '%')->orWhere('type', 'like', '%' . $search . '%')->orWhere('location', 'like', '%' . $search . '%');
            }
        })->take($take)->skip($skip)->orderBy('id','DESC')->get();

        $arr = [];
        foreach ($data as $key => $item) {
            $arr[$key][] = $item->id;
            $arr[$key][] = $item->name;
            $arr[$key][] = $item->type;
            $arr[$key][] = $item->location;
            $arr[$key][] = '<div class="btn-group">
                                        <a href="' . route('admin.location.ward.edit', $item->id) . '" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="' . route('admin.location.ward.destroy', $item->id) . '"><i class="fa fa-trash"></i></button>
                                    </div>';
        }
        return $arr;
    }
}
