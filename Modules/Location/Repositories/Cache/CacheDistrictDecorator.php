<?php

namespace Modules\Location\Repositories\Cache;

use Modules\Location\Repositories\DistrictRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDistrictDecorator extends BaseCacheDecorator implements DistrictRepository
{
    public function __construct(DistrictRepository $district)
    {
        parent::__construct();
        $this->entityName = 'location.districts';
        $this->repository = $district;
    }
}
