<?php

namespace Modules\Location\Repositories\Cache;

use Modules\Location\Repositories\WardRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheWardDecorator extends BaseCacheDecorator implements WardRepository
{
    public function __construct(WardRepository $ward)
    {
        parent::__construct();
        $this->entityName = 'location.wards';
        $this->repository = $ward;
    }
}
