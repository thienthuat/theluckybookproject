<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class Breadcrumb extends Model
{
    protected $table = 'product__breadcrumbs';
    protected $fillable = [
        'product_id',
        'category_id'
    ];
}
