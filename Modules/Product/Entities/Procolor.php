<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Procolor extends Model
{
    protected $table = 'product__procolors';
    protected $fillable = ['product_id','color_id'];
}
