<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Category extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'product__categories';
    public $translatedAttributes = ['title', 'slug', 'description', 'meta_title', 'meta_keyword', 'meta_description'];
    protected $fillable = ['parent_id', 'is_home', 'title', 'slug', 'description', 'status', 'meta_title', 'meta_keyword', 'meta_description', 'is_menu', 'order'];

    public function product_home()
    {
        return $this->hasMany(Product::class, 'category_id')->where('product__products.is_home', '=', 1);
    }

    public function product_home_hot()
    {
        return $this->hasMany(Product::class, 'category_id')->where('product__products.is_hot', '=', 1);
    }
}
