<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Prosize;

class Size extends Model
{
    protected $table = 'product__sizes';
    protected $fillable = ['title'];

    public function scopegetPriceById($query, $id_product, $size_id)
    {
        $data = $query->select('product__prosizes.price','product__prosizes.order')->join('product__prosizes', 'product__sizes.id', '=', 'product__prosizes.size_id')
            ->where('product__prosizes.size_id', $size_id)
            ->where('product__prosizes.product_id', $id_product)
            ->first()->toArray();
        return $data;
    }
}
