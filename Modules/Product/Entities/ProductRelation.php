<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductRelation extends Model
{

    protected $table = 'product__relations';

    protected $fillable = ['product_id', 'product_relation_id'];
}
