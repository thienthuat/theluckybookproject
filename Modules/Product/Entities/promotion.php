<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class promotion extends Model
{
    protected $table = 'product__promotions';
    protected $fillable = ['code', 'start_date', 'end_date', 'percent_up'];
}
