<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{

    protected $table = 'product__colors';
    protected $fillable = ['title','code'];
}
