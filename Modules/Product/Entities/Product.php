<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Tag\Contracts\TaggableInterface;
use Modules\Tag\Traits\TaggableTrait;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model implements TaggableInterface
{
    use Translatable, TaggableTrait, NamespacedEntity, MediaRelation;

    protected $table = 'product__products';
    public $translatedAttributes = ['title', 'slug', 'SKU', 'sumary', 'description', 'quanlity', 'price', 'price_sale', 'meta_title', 'meta_keyword', 'meta_description'];
    protected $fillable = ['is_home', 'is_new', 'view', 'is_hot', 'check_excluded', 'is_selling', 'is_sale', 'status', 'title', 'slug', 'SKU', 'sumary', 'description', 'quanlity', 'price', 'price_sale', 'meta_title', 'meta_keyword', 'meta_description', 'percent_sale'];

    protected static $entityNamespace = 'asgardcms/product';

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'product__prosizes', 'product_id', 'size_id')->withPivot('order', 'price')->withTimestamps();
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class, 'product__procolors', 'product_id', 'color_id')->withTimestamps();
    }

    public function getThumbnailAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'avatar_product')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function productCate()
    {
        return $this->belongsToMany(Category::class, 'product__productcates', 'product_id', 'category_id')->withTimestamps();
    }

    public function breadcrumbs()
    {
        return $this->belongsToMany(Category::class, 'product__breadcrumbs', 'product_id', 'category_id')->withTimestamps();
    }

    public function productCateID()
    {
        return $this->productCate->pluck('id')->toArray();
    }

    public function productColorID()
    {
        return $this->colors->pluck('id')->toArray();
    }

    public function productSizeID()
    {
        return $this->sizes->pluck('id')->toArray();
    }

    public function getMultipleThumbnailAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'multiple_images_product')->get();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }

    public function productRelations()
    {
        return $this->belongsToMany(Product::class, 'product__relations', 'product_id', 'product_relation_id');
    }
}
