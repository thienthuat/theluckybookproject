<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Prosize extends Model
{
    protected $table = 'product__prosizes';
    protected $fillable = ['size_id', 'product_id', 'price', 'order'];
}
