<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Productcate extends Model
{
    protected $table = 'product__productcates';
    protected $fillable = ['product_id','category_id'];
}
