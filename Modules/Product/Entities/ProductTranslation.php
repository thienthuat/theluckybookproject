<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'SKU', 'sumary', 'description', 'quanlity', 'price', 'price_sale', 'meta_title', 'meta_keyword', 'meta_description'];
    protected $table = 'product__product_translations';
}
