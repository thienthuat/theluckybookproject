<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCheckMenuCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product__categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->tinyInteger('is_menu')->after('is_home')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product__categories', function (Blueprint $table) {
            $table->dropColumn('is_menu');
        });
    }
}
