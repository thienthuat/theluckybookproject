<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ColorRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentColorRepository extends EloquentBaseRepository implements ColorRepository
{
    public function getColorGroupBy($arr_product_id)
    {
        return $this->model->select('code', 'id')->whereIn('product_id', $arr_product_id)->groupBy('code')->get()->toArray();
    }
}
