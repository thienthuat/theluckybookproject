<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\BreadcrumbRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBreadcrumbRepository extends EloquentBaseRepository implements BreadcrumbRepository
{
}
