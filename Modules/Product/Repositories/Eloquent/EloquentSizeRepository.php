<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\SizeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSizeRepository extends EloquentBaseRepository implements SizeRepository
{
    public function getSizeGroupBy($arr_product_id)
    {
        return $this->model->select('title', 'id')->whereIn('product_id', $arr_product_id)->groupBy('title')->get()->toArray();
    }
}
