<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ProductcateRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProductcateRepository extends EloquentBaseRepository implements ProductcateRepository
{
    public function getProductIdByArrCate($cate = [])
    {
        if (count($cate) > 0) {
            $cates = $this->model->select('product_id')->whereIn('category_id', $cate)->groupBy('product_id')->get()->toArray();
        } else {
            $cates = $this->model->select('product_id')->groupBy('product_id')->get()->toArray();
        }
        return array_column($cates, 'product_id');
    }
}
