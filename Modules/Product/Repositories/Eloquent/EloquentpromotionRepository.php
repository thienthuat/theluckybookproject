<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\promotionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentpromotionRepository extends EloquentBaseRepository implements promotionRepository
{
}
