<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

use Modules\Product\Events\CategoryWasCreated;
use Modules\Product\Events\CategoryWasUpdated;
use Modules\Product\Events\CategoryWasDeleted;

use Illuminate\Database\Eloquent\Builder;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
    /**
     * Create a Category
     * @param  array $data
     * @return mixed
     */
    public function create($data)
    {
        $Category = $this->model->create($data);

        event(new CategoryWasCreated($Category, $data));

        return $Category;
    }

    /**
     * Update a resource
     * @param $Category
     * @param  array $data
     * @return mixed
     */
    public function update($Category, $data)
    {
        $Category->update($data);

        event(new CategoryWasUpdated($Category, $data));

        return $Category;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function destroy($model)
    {
        event(new CategoryWasDeleted($model->id, get_class($model)));

        return $model->delete();
    }

    /**
     * Find a resource by the given slug
     *
     * @param  string $slug
     * @return object
     */
    public function findBySlug($slug)
    {
        return $this->model->whereHas('translations', function (Builder $q) use ($slug) {
            $q->where('slug', $slug);
        })->with('translations')->firstOrFail();
    }

    public function getCategoryIsHome()
    {
        return $this->model->where('is_home', 1)->get();
    }

    public function getCategoryByParent($parent = '')
    {
        return $this->model->where('parent_id', '=', $parent)->where('status', 1)->get();
    }

    public function getMenuCategoryParent()
    {
        return $this->model->where('is_menu', 1)->where('status', 1)->orderBy('order', 'DESC')->get();
    }

    public function model()
    {
        return $this->model;
    }
}
