<?php

namespace Modules\Product\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Product\Events\ProductIsCreating;
use Modules\Product\Events\ProductIsUpdating;
use Modules\Product\Events\ProductWasCreated;
use Modules\Product\Events\ProductWasDeleted;
use Modules\Product\Events\ProductWasUpdated;
use Modules\Product\Repositories\ProductRepository;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{

    /**
     * @param  mixed $data
     * @return object
     */
    public function create($data)
    {
        event($event = new ProductIsCreating($data));
        $Product = $this->model->create($event->getAttributes());
        event(new ProductWasCreated($Product, $data));
        $Product->setTags(array_get($data, 'tags', []));
        $Product->productCate()->sync(array_get($data, 'category_id', []));
        $Product->colors()->sync(array_get($data, 'color', []));
        $Product->sizes()->sync(array_get($data, 'price_size', []));
        return $Product;
    }

    /**
     * @param $model
     * @param  array $data
     * @return object
     */
    public function update($model, $data)
    {
        event($event = new ProductIsUpdating($model, $data));
        $model->update($event->getAttributes());

        event(new ProductWasUpdated($model, $data));

        $model->setTags(array_get($data, 'tags', []));
        $model->productCate()->sync(array_get($data, 'category_id', []));
        $model->colors()->sync(array_get($data, 'color', []));
        $model->sizes()->sync(array_get($data, 'price_size', []));
        return $model;
    }

    public function getAllProductPaginate($paginate = 9)
    {
        return $this->model->where('status', 1)->paginate($paginate);
    }

    public function destroy($Product)
    {
        $Product->untag();

        event(new ProductWasDeleted($Product->id, get_class($Product)));

        return $Product->delete();
    }

    public function getProductByAction($action = 'is_new', $amount = '5')
    {
        return $this->model->where($action, 1)->where('status', 1)->orderBy('created_at', 'desc')->take($amount)->get();
    }

    public function getProductByActionPagination($action = 'is_new', $pagination = '24')
    {
        return $this->model->where($action, 1)->where('status', 1)->orderBy('created_at', 'desc')->paginate($pagination);
    }

    public function getProductRelated($product, $arr_product_id, $amount = 10)
    {
        return $this->model->whereIn('id', $arr_product_id)
			->where('id', '!=', $product->id)
			->whereHas('translations', function (Builder $q){
                $q->where('quanlity','>', 0);
            })
			->where('status', 1)
			->take($amount)->get();
    }

    public function getProductPromotion($paginate = 9)
    {
        return $this->model->where('is_sale', 1)->orderBy('created_at', 'desc')->paginate($paginate);
    }

    public function model()
    {
        return $this->model;
    }

    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                $q->where('slug', $slug);
                $q->where('locale', $locale);
            })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }

    public function getProductByCateFilter($arr_product_id, $price_to, $price_from, $color, $size, $pagination)
    {
        $products = $this->model
			->join('product__product_translations as product_translations', 'product_translations.product_id', '=', 'product__products.id')
			->where('status', 1)->whereIn('product__products.id', $arr_product_id)
			->with('translations')
            ->when($price_to, function ($query) use ($price_to) {
                return $query->whereHas('translations', function (Builder $query) use ($price_to) {
                    if ($price_to != null && $price_to != '') {
                        $query->where('price', '>=', $price_to);
                    }
                });
            })->when($price_from, function ($query) use ($price_from) {
                return $query->whereHas('translations', function (Builder $query) use ($price_from) {
                    if ($price_from != null && $price_from != '') {
                        $query->where('price', '<=', $price_from);
                    }
                });
            })->when($color, function ($query) use ($color) {
                return $query->whereHas('colors', function (Builder $query) use ($color) {
                    if ($color != null && $color != '') {
                        $query->whereIn('color_id', explode('.', $color));
                    }
                });
            })->when($size, function ($query) use ($size) {
                return $query->whereHas('sizes', function (Builder $query) use ($size) {
                    if ($size != null && $size != '') {
                        $query->whereIn('size_id', explode('.', $size));
                    }
                });
            })
			->orderBy('product_translations.quanlity', 'desc')
			->orderBy('product__products.created_at', 'desc')
			->paginate($pagination);

        return $products;
    }
}
