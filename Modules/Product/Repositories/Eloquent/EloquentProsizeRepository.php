<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ProsizeRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProsizeRepository extends EloquentBaseRepository implements ProsizeRepository
{
    public function getSizeJoin($arr_product_id)
    {
        return $this->model->rightJoin('product__sizes', 'product__sizes.id', '=', 'product__prosizes.size_id')
            ->whereIn('product__prosizes.product_id', $arr_product_id)->groupBy('product__prosizes.size_id')->get();
    }
}
