<?php

namespace Modules\Product\Repositories\Eloquent;

use Modules\Product\Repositories\ProcolorRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProcolorRepository extends EloquentBaseRepository implements ProcolorRepository
{
    public function getColorJoin($arr_product_id)
    {
        return $this->model->rightJoin('product__colors', 'product__colors.id', '=', 'product__procolors.color_id')
            ->whereIn('product__procolors.product_id', $arr_product_id)->groupBy('product__procolors.color_id')->get();
    }
}
