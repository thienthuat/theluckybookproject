<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\ProductcateRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProductcateDecorator extends BaseCacheDecorator implements ProductcateRepository
{
    public function __construct(ProductcateRepository $productcate)
    {
        parent::__construct();
        $this->entityName = 'product.productcates';
        $this->repository = $productcate;
    }
}
