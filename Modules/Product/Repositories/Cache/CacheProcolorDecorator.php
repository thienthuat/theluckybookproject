<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\ProcolorRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProcolorDecorator extends BaseCacheDecorator implements ProcolorRepository
{
    public function __construct(ProcolorRepository $procolor)
    {
        parent::__construct();
        $this->entityName = 'product.procolors';
        $this->repository = $procolor;
    }
}
