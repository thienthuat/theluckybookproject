<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\promotionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachepromotionDecorator extends BaseCacheDecorator implements promotionRepository
{
    public function __construct(promotionRepository $promotion)
    {
        parent::__construct();
        $this->entityName = 'product.promotions';
        $this->repository = $promotion;
    }
}
