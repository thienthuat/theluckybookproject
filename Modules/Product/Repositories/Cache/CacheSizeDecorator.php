<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\SizeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSizeDecorator extends BaseCacheDecorator implements SizeRepository
{
    public function __construct(SizeRepository $size)
    {
        parent::__construct();
        $this->entityName = 'product.sizes';
        $this->repository = $size;
    }
}
