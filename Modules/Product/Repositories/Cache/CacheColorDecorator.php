<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\ColorRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheColorDecorator extends BaseCacheDecorator implements ColorRepository
{
    public function __construct(ColorRepository $color)
    {
        parent::__construct();
        $this->entityName = 'product.colors';
        $this->repository = $color;
    }
}
