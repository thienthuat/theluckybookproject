<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\BreadcrumbRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBreadcrumbDecorator extends BaseCacheDecorator implements BreadcrumbRepository
{
    public function __construct(BreadcrumbRepository $breadcrumb)
    {
        parent::__construct();
        $this->entityName = 'product.breadcrumbs';
        $this->repository = $breadcrumb;
    }
}
