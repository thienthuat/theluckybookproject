<?php

namespace Modules\Product\Repositories\Cache;

use Modules\Product\Repositories\ProsizeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheProsizeDecorator extends BaseCacheDecorator implements ProsizeRepository
{
    public function __construct(ProsizeRepository $prosize)
    {
        parent::__construct();
        $this->entityName = 'product.prosizes';
        $this->repository = $prosize;
    }
}
