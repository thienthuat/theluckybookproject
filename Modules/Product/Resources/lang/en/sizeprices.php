<?php

return [
    'list resource' => 'List sizeprices',
    'create resource' => 'Create sizeprices',
    'edit resource' => 'Edit sizeprices',
    'destroy resource' => 'Destroy sizeprices',
    'title' => [
        'sizeprices' => 'Sizeprice',
        'create sizeprice' => 'Create a sizeprice',
        'edit sizeprice' => 'Edit a sizeprice',
    ],
    'button' => [
        'create sizeprice' => 'Create a sizeprice',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
