<?php

return [
    'list resource' => 'List categories',
    'create resource' => 'Create categories',
    'edit resource' => 'Edit categories',
    'destroy resource' => 'Destroy categories',
    'title' => [
        'categories' => 'Category',
        'create category' => 'Create a category',
        'edit category' => 'Edit a category',
    ],
    'button' => [
        'create category' => 'Create a category',
    ],
    'table' => [
    ],
    'form' => [
        'title' => 'Title Category',
        'slug' => 'Slug Category',
        'description' => 'Description Category',
        'status' => [
            'show' => 'Show',
            'hide' => 'Hide'
        ]
    ],
    'messages' => [
        'title is required' => 'The title category is required.',
        'slug is required' => 'The slug category is required.',
    ],
    'validation' => [

    ],
];
