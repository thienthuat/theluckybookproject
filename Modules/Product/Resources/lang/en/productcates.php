<?php

return [
    'list resource' => 'List productcates',
    'create resource' => 'Create productcates',
    'edit resource' => 'Edit productcates',
    'destroy resource' => 'Destroy productcates',
    'title' => [
        'productcates' => 'Productcate',
        'create productcate' => 'Create a productcate',
        'edit productcate' => 'Edit a productcate',
    ],
    'button' => [
        'create productcate' => 'Create a productcate',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
