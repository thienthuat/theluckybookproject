<?php

return [
    'list resource' => 'List prosizes',
    'create resource' => 'Create prosizes',
    'edit resource' => 'Edit prosizes',
    'destroy resource' => 'Destroy prosizes',
    'title' => [
        'prosizes' => 'Prosize',
        'create prosize' => 'Create a prosize',
        'edit prosize' => 'Edit a prosize',
    ],
    'button' => [
        'create prosize' => 'Create a prosize',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
