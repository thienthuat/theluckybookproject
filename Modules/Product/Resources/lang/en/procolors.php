<?php

return [
    'list resource' => 'List procolors',
    'create resource' => 'Create procolors',
    'edit resource' => 'Edit procolors',
    'destroy resource' => 'Destroy procolors',
    'title' => [
        'procolors' => 'Procolor',
        'create procolor' => 'Create a procolor',
        'edit procolor' => 'Edit a procolor',
    ],
    'button' => [
        'create procolor' => 'Create a procolor',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
