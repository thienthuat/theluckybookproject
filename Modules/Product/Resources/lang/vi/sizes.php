<?php

return [
    'list resource' => 'List sizes',
    'create resource' => 'Create sizes',
    'edit resource' => 'Edit sizes',
    'destroy resource' => 'Destroy sizes',
    'title' => [
        'sizes' => 'Size',
        'create size' => 'Create a size',
        'edit size' => 'Edit a size',
    ],
    'button' => [
        'create size' => 'Create a size',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
