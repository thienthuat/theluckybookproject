<?php

return [
    'list resource' => 'List products',
    'create resource' => 'Create products',
    'edit resource' => 'Edit products',
    'destroy resource' => 'Destroy products',
    'title' => [
        'products' => 'Product',
        'create product' => 'Create a product',
        'edit product' => 'Edit a product',
    ],
    'button' => [
        'create product' => 'Create a product',
    ],
    'table' => [
    ],
    'form' => [
        'title' => 'Title Product',
        'slug' => 'Slug Product',
        'sumary' => 'Sumary Product',
        'description' => 'Description Product',
        'price' => 'Price',
        'price_sale' => 'Price Sale',
        'SKU' => 'SKU Product',
        'color_code' => 'Color Code Product',
        'color_title' => 'Color Title Product',
        'size_title' => 'Size Title Product',
        'size_price' => 'Size Price Product',
        'SKU' => 'SKU Product',
        'quanlity' => 'Quanlity',
        'status' => [
            'show' => 'Show',
            'hide' => 'Hide'
        ]
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
