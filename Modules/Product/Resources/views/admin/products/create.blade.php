@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::products.title.create product') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.product.product.index') }}">{{ trans('product::products.title.products') }}</a>
        </li>
        <li class="active">{{ trans('product::products.title.create product') }}</li>
    </ol>
@stop

@section('content')
    <style>
        .jsThumbnailImageWrapper img {
            width: 100%;
        }
        .box-price-size {
            display: flex;
            justify-content: flex-start;
        }
        .box-price-size input {
            padding: 5px;
        }
        .box-price-size input:last-child { width: 100px; }
    </style>
    {!! Form::open(['route' => ['admin.product.product.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('product::admin.products.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                    <div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class='form-group'>
									{!! Form::label("view", 'View number') !!}
									{!! Form::number("view", old("view"), ['class' => "form-control"]) !!}
								</div>
							</div>
							<div class="col-md-6">
								<div class='form-group'>
									{!! Form::label("percent_sale", 'Percent Sale') !!}
									{!! Form::text("percent_sale", old("percent_sale"), ['class' => "form-control"]) !!}
								</div>
							</div>
						</div>
                        <div class="form-group">
                            {!! Form::label("status", 'Status') !!}
                            <div class="checkbox">
                                <label for="status_0">
                                    <input id="status_0"
                                           name="status"
                                           type="radio"
                                           class="flat-blue"
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="status_1">
                                    <input id="status_1"
                                           name="status"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("IsHome", 'Is Home') !!}
                            <div class="checkbox">
                                <label for="IsHome_0">
                                    <input id="IsHome_0"
                                           name="is_home"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="IsHome_1">
                                    <input id="IsHome_1"
                                           name="is_home"
                                           type="radio"
                                           class="flat-blue"
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("IsNew", 'Is New') !!}
                            <div class="checkbox">
                                <label for="IsNew_0">
                                    <input id="IsNew_0"
                                           name="is_new"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="IsNew_1">
                                    <input id="IsNew_1"
                                           name="is_new"
                                           type="radio"
                                           class="flat-blue"
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("IsHot", 'Is Hot') !!}
                            <div class="checkbox">
                                <label for="IsHot_0">
                                    <input id="IsHot_0"
                                           name="is_hot"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="IsHot_1">
                                    <input id="IsHot_1"
                                           name="is_hot"
                                           type="radio"
                                           class="flat-blue"
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("IsSale", 'Is Sale') !!}
                            <div class="checkbox">
                                <label for="IsSale_0">
                                    <input id="IsSale_0"
                                           name="is_sale"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="IsSale_1">
                                    <input id="IsSale_1"
                                           name="is_sale"
                                           type="radio"
                                           class="flat-blue"
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("IsSelling", 'Is Selling') !!}
                            <div class="checkbox">
                                <label for="is_selling_0">
                                    <input id="is_selling_0"
                                           name="is_selling"
                                           type="radio"
                                           class="flat-blue"
                                           checked
                                           value="0"/>
                                    {{ trans('product::categories.form.status.hide') }}
                                </label>
                                <label for="is_selling_1">
                                    <input id="is_selling_1"
                                           name="is_selling"
                                           type="radio"
                                           class="flat-blue"
                                           value="1"/>
                                    {{ trans('product::categories.form.status.show') }}
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="check_excluded">
                                    <input type="hidden" name="check_excluded" value="0">
                                    <input id="check_excluded" name="check_excluded" type="checkbox" class="flat-blue" value="1" />
                                        EXCLUDED FROM PROMOTIONS
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat"
                           href="{{ route('admin.product.product.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Category</h3>
                </div>
                <div class="box-body">
                    <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                        @foreach($categories as $category)
                            @if($category['level'] == 1)
                                @php($sys = '')
                            @else
                                @php($sys = str_repeat('-', ($category['level']-1)*2))
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    {{$sys}} <label for="category_id{{$category['id']}}">
                                        <input id="category_id{{$category['id']}}"
                                               name="category_id[]"
                                               type="checkbox"
                                               class="flat-blue"
                                               {{ (is_array(old('category_id')) && in_array($category['id'], old('category_id'))) ? ' checked' : '' }}
                                               value="{{$category['id']}}"/>
                                        {{ $category['title'] }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Product Relation</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <select name="prodcut_relation_id[]" id="prodcut_relation_id" class="form-control select2" multiple>
                            <option value=""></option>
                            @foreach ($products as $product)
                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Breadcrumb Product</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <select name="breadcrumb_id[]" id="breadcrumb_id" class="form-control select2" multiple>
                            <option value=""></option>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    @tags('asgardcms/product')
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    @mediaSingle('avatar_product')
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    @mediaMultiple('multiple_images_product')
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <div id="modalColor"></div>
    <div id="modalSize"></div>
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('.colorpicker').colorpicker()
        $(document).keypressAction({
            actions: [
                {key: 'b', route: "<?= route('admin.product.product.index') ?>"}
            ]
        });
    });
</script>
<script>
    $(document).ready(function () {

        $(".check-input-size").click(function () {

            var id = $(this).val()
            var html = '<input type="number" name="price_size[' + id + '][price]" class="form-controll" required placeholder="Enter Price">';
            html += '<input type="number" name="price_size[' + id + '][order]" class="form-controll" required placeholder="Enter Order">';
            if ($(this).is(':checked')) {
                $(this).parents('.box-content-size').find('.box-price-size').html(html);
            } else {
                $(this).parents('.box-content-size').find('.box-price-size').html('');
            }
        });
        $('#btn_add_color').click(function () {
            var url = $(this).attr('data-url')
            $.ajax({
                url: url, success: function (result) {
                    $("#modalColor").html(result.view);
                    $("#modalAddColor").modal('show');
                }
            });
        })
        $('#btn_add_size').click(function () {
            var url = $(this).attr('data-url')
            $.ajax({
                url: url, success: function (result) {
                    $("#modalSize").html(result.view);
                    $("#modalAddSize").modal('show');
                }
            });
        })
        $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });
    });
</script>
@endpush
