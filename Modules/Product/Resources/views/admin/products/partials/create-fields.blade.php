<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[title]", trans('product::products.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}[title]"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('product::products.form.title')]) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[slug]", trans('product::products.form.slug')) !!}
        {!! Form::text("{$lang}[slug]", old("{$lang}[slug]"), ['class' => 'form-control slug', 'data-slug' => 'target', 'placeholder' => trans('product::products.form.slug')]) !!}
        {!! $errors->first("{$lang}.slug", '<span class="help-block">:message</span>') !!}
    </div>
    @editor('sumary', trans('product::products.form.sumary'), old("{$lang}.sumary"), $lang)
    <div class="row">
        <div class="col-md-6">
            <div class='form-group{{ $errors->has("{$lang}.price") ? ' has-error' : '' }}'>
                {!! Form::label("{$lang}[price]", trans('product::products.form.price')) !!}
                {!! Form::number("{$lang}[price]", old("$lang.price"), ['class' => "form-control", 'placeholder' => trans('product::products.form.price')]) !!}
                {!! $errors->first("{$lang}.price", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class='form-group{{ $errors->has("{$lang}[price_sale]") ? ' has-error' : '' }}'>
                {!! Form::label("{$lang}[price_sale]", trans('product::products.form.price_sale')) !!}
                {!! Form::number("{$lang}[price_sale]", old("$lang.price_sale"), ['class' => "form-control", 'placeholder' => trans('product::products.form.price_sale')]) !!}
                {!! $errors->first("{$lang}[price_sale]", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class='form-group{{ $errors->has("{$lang}.SKU") ? ' has-error' : '' }}'>
                {!! Form::label("{$lang}[SKU]", trans('product::products.form.SKU')) !!}
                {!! Form::text("{$lang}[SKU]", old("$lang.SKU"), ['class' => "form-control", 'placeholder' => trans('product::products.form.SKU')]) !!}
                {!! $errors->first("{$lang}.SKU", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class='form-group{{ $errors->has("{$lang}.quanlity") ? ' has-error' : '' }}'>
                {!! Form::label("{$lang}[quanlity]", trans('product::products.form.quanlity')) !!}
                {!! Form::number("{$lang}[quanlity]", old("$lang.quanlity"), ['class' => "form-control", 'placeholder' => trans('product::products.form.quanlity')]) !!}
                {!! $errors->first("{$lang}.quanlity", '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
    @editor('description', trans('product::products.form.description'), old("{$lang}.description"), $lang)
    <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo-{{$lang}}">
                        {{ trans('page::pages.form.meta_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseTwo-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("{$lang}.meta_title") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[meta_title]", trans('page::pages.form.meta_title')) !!}
                        {!! Form::text("{$lang}[meta_title]", old("$lang.meta_title"), ['class' => "form-control", 'placeholder' => trans('page::pages.form.meta_title')]) !!}
                        {!! $errors->first("{$lang}.meta_title", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}.meta_keyword") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[meta_keyword]", trans('page::pages.form.meta_keyword')) !!}
                        {!! Form::text("{$lang}[meta_keyword]", old("$lang.meta_keyword"), ['class' => "form-control","data-role"=>"tagsinput" ,'placeholder' => trans('page::pages.form.meta_keyword')]) !!}
                        {!! $errors->first("{$lang}.meta_keyword", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}.meta_description") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[meta_description]", trans('page::pages.form.meta_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[meta_description]" rows="10"
                                  cols="80">{{ old("$lang.meta_description") }}</textarea>
                        {!! $errors->first("{$lang}.meta_description", '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
