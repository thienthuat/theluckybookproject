@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::products.title.products') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('product::products.title.products') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.product.product.create') }}" class="btn btn-primary btn-flat"
                       style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('product::products.button.create product') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Avatar</th>
                                <th>Is Home</th>
                                <th>Is Hot</th>
                                <th>Is Sale</th>
                                <th>Is Selling</th>
                                <th>Is New</th>
                                <th>Qty</th>
                                <th>Status</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($products)): ?>
                            <?php foreach ($products as $product): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.product.product.edit', [$product->id]) }}">
                                        {{ $product->id }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.product.product.edit', [$product->id]) }}">
                                        {{ $product->title }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.product.product.edit', [$product->id]) }}">
                                        @php($image= $product->getThumbnailAttribute()!=""?$product->getThumbnailAttribute()->path:"")
                                        <img src="{{url($image)}}" width="100px"/>
                                    </a>
                                </td>
                                <td>
                                    @if($product->is_home == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td><td>
                                    @if($product->is_hot == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->is_sale == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->is_selling == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->is_new == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->quanlity <= 0)
                                        <span class="label label-danger">Out of stock</span>
                                    @else
                                        <span class="label label-success">Available</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->status == 0)
                                        <span class="label label-danger">Hide</span>
                                    @else
                                        <span class="label label-success">Show</span>
                                    @endif
                                </td>

                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.product.product.edit', [$product->id]) }}"
                                           class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal"
                                                data-target="#modal-delete-confirmation"
                                                data-action-target="{{ route('admin.product.product.destroy', [$product->id]) }}">
                                            <i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Avatar</th>
                                <th>Is Home</th>
                                <th>Is Hot</th>
                                <th>Is Sale</th>
                                <th>Is Selling</th>
                                <th>Is New</th>
                                <th>Qty</th>
                                <th>Status</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('product::products.title.create product') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).keypressAction({
                actions: [
                    {key: 'c', route: "<?= route('admin.product.product.create') ?>"}
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[0, "desc"]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
