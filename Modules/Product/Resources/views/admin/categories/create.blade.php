@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::categories.title.create category') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i
                        class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li>
            <a href="{{ route('admin.product.category.index') }}">{{ trans('product::categories.title.categories') }}</a>
        </li>
        <li class="active">{{ trans('product::categories.title.create category') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.product.category.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('product::admin.categories.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                    <div class="box-footer">
                        <button type="submit"
                                class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.product.category.index')}}"><i
                                    class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label for="parent_id">{{trans('product::categories.title.categories')}}</label>
                        <select name="parent_id" id="parent_id" class="form-control select2">
                            <option value="0">Parent</option>
                            @foreach($menus as $menu)
                                @if($menu['level'] == 1)
                                    <option value="{{$menu['id']}}">- {{$menu['title']}}</option>
                                @else
                                    @php($name = str_repeat('-', ($menu['level']-1) * 1) . '-' . $menu['title']);
                                    <option value="{{$menu['id']}}">{{$name}}</option>

                                @endif
                            @endforeach
                        </select>
                    </div>
                    <hr>
                    <div class='form-group'>
                        {!! Form::label("order", 'Order') !!}
                        {!! Form::text("order", old("order",$order+1), ['class' => "form-control"]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label("status", 'Status') !!}
                        <div class="checkbox">
                            <label for="status_0">
                                <input id="status_0"
                                       name="status"
                                       type="radio"
                                       class="flat-blue"
                                       value="0"/>
                                {{ trans('product::categories.form.status.hide') }}
                            </label>
                            <label for="status_1">
                                <input id="status_1"
                                       name="status"
                                       type="radio"
                                       class="flat-blue"
                                       checked
                                       value="1"/>
                                {{ trans('product::categories.form.status.show') }}
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("is_menu", 'is Menu') !!}
                        <div class="checkbox">
                            <label for="is_menu_0">
                                <input id="is_menu_0"
                                       name="is_menu"
                                       type="radio"
                                       class="flat-blue"
                                       value="0"/>
                                {{ trans('product::categories.form.status.hide') }}
                            </label>
                            <label for="is_menu_1">
                                <input id="is_menu_1"
                                       name="is_menu"
                                       type="radio"
                                       class="flat-blue"
                                       checked
                                       value="1"/>
                                {{ trans('product::categories.form.status.show') }}
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("is_home", 'Is Home') !!}
                        <div class="checkbox">
                            <label for="is_home_0">
                                <input id="is_home_0"
                                       name="is_home"
                                       type="radio"
                                       class="flat-blue"
                                       checked
                                       value="0"/>
                                {{ trans('product::categories.form.status.hide') }}
                            </label>
                            <label for="is_home_1">
                                <input id="is_home_1"
                                       name="is_home"
                                       type="radio"
                                       class="flat-blue"
                                       value="1"/>
                                {{ trans('product::categories.form.status.show') }}
                            </label>
                        </div>
                    </div>
                    @mediaSingle('banner_category_product')
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();
            $(document).keypressAction({
                actions: [
                    {key: 'b', route: "<?= route('admin.product.category.index') ?>"}
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
