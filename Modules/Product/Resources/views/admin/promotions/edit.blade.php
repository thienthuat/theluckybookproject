@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::promotions.title.edit promotion') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.product.promotion.index') }}">{{ trans('product::promotions.title.promotions') }}</a></li>
        <li class="active">{{ trans('product::promotions.title.edit promotion') }}</li>
    </ol>
@stop
@push('css-stack')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@section('content')
    {!! Form::open(['route' => ['admin.product.promotion.update', $promotion->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class='form-group'>
                                {!! Form::label("code", 'Code') !!}
                                {!! Form::text("code", old("code",$promotion->code), ['class' => "form-control"]) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group'>
                                {!! Form::label("percent_up", 'Percent Discount') !!}
                                <input class="form-control" name="percent_up" type="number" value="{{old('percent_up',$promotion->percent_up)}}" min="0" max="100" id="percent_up">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class='form-group'>
                                {!! Form::label("start_date", 'Start Date') !!}
                                {!! Form::text("start_date", old("start_date",$promotion->start_date), ['class' => "form-control datepicker"]) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group'>
                                {!! Form::label("end_date", 'End Date') !!}
                                {!! Form::text("end_date", old("end_date",$promotion->end_date), ['class' => "form-control datepicker"]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.product.promotion.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        } );
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.product.promotion.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
