<div class="modal fade" id="modalAddColor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form action="{{route('admin.product.color.storeAjax')}}" method="post" id="formStoreAjax">
            {!! csrf_field() !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Color</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="title_color">Title</label>
                        <input type="text" class="form-control" id="title_color" name="title_color">
                    </div>
                    <div class="form-group">
                        <label for="code_color">Code</label>
                        <input type="text" class="form-control colorpicker" id="code_color" name="code_color">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_color_ajax">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.colorpicker').colorpicker()
        $("#save_color_ajax").click(function () {
            $.ajax({
                url: $('#formStoreAjax').attr('action'),
                type: 'post',
                data: $('#formStoreAjax').serialize(),
                success: function (result) {
                    var html = '';
                    html += '<div class="row"><div class="col-md-12"><label for="color' + result.id + '" style="display: flex; align-items: center;">';
                    html += '<input id="color' + result.id + '" name="color[]" type="checkbox" class="flat-blue" value="' + result.id + '"/>';
                    html += '<span style="display: inline-block; margin: 0 5px; height: 20px; width: 50px; background-color: ' + result.code + '"></span>';
                    html += '<span>' + result.title + '</span></label></div></div>';
                    $('.box-add-color-multi').before(html);
                    $('#modalAddColor').modal('hide');
                    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue'
                    });
                }
            });
        });
    })
</script>