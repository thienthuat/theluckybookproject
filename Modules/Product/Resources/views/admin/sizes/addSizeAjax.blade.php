<div class="modal fade" id="modalAddSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form action="{{route('admin.product.size.storeAjax')}}" method="post" id="formStoreSizeAjax">
            {!! csrf_field() !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Size</h4>
                </div>
                <div class="modal-body">
                    <div id="statusSize"></div>
                    <div class="form-group">
                        <label for="title_size">Title</label>
                        <input type="text" class="form-control" id="title_size" name="title_size">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save_size_ajax">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#save_size_ajax").click(function () {
            $.ajax({
                url: $('#formStoreSizeAjax').attr('action'),
                type: 'post',
                data: $('#formStoreSizeAjax').serialize(),
                success: function (result) {
                    if (result.status == 'error') {
                        var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert"';
                        html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                        html += '</button><span>' + result.message + '</span></div>';
                        $('#statusSize').html(html);
                    } else {
                        var html = '';
                        html += '<div class="row box-content-size" style="margin-bottom: 1rem"><div class="col-md-4"><label for="size' + result.id + '" style="display: flex; align-items: center;">';
                        html += '<input id="size' + result.id + '" name="size[]" type="checkbox" class="check-input-size" value="' + result.id + '"/>';
                        html += '<span style="margin-left: .5rem">' + result.title + '</span></label></div>';
                        html += '<div class="col-xs-6"><div class="box-price-size"></div></div></div>';
                        $('.box-add-size-multi').before(html);
                        $('#modalAddSize').modal('hide');
                        $(".check-input-size").click(function () {

                            var id = $(this).val()
                            var html = '<input type="number" name="price_size[' + id + '][price]" class="form-controll" required placeholder="Enter Price">';
                            html += '<input type="number" name="price_size[' + id + '][order]" class="form-controll" required placeholder="Enter Order">';
                            if ($(this).is(':checked')) {
                                $(this).parents('.box-content-size').find('.box-price-size').html(html);
                            } else {
                                $(this).parents('.box-content-size').find('.box-price-size').html('');
                            }
                        });
                    }
                }
            });
        });
    })
</script>