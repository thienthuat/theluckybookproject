<?php

namespace Modules\Product\Events;

use Modules\Media\Contracts\DeletingMedia;

class CategoryWasDeleted implements DeletingMedia
{
    /**
     * @var string
     */
    private $CategoryClass;
    /**
     * @var int
     */
    private $CategoryId;

    public function __construct($CategoryId, $CategoryClass)
    {
        $this->CategoryClass = $CategoryClass;
        $this->CategoryId = $CategoryId;
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->CategoryId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return $this->CategoryClass;
    }
}