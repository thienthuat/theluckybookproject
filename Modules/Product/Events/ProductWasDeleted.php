<?php

namespace Modules\Product\Events;

use Modules\Media\Contracts\DeletingMedia;

class ProductWasDeleted implements DeletingMedia
{
    /**
     * @var string
     */
    private $ProductClass;
    /**
     * @var int
     */
    private $ProductId;

    public function __construct($ProductId, $ProductClass)
    {
        $this->ProductClass = $ProductClass;
        $this->ProductId = $ProductId;
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->ProductId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return $this->ProductClass;
    }
}