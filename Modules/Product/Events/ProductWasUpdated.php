<?php

namespace Modules\Product\Events;

use Modules\Product\Entities\Product;
use Modules\Media\Contracts\StoringMedia;

class ProductWasUpdated implements StoringMedia
{
    /**
     * @var array
     */
    public $data;
    /**
     * @var Product
     */
    public $Product;

    public function __construct(Product $Product, array $data)
    {
        $this->data = $data;
        $this->Product = $Product;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity()
    {
        return $this->Product;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData()
    {
        return $this->data;
    }
}