<?php

namespace Modules\Product\Events;

use Modules\Core\Contracts\EntityIsChanging;
use Modules\Core\Events\AbstractEntityHook;
use Modules\Product\Entities\Product;

class ProductIsUpdating extends AbstractEntityHook implements EntityIsChanging
{
    /**
     * @var Product
     */
    private $Product;

    public function __construct(Product $Product, array $attributes)
    {
        $this->Product = $Product;
        parent::__construct($attributes);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->Product;
    }
}
