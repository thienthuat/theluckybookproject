<?php

return [
    'product.categories' => [
        'index' => 'product::categories.list resource',
        'create' => 'product::categories.create resource',
        'edit' => 'product::categories.edit resource',
        'destroy' => 'product::categories.destroy resource',
    ],
    'product.products' => [
        'index' => 'product::products.list resource',
        'create' => 'product::products.create resource',
        'edit' => 'product::products.edit resource',
        'destroy' => 'product::products.destroy resource',
    ],
    'product.colors' => [
        'index' => 'product::colors.list resource',
        'create' => 'product::colors.create resource',
        'edit' => 'product::colors.edit resource',
        'destroy' => 'product::colors.destroy resource',
    ],
    'product.sizes' => [
        'index' => 'product::sizes.list resource',
        'create' => 'product::sizes.create resource',
        'edit' => 'product::sizes.edit resource',
        'destroy' => 'product::sizes.destroy resource',
    ],
    'product.productcates' => [
        'index' => 'product::productcates.list resource',
        'create' => 'product::productcates.create resource',
        'edit' => 'product::productcates.edit resource',
        'destroy' => 'product::productcates.destroy resource',
    ],
    'product.procolors' => [
        'index' => 'product::procolors.list resource',
        'create' => 'product::procolors.create resource',
        'edit' => 'product::procolors.edit resource',
        'destroy' => 'product::procolors.destroy resource',
    ],
    'product.prosizes' => [
        'index' => 'product::prosizes.list resource',
        'create' => 'product::prosizes.create resource',
        'edit' => 'product::prosizes.edit resource',
        'destroy' => 'product::prosizes.destroy resource',
    ],
    'product.promotions' => [
        'index' => 'product::promotions.list resource',
        'create' => 'product::promotions.create resource',
        'edit' => 'product::promotions.edit resource',
        'destroy' => 'product::promotions.destroy resource',
    ],
    'product.breadcrumbs' => [
        'index' => 'product::breadcrumbs.list resource',
        'create' => 'product::breadcrumbs.create resource',
        'edit' => 'product::breadcrumbs.edit resource',
        'destroy' => 'product::breadcrumbs.destroy resource',
    ],
// append










];
