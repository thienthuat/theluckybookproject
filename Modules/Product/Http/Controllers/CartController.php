<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Product\Repositories\ColorRepository;
use Modules\Product\Repositories\SizeRepository;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Location\Repositories\DistrictRepository;
use Modules\Customer\Repositories\AddressRepository;
use Modules\Shopping\Repositories\InvoiceitemRepository;
use Modules\Shopping\Repositories\InvoiceRepository;
use Modules\Page\Repositories\PageRepository;
use Modules\Shopping\Repositories\MethodRepository;
use Modules\Product\Repositories\promotionRepository;
use Auth;
use Session;
use Hash;
use Mail;
use Hashids\Hashids;
use Cart;
use Imagy;
use Theme;
use Validator;
use Modules\Setting\Contracts\Setting;

class CartController extends BasePublicController
{
    protected $app;
    protected $customerRepository;
    protected $hashids;
    protected $settingRepository;
    protected $productRepository;
    protected $categoryRepository;
    protected $colorRepository;
    protected $sizeRepository;
    protected $provinceRepository;
    protected $addressRepository;
    protected $invoiceitemRepository;
    protected $invoiceRepository;
    protected $pageRepository;
    protected $methodRepository;
    protected $districtRepository;
    protected $request;
    protected $promotionRepository;
    protected $setting;

    public function __construct(
        Application $app,
        CustomerRepository $customerRepository,
        ProductRepository $productRepository,
        SettingRepository $settingRepository,
        CategoryRepository $categoryRepository,
        ColorRepository $colorRepository,
        SizeRepository $sizeRepository,
        ProvinceRepository $provinceRepository,
        AddressRepository $addressRepository,
        MethodRepository $methodRepository,
        InvoiceitemRepository $invoiceitemRepository,
        PageRepository $pageRepository,
        InvoiceRepository $invoiceRepository,
        DistrictRepository $districtRepository,
        promotionRepository $promotionRepository,
        Request $request,
        Setting $setting
    ) {
        $this->app = $app;
        $this->customerRepository = $customerRepository;
        $this->settingRepository = $settingRepository;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->colorRepository = $colorRepository;
        $this->sizeRepository = $sizeRepository;
        $this->provinceRepository = $provinceRepository;
        $this->addressRepository = $addressRepository;
        $this->invoiceitemRepository = $invoiceitemRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->pageRepository = $pageRepository;
        $this->methodRepository = $methodRepository;
        $this->districtRepository = $districtRepository;
        $this->promotionRepository = $promotionRepository;
        $this->request = $request;
        $this->setting = $setting;
        $this->hashids = new Hashids('123456789QWERTYUIOPASDFGHJKLZaSaWeXCVBNM1212213123', 10, '123456789QWERTYUIOPASDFGHJKLZXCVBNM');
    }

    public function IndexCart()
    {
        $getTotalCartAndPromo = $this->getTotalCartAndPromo();
        $coupon = $getTotalCartAndPromo['coupon'];
        $totalCart = $getTotalCartAndPromo['totalCart'];
        $totalCartAfter = $getTotalCartAndPromo['totalCartAfter'];
        $promo = $getTotalCartAndPromo['promo'];

        return view('pages.cart.index', ['totalCart' => number_format($totalCart), 'totalCartAfter' => number_format($totalCartAfter), 'coupon' => $coupon, 'promo' => number_format($promo)]);
    }

    public function checkoutCart()
    {
        if (Cart::content()->count() > 0) {
            $provinces = $this->provinceRepository->all();
            $address = [];
            $user = Auth::guard('customer')->user();
            $payment_methods = $this->methodRepository->all();

            if ($user) {
                $address = $user->addressre()->orderBy('default_shipping_address', 'DESC')->get();
            }

            $getTotalCartAndPromo = $this->getTotalCartAndPromo();
            $priceShip = $getTotalCartAndPromo['priceShip'];
            $totalCart = $getTotalCartAndPromo['totalCart'];
            $totalCartAfter = $getTotalCartAndPromo['totalCartAfter'] + $priceShip;
            $promo = $getTotalCartAndPromo['promo'];
            return view(
                'pages.cart.checkout',
                [
                    'provinces' => $provinces,
                    'address' => $address,
                    'payment_methods' => $payment_methods,
                    'totalCart' => number_format($totalCart),
                    'totalCartAfter' => number_format($totalCartAfter),
                    'price_ship' => number_format($priceShip),
                    'promo' => number_format($promo)
                ]
            );
        } else {
            return view('error.404');
        }
    }

    public function deleteAllCart()
    {
        Cart::destroy();
        session(['cart_coupon' => ""]);
        Session::flash('success', "Xoá giỏ hàng thành công.");
        return redirect()->back();
    }

    public function updateAllCart(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $negative = array_filter(array_values($data), function ($v) {
            return $v < 0;
        });
        if ($negative) {
            Session::flash('error', "Số lượng sản phẩm không được nhỏ hơn 0.");
            return redirect()->back();
        }
        $message = "Cập nhật giỏ hàng thành công.";
        foreach ($data as $key => $item) {
            $cart = Cart::get($key);
            $product = $this->productRepository->find($cart->id);
            $product_in_cart_qty = isset($cart->qty) ? $cart->qty : 0;
            if ($product_in_cart_qty + $item > $product->quanlity) {
                $message = 'Có sản phẩm không đủ cung cấp.';
            } else {
                Cart::update($key, $item);
            }
        }
        Session::flash('success', $message);
        return redirect()->back();
    }

    public function checkoutNoAuth(Request $request)
    {
        $user = Auth::guard('customer')->user();
        if ($user) {
            if ($request->shippingAddressId == '0') {
                $rules = [
                    'fullname' => 'required',
                    'address' => 'required',
                    'province_id' => 'required',
                    'district_id' => 'required',
                    'ward_id' => 'required',
                    'phone' => 'required'
                ];
                $messages = [
                    'fullname.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                    'address.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                    'province_id.required' => 'Vui lòng chọn Tỉnh/Thành phố.',
                    'district_id.required' => 'Vui lòng chọn Quận/huyện.',
                    'ward_id.required' => 'Vui lòng chọn Phường, xã.',
                    'phone.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                ];
                $validator = Validator::make($request->all(), $rules, $messages);
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $data = [
                        'full_name' => strip_tags($request->fullname),
                        'city' => strip_tags($request->province_id),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => strip_tags($user->id)
                    ];
                    $this->addressRepository->create($data);
                    $data_invoice = [
                        'full_name' => strip_tags($request->fullname),
                        'city' => strip_tags($request->province_id),
                        'email' => strip_tags($user->email),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => strip_tags($user->id),
                        'total' => Cart::total(0, '.', ''),
                        'sub_total' => Cart::subtotal(0, '.', ''),
                        'payment' => strip_tags($request->payment_method),
                        'qty' => Cart::count(),
                        'note' => strip_tags($request->note)
                    ];
                }
            } else {
                $ship_address = $this->addressRepository->findByAttributes(['id' => strip_tags($request->shippingAddressId), 'customer_id' => $user->id]);
                if ($ship_address) {
                    $data_invoice = [
                        'full_name' => strip_tags($ship_address->full_name),
                        'email' => strip_tags($user->email),
                        'city' => strip_tags($ship_address->city),
                        'district' => strip_tags($ship_address->district),
                        'wards' => strip_tags($ship_address->wards),
                        'address' => strip_tags($ship_address->address),
                        'phone' => strip_tags($ship_address->phone),
                        'customer_id' => strip_tags($user->id),
                        'total' => Cart::total(0, '.', ''),
                        'sub_total' => Cart::subtotal(0, '.', ''),
                        'payment' => strip_tags($request->payment_method),
                        'qty' => Cart::count(),
                        'note' => strip_tags($request->note)
                    ];
                } else {
                    Session::flash('error', "Bạn chưa có địa chỉ giao hàng, chọn hoặc tạo mới địa chỉ giao hàng.");
                    return redirect()->back()
                        ->withInput();
                }
            }
        } else {
            $rules = [
                'fullname' => 'required',
                'email' => 'required|email|unique:customer__customers',
                'address' => 'required',
                'province_id' => 'required',
                'district_id' => 'required',
                'ward_id' => 'required',
                'phone' => 'required'
            ];
            $messages = [
                'fullname.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                'email.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                'email.email' => 'Vui lòng nhập địa chỉ email hợp lệ. Ví dụ abc@domain.com',
                'email.unique' => 'Email đã tồn tại trong hệ thống. Xin vui lòng đăng nhập để mua hàng.',
                'address.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
                'province_id.required' => 'Vui lòng chọn Tỉnh/Thành phố.',
                'district_id.required' => 'Vui lòng chọn Quận/huyện.',
                'ward_id.required' => 'Vui lòng chọn Phường, xã.',
                'phone.required' => 'Vui lòng nhập đầy đủ vào ô bên trên.',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if (isset($request->add_new_password) && $request->add_new_password == '1') {
                    $data_customer['first_name'] = strip_tags($request->fullname);
                    $data_customer['last_name'] = strip_tags($request->fullname);
                    $data_customer['password'] = Hash::make(strip_tags($request->password));
                    $data_customer['email'] = strip_tags($request->email);
                    $customer = $this->customerRepository->create($data_customer);
                    if ($customer) {
                        $customer->code = $this->hashids->encode($customer->id);
                        $customer->save();
                        $data = [
                            'full_name' => strip_tags($request->fullname),
                            'city' => strip_tags($request->province_id),
                            'district' => strip_tags($request->district_id),
                            'wards' => strip_tags($request->ward_id),
                            'address' => strip_tags($request->address),
                            'phone' => strip_tags($request->phone),
                            'customer_id' => strip_tags($customer->id)
                        ];
                        $this->addressRepository->create($data);
                    }
                    $data_invoice = [
                        'full_name' => strip_tags($request->fullname),
                        'city' => strip_tags($request->province_id),
                        'email' => strip_tags($request->email),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => $customer->id,
                        'total' => Cart::total(0, '.', ''),
                        'sub_total' => Cart::subtotal(0, '.', ''),
                        'payment' => strip_tags($request->payment_method),
                        'qty' => Cart::count(),
                        'note' => strip_tags($request->note)
                    ];
                } else {
                    $data_invoice = [
                        'full_name' => strip_tags($request->fullname),
                        'city' => strip_tags($request->province_id),
                        'email' => strip_tags($request->email),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => 0,
                        'total' => Cart::total(0, '.', ''),
                        'sub_total' => Cart::subtotal(0, '.', ''),
                        'payment' => strip_tags($request->payment_method),
                        'qty' => Cart::count(),
                        'note' => strip_tags($request->note)
                    ];
                }
            }
        }

        $dataTotalCart = $this->getTotalCartAndPromo();

        $data_ship_price = $dataTotalCart['priceShip'];

        $data_invoice['price_ship'] = $data_ship_price;
        $data_invoice['total'] = $dataTotalCart['totalInt'];
        $data_invoice['id_promotion'] = isset($dataTotalCart['promoID']) ? $dataTotalCart['promoID'] : 0;
        $data_invoice['point_request'] = $dataTotalCart['point'];
        $data_invoice['total_promo'] = $dataTotalCart['totalPromoInt'];

        $invoice = $this->invoiceRepository->create($data_invoice);
        if ($invoice) {
            $invoice->code = $this->hashids->encode($invoice->id);
            $invoice->save();
            if ($user) {
                $user->point = $user->point - $dataTotalCart['point'];
                $user->save();
            }
            foreach (Cart::content() as $item) {
                $item_invoice = [
                    'product_id' => $item->id,
                    'title' => $item->name,
                    'slug' => $item->options->has('slug') ? $item->options->slug : '',
                    'price' => $item->price,
                    'qty' => $item->qty,
                    'size_id' => $item->options->has('size_id') ? $item->options->size_id : '',
                    'size' => $item->options->has('size') ? $item->options->size : '',
                    'color' => $item->options->has('color') ? $item->options->color : '',
                    'color_id' => $item->options->has('color_id') ? $item->options->color_id : '',
                    'invoice_id' => $invoice->id,
                ];
                $pro = $this->productRepository->find($item->id);
                if ($pro) {
                    $pro->quanlity = $pro->quanlity - $item->qty;
                    $pro->save();
                }
                $this->invoiceitemRepository->create($item_invoice);
            }
            Mail::send('emails.order', ['invoice' => $invoice], function ($message) use ($invoice) {
                $message->to($invoice->email, $invoice->full_name)->subject('Thông báo đặt hàng thành công');
            });

            Mail::send('emails.order', ['invoice' => $invoice], function ($message) use ($invoice) {
                $message->to(config('mail.from.address'), $invoice->full_name . '-' . $invoice->code)->subject('Thông tin đặt hàng thành công');
            });

            $cart_data = Cart::content();
            $time_now = strtotime("now");
            $token = md5($time_now);
            session(['time_new_token' => $time_now]);
            session(['cart_point' => null]);
            session(['cart_coupon' => null]);
            Cart::destroy();

            return redirect()->route('page.cart.checkoutSuccess', ['invoice' => $invoice, 'token' => $token, 'totalPromo' => $dataTotalCart['totalPromo']]);
        } else {
            return redirect()->route('page.cart.checkoutError');
        }
    }

    private function getTemplateForPage($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    public function checkoutSuccess()
    {
        $invoice_id = $this->request->invoice;
        $token = $this->request->token;
        $totalPromo = $this->request->totalPromo;
        $invoice = $this->invoiceRepository->find($invoice_id);
        $value = session('time_new_token');
        session()->forget('time_new_token');
        if ($token === md5($value)) {
            if ($invoice) {
                $page = $this->pageRepository->findBySlug(trans('frontend::url.page_checkout_success'));
                if ($page) {
                    $template = $this->getTemplateForPage($page);

                    return view($template, compact('page', 'invoice', 'totalPromo'));
                } else {
                    return view('error.404');
                }
            } else {
                return redirect()->route('homepage');
            }
        } else {
            return redirect()->route('homepage');
        }
    }

    public function checkoutError()
    {
    }

    public function updateItemCart(Request $request)
    {
        if ($request->ajax()) {
            $cart_id = strip_tags($request->cart_id);
            $qty = strip_tags($request->qty);
            if ($qty <= 0) {
                return response()->json([
                    'status' => '500',
                    'message' => 'Số lượng sản phẩm không được nhỏ hơn 0.'
                ]);
            } else {
                $cart = Cart::get($cart_id);
                $product = $this->productRepository->find($cart->id);
                $product_in_cart_qty = isset($cart->qty) ? $cart->qty : 0;
                if ($product_in_cart_qty + $qty > $product->quanlity) {
                    return response()->json(['status' => '404', 'message' => 'Số lượng sản phẩm không đủ cung cấp.']);
                }

                Cart::update($cart_id, $qty);
                return response()->json([
                    'status' => '200',
                    'count_cart' => Cart::count(),
                    'subtotal_cart' => Cart::subtotal(),
                    'view_header_cart' => view('partials.cart.header-mini-cart')->render(),
                    'view_page_cart' => view('partials.cart.cart')->render()
                ]);
            }
        }
    }

    public function testEmail()
    {
        return view('emails.order');
    }

    public function addCodePromotion(Request $request)
    {
        $dataCode = strip_tags($request->code);
        $totalCart = Cart::subtotal(0, '.', '');
        $code = $this->promotionRepository->findByAttributes(['code' => $dataCode]);
        if ($code) {
            if (strtotime($code->start_date) <= strtotime(date('Y-m-d')) && strtotime($code->end_date) >= strtotime(date('Y-m-d'))) {
                $promo = $this->calPricePro($code);
                $pricePromotion = (int) $totalCart - $promo;
                session(['cart_coupon' => $dataCode]);
                return response()->json(['pricePromotion' => number_format($pricePromotion), 'promo' => number_format($promo), 'status' => 200]);
            } else {
                return response()->json(['error' => true, 'message' => 'Mã giảm giá/quà tặng hết hạn.', 'pricePromotion' => number_format($totalCart)], 200);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Mã giảm giá/quà tặng không tồn tại.', 'pricePromotion' => number_format($totalCart)], 200);
        }
    }

    private function calPricePro($promo)
    {
        $totalPriceCart = 0;
        foreach (Cart::content() as $cart) {
            if ($cart->options->excluded == 0) {
                $totalPriceCart += ($cart->price*$cart->qty);
            }
        }
        session(['cart_coupon' => $promo->code]);
        return (((int) $totalPriceCart * (int) $promo->percent_up) / 100);;
    }

    private function getTotalCartAndPromo()
    {
        $coupon = session('cart_coupon');
        $point = session('cart_point');
        $totalCart = Cart::total(0, '.', '');
        $totalCartAfter = Cart::total(0, '.', '');
        $code = $this->promotionRepository->findByAttributes(['code' => $coupon]);
        $promo = 0;
        $promoID = 0;
        $total = 0;
        $priceShip = 0;
        if ($totalCart < 399000) {
            $priceShip = 30000;
        }

        if ($code) {
            if (strtotime($code->start_date) <= strtotime(date('Y-m-d')) && strtotime($code->end_date) >= strtotime(date('Y-m-d'))) {
                $promo = $this->calPricePro($code);
                $totalCartAfter = $totalCart - $promo;
                $promoID = $code->id;
            } else {
                session(['cart_coupon' => null]);
                $coupon = "";
            }
        } else {
            session(['cart_coupon' => null]);
            $coupon = "";
        }

        $pricePoint = 0;
        if ($point != null) {
            $pricePoint = $point * 1000;
        }
        $total = $totalCartAfter + $priceShip - $pricePoint;
        return [
            'totalCart' => (int) $totalCart,
            'totalCartAfter' => (int) $totalCartAfter,
            'pricePoint' => $pricePoint,
            'promo' => $promo,
            'promoID' => $promoID,
            'coupon' => $coupon,
            'point' => $point,
            'total' => number_format($total),
            'totalInt' => $total,
            'totalPromo' => number_format($pricePoint + $promo),
            'totalPromoInt' => $pricePoint + $promo,
            'priceShip' => $priceShip
        ];
    }

    public function addCodePoint(Request $request)
    {
        $getDataCart = $this->getTotalCartAndPromo();
        $user = Auth::guard('customer')->user();
        if ($user) {
            $point = $user->point;
            $pointRequest = $request->point;
            if ($pointRequest > $point) {
                session(['cart_point' => null]);
                $pointAfter = $point - $pointRequest;
                $getDataCartAfter = $this->getTotalCartAndPromo();
                $getDataCartAfter =  array_merge($getDataCartAfter, ['pointAfter' => $pointAfter, 'error' => true, 'message' => 'Bạn đã nhập quá số lượng điểm tích lũy của bạn.']);
                return response()->json($getDataCartAfter, 200);
            } else {
                $totalCart = $getDataCart['totalCart'];
                $pricePoint = (int) ($pointRequest);

                if ($pricePoint > $totalCart) {
                    session(['cart_point' => null]);
                    $pointAfter = $point - $pointRequest;
                    $getDataCartAfter = $this->getTotalCartAndPromo();
                    $getDataCartAfter =  array_merge($getDataCartAfter, ['pointAfter' => $pointAfter, 'error' => true, 'message' => 'Bạn đã nhập quá số tiền trong giỏ hàng.']);
                    return response()->json($getDataCartAfter, 200);
                } else {
                    session(['cart_point' => $pointRequest]);
                    $pointAfter = $point - $pointRequest;
                    $getDataCartAfter = $this->getTotalCartAndPromo();
                    $getDataCartAfter =  array_merge($getDataCartAfter, ['pointAfter' => $pointAfter]);
                    return response()->json($getDataCartAfter, 200);
                }
            }
        }
    }
}
