<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Size;
use Modules\Product\Http\Requests\CreateSizeRequest;
use Modules\Product\Http\Requests\UpdateSizeRequest;
use Modules\Product\Repositories\SizeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SizeController extends AdminBaseController
{
    /**
     * @var SizeRepository
     */
    private $size;

    public function __construct(SizeRepository $size)
    {
        parent::__construct();

        $this->size = $size;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sizes = $this->size->all();

        return view('product::admin.sizes.index', compact('sizes'));
    }

    public function addSizeAjax()
    {
        return \response()->json(['view' => view('product::admin.sizes.addSizeAjax')->render()]);
    }

    public function storeAjax(Request $request)
    {

        $data = ['title' => $request->title_size];
        if ($request->title_size == '' ) {
            return \response()->json(['status' => 'error', 'message' => 'Please enter all filed']);
        } else {
            $check_size = $this->size->findByAttributes($data);
            if ($check_size) {
                return \response()->json(['status' => 'error', 'message' => 'The size exsit in system']);
            } else {
                $size = $this->size->create($data);
                return \response()->json(['title' => $size->title, 'id' => $size->id]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSizeRequest $request
     * @return Response
     */
    public function store(CreateSizeRequest $request)
    {
        $this->size->create($request->all());

        return redirect()->route('admin.product.size.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::sizes.title.sizes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Size $size
     * @return Response
     */
    public function edit(Size $size)
    {
        return view('product::admin.sizes.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Size $size
     * @param  UpdateSizeRequest $request
     * @return Response
     */
    public function update(Size $size, UpdateSizeRequest $request)
    {
        $this->size->update($size, $request->all());

        return redirect()->route('admin.product.size.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::sizes.title.sizes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Size $size
     * @return Response
     */
    public function destroy(Size $size)
    {
        $this->size->destroy($size);

        return redirect()->route('admin.product.size.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::sizes.title.sizes')]));
    }
}
