<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Procolor;
use Modules\Product\Http\Requests\CreateProcolorRequest;
use Modules\Product\Http\Requests\UpdateProcolorRequest;
use Modules\Product\Repositories\ProcolorRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ProcolorController extends AdminBaseController
{
    /**
     * @var ProcolorRepository
     */
    private $procolor;

    public function __construct(ProcolorRepository $procolor)
    {
        parent::__construct();

        $this->procolor = $procolor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$procolors = $this->procolor->all();

        return view('product::admin.procolors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.procolors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProcolorRequest $request
     * @return Response
     */
    public function store(CreateProcolorRequest $request)
    {
        $this->procolor->create($request->all());

        return redirect()->route('admin.product.procolor.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::procolors.title.procolors')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Procolor $procolor
     * @return Response
     */
    public function edit(Procolor $procolor)
    {
        return view('product::admin.procolors.edit', compact('procolor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Procolor $procolor
     * @param  UpdateProcolorRequest $request
     * @return Response
     */
    public function update(Procolor $procolor, UpdateProcolorRequest $request)
    {
        $this->procolor->update($procolor, $request->all());

        return redirect()->route('admin.product.procolor.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::procolors.title.procolors')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Procolor $procolor
     * @return Response
     */
    public function destroy(Procolor $procolor)
    {
        $this->procolor->destroy($procolor);

        return redirect()->route('admin.product.procolor.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::procolors.title.procolors')]));
    }
}
