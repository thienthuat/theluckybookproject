<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Prosize;
use Modules\Product\Http\Requests\CreateProsizeRequest;
use Modules\Product\Http\Requests\UpdateProsizeRequest;
use Modules\Product\Repositories\ProsizeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ProsizeController extends AdminBaseController
{
    /**
     * @var ProsizeRepository
     */
    private $prosize;

    public function __construct(ProsizeRepository $prosize)
    {
        parent::__construct();

        $this->prosize = $prosize;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$prosizes = $this->prosize->all();

        return view('product::admin.prosizes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.prosizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProsizeRequest $request
     * @return Response
     */
    public function store(CreateProsizeRequest $request)
    {
        $this->prosize->create($request->all());

        return redirect()->route('admin.product.prosize.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::prosizes.title.prosizes')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Prosize $prosize
     * @return Response
     */
    public function edit(Prosize $prosize)
    {
        return view('product::admin.prosizes.edit', compact('prosize'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Prosize $prosize
     * @param  UpdateProsizeRequest $request
     * @return Response
     */
    public function update(Prosize $prosize, UpdateProsizeRequest $request)
    {
        $this->prosize->update($prosize, $request->all());

        return redirect()->route('admin.product.prosize.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::prosizes.title.prosizes')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Prosize $prosize
     * @return Response
     */
    public function destroy(Prosize $prosize)
    {
        $this->prosize->destroy($prosize);

        return redirect()->route('admin.product.prosize.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::prosizes.title.prosizes')]));
    }
}
