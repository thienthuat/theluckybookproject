<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Category;
use Modules\Product\Http\Requests\CreateCategoryRequest;
use Modules\Product\Http\Requests\UpdateCategoryRequest;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CategoryController extends AdminBaseController
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        parent::__construct();

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = $this->category->all();

        return view('product::admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    private function recursive($source, $parent, $level, &$newArray)
    {
        if (count($source) > 0) {
            foreach ($source as $key => $value) {
                if ($value->parent_id == $parent) {
                    $value->level = $level;
                    $newArray[] = $value;
                    unset($source[$key]);
                    $newParent = $value->id;
                    $this->recursive($source, $newParent, $level + 1, $newArray);
                }
            }
        }
    }

    public function create()
    {
        $alls = $this->category->all();
        $order = max(Category::pluck('order')->toArray());
        $menus = [];
        $this->recursive($alls, 0, 1, $menus);
        return view('product::admin.categories.create', compact('menus','order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $this->category->create($request->all());

        return redirect()->route('admin.product.category.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::categories.title.categories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        $alls = $this->category->all();
        $menus = [];
        $this->recursive($alls, 0, 1, $menus);
        return view('product::admin.categories.edit', compact('category', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     * @param  UpdateCategoryRequest $request
     * @return Response
     */
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        $this->category->update($category, $request->all());

        return redirect()->route('admin.product.category.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::categories.title.categories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        $this->category->destroy($category);

        return redirect()->route('admin.product.category.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::categories.title.categories')]));
    }
}
