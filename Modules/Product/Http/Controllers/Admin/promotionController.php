<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\promotion;
use Modules\Product\Http\Requests\CreatepromotionRequest;
use Modules\Product\Http\Requests\UpdatepromotionRequest;
use Modules\Product\Repositories\promotionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class promotionController extends AdminBaseController
{
    /**
     * @var promotionRepository
     */
    private $promotion;

    public function __construct(promotionRepository $promotion)
    {
        parent::__construct();

        $this->promotion = $promotion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $promotions = $this->promotion->all();

        return view('product::admin.promotions.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatepromotionRequest $request
     * @return Response
     */
    public function store(CreatepromotionRequest $request)
    {
        $this->promotion->create($request->all());

        return redirect()->route('admin.product.promotion.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::promotions.title.promotions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  promotion $promotion
     * @return Response
     */
    public function edit(promotion $promotion)
    {
        return view('product::admin.promotions.edit', compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  promotion $promotion
     * @param  UpdatepromotionRequest $request
     * @return Response
     */
    public function update(promotion $promotion, UpdatepromotionRequest $request)
    {
        $this->promotion->update($promotion, $request->all());

        return redirect()->route('admin.product.promotion.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::promotions.title.promotions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  promotion $promotion
     * @return Response
     */
    public function destroy(promotion $promotion)
    {
        $this->promotion->destroy($promotion);

        return redirect()->route('admin.product.promotion.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::promotions.title.promotions')]));
    }
}
