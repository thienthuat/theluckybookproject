<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Productcate;
use Modules\Product\Http\Requests\CreateProductcateRequest;
use Modules\Product\Http\Requests\UpdateProductcateRequest;
use Modules\Product\Repositories\ProductcateRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ProductcateController extends AdminBaseController
{
    /**
     * @var ProductcateRepository
     */
    private $productcate;

    public function __construct(ProductcateRepository $productcate)
    {
        parent::__construct();

        $this->productcate = $productcate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$productcates = $this->productcate->all();

        return view('product::admin.productcates.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.productcates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductcateRequest $request
     * @return Response
     */
    public function store(CreateProductcateRequest $request)
    {
        $this->productcate->create($request->all());

        return redirect()->route('admin.product.productcate.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::productcates.title.productcates')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Productcate $productcate
     * @return Response
     */
    public function edit(Productcate $productcate)
    {
        return view('product::admin.productcates.edit', compact('productcate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Productcate $productcate
     * @param  UpdateProductcateRequest $request
     * @return Response
     */
    public function update(Productcate $productcate, UpdateProductcateRequest $request)
    {
        $this->productcate->update($productcate, $request->all());

        return redirect()->route('admin.product.productcate.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::productcates.title.productcates')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Productcate $productcate
     * @return Response
     */
    public function destroy(Productcate $productcate)
    {
        $this->productcate->destroy($productcate);

        return redirect()->route('admin.product.productcate.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::productcates.title.productcates')]));
    }
}
