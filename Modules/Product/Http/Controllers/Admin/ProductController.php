<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Product\Repositories\ColorRepository;
use Modules\Product\Repositories\SizeRepository;
use DB;

class ProductController extends AdminBaseController
{
    /**
     * @var ProductRepository
     */
    private $product;
    private $categoryRepository;
    private $colorRepository;
    private $sizeRepository;

    public function __construct(ProductRepository $product, CategoryRepository $categoryRepository, ColorRepository $colorRepository, SizeRepository $sizeRepository)
    {
        parent::__construct();

        $this->product = $product;
        $this->categoryRepository = $categoryRepository;
        $this->colorRepository = $colorRepository;
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = $this->product->all();

        return view('product::admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    private function recursive($source, $parent, $level, &$newArray)
    {
        if (count($source) > 0) {
            foreach ($source as $key => $value) {
                if ($value['parent_id'] == $parent) {
                    $value['level'] = $level;
                    $newArray[] = $value;
                    unset($source[$key]);
                    $newParent = $value['id'];
                    $this->recursive($source, $newParent, $level + 1, $newArray);
                }
            }
        }
    }

    public function create()
    {
        $categories = [];
        $alls = $this->categoryRepository->all();
        $colors = $this->colorRepository->all();
        $sizes = $this->sizeRepository->all();
        $products = $this->product->all();
        $this->recursive($alls, 0, 1, $categories);
        return view('product::admin.products.create', compact('categories', 'colors', 'sizes', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $Product = $this->product->create($request->all());
        $category = $Product->productCate;
        $key = '';
        $prodcut_relation_id = $request->prodcut_relation_id;
        if (count($prodcut_relation_id) > 0) {
            $Product->productRelations()->sync($prodcut_relation_id);
        }
        $breadcrumb_id = $request->breadcrumb_id;
        if (count($breadcrumb_id) > 0) {
            $Product->breadcrumbs()->sync($breadcrumb_id);
        }

        foreach ($category as $item) {
            $key .= $item->title . ',';
        }
        if ($Product->meta_title == '') {
            $Product->meta_title = $Product->title;
        }
        if ($Product->meta_keyword == '') {
            $Product->meta_keyword = $key . $Product->title;
        }
        if ($Product->meta_description == '') {
            $Product->meta_description = $Product->title;
        }
        $Product->save();
        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return Response
     */
    public function edit(Product $product)
    {
        $categories = [];
        $colors = $this->colorRepository->all();
        $sizes = $this->sizeRepository->all();
        $alls = $this->categoryRepository->all();
        $products = $this->product->all();
        $arr_pro_relate_id = $product->productRelations()->pluck('product_relation_id')->toArray();
        $arr_breadcrumb_id = $product->breadcrumbs()->pluck('category_id')->toArray();

        $this->recursive($alls, 0, 1, $categories);
        return view('product::admin.products.edit', compact('product', 'categories', 'colors', 'sizes', 'products', 'arr_pro_relate_id', 'arr_breadcrumb_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Product $product
     * @param  UpdateProductRequest $request
     * @return Response
     */
    public function update(Product $product, UpdateProductRequest $request)
    {
        $Product = $this->product->update($product, $request->all());
        $category = $Product->productCate;
        $key = '';
        foreach ($category as $item) {
            $key .= $item->title . ',';
        }
        if ($Product->meta_title == '') {
            $Product->meta_title = $Product->title;
        }
        if ($Product->meta_keyword == '') {
            $Product->meta_keyword = $key . $Product->title;
        }
        if ($Product->meta_description == '') {
            $Product->meta_description = '' . $Product->title . ' sản xuất tại Việt Nam từ thương hiệu thời trang trẻ em uy tín Tutupetti, ' . $Product->title . ' xinh xắn, dể thương, sang trọng, giá rẻ';
        }
        $Product->save();
        $prodcut_relation_id = $request->prodcut_relation_id;
        if (count($prodcut_relation_id) > 0) {
            $Product->productRelations()->sync($prodcut_relation_id);
        }

        $breadcrumb_id = $request->breadcrumb_id;
        if (count($breadcrumb_id) > 0) {
            $Product->breadcrumbs()->sync($breadcrumb_id);
        }
        
        if ($request->get('button') === 'index') {
            return redirect()->route('admin.product.product.index')
                ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::products.title.products')]));
        }
        return redirect()->back()
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $this->product->destroy($product);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::products.title.products')]));
    }
}
