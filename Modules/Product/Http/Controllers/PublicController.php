<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Product\Repositories\ColorRepository;
use Modules\Product\Repositories\SizeRepository;
use Modules\Page\Repositories\PageRepository;
use Modules\Product\Repositories\ProductcateRepository;
use Modules\Product\Repositories\ProcolorRepository;
use Modules\Product\Repositories\ProsizeRepository;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use Session;
use Hash;
use Mail;
use Hashids\Hashids;
use Cart;
use Imagy;
use Theme;

class PublicController extends BasePublicController
{
    protected $app;
    protected $customerRepository;
    protected $hashids;
    protected $settingRepository;
    protected $productRepository;
    protected $categoryRepository;
    protected $colorRepository;
    protected $sizeRepository;
    protected $request;
    protected $pageRepository;
    protected $productcateRepository;
    protected $procolorRepository;
    protected $prosizeRepository;

    public function __construct (Application $app,
                                 CustomerRepository $customerRepository,
                                 ProductRepository $productRepository,
                                 SettingRepository $settingRepository,
                                 CategoryRepository $categoryRepository,
                                 ColorRepository $colorRepository,
                                 SizeRepository $sizeRepository,
                                 PageRepository $pageRepository,
                                 ProductcateRepository $productcateRepository,
                                 Request $request,
                                 ProsizeRepository $prosizeRepository,
                                 ProcolorRepository $procolorRepository)
    {
        $this->app = $app;
        $this->customerRepository = $customerRepository;
        $this->settingRepository = $settingRepository;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->colorRepository = $colorRepository;
        $this->sizeRepository = $sizeRepository;
        $this->pageRepository = $pageRepository;
        $this->productcateRepository = $productcateRepository;
        $this->procolorRepository = $procolorRepository;
        $this->prosizeRepository = $prosizeRepository;
        $this->request = $request;
        $this->hashids = new Hashids('', 10, '123456789QƯERTYUIOPASDFGHJKLZXCVBNM');
    }

    public function addToCart (Request $request)
    {
        if ($request->ajax()) {
            $product_id = strip_tags($request->product_id);
            $price = strip_tags($request->price);
            $qty = strip_tags($request->qty);
            $type_color = strip_tags($request->type_color);
            $type_size = strip_tags($request->type_size);
            $product = $this->productRepository->find($product_id);
            if ($qty <= 0) {
                return response()->json(['status' => '404', 'message' => 'Số lượng sản phẩm phải lớn hơn 0.']);
            } else {
                if ($product) {
                    $product_in_cart = Cart::search(function ($cartItem) use ($product) {
                        return $cartItem->id === $product->id;
                    })->first();
                    $product_in_cart_qty = isset($product_in_cart->qty) ? $product_in_cart->qty : 0;
                    if ($product_in_cart_qty + $qty > $product->quanlity) {
                        return response()->json(['status' => '404', 'message' => 'Số lượng sản phẩm không đủ cung cấp.']);
                    }
                    $color = $this->colorRepository->find($type_color);
                    $size = $this->sizeRepository->find($type_size);
                    $image = $product->getThumbnailAttribute() != '' ? Imagy::getThumbnail($product->getThumbnailAttribute()->path, 'smallThumb') : Theme::url('images/img404.png');
                    Cart::add($product->id, $product->title, $qty, $price, [
                        'size' => $size ? $size->title : '',
                        'size_id' => $size ? $size->id : '',
                        'color' => $color ? $color->title : '',
                        'color_id' => $color ? $color->id : '',
                        'excluded' => $product->check_excluded,
                        'avatar' => $image,
                        'slug' => $product->slug]);
                    return response()->json(['status' => '200',
                        'count_cart' => Cart::count(),
                        'subtotal_cart' => Cart::subtotal(),
                        'view_header_cart' => view('partials.cart.header-mini-cart')->render()]);
                } else {
                    return response()->json(['status' => '404', 'message' => 'Không có sản phẩm trong hệ thông.']);
                }
            }
        }
    }

    public function deleteItemCart ($id, Request $request)
    {
        if ($request->ajax()) {
            $cart_id = strip_tags($id);
            Cart::remove($cart_id);
            return response()->json(['status' => '200',
                'count_cart' => Cart::count(),
                'subtotal_cart' => Cart::subtotal(),
                'view_header_cart' => view('partials.cart.header-mini-cart')->render(),
                'view_page_cart' => view('partials.cart.cart')->render()]);
        }
    }

    public function getProduct ()
    {
        $page = $this->pageRepository->findBySlug(trans('frontend::url.page_product'));
        if ($page) {
            $template = $this->getTemplateForPage($page);

            $range_max = $this->request->get('range_max') != null ? $this->request->get('range_max') : 5200000;
            $range_max_default = $this->request->get('range_max');
            $range_min = $this->request->get('range_min') != null ? $this->request->get('range_min') : 0;
            $range_min_default = $this->request->get('range_min');
            $danh_muc = $this->request->get('danh_muc');
            $filter_color = $this->request->get('filter_color');
            $filter_size = $this->request->get('filter_size');
            $url = '?';
            if ($range_min_default != null) {
                $url .= '&range_min='.$range_min_default;
            }
            if ($range_max_default != null) {
                $url .= '&range_max='.$range_max_default;
            }
            if ($filter_size != null) {
                $url .= '&filter_size='.$filter_size;
            }
            if ($filter_color != null) {
                $url .= '&filter_color='.$filter_color;
            }
            $arr_product_id = $this->productcateRepository->getProductIdByArrCate();
            $listSizes = $this->sizeRepository->all();
            $listColors = $this->colorRepository->all();
            $paginations = 24;
            $products = $this->productRepository->getProductByCateFilter($arr_product_id, $range_min, $range_max, $filter_color, $filter_size, $paginations);
            $products->setPath('custom/url');
            $products->withPath($url);
            $categories = $this->categoryRepository->all();
            return view($template, compact('page', 'products', 'categories', 'range_max', 'range_min', 'danh_muc', 'listSizes',
                'filter_color',
                'filter_size',
                'listColors'));
        } else {
            return view('error.404');
        }
    }

    private function getCategoryByParentID ($parentID = [], &$arr)
    {
        foreach ($parentID as $item) {
            if ($item != '') {
                $datas = $this->categoryRepository->getCategoryByParent($item)->toArray();
                $data_ids = [];
                array_push($arr, intval($item));
                foreach ($datas as $data) {
                    if ($data != null) {
                        array_push($data_ids, intval($data['id']));
                    }
                }
                $this->getCategoryByParentID($data_ids, $arr);
            }
        }
    }

    private function getParentCategory ($cate_id)
    {
        $category = $this->categoryRepository->find($cate_id);
        if ($category->parent_id == 0) {
            return $category;
        } else {
            $category_p = $this->categoryRepository->find($category->parent_id);
            if ($category_p->parent_id == 0) {
                return $category_p;
            } else {
                return $this->getParentCategory($category_p->id);
            }
        }
    }

    private function recursive ($source, $parent, &$newArray)
    {
        if (count($source) > 0) {
            foreach ($source as $key => $value) {
                if ($value['parent_id'] == $parent) {
                    $newArray[] = $value;
                    unset($source[$key]);
                    $newParent = $value['id'];
                    $this->recursive($source, $newParent, $newArray);
                }
            }
        }
    }

    public function getProductNew ()
    {
        $products = $this->productRepository->getProductByAction();
        return view('partials.products.new', compact('products'));
    }

    private function getTemplateForPage ($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    public function getProductCategory ($slug = '')
    {
        if ($slug == '') {
            return redirect()->route('page.product.getProduct');
        } else {
            $category_current = $this->categoryRepository->findBySlug($slug);
            if ($category_current) {
                $all_category = $this->categoryRepository->all()->toArray();

                $category_sidebar = $this->getParentCategory($category_current->id);
                $arr_category_sidebar = [];
                $this->recursive($all_category, $category_sidebar->id, $arr_category_sidebar);
                $arr_category_sidebar_id = array_column($arr_category_sidebar, 'id');
                array_push($arr_category_sidebar_id, $category_sidebar->id);
                $listCategorySidebar = $this->categoryRepository->model()->whereIn('id', $arr_category_sidebar_id)->orderBy('order', 'DESC')->get();

                $categories_parent = [];
                $this->recursive($all_category, $category_current->id, $categories_parent);
                $arr_category_id = array_column($categories_parent, 'id');
                array_push($arr_category_id, $category_current->id);

                $range_max = $this->request->get('range_max') != null ? $this->request->get('range_max') : 5200000;
                $range_max_default = $this->request->get('range_max');
                $range_min = $this->request->get('range_min') != null ? $this->request->get('range_min') : 0;
                $range_min_default = $this->request->get('range_min');
                $danh_muc = $this->request->get('danh_muc');
                $filter_color = $this->request->get('filter_color');
                $filter_size = $this->request->get('filter_size');
                $categories = $this->categoryRepository->getCategoryByParent($category_current->id);
                $url = '?';
                if ($range_min_default != null) {
                    $url .= '&range_min='.$range_min_default;
                }
                if ($range_max_default != null) {
                    $url .= '&range_max='.$range_max_default;
                }
                if ($filter_size != null) {
                    $url .= '&filter_size='.$filter_size;
                }
                if ($filter_color != null) {
                    $url .= '&filter_color='.$filter_color;
                }
                $arr_product_id = $this->productcateRepository->getProductIdByArrCate($arr_category_id);
                $listSizes = $this->prosizeRepository->getSizeJoin($arr_product_id);
                $listColors = $this->procolorRepository->getColorJoin($arr_product_id);
                $paginations = 24;
                $products = $this->productRepository->getProductByCateFilter($arr_product_id, $range_min, $range_max, $filter_color, $filter_size, $paginations);
                $products->setPath('custom/url');
                $products->withPath($url);
                return view('pages.products.category', compact('category_current',
                    'products',
                    'categories',
                    'range_max',
                    'range_min',
                    'danh_muc',
                    'listCategorySidebar',
                    'listSizes',
                    'filter_color',
                    'filter_size',
                    'listColors'));
            } else {
                return view('error.404');
            }
        }
    }

    public function searchProduct ()
    {
        $key = $this->request->get('q');
        if ($key == null || $key == '') {
            return redirect()->route('homepage');
        } else {
            $products = $this->productRepository->model()->where('status', 1)->whereHas('translations', function (Builder $query) use ($key) {
                if ($key != null && $key != '') {
                    $query->where('title', 'LIKE', '%'.$key.'%');
                }
            })->paginate(24);
            return view('pages.products.search', compact('products', 'key'));
        }
    }

    public function getProductPromotion ()
    {
        $products = $this->productRepository->getProductPromotion();
        return view('pages.products.promotion', compact('products'));
    }

}
