<?php

use Illuminate\Routing\Router;

/** @var Router $router */

if (!App::runningInConsole()) {
    $router->group(['prefix' => '/ajax-product'], function (Router $router) {
        $router->post('add-to-cart', [
            'as' => 'page.cart.addToCart',
            'uses' => 'PublicController@addToCart'
        ]);
        $router->get('delete-item-cart/{id?}', [
            'as' => 'page.cart.deleteItemCart',
            'uses' => 'PublicController@deleteItemCart'
        ]);
        $router->post('update-item-cart', [
            'as' => 'page.cart.updateItemCart',
            'uses' => 'CartController@updateItemCart'
        ]);
    });
    $router->group(['prefix' => '/gio-hang'], function (Router $router) {
        $router->get('/', [
            'as' => 'page.cart.IndexCart',
            'uses' => 'CartController@IndexCart'
        ]);
        $router->get('/xoa-tat-ca-gio-hang', [
            'as' => 'page.cart.deleteAllCart',
            'uses' => 'CartController@deleteAllCart'
        ]);
        $router->post('/cap-nhat-gio-hang', [
            'as' => 'page.cart.updateAllCart',
            'uses' => 'CartController@updateAllCart'
        ]);
        $router->post('/thanh-toan', [
            'as' => 'page.cart.checkoutNoAuth',
            'uses' => 'CartController@checkoutNoAuth'
        ]);
        $router->get('/thanh-toan', [
            'as' => 'page.cart.checkoutCart',
            'uses' => 'CartController@checkoutCart'
        ]);
        $router->get(trans('frontend::url.page_checkout_success'), [
            'as' => 'page.cart.checkoutSuccess',
            'uses' => 'CartController@checkoutSuccess'
        ]);
        $router->get('/thanh-toan-loi', [
            'as' => 'page.cart.checkoutError',
            'uses' => 'CartController@checkoutError'
        ]);

        $router->get('addCodePromotion', [
            'as' => 'page.cart.addCodePromotion',
            'uses' => 'CartController@addCodePromotion'
        ]);
        $router->get('addCodePoint', [
            'as' => 'page.cart.addCodePoint',
            'uses' => 'CartController@addCodePoint'
        ]);
    });
    $router->get(trans('frontend::url.page_product'), [
        'as' => 'page.product.getProduct',
        'uses' => 'PublicController@getProduct'
    ]);
    $router->get(trans('frontend::url.page_product_promotion'), [
        'as' => 'page.product.getProductPromotion',
        'uses' => 'PublicController@getProductPromotion'
    ]);
    $router->get(trans('frontend::url.page_category_product').'/{slug?}', [
        'as' => 'page.product.getProductCategory',
        'uses' => 'PublicController@getProductCategory'
    ]);
    $router->get('/tim-kiem-san-pham', [
        'as' => 'page.product.searchProduct',
        'uses' => 'PublicController@searchProduct'
    ]);

    $router->get('/test-email', [
        'as' => 'page.product.testEmail',
        'uses' => 'CartController@testEmail'
    ]);

}
