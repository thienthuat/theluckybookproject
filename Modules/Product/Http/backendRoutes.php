<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/product'], function (Router $router) {
    $router->bind('categoryP', function ($id) {
        return app('Modules\Product\Repositories\CategoryRepository')->find($id);
    });
    $router->get('categories', [
        'as' => 'admin.product.category.index',
        'uses' => 'CategoryController@index',
        'middleware' => 'can:product.categories.index'
    ]);
    $router->get('categories/create', [
        'as' => 'admin.product.category.create',
        'uses' => 'CategoryController@create',
        'middleware' => 'can:product.categories.create'
    ]);
    $router->post('categories', [
        'as' => 'admin.product.category.store',
        'uses' => 'CategoryController@store',
        'middleware' => 'can:product.categories.create'
    ]);
    $router->get('categories/{categoryP}/edit', [
        'as' => 'admin.product.category.edit',
        'uses' => 'CategoryController@edit',
        'middleware' => 'can:product.categories.edit'
    ]);
    $router->put('categories/{categoryP}', [
        'as' => 'admin.product.category.update',
        'uses' => 'CategoryController@update',
        'middleware' => 'can:product.categories.edit'
    ]);
    $router->delete('categories/{categoryP}', [
        'as' => 'admin.product.category.destroy',
        'uses' => 'CategoryController@destroy',
        'middleware' => 'can:product.categories.destroy'
    ]);
    $router->bind('product', function ($id) {
        return app('Modules\Product\Repositories\ProductRepository')->find($id);
    });
    $router->get('products', [
        'as' => 'admin.product.product.index',
        'uses' => 'ProductController@index',
        'middleware' => 'can:product.products.index'
    ]);
    $router->get('products/create', [
        'as' => 'admin.product.product.create',
        'uses' => 'ProductController@create',
        'middleware' => 'can:product.products.create'
    ]);
    $router->post('products', [
        'as' => 'admin.product.product.store',
        'uses' => 'ProductController@store',
        'middleware' => 'can:product.products.create'
    ]);
    $router->get('products/{product}/edit', [
        'as' => 'admin.product.product.edit',
        'uses' => 'ProductController@edit',
        'middleware' => 'can:product.products.edit'
    ]);
    $router->put('products/{product}', [
        'as' => 'admin.product.product.update',
        'uses' => 'ProductController@update',
        'middleware' => 'can:product.products.edit'
    ]);
    $router->delete('products/{product}', [
        'as' => 'admin.product.product.destroy',
        'uses' => 'ProductController@destroy',
        'middleware' => 'can:product.products.destroy'
    ]);
    $router->bind('color', function ($id) {
        return app('Modules\Product\Repositories\ColorRepository')->find($id);
    });
    $router->get('colors', [
        'as' => 'admin.product.color.index',
        'uses' => 'ColorController@index',
        'middleware' => 'can:product.colors.index'
    ]);
    $router->get('addColorAjax', [
        'as' => 'admin.product.color.addColorAjax',
        'uses' => 'ColorController@addColorAjax',
        'middleware' => 'can:product.colors.index'
    ]);
    $router->get('colors/create', [
        'as' => 'admin.product.color.create',
        'uses' => 'ColorController@create',
        'middleware' => 'can:product.colors.create'
    ]);
    $router->post('colors', [
        'as' => 'admin.product.color.store',
        'uses' => 'ColorController@store',
        'middleware' => 'can:product.colors.create'
    ]);

    $router->post('colors-ajax', [
        'as' => 'admin.product.color.storeAjax',
        'uses' => 'ColorController@storeAjax'
    ]);

    $router->get('colors/{color}/edit', [
        'as' => 'admin.product.color.edit',
        'uses' => 'ColorController@edit',
        'middleware' => 'can:product.colors.edit'
    ]);
    $router->put('colors/{color}', [
        'as' => 'admin.product.color.update',
        'uses' => 'ColorController@update',
        'middleware' => 'can:product.colors.edit'
    ]);
    $router->delete('colors/{color}', [
        'as' => 'admin.product.color.destroy',
        'uses' => 'ColorController@destroy',
        'middleware' => 'can:product.colors.destroy'
    ]);
    $router->bind('size', function ($id) {
        return app('Modules\Product\Repositories\SizeRepository')->find($id);
    });
    $router->get('sizes', [
        'as' => 'admin.product.size.index',
        'uses' => 'SizeController@index',
        'middleware' => 'can:product.sizes.index'
    ]);

    $router->get('addSizeAjax', [
        'as' => 'admin.product.size.addSizeAjax',
        'uses' => 'SizeController@addSizeAjax',
        'middleware' => 'can:product.sizes.index'
    ]);

    $router->get('sizes/create', [
        'as' => 'admin.product.size.create',
        'uses' => 'SizeController@create',
        'middleware' => 'can:product.sizes.create'
    ]);
    $router->post('sizes', [
        'as' => 'admin.product.size.store',
        'uses' => 'SizeController@store',
        'middleware' => 'can:product.sizes.create'
    ]);

    $router->post('sizes-ajax', [
        'as' => 'admin.product.size.storeAjax',
        'uses' => 'SizeController@storeAjax'
    ]);

    $router->get('sizes/{size}/edit', [
        'as' => 'admin.product.size.edit',
        'uses' => 'SizeController@edit',
        'middleware' => 'can:product.sizes.edit'
    ]);
    $router->put('sizes/{size}', [
        'as' => 'admin.product.size.update',
        'uses' => 'SizeController@update',
        'middleware' => 'can:product.sizes.edit'
    ]);
    $router->delete('sizes/{size}', [
        'as' => 'admin.product.size.destroy',
        'uses' => 'SizeController@destroy',
        'middleware' => 'can:product.sizes.destroy'
    ]);
    $router->get('sizes/{id_size}', [
        'as' => 'admin.product.size.destroyget',
        'uses' => 'SizeController@destroy',
        'middleware' => 'can:product.sizes.destroy'
    ]);
    $router->bind('productcate', function ($id) {
        return app('Modules\Product\Repositories\ProductcateRepository')->find($id);
    });
    $router->get('productcates', [
        'as' => 'admin.product.productcate.index',
        'uses' => 'ProductcateController@index',
        'middleware' => 'can:product.productcates.index'
    ]);
    $router->get('productcates/create', [
        'as' => 'admin.product.productcate.create',
        'uses' => 'ProductcateController@create',
        'middleware' => 'can:product.productcates.create'
    ]);
    $router->post('productcates', [
        'as' => 'admin.product.productcate.store',
        'uses' => 'ProductcateController@store',
        'middleware' => 'can:product.productcates.create'
    ]);
    $router->get('productcates/{productcate}/edit', [
        'as' => 'admin.product.productcate.edit',
        'uses' => 'ProductcateController@edit',
        'middleware' => 'can:product.productcates.edit'
    ]);
    $router->put('productcates/{productcate}', [
        'as' => 'admin.product.productcate.update',
        'uses' => 'ProductcateController@update',
        'middleware' => 'can:product.productcates.edit'
    ]);
    $router->delete('productcates/{productcate}', [
        'as' => 'admin.product.productcate.destroy',
        'uses' => 'ProductcateController@destroy',
        'middleware' => 'can:product.productcates.destroy'
    ]);
    $router->bind('procolor', function ($id) {
        return app('Modules\Product\Repositories\ProcolorRepository')->find($id);
    });
    $router->get('procolors', [
        'as' => 'admin.product.procolor.index',
        'uses' => 'ProcolorController@index',
        'middleware' => 'can:product.procolors.index'
    ]);
    $router->get('procolors/create', [
        'as' => 'admin.product.procolor.create',
        'uses' => 'ProcolorController@create',
        'middleware' => 'can:product.procolors.create'
    ]);
    $router->post('procolors', [
        'as' => 'admin.product.procolor.store',
        'uses' => 'ProcolorController@store',
        'middleware' => 'can:product.procolors.create'
    ]);
    $router->get('procolors/{procolor}/edit', [
        'as' => 'admin.product.procolor.edit',
        'uses' => 'ProcolorController@edit',
        'middleware' => 'can:product.procolors.edit'
    ]);
    $router->put('procolors/{procolor}', [
        'as' => 'admin.product.procolor.update',
        'uses' => 'ProcolorController@update',
        'middleware' => 'can:product.procolors.edit'
    ]);
    $router->delete('procolors/{procolor}', [
        'as' => 'admin.product.procolor.destroy',
        'uses' => 'ProcolorController@destroy',
        'middleware' => 'can:product.procolors.destroy'
    ]);
    $router->bind('prosize', function ($id) {
        return app('Modules\Product\Repositories\ProsizeRepository')->find($id);
    });
    $router->get('prosizes', [
        'as' => 'admin.product.prosize.index',
        'uses' => 'ProsizeController@index',
        'middleware' => 'can:product.prosizes.index'
    ]);
    $router->get('prosizes/create', [
        'as' => 'admin.product.prosize.create',
        'uses' => 'ProsizeController@create',
        'middleware' => 'can:product.prosizes.create'
    ]);
    $router->post('prosizes', [
        'as' => 'admin.product.prosize.store',
        'uses' => 'ProsizeController@store',
        'middleware' => 'can:product.prosizes.create'
    ]);
    $router->get('prosizes/{prosize}/edit', [
        'as' => 'admin.product.prosize.edit',
        'uses' => 'ProsizeController@edit',
        'middleware' => 'can:product.prosizes.edit'
    ]);
    $router->put('prosizes/{prosize}', [
        'as' => 'admin.product.prosize.update',
        'uses' => 'ProsizeController@update',
        'middleware' => 'can:product.prosizes.edit'
    ]);
    $router->delete('prosizes/{prosize}', [
        'as' => 'admin.product.prosize.destroy',
        'uses' => 'ProsizeController@destroy',
        'middleware' => 'can:product.prosizes.destroy'
    ]);
    $router->bind('sizeprice', function ($id) {
        return app('Modules\Product\Repositories\SizepriceRepository')->find($id);
    });
    $router->get('sizeprices', [
        'as' => 'admin.product.sizeprice.index',
        'uses' => 'SizepriceController@index',
        'middleware' => 'can:product.sizeprices.index'
    ]);
    $router->get('sizeprices/create', [
        'as' => 'admin.product.sizeprice.create',
        'uses' => 'SizepriceController@create',
        'middleware' => 'can:product.sizeprices.create'
    ]);
    $router->post('sizeprices', [
        'as' => 'admin.product.sizeprice.store',
        'uses' => 'SizepriceController@store',
        'middleware' => 'can:product.sizeprices.create'
    ]);
    $router->get('sizeprices/{sizeprice}/edit', [
        'as' => 'admin.product.sizeprice.edit',
        'uses' => 'SizepriceController@edit',
        'middleware' => 'can:product.sizeprices.edit'
    ]);
    $router->put('sizeprices/{sizeprice}', [
        'as' => 'admin.product.sizeprice.update',
        'uses' => 'SizepriceController@update',
        'middleware' => 'can:product.sizeprices.edit'
    ]);
    $router->delete('sizeprices/{sizeprice}', [
        'as' => 'admin.product.sizeprice.destroy',
        'uses' => 'SizepriceController@destroy',
        'middleware' => 'can:product.sizeprices.destroy'
    ]);
    $router->bind('promotion', function ($id) {
        return app('Modules\Product\Repositories\promotionRepository')->find($id);
    });
    $router->get('promotions', [
        'as' => 'admin.product.promotion.index',
        'uses' => 'promotionController@index',
        'middleware' => 'can:product.promotions.index'
    ]);
    $router->get('promotions/create', [
        'as' => 'admin.product.promotion.create',
        'uses' => 'promotionController@create',
        'middleware' => 'can:product.promotions.create'
    ]);
    $router->post('promotions', [
        'as' => 'admin.product.promotion.store',
        'uses' => 'promotionController@store',
        'middleware' => 'can:product.promotions.create'
    ]);
    $router->get('promotions/{promotion}/edit', [
        'as' => 'admin.product.promotion.edit',
        'uses' => 'promotionController@edit',
        'middleware' => 'can:product.promotions.edit'
    ]);
    $router->put('promotions/{promotion}', [
        'as' => 'admin.product.promotion.update',
        'uses' => 'promotionController@update',
        'middleware' => 'can:product.promotions.edit'
    ]);
    $router->delete('promotions/{promotion}', [
        'as' => 'admin.product.promotion.destroy',
        'uses' => 'promotionController@destroy',
        'middleware' => 'can:product.promotions.destroy'
    ]);
   
// append










});
