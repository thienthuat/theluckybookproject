<?php

namespace Modules\Product\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateProductRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'category_id' => 'required'
        ];
    }

    public function translationRules()
    {
        $id = $this->route()->parameter('product')->id;
        return [
            'title' => 'required|max:255',
            "slug" => "required|unique:product__product_translations,slug,$id,product_id,locale,$this->localeKey",
            'price' => 'required',
            'SKU' => "required|unique:product__product_translations,SKU,$id,product_id,locale,$this->localeKey"
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Category is required'
        ];
    }

    public function translationMessages()
    {
        return [
            'SKU.unique' => 'SKU exist in system.',
            'slug.unique' => 'slug exist in system.',
            'title.required' => 'Title is required',
            'price.required' => 'Price is required',
            'SKU.required' => 'SKU is required',
            'slug.required' => 'Slug is required',
        ];
    }
}
