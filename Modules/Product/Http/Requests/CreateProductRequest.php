<?php

namespace Modules\Product\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateProductRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'category_id' => 'required'
        ];
    }

    public function translationRules()
    {
        return [
            'title'=> 'required',
            'slug' => "required|unique:product__product_translations,slug,null,product_id,locale,$this->localeKey",
            'price'=> 'required',
            'SKU'=>"required|unique:product__product_translations,SKU,null,product_id,locale,$this->localeKey"
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'category_id.required'=>'Category is required'
        ];
    }

    public function translationMessages()
    {
        return [
            'SKU.unique'=>'SKU exist in system.',
            'slug.unique'=>'slug exist in system.',
            'title.required'=>'Title is required',
            'price.required'=>'Price is required',
            'SKU.required'=>'SKU is required',
            'slug.required'=>'Slug is required',
        ];
    }
}
