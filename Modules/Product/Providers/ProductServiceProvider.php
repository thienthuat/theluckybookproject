<?php

namespace Modules\Product\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Product\Entities\Product;
use Modules\Product\Events\Handlers\RegisterProductSidebar;
use Modules\Tag\Repositories\TagManager;


class ProductServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterProductSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('product', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->app[TagManager::class]->registerNamespace(new Product());
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Product\Repositories\CategoryRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentCategoryRepository(new \Modules\Product\Entities\Category());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheCategoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\ProductRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentProductRepository(new \Modules\Product\Entities\Product());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheProductDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\ColorRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentColorRepository(new \Modules\Product\Entities\Color());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheColorDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\SizeRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentSizeRepository(new \Modules\Product\Entities\Size());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheSizeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\ProductcateRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentProductcateRepository(new \Modules\Product\Entities\Productcate());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheProductcateDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\ProcolorRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentProcolorRepository(new \Modules\Product\Entities\Procolor());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheProcolorDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\ProsizeRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentProsizeRepository(new \Modules\Product\Entities\Prosize());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheProsizeDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\SizepriceRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentSizepriceRepository(new \Modules\Product\Entities\Sizeprice());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheSizepriceDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\promotionRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentpromotionRepository(new \Modules\Product\Entities\promotion());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CachepromotionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Product\Repositories\BreadcrumbRepository',
            function () {
                $repository = new \Modules\Product\Repositories\Eloquent\EloquentBreadcrumbRepository(new \Modules\Product\Entities\Breadcrumb());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Product\Repositories\Cache\CacheBreadcrumbDecorator($repository);
            }
        );
// add bindings










    }
}
