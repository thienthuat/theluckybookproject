<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/slider'], function (Router $router) {
    $router->bind('slider', function ($id) {
        return app('Modules\Slider\Repositories\SliderRepository')->find($id);
    });
    $router->get('sliders', [
        'as' => 'admin.slider.slider.index',
        'uses' => 'SliderController@index',
        'middleware' => 'can:slider.sliders.index'
    ]);
    $router->get('sliders/create', [
        'as' => 'admin.slider.slider.create',
        'uses' => 'SliderController@create',
        'middleware' => 'can:slider.sliders.create'
    ]);
    $router->post('sliders', [
        'as' => 'admin.slider.slider.store',
        'uses' => 'SliderController@store',
        'middleware' => 'can:slider.sliders.create'
    ]);
    $router->get('sliders/{slider}/edit', [
        'as' => 'admin.slider.slider.edit',
        'uses' => 'SliderController@edit',
        'middleware' => 'can:slider.sliders.edit'
    ]);
    $router->put('sliders/{slider}', [
        'as' => 'admin.slider.slider.update',
        'uses' => 'SliderController@update',
        'middleware' => 'can:slider.sliders.edit'
    ]);
    $router->delete('sliders/{slider}', [
        'as' => 'admin.slider.slider.destroy',
        'uses' => 'SliderController@destroy',
        'middleware' => 'can:slider.sliders.destroy'
    ]);
// append

});
