<?php

namespace Modules\Slider\Events;

use Modules\Media\Contracts\DeletingMedia;

class SliderWasDeleted implements DeletingMedia
{
    /**
     * @var string
     */
    private $sliderClass;
    /**
     * @var int
     */
    private $sliderId;

    public function __construct($sliderId, $sliderClass)
    {
        $this->sliderClass = $sliderClass;
        $this->sliderId = $sliderId;
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->sliderId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return $this->sliderClass;
    }
}