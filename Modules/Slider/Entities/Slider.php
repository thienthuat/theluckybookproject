<?php

namespace Modules\Slider\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Slider extends Model
{
    use Translatable, MediaRelation;

    protected $table = 'slider__sliders';
    public $translatedAttributes = ['title', 'link', 'text_link', 'sumary'];
    protected $fillable = ['status', 'title', 'link', 'text_link', 'sumary'];

    public function getThumbnailAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'image_slider')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }
}
