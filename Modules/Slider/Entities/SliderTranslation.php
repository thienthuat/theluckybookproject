<?php

namespace Modules\Slider\Entities;

use Illuminate\Database\Eloquent\Model;

class SliderTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'link', 'text_link', 'sumary'];
    protected $table = 'slider__slider_translations';
}
