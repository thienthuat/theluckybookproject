<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        <?php $old = $slider->hasTranslation($lang) ? $slider->translate($lang)->title : '' ?>
        {!! Form::label("{$lang}[title]", trans('slider::sliders.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}[title]",$old), ['class' => 'form-control', 'placeholder' => trans('slider::sliders.form.title')]) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.sumary") ? ' has-error' : '' }}'>
        <?php $old = $slider->hasTranslation($lang) ? $slider->translate($lang)->sumary : '' ?>
        {!! Form::label("{$lang}[sumary]", trans('slider::sliders.form.sumary')) !!}
        {!! Form::text("{$lang}[sumary]", old("{$lang}[sumary]",$old), ['class' => 'form-control', 'placeholder' => trans('slider::sliders.form.sumary')]) !!}
        {!! $errors->first("{$lang}.sumary", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.link") ? ' has-error' : '' }}'>
        <?php $old = $slider->hasTranslation($lang) ? $slider->translate($lang)->link : '' ?>
        {!! Form::label("{$lang}[link]", trans('slider::sliders.form.link')) !!}
        {!! Form::text("{$lang}[link]", old("{$lang}[link]",$old), ['class' => 'form-control', 'placeholder' => trans('slider::sliders.form.link')]) !!}
        {!! $errors->first("{$lang}.link", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("{$lang}.text_link") ? ' has-error' : '' }}'>
        <?php $old = $slider->hasTranslation($lang) ? $slider->translate($lang)->text_link : '' ?>
        {!! Form::label("{$lang}[text_link]", trans('slider::sliders.form.text_link')) !!}
        {!! Form::text("{$lang}[text_link]", old("{$lang}[text_link]",$old), ['class' => 'form-control', 'placeholder' => trans('slider::sliders.form.text_link')]) !!}
        {!! $errors->first("{$lang}.text_link", '<span class="help-block">:message</span>') !!}
    </div>
</div>
