<?php

namespace Modules\Slider\Repositories\Eloquent;

use Modules\Slider\Repositories\SliderRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

use Modules\Slider\Events\SliderWasCreated;
use Modules\Slider\Events\SliderWasUpdated;
use Modules\Slider\Events\SliderWasDeleted;

use Illuminate\Database\Eloquent\Builder;

class EloquentSliderRepository extends EloquentBaseRepository implements SliderRepository
{
    /**
     * Create a Slider
     * @param  array $data
     * @return mixed
     */
    public function create($data)
    {
        $Slider = $this->model->create($data);

        event(new SliderWasCreated($Slider, $data));

        return $Slider;
    }
    /**
     * Update a resource
     * @param $Slider
     * @param  array $data
     * @return mixed
     */
    public function update($Slider, $data)
    {
        $Slider->update($data);

        event(new SliderWasUpdated($Slider, $data));

        return $Slider;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function destroy($model)
    {
        event(new SliderWasDeleted($model->id, get_class($model)));

        return $model->delete();
    }

    /**
     * Find a resource by the given slug
     *
     * @param  string $slug
     * @return object
     */
    public function findBySlug($slug)
    {
        return $this->model->whereHas('translations', function (Builder $q) use ($slug) {
            $q->where('slug', $slug);
        })->with('translations')->firstOrFail();
    }
}
