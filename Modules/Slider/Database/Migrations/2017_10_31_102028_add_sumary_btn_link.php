<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSumaryBtnLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slider__slider_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->text('sumary')->after('title')->nullable();
            $table->string('text_link')->after('sumary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slider__slider_translations', function (Blueprint $table) {
            $table->dropColumn('sumary');
            $table->dropColumn('text_link');
        });
    }
}
