<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Blog\Repositories\PostRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Repositories\ProductcateRepository;
use Modules\Ads\Repositories\AdsRepository;

class PublicController extends BasePublicController
{
    /**
     * @var PageRepository
     */
    private $page;
    /**
     * @var Application
     */
    private $app;
    private $sliderRepository;
    private $categoryProductRepository;
    private $productRepository;
    private $postRepository;
    private $adsRepository;

    public function __construct(
        PageRepository $page,
        Application $app,
        SliderRepository $sliderRepository,
        ProductRepository $productRepository,
        PostRepository $postRepository,
        ProductcateRepository $productcateRepository,
        CategoryRepository $categoryProductRepository,
        AdsRepository $adsRepository
    ) {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
        $this->sliderRepository = $sliderRepository;
        $this->productRepository = $productRepository;
        $this->postRepository = $postRepository;
        $this->productcateRepository = $productcateRepository;
        $this->categoryProductRepository = $categoryProductRepository;
        $this->adsRepository = $adsRepository;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug)
    {
        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);
        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    public function contact()
    {
        $page = $this->findPageForSlug(trans('frontend::url.page_contact'));

        $this->throw404IfNotFound($page);
        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    public function about()
    {
        $page = $this->findPageForSlug(trans('frontend::url.page_about'));

        $this->throw404IfNotFound($page);
        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage()
    {
        $page = $this->page->findHomepage();
        $this->throw404IfNotFound($page);
        $sliders = $this->sliderRepository->all();
        $ads = $this->adsRepository->getAdsHomepage(6);
        $template = $this->getTemplateForPage($page);
        $categories = $this->categoryProductRepository->getCategoryIsHome();
        $blogs = $this->postRepository->latest();
        $product_hots = $this->productRepository->getProductByAction('is_hot', 100);
        $product_news = $this->productRepository->getProductByAction('is_new', 100);
        $product_sales = $this->productRepository->getProductByAction('is_sale', 100);
        $product_sellings = $this->productRepository->getProductByAction('is_selling', 100);
        return view($template, compact('page', 'sliders', 'categories', 'product_hots', 'blogs', 'product_news', 'product_sales', 'product_sellings','ads'));
    }

    public function detailProduct($slug)
    {
        $page = $this->findPageForSlug($slug);
        if ($page) {
            $template = $this->getTemplateForPage($page);
            if ($template == 'pages.products.status.sale') {
                $products = $this->productRepository->getProductByActionPagination('is_sale');
                return view($template, compact('page', 'products'));
            } else if ($template == 'pages.products.status.new') {
                $products = $this->productRepository->getProductByActionPagination('is_new');
                return view($template, compact('page', 'products'));
            } else if ($template == 'pages.products.status.hot') {
                $products = $this->productRepository->getProductByActionPagination('is_sale');
                return view($template, compact('page', 'products'));
            } else if ($template == 'pages.products.status.selling') {
                $products = $this->productRepository->getProductByActionPagination('is_selling');
                return view($template, compact('page', 'products'));
            } else {
                return view($template, compact('page'));
            }
        } else {
            $product = $this->productRepository->findBySlug($slug);
            if ($product) {
                $cateOne = null;
                $cateTwo = null;
                $arrCate = $product->productCate->pluck('parent_id')->toArray();
                $product_relateds = $product->productRelations;
                $breadcrumbs = $product->breadcrumbs;
                return view('pages.products.detail', compact('product', 'product_relateds','breadcrumbs'));
            } else {
                return redirect()->route('page.404');
            }
        }
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug)
    {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page)
    {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }
}
