<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (!App::runningInConsole()) {
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
        'middleware' => config('asgard.page.config.middleware'),
    ]);
    $router->get(trans('frontend::url.page_contact'), [
        'uses' => 'PublicController@contact',
        'as' => 'page.contact',
    ]);
    $router->get(trans('frontend::url.page_about'), [
        'uses' => 'PublicController@about',
        'as' => 'page.about',
    ]);
    $router->any('/{slug}', [
        'uses' => 'PublicController@detailProduct',
        'as' => 'page.detailProduct'
    ]);
    $router->get('page/{slug}', [
        'uses' => 'PublicController@uri',
        'as' => 'page'
    ]);
}
