<?php

namespace Modules\Emailmarketing\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Emailmarketing\Events\Handlers\RegisterEmailmarketingSidebar;

class EmailmarketingServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterEmailmarketingSidebar::class);
    }

    public function boot()
    {
        $this->publishConfig('emailmarketing', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Emailmarketing\Repositories\EmailmarketingRepository',
            function () {
                $repository = new \Modules\Emailmarketing\Repositories\Eloquent\EloquentEmailmarketingRepository(new \Modules\Emailmarketing\Entities\Emailmarketing());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Emailmarketing\Repositories\Cache\CacheEmailmarketingDecorator($repository);
            }
        );
// add bindings

    }
}
