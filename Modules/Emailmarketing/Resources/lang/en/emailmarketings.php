<?php

return [
    'list resource' => 'List emailmarketings',
    'create resource' => 'Create emailmarketings',
    'edit resource' => 'Edit emailmarketings',
    'destroy resource' => 'Destroy emailmarketings',
    'title' => [
        'emailmarketings' => 'Emailmarketing',
        'create emailmarketing' => 'Create a emailmarketing',
        'edit emailmarketing' => 'Edit a emailmarketing',
    ],
    'button' => [
        'create emailmarketing' => 'Create a emailmarketing',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
