<?php

return [
    'list resource' => 'List Email Marketings',
    'create resource' => 'Create Email Marketings',
    'edit resource' => 'Edit Email Marketings',
    'destroy resource' => 'Destroy Email Marketings',
    'title' => [
        'emailmarketings' => 'Email Marketings',
        'create emailmarketing' => 'Create a Email Marketings',
        'edit emailmarketing' => 'Edit a Email Marketings',
    ],
    'button' => [
        'create emailmarketing' => 'Create a Email Marketings',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
