<?php

namespace Modules\Emailmarketing\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Emailmarketing extends Model
{
    protected $table = 'emailmarketing__emailmarketings';
    protected $fillable = ['email','checked'];
}
