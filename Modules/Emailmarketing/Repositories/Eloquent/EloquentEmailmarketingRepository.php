<?php

namespace Modules\Emailmarketing\Repositories\Eloquent;

use Modules\Emailmarketing\Repositories\EmailmarketingRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEmailmarketingRepository extends EloquentBaseRepository implements EmailmarketingRepository
{
}
