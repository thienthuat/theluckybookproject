<?php

namespace Modules\Emailmarketing\Repositories\Cache;

use Modules\Emailmarketing\Repositories\EmailmarketingRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEmailmarketingDecorator extends BaseCacheDecorator implements EmailmarketingRepository
{
    public function __construct(EmailmarketingRepository $emailmarketing)
    {
        parent::__construct();
        $this->entityName = 'emailmarketing.emailmarketings';
        $this->repository = $emailmarketing;
    }
}
