<?php

namespace Modules\Emailmarketing\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface EmailmarketingRepository extends BaseRepository
{
}
