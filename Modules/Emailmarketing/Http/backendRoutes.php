<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/emailmarketing'], function (Router $router) {
    $router->bind('emailmarketing', function ($id) {
        return app('Modules\Emailmarketing\Repositories\EmailmarketingRepository')->find($id);
    });
    $router->get('emailmarketings', [
        'as' => 'admin.emailmarketing.emailmarketing.index',
        'uses' => 'EmailmarketingController@index',
        'middleware' => 'can:emailmarketing.emailmarketings.index'
    ]);
    $router->get('emailmarketings/create', [
        'as' => 'admin.emailmarketing.emailmarketing.create',
        'uses' => 'EmailmarketingController@create',
        'middleware' => 'can:emailmarketing.emailmarketings.create'
    ]);
    $router->post('emailmarketings', [
        'as' => 'admin.emailmarketing.emailmarketing.store',
        'uses' => 'EmailmarketingController@store',
        'middleware' => 'can:emailmarketing.emailmarketings.create'
    ]);
    $router->get('emailmarketings/{emailmarketing}/edit', [
        'as' => 'admin.emailmarketing.emailmarketing.edit',
        'uses' => 'EmailmarketingController@edit',
        'middleware' => 'can:emailmarketing.emailmarketings.edit'
    ]);
    $router->put('emailmarketings/{emailmarketing}', [
        'as' => 'admin.emailmarketing.emailmarketing.update',
        'uses' => 'EmailmarketingController@update',
        'middleware' => 'can:emailmarketing.emailmarketings.edit'
    ]);
    $router->delete('emailmarketings/{emailmarketing}', [
        'as' => 'admin.emailmarketing.emailmarketing.destroy',
        'uses' => 'EmailmarketingController@destroy',
        'middleware' => 'can:emailmarketing.emailmarketings.destroy'
    ]);
// append

});
