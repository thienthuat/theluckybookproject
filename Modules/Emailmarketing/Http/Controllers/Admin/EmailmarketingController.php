<?php

namespace Modules\Emailmarketing\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Emailmarketing\Entities\Emailmarketing;
use Modules\Emailmarketing\Http\Requests\CreateEmailmarketingRequest;
use Modules\Emailmarketing\Http\Requests\UpdateEmailmarketingRequest;
use Modules\Emailmarketing\Repositories\EmailmarketingRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class EmailmarketingController extends AdminBaseController
{
    /**
     * @var EmailmarketingRepository
     */
    private $emailmarketing;

    public function __construct(EmailmarketingRepository $emailmarketing)
    {
        parent::__construct();

        $this->emailmarketing = $emailmarketing;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $emailmarketings = $this->emailmarketing->all();

        return view('emailmarketing::admin.emailmarketings.index', compact('emailmarketings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('emailmarketing::admin.emailmarketings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEmailmarketingRequest $request
     * @return Response
     */
    public function store(CreateEmailmarketingRequest $request)
    {
        $this->emailmarketing->create($request->all());

        return redirect()->route('admin.emailmarketing.emailmarketing.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('emailmarketing::emailmarketings.title.emailmarketings')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Emailmarketing $emailmarketing
     * @return Response
     */
    public function edit(Emailmarketing $emailmarketing)
    {
        return view('emailmarketing::admin.emailmarketings.edit', compact('emailmarketing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Emailmarketing $emailmarketing
     * @param  UpdateEmailmarketingRequest $request
     * @return Response
     */
    public function update(Emailmarketing $emailmarketing, UpdateEmailmarketingRequest $request)
    {
        $this->emailmarketing->update($emailmarketing, $request->all());

        return redirect()->route('admin.emailmarketing.emailmarketing.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('emailmarketing::emailmarketings.title.emailmarketings')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Emailmarketing $emailmarketing
     * @return Response
     */
    public function destroy(Emailmarketing $emailmarketing)
    {
        $this->emailmarketing->destroy($emailmarketing);

        return redirect()->route('admin.emailmarketing.emailmarketing.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('emailmarketing::emailmarketings.title.emailmarketings')]));
    }
}
