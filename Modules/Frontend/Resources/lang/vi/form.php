<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 10/30/17
 * Time: 1:00 PM
 */
return [
    'first_name' => 'Tên',
    'last_name' => 'Họ và tên đệm',
    'phone' => 'Số điện thoại',
    'addres' => 'Địa chỉ',
    'city' => 'Thành phố',
    'edit' => 'Chỉnh sửa',
    'save' => 'Lưu',
    'current_password' => 'Mật khẩu hiện tại',
    'new_password' => 'Mật khẩu mới',
    'confirm_new_password' => 'Nhập lại mật khẩu',
];