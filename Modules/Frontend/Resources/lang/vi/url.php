<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 10/29/17
 * Time: 8:41 PM
 */
return [
    'login' => 'dang-nhap',
    'register' => 'dang-ky',
    'logout' => 'dang-xuat',
    'forgot_password' => 'quen-mat-khau',
    'reset_password' => 'dat-lai-mat-khau',
    'page_contact' => 'lien-he',
    'page_about' => 'gioi-thieu-ve-theluckybook',
    'page_product' => 'san-pham',
    'page_checkout_success' => 'thanh-toan-thanh-cong',
    'page_category_product' => 'danh-muc',
    'page_product_promotion' => 'khuyen-mai',
];