<?php
/**
 * Created by PhpStorm.
 * User: nguyenlinh
 * Date: 10/29/17
 * Time: 9:03 AM
 */
return [
    'homepage' => 'Home',
    'my_account' => 'Tài Khoản',
    'login' => 'Đăng Nhập',
    'logout' => 'Đăng Xuất',
    'new_customer' => 'Khách hàng mới',
    'note_new_customer' => 'Tạo tài khoản có nhiều lợi ích: kiểm tra nhanh hơn, giữ nhiều địa chỉ, theo dõi đơn đặt hàng và hơn thế nữa.',
    'registed_customer' => 'Khách hàng đã đăng ký',
    'note_registed_customer' => 'Nếu bạn có tài khoản thì đăng nhập bằng email của bạn',
    'forgot_password' => 'Quên mật khẩu ?',
    'new_register' => 'Đăng ký mới',
    'register' => 'Đăng ký',
    'personal_info' => 'Thông tin cá nhân',
    'signin_info' => 'Thông tin đăng nhập',
    'back' => 'Trở về',
    'send' => 'Gửi',
    'my_wish_list' => 'Sản phẩm yêu thích',
    'my_order' => 'Đơn hàng',
    'my_account_dashboard' => 'Bảng điều khiển tài khoản',
    'my_account_info' => 'Thông tin tài khoản',
    'my_address_manager' => 'Sổ địa chỉ',
    'more_address' => 'Bổ sung địa chỉ giao hàng',
    'add_address' => 'Thêm địa chỉ mới',
    'my_dashboard' => 'Bảng điều khiển của tôi',
    'address_book' => 'Danh sách địa chỉ',
    'default_shipping_address' => 'Địa chỉ giao hành mặc định',
    'default_billing_address' => 'Địa chỉ thanh toán mặc định',
    'default_billing_address_alert' => 'Bạn chưa đặt địa chỉ thanh toán mặc định.',
    'default_shipping_address_alert' => 'Bạn chưa đặt địa chỉ giao hàng mặc định.',
    'change_password' => 'Thay đổi mật khẩu',
    'note_forgot_password' => 'Vui lòng nhập địa chỉ email của bạn bên dưới để nhận liên kết đặt lại mật khẩu.',
    'trending_item' => 'Xu Hướng Thời Trang',
    'trending_item_note' => 'Bộ sưu tập các sản phẩm bán chạy và theo xu hướng thời trang của chúng tôi.',
    'hot_product' => 'Sản phẩm nỗi bật',
    'note_hot_product' => 'Chúng tôi mang theo thời trang thiết kế bán lẻ cho nhiều hơn nữa tại các cửa hàng khác.',
    'note_news' => 'Những bài viết hữu ích cho cuộc sống.'
];