<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Auth;
use Modules\Setting\Entities\Setting;
use Session;
use Hash;
use Mail;
use Hashids\Hashids;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Customer\Repositories\keyRepository;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Product\Repositories\CategoryRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Customer\Entities\Favourite;
use Modules\Emailmarketing\Repositories\EmailmarketingRepository;
use Modules\Location\Repositories\ProvinceRepository;
use Modules\Customer\Repositories\AddressRepository;
use Socialite;

class PublicController extends BasePublicController
{
    protected $app;
    protected $customerRepository;
    protected $hashids;
    protected $keyRepository;
    protected $settingRepository;
    protected $categoryRepository;
    protected $productRepository;
    protected $emailmarketingRepository;
    protected $provinceRepository;
    protected $addressRepository;

    public function __construct(Application $app,
                                CustomerRepository $customerRepository,
                                keyRepository $keyRepository,
                                CategoryRepository $categoryRepository,
                                ProductRepository $productRepository,
                                EmailmarketingRepository $emailmarketingRepository,
                                SettingRepository $settingRepository,
                                AddressRepository $addressRepository,
                                ProvinceRepository $provinceRepository)
    {
        $this->app = $app;
        $this->customerRepository = $customerRepository;
        $this->keyRepository = $keyRepository;
        $this->settingRepository = $settingRepository;
        $this->categoryRepository = $categoryRepository;
        $this->emailmarketingRepository = $emailmarketingRepository;
        $this->provinceRepository = $provinceRepository;
        $this->productRepository = $productRepository;
        $this->addressRepository = $addressRepository;
        $this->hashids = new Hashids('', 10, '123456789QƯERTYUIOPASDFGHJKLZXCVBNM');
    }

    public function header()
    {
        return view('partials.header');
    }

    public function footer()
    {
        return view('partials.footer');
    }

    public function menu()
    {
        $category = $this->categoryRepository->getMenuCategoryParent()->toArray();
        return view('partials.menu', compact('category'));
    }


    public function login()
    {
        $user = Auth::guard('customer')->user();
        if ($user) {
            return redirect()->route('page.customer.account');
        } else {
            return view('pages.accounts.login');
        }
    }

    public function logout()
    {
        //Auth::guard('customer')->logout();

        Auth::logout();

        Session::flush();
        return redirect()->route('homepage');
    }

    public function postLogin(Request $request)
    {
        $customer = $this->customerRepository->model()->where('email', strtolower($request->email))->first();
        if ($customer) {
            $remember = $request->remember != '' ? true : false;
            $credentials = array(
                'email' => strtolower($customer->email),
                'password' => $request->password
            );
            if (Auth::guard('customer')->attempt($credentials, $remember)) {
                Session::flash('success', "Đăng nhập thành công !");
                $IDProductAddToWishList = session('IDProductAddToWishList');
                if (isset($IDProductAddToWishList) && $IDProductAddToWishList != '') {
                    Favourite::updateOrCreate(['customer_id' => $customer->id, 'product_id' => $IDProductAddToWishList]);
                    return redirect()->route('page.customer.myWishList');
                } else {
                    return redirect()->intended();
                }
            } else {
                Session::flash('error', "Mật khẩu không chính xác");
                return redirect()->back()->withInput();
            }
        } else {
            Session::flash('error', "Email không tồn tại !");
            return redirect()->back()->withInput();
        }
    }

    public function postLoginModal(Request $request)
    {
        $customer = $this->customerRepository->model()->where('email', strtolower($request->email_login))->first();
        if ($customer) {
            $remember = $request->remember != '' ? true : false;
            $credentials = array(
                'email' => strtolower($customer->email),
                'password' => $request->password
            );
            if (Auth::guard('customer')->attempt($credentials, $remember)) {
                return response()->json(['status' => 200, 'message' => 'Đăng nhập thành công.']);
            } else {
                return response()->json(['status' => 404, 'message' => 'Mật khẩu không chính xác.']);
            }
        } else {
            return response()->json(['status' => 404, 'message' => 'Email không tồn tại !']);
        }
    }

    public function register()
    {
        $provinces = $this->provinceRepository->all();
        return view('pages.accounts.register', compact('provinces'));
    }

    public function postRegister(Request $request)
    {
        $customer = $this->customerRepository->model()->where('email', strtolower($request->email))->first();
        if (!$customer) {
            $data = [];
            $data['first_name'] = strip_tags($request->firstname);
            $data['last_name'] = strip_tags($request->lastname);
            $data['password'] = Hash::make(strip_tags($request->password));
            $data['email'] = strip_tags($request->email);
            $data['sex'] = strip_tags($request->sex);
            $data['birthday'] = strip_tags($request->birthday);
            $data['address'] = strip_tags($request->address);
            $data['city'] = strip_tags($request->province_id);
            $data['district'] = strip_tags($request->district_id);
            $data['ward'] = strip_tags($request->ward_id);
            $data['phone'] = strip_tags($request->phone);
            if ($data['first_name'] == '' || $data['last_name'] == '' ||
                $data['password'] == '' || $data['email'] == '' ||
                $data['sex'] == '' || $data['birthday'] == ''
            ) {
                Session::flash('error', "Hãy nhập đầy đủ thông tin.");
                return redirect()->back()->withInput();
            } else {
                $user = $this->customerRepository->create($data);
                if ($user) {
                    $user->code = $this->hashids->encode($user->id);
                    $user->save();
                    $data = [
                        'full_name' => strip_tags($request->lastname) . ' ' . strip_tags($request->firstname),
                        'city' => strip_tags($request->province_id),
                        'district' => strip_tags($request->district_id),
                        'wards' => strip_tags($request->ward_id),
                        'address' => strip_tags($request->address),
                        'phone' => strip_tags($request->phone),
                        'customer_id' => strip_tags($user->id)
                    ];
                    $this->addressRepository->create($data);
                    Auth::guard('customer')->login($user);
                    Session::flash('success', "Cảm ơn bạn đã đăng ký.");
                    return redirect()->route('page.customer.account');
                } else {
                    Session::flash('error', "Hệ thống bị lỗi.");
                    return redirect()->back()->withInput();
                }
            }
        } else {
            Session::flash('error', "Email đã tồn tại trong hệ thống.");
            return redirect()->back()->withInput();
        }
    }

    public function forgotPassword()
    {
        return view('pages.accounts.forgot-password');
    }

    public function postForgotPassword(Request $request)
    {
        $customer = $this->customerRepository->model()->where('email', strtolower($request->email))->first();
        if ($customer) {
            $cur_key = $customer->key()->first();
            if ($cur_key) {
                $key = $cur_key;
            } else {
                $key = $this->keyRepository->create(['key' => md5($request->email), 'customer_id' => $customer->id]);
            }
            $message = route('page.resetPassword', $key->key);
            $to = $customer->email;
            $from = $this->settingRepository->get('contact::email_revice') ? $this->settingRepository->get('contact::email_revice')->plainValue : '';
            $this->sendEmail($from, $to, trans('frontend::frontend.forgot_password') . ' ' . $_SERVER['HTTP_HOST'], $message);
            Session::flash('success', "Chúng tôi đã gửi một đường liên kết vào email " . $customer->email . ". Hãy kiểm tra email để thay đổi mật khẩu.");
            return redirect()->route('page.login');
        } else {
            Session::flash('error', "Email không tồn tại !");
            return redirect()->back()->withInput();
        }
    }

    public function resetPassword($token)
    {
        $key = $this->keyRepository->findByAttributes(['key' => $token]);
        if ($key) {
            return view('pages.accounts.reset-password', ['key' => $token]);
        } else {
            Session::flash('error', "Đường link không tồn tại hoặc đã hết hạn.");
            return redirect()->route('page.login');
        }
    }

    public function postResetPassword(Request $request)
    {
        $key = $this->keyRepository->findByAttributes(['key' => strip_tags($request->key_token)]);
        if ($key) {
            $customer = $this->customerRepository->find($key->customer_id);
            $customer->password = Hash::make(strip_tags($request->new_password));
            $customer->save();
            $this->keyRepository->destroy($key);
            Session::flash('success', "Đổi mật khẩu thành công hãy đăng nhập bằng mật khẩu mới.");
            return redirect()->route('page.login');
        } else {
            Session::flash('error', "Đường link không tồn tại hoặc đã hết hạn.");
            return redirect()->route('page.login');
        }
    }

    public function sendEmail($from, $to, $subject, $message)
    {
        Mail::send('emails.forgot-password', ['data' => $message], function ($m) use ($from, $to, $subject) {
            $m->from($from, $subject);
            $m->to($to)->subject($subject);;
        });
    }

    public function notFound404()
    {

    }

    public function emailMarketing(Request $request)
    {
        $email = $this->emailmarketingRepository->findByAttributes(['email' => strip_tags($request->email)]);
        if (!$email) {
            $this->emailmarketingRepository->create(['email' => strip_tags($request->email)]);
        }
        return response()->json(['status' => 200, 'message' => 'Cảm ơn bạn đã đăng ký nhận tin.']);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        if ($user) {
            $customer = $this->customerRepository->findByAttributes(['facebook_id' => $user->getId()]);
            if ($customer) {
                Auth::guard('customer')->login($customer);
                return redirect()->intended();
            } else {
                $data = [
                    'first_name' => $user->getName(),
                    'last_name' => $user->getName(),
                    'facebook_id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'password' => Hash::make('TUTUPETTI.VN1992')
                ];
                $customer = $this->customerRepository->create($data);
                if ($customer) {
                    $customer->code = $this->hashids->encode($customer->id);
                    $customer->save();
                    Auth::guard('customer')->login($customer);
                    Session::flash('success', "Đăng nhập thành công !");
                    return redirect()->intended();
                }
            }
        } else {
            return redirect()->route('page.login');
        }
    }
}
