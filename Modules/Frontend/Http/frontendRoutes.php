<?php

use Illuminate\Routing\Router;

/** @var Router $router */

if (!App::runningInConsole()) {
    $router->get(trans('frontend::url.login'), [
        'uses' => 'PublicController@login',
        'as' => 'page.login'
    ]);
    $router->get(trans('frontend::url.logout'), [
        'uses' => 'PublicController@logout',
        'as' => 'page.logout'
    ]);
    $router->post(trans('frontend::url.login'), [
        'uses' => 'PublicController@postLogin',
        'as' => 'page.postLogin'
    ]);
    $router->post('/login-modal', [
        'uses' => 'PublicController@postLoginModal',
        'as' => 'page.postLoginModal'
    ]);
    $router->get(trans('frontend::url.register'), [
        'uses' => 'PublicController@register',
        'as' => 'page.register'
    ]);

    $router->post(trans('frontend::url.register'), [
        'uses' => 'PublicController@postRegister',
        'as' => 'page.postRegister'
    ]);

    $router->post('/create-email-marketing', [
        'uses' => 'PublicController@emailMarketing',
        'as' => 'page.emailMarketing'
    ]);

    $router->get(trans('frontend::url.forgot_password'), [
        'uses' => 'PublicController@forgotPassword',
        'as' => 'page.forgotPassword'
    ]);

    $router->post(trans('frontend::url.forgot_password'), [
        'uses' => 'PublicController@postForgotPassword',
        'as' => 'page.postForgotPassword'
    ]);
    $router->get(trans('frontend::url.reset_password') . '/{token}', [
        'uses' => 'PublicController@resetPassword',
        'as' => 'page.resetPassword'
    ]);
    $router->post(trans('frontend::url.reset_password'), [
        'uses' => 'PublicController@postResetPassword',
        'as' => 'page.postResetPassword'
    ]);
    $router->get('dang-nhap-facebook', [
        'as' => 'page.login.facebook',
        'uses' => 'PublicController@redirectToProvider'
    ]);
    $router->get('/facebook', [
        'as' => 'page.login.getfacebook',
        'uses' => 'PublicController@handleProviderCallback'
    ]);
    $router->get('404', [
        'uses' => 'PublicController@notFound404',
        'as' => 'page.404'
    ]);
}

