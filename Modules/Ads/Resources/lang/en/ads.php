<?php

return [
    'list resource' => 'List ads',
    'create resource' => 'Create ads',
    'edit resource' => 'Edit ads',
    'destroy resource' => 'Destroy ads',
    'title' => [
        'ads' => 'Ads',
        'create ads' => 'Create a ads',
        'edit ads' => 'Edit a ads',
    ],
    'button' => [
        'create ads' => 'Create a ads',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
