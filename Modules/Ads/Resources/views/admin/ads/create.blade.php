@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('ads::ads.title.create ads') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.ads.ads.index') }}">{{ trans('ads::ads.title.ads') }}</a></li>
        <li class="active">{{ trans('ads::ads.title.create ads') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.ads.ads.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="box-body">
                        <div class='form-group {{ $errors->has("title") ? ' has-error' : '' }}'>
                            {!! Form::label("title", "Title" ) !!}
                            {!! Form::text("title", old("title"), ['class' => 'form-control', 'placeholder' => "Title Ads"]) !!}
                            {!! $errors->first("title", '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class='form-group {{ $errors->has("link") ? ' has-error' : '' }}'>
                            {!! Form::label("link", "Link" ) !!}
                            {!! Form::text("link", old("link"), ['class' => 'form-control', 'placeholder' => "Link Ads"]) !!}
                            {!! $errors->first("link", '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    
                    @mediaSingle('image_ads')

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.ads.ads.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.ads.ads.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
