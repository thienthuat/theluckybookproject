<?php

namespace Modules\Ads\Events;

use Modules\Ads\Entities\Ads;
use Modules\Media\Contracts\StoringMedia;

class AdsWasCreated implements StoringMedia
{
    /**
     * @var array
     */
    public $data;
    /**
     * @var Ads
     */
    public $ads;

    public function __construct($ads, array $data)
    {
        $this->data = $data;
        $this->ads = $ads;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity()
    {
        return $this->ads;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData()
    {
        return $this->data;
    }
}