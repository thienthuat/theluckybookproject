<?php

namespace Modules\Ads\Events;

use Modules\Media\Contracts\DeletingMedia;

class AdsWasDeleted implements DeletingMedia
{
    /**
     * @var string
     */
    private $AdsClass;
    /**
     * @var int
     */
    private $adsId;

    public function __construct($adsId, $adsClass)
    {
        $this->adsClass = $adsClass;
        $this->adsId = $adsId;
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->adsId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return $this->adsClass;
    }
}