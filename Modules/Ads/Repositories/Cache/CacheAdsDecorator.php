<?php

namespace Modules\Ads\Repositories\Cache;

use Modules\Ads\Repositories\AdsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAdsDecorator extends BaseCacheDecorator implements AdsRepository
{
    public function __construct(AdsRepository $ads)
    {
        parent::__construct();
        $this->entityName = 'ads.ads';
        $this->repository = $ads;
    }

    public function getAdsHomepage($limit = 6) {
        
    }
}
