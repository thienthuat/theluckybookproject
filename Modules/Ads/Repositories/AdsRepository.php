<?php

namespace Modules\Ads\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface AdsRepository extends BaseRepository
{
    public function getAdsHomepage($limit = 6);
}
