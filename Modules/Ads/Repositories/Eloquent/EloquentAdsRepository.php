<?php

namespace Modules\Ads\Repositories\Eloquent;

use Modules\Ads\Events\AdsWasCreated;
use Modules\Ads\Events\AdsWasDeleted;
use Modules\Ads\Events\AdsWasUpdated;
use Modules\Ads\Repositories\AdsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAdsRepository extends EloquentBaseRepository implements AdsRepository
{
    /**
     * Create a ads
     * @param  array $data
     * @return mixed
     */
    public function create($data)
    {
        $ads = $this->model->create($data);

        event(new AdsWasCreated($ads, $data));

        return $ads;
    }
    /**
     * Update a resource
     * @param $ads
     * @param  array $data
     * @return mixed
     */
    public function update($ads, $data)
    {
        $ads->update($data);

        event(new AdsWasUpdated($ads, $data));

        return $ads;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function destroy($model)
    {
        event(new AdsWasDeleted($model->id, get_class($model)));

        return $model->delete();
    }

    public function getAdsHomepage($limit = 6)
    {
        return $this->model->limit($limit)->orderBy('id', 'DESC')->get();
    }
}
