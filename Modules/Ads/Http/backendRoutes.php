<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/ads'], function (Router $router) {
    $router->bind('ads', function ($id) {
        return app('Modules\Ads\Repositories\AdsRepository')->find($id);
    });
    $router->get('ads', [
        'as' => 'admin.ads.ads.index',
        'uses' => 'AdsController@index',
        'middleware' => 'can:ads.ads.index'
    ]);
    $router->get('ads/create', [
        'as' => 'admin.ads.ads.create',
        'uses' => 'AdsController@create',
        'middleware' => 'can:ads.ads.create'
    ]);
    $router->post('ads', [
        'as' => 'admin.ads.ads.store',
        'uses' => 'AdsController@store',
        'middleware' => 'can:ads.ads.create'
    ]);
    $router->get('ads/{ads}/edit', [
        'as' => 'admin.ads.ads.edit',
        'uses' => 'AdsController@edit',
        'middleware' => 'can:ads.ads.edit'
    ]);
    $router->put('ads/{ads}', [
        'as' => 'admin.ads.ads.update',
        'uses' => 'AdsController@update',
        'middleware' => 'can:ads.ads.edit'
    ]);
    $router->delete('ads/{ads}', [
        'as' => 'admin.ads.ads.destroy',
        'uses' => 'AdsController@destroy',
        'middleware' => 'can:ads.ads.destroy'
    ]);
// append

});
