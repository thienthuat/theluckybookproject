<?php

namespace Modules\Ads\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Ads\Entities\Ads;
use Modules\Ads\Http\Requests\CreateAdsRequest;
use Modules\Ads\Http\Requests\UpdateAdsRequest;
use Modules\Ads\Repositories\AdsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class AdsController extends AdminBaseController
{
    /**
     * @var AdsRepository
     */
    private $ads;

    public function __construct(AdsRepository $ads)
    {
        parent::__construct();

        $this->ads = $ads;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $ads = $this->ads->all();

        return view('ads::admin.ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('ads::admin.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAdsRequest $request
     * @return Response
     */
    public function store(CreateAdsRequest $request)
    {
        $this->ads->create($request->all());

        return redirect()->route('admin.ads.ads.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('ads::ads.title.ads')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ads $ads
     * @return Response
     */
    public function edit(Ads $ads)
    {
        return view('ads::admin.ads.edit', compact('ads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Ads $ads
     * @param  UpdateAdsRequest $request
     * @return Response
     */
    public function update(Ads $ads, UpdateAdsRequest $request)
    {
        $this->ads->update($ads, $request->all());

        return redirect()->route('admin.ads.ads.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('ads::ads.title.ads')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ads $ads
     * @return Response
     */
    public function destroy(Ads $ads)
    {
        $this->ads->destroy($ads);

        return redirect()->route('admin.ads.ads.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('ads::ads.title.ads')]));
    }
}
