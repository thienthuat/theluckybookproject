<?php

namespace Modules\Ads\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Ads\Events\Handlers\RegisterAdsSidebar;

class AdsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterAdsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('ads', array_dot(trans('ads::ads')));
            // append translations

        });
    }

    public function boot()
    {
        $this->publishConfig('ads', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Ads\Repositories\AdsRepository',
            function () {
                $repository = new \Modules\Ads\Repositories\Eloquent\EloquentAdsRepository(new \Modules\Ads\Entities\Ads());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Ads\Repositories\Cache\CacheAdsDecorator($repository);
            }
        );
// add bindings

    }
}
