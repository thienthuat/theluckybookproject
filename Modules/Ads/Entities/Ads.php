<?php

namespace Modules\Ads\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Ads extends Model
{
    use MediaRelation;
    protected $table = 'ads__ads';
    protected $fillable = ['title','link'];

    public function getThumbnailAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'image_ads')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }
}
