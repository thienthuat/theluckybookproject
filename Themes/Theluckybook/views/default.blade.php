@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>$page->meta_title])
@stop
@section('content')
    <section class="container">
        @php($banner = $page->getBannerForPage() !='' ? $page->getBannerForPage()->path:'')
        @if($banner != '')
            <div class="box-banner-for-page">
                <img src="{{url($banner)}}" alt="{{$page->title}}" class="img-fluid">
            </div>
        @endif
        <div class="breadcrumbs">
            <ul class="items">
                <li class="item home">
                    <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                        {{trans('frontend::frontend.homepage')}}
                    </a>
                </li>
                <li class="item">
                    <strong>{{$page->title}}</strong>
                </li>
            </ul>
        </div>
    </section>
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{$page->title}}
                </h1>
            </div>
            <div class="box-content-contact">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        {!! $page->body !!}
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop

