@php($customer_user = Auth::guard('customer')->user())
<header class="header">

    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="box-hotline">
                        <strong>Hotline:&nbsp;</strong><span>@setting('contact::hotline')</span>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="box-user">
                        <div class="my-account">
                            @if($customer_user)
                                <span>Chào: <a href="{{route('page.customer.account')}}">
                                        {{$customer_user->first_name}} {{$customer_user->last_name}}
                                    </a> <i class="fa fa-angle-down"></i></span>
                                <ul>
                                    <li><a href="{{route('page.customer.account')}}"
                                           title="{{trans('frontend::frontend.my_account')}}">
                                            {{trans('frontend::frontend.my_account')}}
                                        </a></li>
                                    <li><a href="{{route('page.customer.myWishList')}}"
                                           title="{{trans('frontend::frontend.my_wish_list')}}">{{trans('frontend::frontend.my_wish_list')}}</a>
                                    </li>
                                    <li><a href="{{route('page.customer.myOrder')}}"
                                           title="{{trans('frontend::frontend.my_order')}}">{{trans('frontend::frontend.my_order')}}</a>
                                    </li>
                                    <li><a href="{{route('page.logout')}}"
                                           title="{{trans('frontend::frontend.logout')}}">
                                            {{trans('frontend::frontend.logout')}}
                                        </a></li>
                                </ul>
                            @else
                                <span>
                                    <a href="{{route('page.register')}}"
                                       title="{{trans('frontend::frontend.register')}}">
                                        <i class="fa fa-pencil-square-o"></i>
                                        {{trans('frontend::frontend.register')}}
                                    </a>
                                </span>
                                <span>
                                    <a href="{{route('page.login')}}" title="{{trans('frontend::frontend.login')}}">
                                        <i class="fa fa-sign-in"></i>{{trans('frontend::frontend.login')}}
                                    </a>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-center">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-3 col-sm-6 col-7">
                    <div class="header-box-logo">
                        <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                            <img src="{{Theme::url('images/logo.png')}}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6 col-12 order-3 order-md-2">
                    <div class="header-box-search">
                        <form action="{{route('page.product.searchProduct')}}" class="header-form-search"
                              method="get">
                            <div class="field search">
                                <input id="search" type="text" name="q" value=""
                                       placeholder="Search for item..." class="input-text" maxlength="128"
                                       role="combobox" aria-haspopup="false" aria-autocomplete="both"
                                       autocomplete="off">
                                <button type="submit" title="Search" class="action fa fa-search">
                                    <span></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-5 order-2 order-md-3">
                    <div class="header-box-menu">
                        <div class="header-search-cart">

                            <div class="header-box-cart">
                                <a href="javascript:void(0)" class="header-icon-cart">
                                    <i class="icon fa fa-shopping-cart"></i> <div class="text-cart">{{trans('frontend::frontend.page_cart')}}</div>(<span>{{Cart::count()}}</span>)
                                </a>
                                <div class="box-header-content-cart">
                                    <div class="block-minicart">
                                        <div class="minicart-items-wrapper">
                                            <ul class="minicart-items">
                                                @if(Cart::content()->count() > 0)
                                                    @foreach(Cart::content() as $item)
                                                        <li class="product-item">
                                                            <div class="product row">
                                                                <div class="col-3">
                                                                    <a href="{{route('page.detailProduct',$item->options->slug)}}">
                                                                        <img src="{{$item->options->avatar}}"
                                                                             alt="{{$item->name}}" class="img-fluid">
                                                                    </a>
                                                                </div>
                                                                <div class="col-9">
                                                                    <div class="product-item-details">
                                                                        <strong class="item-name">
                                                                            <a href="{{route('page.detailProduct',$item->options->slug)}}"
                                                                               title="{{$item->name}}">{{$item->name}}</a>
                                                                        </strong>
                                                                        <div class="header-cart-price">
                                                                            <span><sup>đ</sup>{{number_format($item->price)}}</span>
                                                                        </div>
                                                                        <div class="actions-cart-header">
                                                                            <div class="qty">
                                                                                <span>SL :</span>
                                                                                <span><input type="number"
                                                                                             value="{{$item->qty}}"></span>
                                                                            </div>
                                                                            <div class="action-cart">
                                                                                <a href="javascript:void(0)"
                                                                                   class="update-header-qty-cart"
                                                                                   data-id="{{$item->rowId}}"
                                                                                   data-url="{{route('page.cart.updateItemCart')}}">
                                                                                    <i class="fa fa-cog"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                                <a href="javascript:void(0)"
                                                                                   class="delete-header-cart"
                                                                                   data-url="{{route('page.cart.deleteItemCart',$item->rowId)}}">
                                                                                    <i class="fa fa-trash"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <p>Không có sản phẩm nào trong giỏ hàng </p>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="actions action-view-cart">
                                            <div class="primary">
                                                <a id="top-cart-btn-view-cart" href="{{route('page.cart.IndexCart')}}"
                                                   class="action primary checkout" title="Giỏ hàng">
                                                    <span>Giỏ hàng</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-ozmegamenu-inner">
            <div class="mobile-bar-icon"><span>mobile icon</span></div>
        </div>
        <div class="mobile-bar-content">
            <div class="mobile-bar-close">Close</div>
            <ul class="tabs-mobile">
                <li class="item item-menu active">Menu</li>

                <li class="item item-setting">{{trans('frontend::frontend.my_account')}}</li>
            </ul>
            <div class="tabs-content-mobile tabs-menu" style="display: block;">
                <nav class="ma-nav-mobile-container">
                    {!! app('\Modules\Frontend\Http\Controllers\PublicController')->menu() !!}
                </nav>

            </div>
            <div class="tabs-content-mobile tabs-setting" style="display: none;">
                @if($customer_user)
                    <span>Chào: <a href="{{route('page.customer.account')}}">
                                        {{$customer_user->first_name}} {{$customer_user->last_name}}
                                    </a></span>
                    <ul>
                        <li><a href="{{route('page.customer.account')}}"
                               title="{{trans('frontend::frontend.my_account')}}">
                                {{trans('frontend::frontend.my_account')}}
                            </a></li>
                        <li><a href="{{route('page.customer.myWishList')}}"
                               title="{{trans('frontend::frontend.my_wish_list')}}">{{trans('frontend::frontend.my_wish_list')}}</a>
                        </li>
                        <li><a href="{{route('page.customer.myOrder')}}"
                               title="{{trans('frontend::frontend.my_order')}}">{{trans('frontend::frontend.my_order')}}</a>
                        </li>
                        <li><a href="{{route('page.logout')}}" title="{{trans('frontend::frontend.logout')}}">
                                {{trans('frontend::frontend.logout')}}
                            </a></li>
                    </ul>
                @else
                    <span><a href="{{route('page.login')}}"
                             title="{{trans('frontend::frontend.login')}}">{{trans('frontend::frontend.login')}}</a></span>
                @endif
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-box-menu">
                        <div class="header-wrap-menu">
                            {!! app('\Modules\Frontend\Http\Controllers\PublicController')->menu() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>