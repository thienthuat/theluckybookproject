<?php
$link_limit = 7;
?>
@if ($paginator->lastPage() > 1)
    <div class="pages">
        @if ($paginator->hasPages())
            <ul class="pages-items">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                @else
                    <li class="pages-item pages-item-prev">
                        <a class="action prev" href="{{ $paginator->previousPageUrl() }}" title="Về trước">
                        </a>
                    </li>
                @endif

                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    <?php
                    $half_total_links = floor($link_limit / 2);
                    $from = $paginator->currentPage() - $half_total_links;
                    $to = $paginator->currentPage() + $half_total_links;
                    if ($paginator->currentPage() < $half_total_links) {
                        $to += $half_total_links - $paginator->currentPage();
                    }
                    if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                        $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                    }
                    ?>
                    @if ($from < $i && $i < $to)
                        <li class="pages-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                            @if($paginator->currentPage() == $i)
                                <strong class="page">
                                    <span>{{ $i }}</span>
                                </strong>
                            @else
                                <a href="{{ $paginator->url($i) }}" class="page">
                                    <span>{{ $i }}</span>
                                </a>
                            @endif
                        </li>
                    @endif
                @endfor
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="pages-item pages-item-next">
                        <a class="action  next" href="{{ $paginator->nextPageUrl() }}" title="Kế tiếp">
                        </a>
                    </li>
                @else

                @endif
            </ul>
        @endif
    </div>
@endif