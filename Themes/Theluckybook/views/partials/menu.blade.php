<ul class="nav navbar-nav">
    <li><a href="{{route('homepage')}}" target="_self" title="{{trans('frontend::frontend.homepage')}}">
            {{trans('frontend::frontend.homepage')}}</a>
    </li>
    <li style="display: none"><a href="{{route('page.about')}}" target="_self" title="{{trans('frontend::frontend.about')}}">
            {{trans('frontend::frontend.about')}}
        </a></li>
    {!! menuMultiple($category,0,1) !!}
    <li><a href="{{route('page.product.getProductPromotion')}}" target="_self" title="{{trans('frontend::frontend.product_promotion')}}">
            {{trans('frontend::frontend.product_promotion')}}</a></li>
    <li style="display: none"><a href="{{route('blog.index')}}" target="_self" title="{{trans('frontend::frontend.blog')}}">
            {{trans('frontend::frontend.blog')}}</a></li>
    <li style="display: none"><a href="{{route('page.contact')}}" target="_self" title="{{trans('frontend::frontend.contact')}}">
            {{trans('frontend::frontend.contact')}}
        </a></li>

</ul>