<ul class="nav items">
    @if(Request::segment(2) == trans('customer::url.my_customer'))
        <li class="nav item current">
            <strong>{{trans('frontend::frontend.my_account_dashboard')}}</strong>
        </li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.account')}}" title="{{trans('frontend::frontend.my_account_dashboard')}}">
                {{trans('frontend::frontend.my_account_dashboard')}}
            </a>
        </li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_customer_edit'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_account_info')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.accountEdit')}}" title="{{trans('frontend::frontend.my_account_info')}}">
                {{trans('frontend::frontend.my_account_info')}}
            </a>
        </li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_manage_address'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_address_manager')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.manageAddress')}}"
               title="{{trans('frontend::frontend.my_address_manager')}}">
                {{trans('frontend::frontend.my_address_manager')}}
            </a>
        </li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_point_accumulated'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_point_accumulated')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.myPointAccumulated')}}"
               title="{{trans('frontend::frontend.my_point_accumulated')}}">
                {{trans('frontend::frontend.my_point_accumulated')}}
            </a>
        </li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_order'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_order')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.myOrder')}}"
               title="{{trans('frontend::frontend.my_order')}}">{{trans('frontend::frontend.my_order')}}</a>
        </li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_wish_list'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_wish_list')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.myWishList')}}" title="{{trans('frontend::frontend.my_wish_list')}}">
                {{trans('frontend::frontend.my_wish_list')}}
            </a></li>
    @endif
    @if(Request::segment(2) == trans('customer::url.my_child_birthday'))
        <li class="nav item current"><strong>{{trans('frontend::frontend.my_child_birthday')}}</strong></li>
    @else
        <li class="nav item">
            <a href="{{route('page.customer.myBirthdayChild')}}"
               title="{{trans('frontend::frontend.my_child_birthday')}}">
                {{trans('frontend::frontend.my_child_birthday')}}
            </a>
        </li>
    @endif
</ul>