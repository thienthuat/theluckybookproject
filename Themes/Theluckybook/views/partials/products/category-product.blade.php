<section class="box-category-product mt-3">
    <div class="row">
        <div class="col-md-6">
            <div class="ma-title">
                <h3><span>{{trans('frontend::frontend.trending_item')}}</span></h3>
                <p class="des">{{trans('frontend::frontend.trending_item_note')}}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tab-category">
                <ul class="nav nav-pills mb-3" id="tabs_category_product" role="tablist">
                    @foreach($categories as $key=>$category)
                    <li class="nav-item">
                        <a class="nav-link {{$key==0?'active':''}}" id="pills-home-tab" data-toggle="pill"
                            href="#{{$category->slug}}" role="tab" aria-controls="pills-home" aria-selected="true"
                            title="{{$category->title}}">{{$category->title}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content" id="pills-tabContent">
                @foreach($categories as $key=>$category)
                <div class="tab-pane fade {{$key==0 ?' show active':''}}" id="{{$category->slug}}" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <div class="owl-carousel owl-theme box-slider-category-product">
                        @php($products = $category->product_home_hot)
                        @if($products->count() >0)
                        @foreach($products as $product)
                        @php($image = $product->getThumbnailAttribute() != '' ?
                        Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                        : Theme::url('images/img404.png'))
                        <div class="item">
                            <div class="products-grid">
                                <div class="product-item">
                                    <div class="product-item-info">
                                        <div class=" product-item-images">
                                            <a href="{{route('page.detailProduct',$product->slug)}}"
                                                title="{{$product->title}}" class="product-item-photo">
                                                <div class="product-image-container">
                                                    <div class="product-image-wrapper">
                                                        <img class="product-image-photo" src="{{url($image)}}"
                                                            alt="{{$product->title}}">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="product-item-details">
                                            <div class="content-name">
                                                <strong class="product-item-name">
                                                    <a title="{{$product->title}}"
                                                        href="{{route('page.detailProduct',$product->slug)}}"
                                                        class="product-item-link">
                                                        {{$product->title}} </a>
                                                </strong>
                                                <a href="javascript:void(0)" class="action wishlist add-to-wishlist"
                                                    data-url="{{route('page.addToWishList',$product->id)}}"
                                                    data-action="add-to-wishlist" title="Add to Wishlist">
                                                </a>
                                            </div>
                                            @if($product->quanlity==0)
                                            <div class="box-dont-has-product">
                                                <span>Sản phẩm hết hàng</span>
                                            </div>
                                            @else
                                            <div class="price-box price-final_price">
                                                @if($product->price_sale > 0)
                                                <span class="price-container price-final_price tax weee">
                                                    <span class="price-wrapper ">
                                                        <span
                                                            class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                                    </span>
                                                    <span class="price-wrapper old-price">
                                                        <span
                                                            class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                    </span>
                                                </span>
                                                @else
                                                <span class="price-container price-final_price tax weee">
                                                    <span class="price-wrapper ">
                                                        <span
                                                            class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                    </span>
                                                </span>
                                                @endif
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>