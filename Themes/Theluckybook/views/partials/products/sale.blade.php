@if($products->count() >0)
    <section class="box-product-sale mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="ma-title">
                    <h3><span>{{trans('frontend::frontend.title_product_sale')}}</span></h3>
                    <p class="des">{{trans('frontend::frontend.sub_title_product_sale')}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-theme box-slider-category-product">
                    @foreach($products as $product)
                        @php($image = $product->getThumbnailAttribute() != '' ?
                        Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                        : Theme::url('images/img404.png'))
                        <div class="item">
                            <div class="products-grid">
                                <div class="product-item">
                                    <div class="product-item-info">
										@if($product->percent_sale)
                                <div class="sale-off-price">
                                    {{$product->percent_sale}}
                                </div>
								@endif
                                        <div class=" product-item-images">
                                            <a href="{{route('page.detailProduct',$product->slug)}}"
                                               title="{{$product->title}}"
                                               class="product-item-photo">
                                                <div class="product-image-container">
                                                    <div class="product-image-wrapper">
                                                        <img class="product-image-photo"
                                                             src="{{url($image)}}"
                                                             alt="{{$product->title}}">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="product-item-details">
                                            <div class="content-name">
                                                <strong class="product-item-name">
                                                    <a title="{{$product->title}}"
                                                       href="{{route('page.detailProduct',$product->slug)}}"
                                                       class="product-item-link">
                                                        {{$product->title}} </a>
                                                </strong>
                                                <a href="javascript:void(0)"
                                                   class="action wishlist add-to-wishlist"
                                                   data-url="{{route('page.addToWishList',$product->id)}}"
                                                   data-action="add-to-wishlist"
                                                   title="Add to Wishlist">
                                                </a>
                                            </div>
                                            <div class="price-box price-final_price">
                                                @if($product->price_sale > 0)
                                                    <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                                                                </span>
                                                                                <span class="price-wrapper old-price">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                                @else
                                                    <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif