<div class="row">
    @foreach($products as $product)
        @php($image = $product->getThumbnailAttribute() != '' ?
                                        Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                                        : Theme::url('images/img404.png'))
        <div class="col-sm-6 col-md-12 col-12">
            <div class="item item-product-new">
                <div class="products-grid">
                    <div class="product-item">
                        <div class="product-item-info">
							@if($product->percent_sale)
                                <div class="sale-off-price">
                                    {{$product->percent_sale}}
                                </div>
								@endif
                            <div class=" product-item-images">
                                <a href="{{route('page.detailProduct',$product->slug)}}"
                                   title="{{$product->title}}"
                                   class="product-item-photo">
                                    <div class="product-image-container">
										
                                        <div class="product-image-wrapper">
                                            <img class="product-image-photo"
                                                 src="{{url($image)}}"
                                                 alt="{{$product->title}}">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="product-item-details">
                                <div class="content-name">
                                    <strong class="product-item-name">
                                        <a title="{{$product->title}}"
                                           href="{{route('page.detailProduct',$product->slug)}}"
                                           class="product-item-link">
                                            {{$product->title}} </a>
                                    </strong>
                                    <a href="javascript:void(0)"
                                       class="action wishlist add-to-wishlist"
                                       data-url="{{route('page.addToWishList',$product->id)}}"
                                       data-action="add-to-wishlist"
                                       title="Add to Wishlist">
                                    </a>
                                </div>
                                <div class="price-box price-final_price">
                                    @if($product->price_sale > 0)
                                        <div class="price-container price-final_price tax weee">
                                            <div class="price-wrapper ">
                                                <span class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                            </div>
                                            <div class="price-wrapper old-price">
                                                <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="price-container price-final_price tax weee">
                                            <div class="price-wrapper ">
                                                <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>