<section class="box-home-blog">
    <div class="ma-title blog-title">
        <h3><span>Tin Tức</span></h3>
        <p class="des">{{trans('frontend::frontend.note_news')}}</p>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="owl-carousel owl-theme" id="home_news_carousel">
                @foreach($blogs as $key=>$blog)
                    @php($image = $blog->getThumbnailAttribute() != '' ? Imagy::getThumbnail($blog->getThumbnailAttribute()->path , 'mediumThumb')
                                    : Theme::url('images/img404.png'))
                    <div class="item">
                        <div class="item-inner">
                            <div class="blog-image">
                                <a class="image"
                                   href="{{route('blog.detail',$blog->slug)}}" title="{{$blog->title}}">
                                    <img src="{{url($image)}}"
                                         alt="{{$blog->title}}">
                                </a>

                            </div>
                            <div class="blog-content">

                                <div class="time-conment">
                                            <span class="date-time">
                                                <span class="date">{{date('d',strtotime($blog->created_at))}} /</span>
                                                <span class="month">{{date('m',strtotime($blog->created_at))}}</span>
                                                <span class="separator">/</span>
                                            </span>
                                    <span class="year">{{date('Y',strtotime($blog->created_at))}}</span>
                                </div>
                                <h3 class="title">
                                    <a href="{{route('blog.detail',$blog->slug)}}"
                                       title="{{$blog->title}}">{{$blog->title}}</a>
                                </h3>
                                <p class="short-des">{{$blog->sumary}}</p>
                                <a class="readmore" href="{{route('blog.detail',$blog->slug)}}"
                                   title="{{trans('blog::blog.reading')}}">
                                    {{trans('blog::blog.reading')}} »
                                </a>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>