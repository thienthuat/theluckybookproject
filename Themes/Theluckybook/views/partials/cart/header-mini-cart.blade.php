<ul class="minicart-items">
    @if(Cart::content()->count() > 0)
        @foreach(Cart::content() as $item)
            <li class="product-item">
                <div class="product row">
                    <div class="col-3">
                        <a href="{{route('page.detailProduct',$item->options->slug)}}">
                            <img src="{{$item->options->avatar}}"
                                 alt="{{$item->name}}" class="img-fluid">
                        </a>
                    </div>
                    <div class="col-9">
                        <div class="product-item-details">
                            <strong class="item-name">
                                <a href="{{route('page.detailProduct',$item->options->slug)}}"
                                   title="{{$item->name}}">{{$item->name}}</a>
                            </strong>
                            <div class="header-cart-price">
                                <span><sup>đ</sup>{{number_format($item->price)}}</span>
                            </div>
                            <div class="actions-cart-header">
                                <div class="qty">
                                    <span>SL :</span>
                                    <span><input type="number"
                                                 value="{{$item->qty}}"></span>
                                </div>
                                <div class="action-cart">
                                    <a href="javascript:void(0)"
                                       class="update-header-qty-cart" data-id="{{$item->rowId}}" data-url="{{route('page.cart.updateItemCart')}}">
                                        <i class="fa fa-cog"
                                           aria-hidden="true"></i>
                                    </a>
                                    <a href="javascript:void(0)"
                                       class="delete-header-cart"
                                       data-url="{{route('page.cart.deleteItemCart',$item->rowId)}}">
                                        <i class="fa fa-trash"
                                           aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @else
        <p>Không có sản phẩm nào trong giỏ hàng </p>
    @endif
</ul>