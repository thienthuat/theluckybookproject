<div class="modal fade" id="modalWishlist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-success" role="alert">
                    Sản phẩm được thêm vào danh sách yêu thích thành công.
                </div>
                <div class="box-product-wishlist">
                    <div class="item">
                        <div class="products-grid">
                            @php($image = $product->getThumbnailAttribute() != '' ?
                                Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                                : Theme::url('images/img404.png'))
                            <div class="product-item">
                                <div class="product-item-info">
                                    <div class=" product-item-images">
                                        <a href="{{route('page.detailProduct',$product->slug)}}"
                                           title="{{$product->title}}"
                                           class="product-item-photo">
                                            <div class="product-image-container">
                                                <div class="product-image-wrapper">
                                                    <img class="product-image-photo"
                                                         src="{{url($image)}}"
                                                         alt="{{$product->title}}">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="product-item-details">
                                        <div class="content-name">
                                            <strong class="product-item-name">
                                                <a title="{{$product->title}}"
                                                   href="{{route('page.detailProduct',$product->slug)}}"
                                                   class="product-item-link">
                                                    {{$product->title}} </a>
                                            </strong>
                                        </div>
                                        <div class="price-box price-final_price">
                                            @if($product->price_sale > 0)
                                                <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                                                                </span>
                                                                                <span class="price-wrapper old-price">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                            @else
                                                <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-action">
                    <div class="row justify-content-between">
                        <div class="col-6">
                            <a href="javascript:void(0)" data-dismiss="modal">Tiếp Tục Mua Hàng</a>
                        </div>
                        <div class="col-6">
                            <a href="{{route('page.customer.myWishList')}}">Danh sách yêu thích</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>