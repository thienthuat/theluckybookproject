<div class="row">
    <div class="col-md-4 order-md-2">
        <div class="block block-collapsible-nav">
            <div class="title block-collapsible-nav-title">
                <strong>Thông tin đơn hàng</strong>
            </div>
            <div class="content block-info-cart">
                <di class="d-flex justify-content-between">
                    <div>
                        <strong><span>Tạm tính:</span></strong>
                    </div>
                    <div>
                        <span>{{Cart::subtotal()}} <sup>đ</sup></span>
                    </div>
                </di>
                <div class="total-cart">
                    <di class="d-flex justify-content-between">
                        <div>
                            <strong><span>Tổng tiền (Đã bao gồm VAT):</span></strong>
                        </div>
                        <div>
                            <span>{{Cart::total()}} <sup>đ</sup></span>
                        </div>
                    </di>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="actions">
                            <div class="primary">
                                <a href="{{route('page.cart.checkoutCart')}}"
                                   class="action primary checkout" title="Tiến hành thanh toán">
                                    <span>Tiến hành thanh toán</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <form action="{{route('page.cart.updateAllCart')}}" method="post">
            {!! csrf_field() !!}
            <div class="block block-cart-info">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="item-theart-cart">
                    <div class="row">
                        <div class="col-md-7">
                            <h5>Sản phẩm</h5>
                        </div>
                        <div class="col-md-5 d-none d-md-block">
                            <div class="row">
                                <div class="col-md-4">Giá</div>
                                <div class="col-md-4">Số lượng</div>
                                <div class="col-md-4">Tạm tính</div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Cart::content()->count() > 0)
                    @foreach(Cart::content() as $item)
                        <div class="item-cart">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-4">
                                            <a href="{{route('page.detailProduct',$item->options->slug)}}">
                                                <img src="{{$item->options->avatar}}"
                                                     alt="{{$item->name}}" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="col-8">
                                            <h3>{{$item->name}}</h3>
                                            @if($item->options->size !='' ||$item->options->size)
                                                <p class="option-item-cart"><i>( {{$item->options->size !='' ? 'Kích thước:'.$item->options->size:''}} {{$item->options->color !='' ? 'Màu sắc:'.$item->options->color:''}} )</i></p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="row info-price">
                                        <div class="col-4">
                                            <span class="d-md-none">Giá:</span>
                                            <span>{{number_format($item->price)}} <sup>đ</sup></span>
                                        </div>
                                        <div class="col-4">
                                            <span class="d-md-none">SL:</span>
                                            <span><input type="number" name="{{$item->rowId}}"
                                                         value="{{$item->qty}}"></span>
                                        </div>
                                        <div class="col-4">
                                            <span class="d-md-none">Tạm tính:</span>
                                            <span>{{number_format($item->price*$item->qty)}}
                                                <sup>đ</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row actions-cart-item">
                                <div class="col-md-12">
                                    <a href="javascript:void(0)"
                                       class="delete-header-cart"
                                       data-url="{{route('page.cart.deleteItemCart',$item->rowId)}}">
                                        <i class="fa fa-trash"
                                           aria-hidden="true"></i> <span>Xoá sản phẩm</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="not-item">Không có sản phẩm nào trong giỏ hàng</p>
                @endif
                <div class="action-cart">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="actions">
                                <div class="primary">
                                    <a id="top-cart-btn-view-cart" href="{{route('homepage')}}"
                                       class="action primary checkout" title="Tiếp tục mua hàng">
                                        <span><i class="fa fa-angle-left" aria-hidden="true"></i> Tiếp tục mua hàng</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-6">
                                    <div class="actions">
                                        <div class="primary">
                                            <a id="top-cart-btn-view-cart"
                                               href="{{route('page.cart.deleteAllCart')}}"
                                               class="action primary checkout" title="Giỏ hàng">
                                                <span><i class="fa fa-trash" aria-hidden="true"></i> Xoá giỏ hàng</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <button type="submit" class="action primary checkout"><i
                                                class="fa fa-refresh" aria-hidden="true"></i> Cập nhật
                                        giỏ hàng
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>