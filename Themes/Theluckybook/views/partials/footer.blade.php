<footer>
    <div class="footer-top">
        <div class="container">
            <div class="footer-static row">
                <div class="col-lg-4 col-md-4">
                    <h3>@setting('core::sumary')</h3>
                    <ul class="add">
                        <li>@setting('contact::address_multiple')</li>
                        <li><a href="javascript:void(0)"><i class="fa fa-clock-o"></i>THỜI GIAN MỞ CỬA: </a><br>@setting('core::time_open')
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4">
                    <h3 class="title">{{trans('frontend::frontend.footer_information')}}</h3>
                    <div class="menu-footer">
                        @menu('information')
                    </div>
                    <h3 class="title">{{trans('frontend::frontend.footer_support_client')}}</h3>
                    <div class="menu-footer">
                        @menu('support_client')
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <form class="form-subscribe"
                          action="{{route('page.emailMarketing')}}"
                          method="post" id="newsletter-validate-detail">
                        {!! csrf_field() !!}
                        <div class="field newsletter">
                            <h3 class="title">Đăng ký nhận bản tin</h3>
                            <div class="control">
                                <input name="email" type="email" id="newsletter" placeholder="địa chỉ email của bạn"
                                       required>
                                <button class="action subscribe" title="Subscribe" type="submit">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <span>Đăng ký</span>
                                </button>
                            </div>
                        </div>

                    </form>

                    <h3>KẾT NỐI VỚI CHÚNG TÔI</h3>
                    <ul class="link-follow">
                        <li class="first">
                            <a class="twitter fa fa-twitter" title="Twitter"
                               href="@setting('contact::twitter')"><span>twitter</span></a></li>
                        <li><a class="google fa fa-google-plus" title="Google" href="@setting('contact::g_plus')"><span>google </span></a>
                        </li>
                        <li><a class="facebook fa fa-facebook" title="Facebook"
                               href="@setting('contact::facebook')"><span>facebook</span></a></li>
                        <li><a class="youtube fa fa-youtube" title="Youtube"
                               href="@setting('contact::youtube')"><span>youtube </span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="SectionContentModal"></div>
<div class="wrap-loading">
    <ul class="cssload-cssload-ballsncups">
        <li>
            <div class="cssload-circle"></div>
            <div class="cssload-ball"></div>
        </li>
        <li>
            <div class="cssload-circle"></div>
            <div class="cssload-ball"></div>
        </li>
        <li>
            <div class="cssload-circle"></div>
            <div class="cssload-ball"></div>
        </li>
        <li>
            <div class="cssload-circle"></div>
            <div class="cssload-ball"></div>
        </li>
        <li>
            <div class="cssload-circle"></div>
            <div class="cssload-ball"></div>
        </li>
    </ul>
</div>
