<meta name='revisit-after' content='1 days'/>
<meta content="index, follow" name="robots">
<title>
    @if(isset($title) && $title != '')
        {{$title}}
    @else
        @setting('core::site-name')
    @endif
    | @setting('core::site-name-mini')
</title>
<meta name="description" content="@if(isset($description) && $description != ''){{$description}}@else{{Setting::get('core::site-description')}}@endif"/>
<meta name="keywords" content="@if(isset($keywords) && $keywords != ''){{$keywords}}@else{{Setting::get('core::keywords')}}@endif"/>

<meta property="og:title"              content=" @if(isset($title) && $title != '') {{$title}}
    @else
        @setting('core::site-name')
    @endif
    | @setting('core::site-name-mini')" />
<meta property="og:type"               content="article" />
<meta property="og:description"        content="@if(isset($description) && $description != ''){{$description}}@else{{Setting::get('core::site-description')}}@endif" />
<meta property="og:image"              content="https://theluckybook.vn/themes/theluckybook/images/image-fb.png" />