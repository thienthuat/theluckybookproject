<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Order</title>
    <style>
        /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
        /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }

            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }

            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }

            #MessageViewBody a {
                color: inherit;
                text-decoration: none;
                font-size: inherit;
                font-family: inherit;
                font-weight: inherit;
                line-height: inherit;
            }

            .btn-primary table td:hover {
                background-color: #34495e !important;
            }

            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>
</head>

<body class=""
    style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

    <table border="0" cellpadding="0" cellspacing="0" class="body"
        style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
        <tr>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
            <td class="container"
                style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                <div class="content"
                    style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <table class="main"
                        style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper"
                                style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                                <table border="0" cellpadding="0" cellspacing="0"
                                    style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                            <img style="margin: 10px 0" src='{{Theme::url('images/logo.png')}}'
                                                alt='logo' />
                                            <h3 style="color: red; font-size: 18px">Đơn hàng của bạn được đặt thành công
                                            </h3>
                                            <p style="margin-bottom: 45px">Mã đơn hàng:
                                                <strong>{{ $invoice->code }}</strong></p>
                                            <div class="des">
                                                Theluckybook cám ơn bạn đã đặt sách. Đơn hàng của bạn sẽ được đóng gói
                                                và gửi cho đơn vị vận chuyển Ninjavan/ Giaohangnhanh/ Vietstar trong 24
                                                giờ tới. Bạn sẽ nhận được sách đóng gói kỹ lưỡng trong 2-3 ngày nếu ở
                                                HCM, 3-4 ngày nếu ở Hà Nội và các tỉnh thành khác.
                                            </div>
                                            <p style="margin-top: 45px">Thông tin của bạn:</p>
                                            <table border="0" cellpadding="0" cellspacing="0"
                                                style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                                <tr>
                                                    <td class="content-block" style="width: 150px">
                                                        <span style="font-weight: bold">Họ tên:</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ $invoice->full_name }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="width: 150px">
                                                        <span style="font-weight: bold">Email khách hàng:</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ $invoice->email }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="width: 150px">
                                                        <span style="font-weight: bold">Mã đơn hàng:</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ $invoice->code }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="width: 150px">
                                                        <span style="font-weight: bold">Ngày đặt hàng:</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ $invoice->created_at }}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="width: 150px">
                                                        <span style="font-weight: bold">Địa chỉ nhận hàng:</span>
                                                    </td>
                                                    <td class="content-block" style="width: 150px">
                                                        <span>{{ $invoice->address }}</span>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table border="0" cellpadding="15" cellspacing="0"
                                                style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; margin-top: 45px; border: 1px solid #dfdfdf">
                                                <tr>
                                                    <td colspan="4" class="content-block"
                                                        style="border-bottom: 1px solid #dfdfdf">
                                                        <span style="font-weight: bold">Thông tin đơn hàng:</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block" style="border-bottom: 1px solid #dfdfdf">
                                                        <span style="font-weight: bold; ">Sản phẩm</span>
                                                    </td>
                                                    <td class="content-block" style="border-bottom: 1px solid #dfdfdf">
                                                        <span style="font-weight: bold">Giá</span>
                                                    </td>
                                                    <td class="content-block" style="border-bottom: 1px solid #dfdfdf">
                                                        <span style="font-weight: bold">Số lượng</span>
                                                    </td>
                                                    <td class="content-block" style="border-bottom: 1px solid #dfdfdf">
                                                        <span style="font-weight: bold">Tổng cộng</span>
                                                    </td>
                                                </tr>
                                                @foreach ($invoice->item_invoice as $item_invoice)
                                                <tr>
                                                    <td class="content-block">
                                                        <span>{{ $item_invoice->title }}</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ number_format($item_invoice->price) }}</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ $item_invoice->qty }}</span>
                                                    </td>
                                                    <td class="content-block">
                                                        <span>{{ number_format($item_invoice->price*$item_invoice->qty) }}</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td colspan="1" class="content-block">

                                                    </td>
                                                    <td colspan="3" class="content-block">
                                                        <span style="font-weight: bold">Tạm tính:</span>
                                                        {{ number_format($invoice->sub_total) }} đ<br>
                                                        <span style="font-weight: bold">Giảm giá:</span>
                                                        {{ number_format($invoice->total_promo) }} đ<br>
                                                        <span style="font-weight: bold">Chi phí vận chuyển:</span>
                                                        {{ number_format($invoice->price_ship) }}
                                                        đ<br>
                                                        <span style="font-weight: bold">Total:</span>
                                                        {{ number_format($invoice->total) }} đ<br>
                                                    </td>
                                                </tr>
                                            </table>

                                            <div class="des" style="margin-top: 10px">
                                                Theluckybook cám ơn bạn đã đặt sách. Đơn hàng của bạn sẽ được đóng gói
                                                và gửi cho đơn vị vận chuyển Ninjavan/ Giaohangnhanh/ Vietstar trong 24
                                                giờ tới. Bạn sẽ nhận được sách đóng gói kỹ lưỡng trong 2-3 ngày nếu ở
                                                HCM, 3-4 ngày nếu ở Hà Nội và các tỉnh thành khác.
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        </tr>
    </table>
</body>

</html>