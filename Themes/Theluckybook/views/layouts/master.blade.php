
<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="vi"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-site-verification" content="G_SV4bMWqQhsdzaaXrrcTYbKbSSZi6cSv5wgvJR0Dlk" />
    

    @yield('meta')
    <link href="https://theluckybook.vn/themes/theluckybook/images/icon-browser.png" rel="shortcut icon" type="image/x-icon"/>
    {!! Theme::style('css/lib.css') !!}
    {!! Theme::style('css/app.css') !!}
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '341211699796882',
      xfbml      : true,
      version    : 'v4.0'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

    
</head>
<body>
<!-- <div class="snowflakes" aria-hidden="true">
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
  <div class="snowflake">
    ❅
  </div>
  <div class="snowflake">
    ❆
  </div>
</div> -->
<section id="SiteContent">
    {!! app('\Modules\Frontend\Http\Controllers\PublicController')->header() !!}
    @yield('content')
    {!! app('\Modules\Frontend\Http\Controllers\PublicController')->footer() !!}
</section>
	{!! Theme::script('js/app.js') !!}
<div class="chat-fb-btn">
   <a href="https://www.facebook.com/messages/t/theluckybook.vn" title="Get in touch with me" target='_blank'>
      <img id="fb_msg_icon" width="10%" height="10%" src="http://store-images.s-microsoft.com/image/apps.7488.13510798886918977.69182166-f125-495d-80d2-44fdaab21523.8fcea13e-5d9a-48a9-9937-b26deeced1b5">
          <span style = "color: #0085FF;">Chat on facebook</span>

    </a>
</div>
 
<div id="fb-root"></div>
@yield('scripts')
</body>
</html>
