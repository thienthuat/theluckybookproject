@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>$post->meta_title,'keywords'=>$post->meta_keywords,'description'=>$post->meta_description])
@stop
@section('content')
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '341211699796882',
      xfbml      : true,
      version    : 'v4.0'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <section class="container">
        <div class="breadcrumbs">
            <ul class="items">
                <li class="item home">
                    <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                        {{trans('frontend::frontend.homepage')}}
                    </a>
                </li>
                <li class="item home">
                    <a href="{{route('blog.index')}}" title="{{trans('blog::blog.title')}}">
                        {{trans('blog::blog.title')}}
                    </a>
                </li>
                <li class="item blog">
                    <strong>{{$post->title}}</strong>
                </li>
            </ul>
        </div>
    </section>
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{$post->title}}
                </h1>
            </div>
            <div class="box-content-blog">
                <div class="row">
                    <div class="col-md-9">
                        <div class="post-list-wrapper">
                            <div class="post-holder post-holder-9">
                                {!! $post->content !!}
                                <br>
                                <div class="box-tag-blog">
                                    <strong>Tags: </strong>
                                    @foreach($tags as $tag)
                                        <a href="{{route('blog.tag',$tag->slug)}}" title="{{$tag->name}}">{{$tag->name}}</a>,
                                    @endforeach
                                </div>
                                <br>
                                <div class="fb-comments" data-href="{{Request::url()}}" data-width="100%"
                                     data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                    {!! app('\Modules\Blog\Http\Controllers\PublicController')->widgetLeft() !!}
                </div>
            </div>
        </section>
    </main>
@stop