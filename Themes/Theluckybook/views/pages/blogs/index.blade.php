@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('blog::blog.title')])
@stop
@section('content')
    <section class="container">
        <div class="breadcrumbs">
            <ul class="items">
                <li class="item home">
                    <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                        {{trans('frontend::frontend.homepage')}}
                    </a>
                </li>
                <li class="item blog">
                    <strong>{{trans('blog::blog.title')}}</strong>
                </li>
            </ul>
        </div>
    </section>
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{trans('blog::blog.title')}}
                </h1>
            </div>
            <div class="box-content-blog">
                <div class="row">
                    <div class="col-md-9">
                        <div class="post-list-wrapper">
                            <ol class="post-list">
                                @foreach($posts as $post)
                                    @php($image = $post->getThumbnailAttribute() != '' ? Imagy::getThumbnail($post->getThumbnailAttribute()->path , 'mediumThumb')
                                                : Theme::url('images/img404.png'))
                                    <li class="post-holder post-holder-9">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12">
                                                <div class="post-images">
                                                    <a href="{{route('blog.detail',$post->slug)}}"
                                                       title="{{$post->title}}">
                                                        <img src="{{url($image)}}" alt="{{$post->title}}" class="img-fluid">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 col-xs-12">
                                                <div class="post-content">
                                                    <div class="post-title-holder clearfix">
                                                        <h2 class="post-title">
                                                            <a class="post-item-link"
                                                               href="{{route('blog.detail',$post->slug)}}"
                                                               title="{{$post->title}}">
                                                                {{$post->title}}
                                                            </a>
                                                        </h2>
                                                    </div>
                                                    <div class="post-description clearfix">
                                                        <p>{{$post->sumary}}</p>
                                                    </div>
                                                    <a class="post-read-more"
                                                       href="{{route('blog.detail',$post->slug)}}"
                                                       title="{{trans('blog::blog.reading')}}">
                                                        {{trans('blog::blog.reading')}} » </a>
                                                </div>
                                            </div>
                                        </div>


                                    </li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="toolbar toolbar-products">
                                    {!! $posts->links('partials.pagination.default') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! app('\Modules\Blog\Http\Controllers\PublicController')->widgetLeft() !!}
                </div>
            </div>
        </section>
    </main>
@stop