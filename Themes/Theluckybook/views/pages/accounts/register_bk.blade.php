@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.register')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{trans('frontend::frontend.register')}}
                </h1>
            </div>
            <div class="box-content-register">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                {{Session::get('error')}}
                            </div>
                        @endif
                        <form class="form" action="{{route('page.postRegister')}}" method="post"
                              id="form_create_account">
                            {!! csrf_field() !!}
                            <fieldset class="fieldset create info">
                                <legend class="legend"><span>{{trans('frontend::frontend.personal_info')}}</span>
                                </legend>
                                <div class="field">
                                    <label class="label" for="firstname">
                                        <span>{{trans('frontend::form.first_name')}}<sup>*</sup></span>
                                    </label>
                                    <div class="control">
                                        <input type="text" id="firstname" name="firstname" value="{{old('firstname')}}"
                                               class="input-text">
                                    </div>
                                </div>
                                <div class="field">
                                    <label class="label" for="lastname">
                                        <span>{{trans('frontend::form.last_name')}}<sup>*</sup></span>
                                    </label>

                                    <div class="control">
                                        <input type="text" id="lastname" name="lastname" value="{{old('lastname')}}"
                                               class="input-text">
                                    </div>
                                </div>
                                <div class="field sex">
                                    <label class="label" for="sex"><span>Giới Tính<sup>*</sup></span></label>
                                    <div class="control">
                                        <select name="sex" id="sex" class="form-control custom-select" required>
                                            <option value="">--Chọn Giới Tính--</option>
                                            <option value="1" {{old('sex') == 1 ? 'selected':''}}>Nam</option>
                                            <option value="2" {{old('sex') == 2 ? 'selected':''}}>Nữ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="field birthday">
                                    <label class="label" for="birthday"><span>Ngày Sinh <sup>*</sup></span></label>
                                    <div class="control">
                                        <input type="date" name="birthday" id="birthday"
                                               value="{{old('birthday')}}"
                                               class="input-text" required>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset create account">
                                <legend class="legend"><span>{{trans('frontend::frontend.signin_info')}}</span></legend>
                                <br>
                                <div class="field required">
                                    <label for="email_address" class="label"><span>Email <sup>*</sup></span></label>
                                    <div class="control">
                                        <input type="email" name="email" autocomplete="email" id="email_address"
                                               value="{{old('email')}}" class="input-text">
                                    </div>
                                </div>
                                <div class="field password required">
                                    <label for="password" class="label"><span>Password <sup>*</sup></span></label>
                                    <div class="control">
                                        <input type="password" name="password" id="password"
                                               class="input-text" minlength="6" value="{{old('password')}}">
                                    </div>

                                </div>
                                <div class="field confirmation">
                                    <label for="password_confirmation"
                                           class="label"><span>Confirm Password <sup>*</sup></span></label>
                                    <div class="control">
                                        <input type="password" name="password_confirmation"
                                               id="password_confirmation" class="input-text" minlength="6"
                                               value="{{old('password_confirmation')}}">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="actions-toolbar">
                                <div class="primary">
                                    <button type="submit" class="action submit primary" title="Create an Account">
                                        <span>{{trans('frontend::frontend.new_register')}}</span>
                                    </button>
                                </div>
                                <div class="secondary">
                                    <a class="action back" href="{{route('page.login')}}"
                                       title="{{trans('frontend::frontend.back')}}">
                                        <span>{{trans('frontend::frontend.back')}}</span>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop