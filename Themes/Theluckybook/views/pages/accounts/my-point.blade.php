@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_point_accumulated')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_point_accumulated')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-addresses">
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-md-12">
                                    <p><span>Điểm tích lũy của bạn: </span> <strong>{{$customer_user->point}}</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_point_accumulated')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop