@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_order')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_order')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-addresses">
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="item-order">
                                            <table class="table table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th scope="col">Mã đơn hàng#</th>
                                                    <th scope="col">Ngày tạo</th>
                                                    <th scope="col">Thanh toán</th>
                                                    <th scope="col">Tổng tiền</th>
                                                    <th scope="col">Trạng thái</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($invoices as $invoice)
                                                    <tr>
                                                        <th scope="row">{{$invoice->code}}</th>
                                                        <td>{{date('d-m-Y H:i',strtotime($invoice->created_at))}}</td>
                                                        <td>{{$invoice->method_payment->title}}</td>
                                                        <td>{{number_format($invoice->total)}}<sup>đ</sup></td>
                                                        <td>{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</td>
                                                        <td>
                                                            <a href="{{route('page.customer.detailOrder',$invoice->code)}}"
                                                               title="Xem đơn hàng">Xem đơn hàng</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop