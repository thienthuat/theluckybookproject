@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.add_address')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.add_address')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-info">
                            <div class="block-content">
                                @if (Session::has('error'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                <div class="block block-dashboard-addresses">
                                    <div class="block-content">
                                        <form class="form form-create-edit-address"
                                              action="{{route('page.customer.postAddMoreAddress')}}"
                                              method="post">
                                            {!! csrf_field() !!}
                                            <div class="form-group row">
                                                <label for="fullname"
                                                       class="col-sm-3 col-form-label">{{trans('frontend::form.first_name')}}</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" id="fullname" name="fullname" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="address" class="col-sm-3 col-form-label">Địa chỉ nhận hàng
                                                    *</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" id="address" name="address" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="province_id" class="col-sm-3 col-form-label">Tỉnh/Thành phố
                                                    *</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <select name="province_id" class="custom-select" id="province_id" data-url="{{route('page.location.ajaxDistrict')}}" required>
                                                        <option value="">Lựa chọn</option>
                                                        @foreach($provinces as $province)
                                                            <option value="{{$province->id}}">{{$province->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="district_id" class="col-sm-3 col-form-label">Quận/huyện
                                                    *</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <select name="district_id" class="custom-select" id="district_id" data-url="{{route('page.location.ajaxWard')}}" required>
                                                        <option value="">Lựa chọn</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ward_id" class="col-sm-3 col-form-label">Phường,
                                                    xã</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <select name="ward_id" class="custom-select" id="ward_id" required>
                                                        <option value="">Lựa chọn</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="phone" class="col-sm-3 col-form-label">Số điện thoại
                                                    *</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="text" class="form-control" id="phone" name="phone" required>
                                                </div>
                                            </div>
                                            <div class="actions-toolbar">
                                                <div class="primary">
                                                    <button type="submit" class="action save primary" title="Lưu">
                                                        <span>Lưu</span>
                                                    </button>
                                                </div>
                                                <div class="secondary">
                                                    <a class="action back" href="{{route('page.customer.manageAddress')}}" title="Trở về"><span>Trở về</span></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop