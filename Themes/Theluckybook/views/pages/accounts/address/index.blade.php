@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_address_manager')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_address_manager')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-info">
                            <div class="block-content">
                                @if (Session::has('error'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                <div id="status"></div>
                                <div class="block block-dashboard-addresses">
                                    <div class="block-title">
                                        <strong>{{trans('frontend::frontend.address_book')}}</strong>
                                    </div>
                                    <div class="block-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="box box-billing-address">
                                                    <strong class="box-title">
                                                        <span>{{trans('frontend::frontend.default_billing_address')}}</span>
                                                    </strong>
                                                    @if($address_payment_default)
                                                        <div class="box-content">
                                                            <address>
                                                                <div class="box-info-address">
                                                                    <p>
                                                                        <strong>{{$address_payment_default->full_name}}</strong>
                                                                    </p>
                                                                    <p>{{$address_payment_default->address}}</p>
                                                                    <p>{{$address_payment_default->citys->name}}
                                                                        , {{$address_payment_default->districts->type}} {{$address_payment_default->districts->name}}
                                                                        , {{$address_payment_default->ward->type}} {{$address_payment_default->ward->name}}</p>
                                                                    <p>{{$address_payment_default->phone}}</p>
                                                                </div>
                                                            </address>
                                                        </div>
                                                        <div class="box-actions">
                                                            <a href="{{route('page.customer.editMoreAddress',$address_payment_default->id)}}"
                                                               title="Chỉnh sửa" class="action edit">Chỉnh sửa</a>
                                                        </div>
                                                    @else
                                                        <div class="box-content">
                                                            <address>
                                                                Bạn chưa đặt địa chỉ thanh toán.
                                                            </address>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="box box-shipping-address">
                                                    <strong class="box-title">
                                                        <span>{{trans('frontend::frontend.default_shipping_address')}}</span>
                                                    </strong>
                                                    @if($address_ship_default)
                                                        <div class="box-content">
                                                            <address>
                                                                <div class="box-info-address">
                                                                    <p>
                                                                        <strong>{{$address_ship_default->full_name}}</strong>
                                                                    </p>
                                                                    <p>{{$address_ship_default->address}}</p>
                                                                    <p>{{$address_ship_default->citys->name}}
                                                                        , {{$address_ship_default->districts->type}} {{$address_ship_default->districts->name}}
                                                                        , {{$address_ship_default->ward->type}} {{$address_ship_default->ward->name}}</p>
                                                                    <p>{{$address_ship_default->phone}}</p>
                                                                </div>
                                                            </address>
                                                        </div>
                                                        <div class="box-actions">
                                                            <a href="{{route('page.customer.editMoreAddress',$address_ship_default->id)}}"
                                                               title="Chỉnh sửa" class="action edit">Chỉnh sửa</a>
                                                        </div>
                                                    @else
                                                        <div class="box-content">
                                                            <address>
                                                                Bạn chưa đặt địa chỉ nhận hàng.
                                                            </address>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block block-dashboard-addresses">
                                    <div class="block-title">
                                        <strong>{{trans('frontend::frontend.more_address')}}</strong>
                                        <a class="action add-address" href="{{route('page.customer.addMoreAddress')}}"
                                           title="{{trans('frontend::frontend.add_address')}}">
                                            <span><i class="fa fa-plus-circle"
                                                     aria-hidden="true"></i>{{trans('frontend::frontend.add_address')}}</span>
                                        </a>
                                    </div>
                                    <div class="block-content">

                                        <div class="row">
                                            <div class="col-md-12">
                                                @foreach($address as $addres)
                                                    <div class="box-item-address">
                                                        <div class="wrapper-address">
                                                            <div class="box-info-address">
                                                                <p><strong>{{$addres->full_name}}</strong></p>
                                                                <p>{{$addres->address}}</p>
                                                                <p>{{$addres->citys->name}}
                                                                    , {{$addres->districts->type}} {{$addres->districts->name}}
                                                                    , {{$addres->ward->type}} {{$addres->ward->name}}</p>
                                                                <p>{{$addres->phone}}</p>
                                                            </div>
                                                            <div class="action-address">
                                                                <a href="{{route('page.customer.editMoreAddress',$addres->id)}}"
                                                                   title="Chỉnh sửa" class="edit">Chỉnh sửa</a>
                                                                <a href="javascript:void(0)"
                                                                   data-url="{{route('page.customer.deleteMoreAddress',$addres->id)}}"
                                                                   class="delete-address">Xóa</a>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="box-change-type-address">
                                                                    @if($addres->default_shipping_address == 0)
                                                                        <a href="{{route('page.customer.setDefaultShip',$addres->id)}}" title="TẠO ĐỊA CHỈ GIAO HÀNG MẶC ĐỊNH">
                                                                            TẠO ĐỊA CHỈ GIAO HÀNG MẶC ĐỊNH
                                                                        </a>
                                                                    @endif
                                                                    @if($addres->default_payment_address == 0)
                                                                        <a href="{{route('page.customer.setDefaultPayment',$addres->id)}}" title="TẠO ĐỊA CHỈ THANH TOÁN MẶC ĐỊNH">
                                                                            TẠO ĐỊA CHỈ THANH TOÁN MẶC ĐỊNH
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop