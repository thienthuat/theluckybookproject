@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_wish_list')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <input type="hidden" value="{{route('page.customer.deleteMyWishList')}}" id="url_hidden_delete">
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_wish_list')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-addresses">
                            <div class="block-content">
                                <div class="box-content-product-page">
                                    <div id="status"></div>
                                    <div class="row">
                                        @foreach($all_favourite as $favourite)
                                            @php($product = $favourite->product)
                                            @php($image = $product->getThumbnailAttribute() != '' ?
                                                        Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                                                        : Theme::url('images/img404.png'))
                                            <div class="item col-md-4 col-sm-6 col-12">
                                                <div class="products-grid">
                                                    <div class="product-item">
                                                        <div class="product-item-info">
                                                            <div class=" product-item-images">
                                                                <a href="{{route('page.detailProduct',$product->slug)}}"
                                                                   title="{{$product->title}}"
                                                                   class="product-item-photo">
                                                                    <div class="product-image-container">
                                                                        <div class="product-image-wrapper">
                                                                            <img class="product-image-photo"
                                                                                 src="{{url($image)}}"
                                                                                 alt="{{$product->title}}">
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="product-item-details">
                                                                <div class="content-name">
                                                                    <strong class="product-item-name">
                                                                        <a title="{{$product->title}}"
                                                                           href="{{route('page.detailProduct',$product->slug)}}"
                                                                           class="product-item-link">
                                                                            {{$product->title}} </a>
                                                                    </strong>
                                                                </div>
                                                                <div class="price-box price-final_price">
                                                                    @if($product->price_sale > 0)
                                                                        <div class="price-container price-final_price tax weee">
                                                                            <span class="price-wrapper ">
                                                                                <span class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                                                            </span>
                                                                            <span class="price-wrapper old-price">
                                                                                <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                            </span>
                                                                        </div>
                                                                    @else
                                                                        <div class="price-container price-final_price tax weee">
                                                                            <div class="price-wrapper ">
                                                                                <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="box-action">
                                                                    <a href="javascript:void(0)" class="delete-item-favourite" data-id="{{$favourite->id}}">Xoá sản phẩm</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop