@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_account')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_dashboard')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-info">
                            <div class="block-title">
                                <strong>
                                    {{trans('frontend::frontend.my_account_info')}}
                                </strong>
                            </div>
                            <div class="block-content">
                                <div class="box box-information">
                                    <strong class="box-title">
                                        <span>{{trans('frontend::frontend.personal_info')}}</span>
                                    </strong>
                                    <div class="box-content">
                                        <p>
                                            {{$customer_user->last_name}} {{$customer_user->first_name}}<br>
                                            {{$customer_user->email}}<br>
                                        </p>
                                    </div>
                                    <div class="box-actions">
                                        <a href="{{route('page.customer.accountEdit')}}"
                                           class="action change-password">
                                            {{trans('frontend::form.edit')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block block-dashboard-addresses">
                            <div class="block-title">
                                <strong>{{trans('frontend::frontend.address_book')}}</strong>
                                <a class="action edit" href="{{route('page.customer.manageAddress')}}"
                                   title="{{trans('frontend::frontend.my_address_manager')}}">
                                    <span>{{trans('frontend::frontend.my_address_manager')}}</span>
                                </a>
                            </div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box box-billing-address">
                                            <strong class="box-title">
                                                <span>{{trans('frontend::frontend.default_billing_address')}}</span>
                                            </strong>
                                            @if($address_payment_default)
                                                <div class="box-content">
                                                    <address>
                                                        <div class="box-info-address">
                                                            <p>
                                                                <strong>{{$address_payment_default->full_name}}</strong>
                                                            </p>
                                                            <p>{{$address_payment_default->address}}</p>
                                                            <p>{{$address_payment_default->citys->name}}
                                                                , {{$address_payment_default->districts->type}} {{$address_payment_default->districts->name}}
                                                                , {{$address_payment_default->ward->type}} {{$address_payment_default->ward->name}}</p>
                                                            <p>{{$address_payment_default->phone}}</p>
                                                        </div>
                                                    </address>
                                                </div>
                                                <div class="box-actions">
                                                    <a href="{{route('page.customer.editMoreAddress',$address_payment_default->id)}}"
                                                       title="Chỉnh sửa" class="action edit">Chỉnh sửa</a>
                                                </div>
                                            @else
                                                <div class="box-content">
                                                    <address>
                                                        Bạn chưa đặt địa chỉ thanh toán.
                                                    </address>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box box-shipping-address">
                                            <strong class="box-title">
                                                <span>{{trans('frontend::frontend.default_shipping_address')}}</span>
                                            </strong>
                                            @if($address_ship_default)
                                                <div class="box-content">
                                                    <address>
                                                        <div class="box-info-address">
                                                            <p>
                                                                <strong>{{$address_ship_default->full_name}}</strong>
                                                            </p>
                                                            <p>{{$address_ship_default->address}}</p>
                                                            <p>{{$address_ship_default->citys->name}}
                                                                , {{$address_ship_default->districts->type}} {{$address_ship_default->districts->name}}
                                                                , {{$address_ship_default->ward->type}} {{$address_ship_default->ward->name}}</p>
                                                            <p>{{$address_ship_default->phone}}</p>
                                                        </div>
                                                    </address>
                                                </div>
                                                <div class="box-actions">
                                                    <a href="{{route('page.customer.editMoreAddress',$address_ship_default->id)}}"
                                                       title="Chỉnh sửa" class="action edit">Chỉnh sửa</a>
                                                </div>
                                            @else
                                                <div class="box-content">
                                                    <address>
                                                        Bạn chưa đặt địa chỉ nhận hàng.
                                                    </address>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop