@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.login')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{trans('frontend::frontend.login')}}
                </h1>
            </div>
            <div class="box-content-login">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="block block-customer-login">
                            <div class="block-title">
                                <strong>
                                    {{trans('frontend::frontend.registed_customer')}}
                                </strong>
                            </div>
                            <div class="block-content">
                                <form class="form form-login" action="{{route('page.postLogin')}}" method="post"
                                      id="login_form">
                                    {!! csrf_field() !!}
                                    <fieldset class="fieldset login">
                                        <div class="field note">{{trans('frontend::frontend.note_registed_customer')}}</div>
                                        <div class="field">
                                            <label class="label" for="email"><span>Email</span></label>
                                            <div class="control">
                                                <input name="email" value="{{old('email')}}" autocomplete="off" id="email"
                                                       type="email" class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label for="pass" class="label"><span>Password</span></label>
                                            <div class="control">
                                                <input name="password" type="password" autocomplete="off"
                                                       class="input-text" id="password" required>
                                            </div>
                                        </div>
                                        <div class="actions-toolbar">
                                            <div class="primary">
                                                <button type="submit" class="action login primary" name="send"
                                                        id="send2"><span> {{trans('frontend::frontend.login')}}</span>
                                                </button>
                                            </div>
                                            <div class="secondary">
                                                <a class="action remind" title="{{trans('frontend::frontend.forgot_password')}}"
                                                   href="{{route('page.forgotPassword')}}">
                                                    <span>{{trans('frontend::frontend.forgot_password')}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block block-new-customer">
                            <div class="block-title">
                                <strong>
                                    {{trans('frontend::frontend.new_customer')}}
                                </strong>
                            </div>
                            <div class="block-content">
                                <p>{{trans('frontend::frontend.note_new_customer')}}</p>
                                <div class="actions-toolbar">
                                    <div class="primary">
                                        <a href="{{route('page.register')}}" title="{{trans('frontend::frontend.new_register')}}"
                                           class="action create primary"><span>{{trans('frontend::frontend.new_register')}}</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop

