@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_account_info')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::form.edit')}} {{trans('frontend::frontend.my_account_info')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-info">
                            <div class="block-title">
                                <strong>
                                    {{trans('frontend::frontend.my_account_info')}}
                                </strong>
                            </div>
                            <div class="block-content">
                                @if (Session::has('error'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('error')}}
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        {{Session::get('success')}}
                                    </div>
                                @endif
                                <form class="form form-edit-account" action="{{route('page.customer.postAccountEdit')}}"
                                      method="post">
                                    {!! csrf_field() !!}
                                    <fieldset class="fieldset info">
                                        <div class="field">
                                            <label class="label" for="firstname">
                                                <span>{{trans('frontend::form.first_name')}} <sup>*</sup></span>
                                            </label>
                                            <div class="control">
                                                <input type="text" id="firstname" name="firstname"
                                                       value="{{old('firstname',$customer_user->first_name)}}"
                                                       class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label class="label" for="lastname">
                                                <span>{{trans('frontend::form.last_name')}} <sup>*</sup></span>
                                            </label>

                                            <div class="control">
                                                <input type="text" id="lastname" name="lastname"
                                                       value="{{old('lastname',$customer_user->last_name)}}"
                                                       class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field email">
                                            <label class="label" for="email"><span>Email <sup>*</sup></span></label>
                                            <div class="control">
                                                <input type="email" name="email" id="email"
                                                       value="{{old('lastname',$customer_user->email)}}"
                                                       class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field birthday">
                                            <label class="label" for="birthday"><span>Ngày Sinh <sup>*</sup></span></label>
                                            <div class="control">
                                                <input type="date" name="birthday" id="birthday"
                                                       value="{{old('birthday', date('Y-m-d',strtotime($customer_user->birthday)))}}"
                                                       class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field sex">
                                            <label class="label" for="sex"><span>Giới Tính<sup>*</sup></span></label>
                                            <div class="control">
                                                <select name="sex" id="sex" class="form-control custom-select" required>
                                                    <option value="">--Chọn Giới Tính--</option>
                                                    <option value="1" {{$customer_user->sex == 1 ? 'selected':''}}>Nam</option>
                                                    <option value="2" {{$customer_user->sex == 2 ? 'selected':''}}>Nữ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field choice">
                                            <input type="checkbox" name="change_password" id="box_check_change_password"
                                                   value="1" class="checkbox"
                                                    {{ (is_array(old('change_password')) && in_array(1, old('change_password'))) ? ' checked' : '' }}
                                            >
                                            <label class="label"
                                                   for="box_check_change_password"><span>{{trans('frontend::frontend.change_password')}}</span></label>
                                        </div>
                                    </fieldset>

                                    <fieldset class="fieldset password" id="box_show_change_password"
                                              style="display: {{ (is_array(old('change_password')) && in_array(1, old('change_password'))) ? ' block' : 'none' }};">
                                        <legend class="legend">
                                            <span>
                                                {{trans('frontend::frontend.change_password')}}
                                            </span>
                                        </legend>
                                        <div class="field password current">
                                            <label class="label"
                                                   for="current_password"><span>
                                                    {{trans('frontend::form.current_password')}}
                                                </span></label>
                                            <div class="control">
                                                <input type="password" class="input-text" name="current_password"
                                                       id="current_password">
                                            </div>
                                        </div>
                                        <div class="field new password required">
                                            <label class="label"
                                                   for="password"><span>{{trans('frontend::form.new_password')}}</span></label>
                                            <div class="control">
                                                <input type="password" class="input-text" name="password" id="password">
                                            </div>
                                        </div>
                                        <div class="field confirm password">
                                            <label class="label" for="password_confirmation">
                                                <span>{{trans('frontend::form.confirm_new_password')}}</span>
                                            </label>
                                            <div class="control">
                                                <input type="password" class="input-text" name="password_confirmation"
                                                       id="password_confirmation">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="actions-toolbar">
                                        <div class="primary">
                                            <button type="submit" class="action save primary"
                                                    title="{{trans('frontend::form.save')}}">
                                                <span>{{trans('frontend::form.save')}}</span>
                                            </button>
                                        </div>
                                        <div class="secondary">
                                            <a class="action back"
                                               href="{{route('page.customer.account')}}"
                                               title="{{trans('frontend::frontend.back')}}"><span>{{trans('frontend::frontend.back')}}</span></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop