@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.forgot_password')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{trans('frontend::frontend.forgot_password')}}
                </h1>
            </div>
            <div class="box-content-login">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block block-customer-login">
                            <div class="block-content">
                                <form class="form form-login" action="{{route('page.postForgotPassword')}}" method="post"
                                      id="form_forgot_password">
                                    {!! csrf_field() !!}
                                    @if (Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close"><span aria-hidden="true">&times;</span>
                                            </button>
                                            {{Session::get('error')}}
                                        </div>
                                    @endif
                                    <fieldset class="fieldset login">
                                        <div class="field note">{{trans('frontend::frontend.note_forgot_password')}}</div>
                                        <div class="field">
                                            <label class="label" for="email"><span>Email</span></label>
                                            <div class="control">
                                                <input name="email" value="{{old('email')}}" autocomplete="off"
                                                       id="email"
                                                       type="email" class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="actions-toolbar">
                                            <div class="primary">
                                                <button type="submit" class="action login primary" name="send"
                                                        id="send2"><span> {{trans('frontend::frontend.send')}}</span>
                                                </button>
                                            </div>
                                            <div class="secondary">
                                                <a class="action remind"
                                                   title="{{trans('frontend::frontend.back')}}"
                                                   href="{{route('page.login')}}">
                                                    <span>{{trans('frontend::frontend.back')}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop