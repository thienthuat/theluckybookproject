@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.change_password')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{trans('frontend::frontend.change_password')}}
                </h1>
            </div>
            <div class="box-content-login">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block block-customer-login">
                            <div class="block-content">
                                <form class="form form-login" action="{{route('page.postResetPassword')}}" method="post"
                                      id="form_reset_password">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="key_token" value="{{$key}}">
                                    @if (Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close"><span aria-hidden="true">&times;</span>
                                            </button>
                                            {{Session::get('error')}}
                                        </div>
                                    @endif
                                    <fieldset class="fieldset login">
                                        <div class="field">
                                            <label class="label"
                                                   for="new_password"><span>{{trans('frontend::form.new_password')}}</span></label>
                                            <div class="control">
                                                <input name="new_password" value="{{old('new_password')}}"
                                                       autocomplete="off"
                                                       id="new_password"
                                                       type="password" class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label class="label"
                                                   for="confirm_new_password"><span>{{trans('frontend::form.confirm_new_password')}}</span></label>
                                            <div class="control">
                                                <input name="confirm_new_password"
                                                       value="{{old('confirm_new_password')}}" autocomplete="off"
                                                       id="confirm_new_password"
                                                       type="password" class="input-text" required>
                                            </div>
                                        </div>
                                        <div class="actions-toolbar">
                                            <div class="primary">
                                                <button type="submit" class="action login primary" name="send"
                                                        id="send2"><span> {{trans('frontend::frontend.send')}}</span>
                                                </button>
                                            </div>
                                            <div class="secondary">
                                                <a class="action remind"
                                                   title="{{trans('frontend::frontend.back')}}"
                                                   href="{{route('page.login')}}">
                                                    <span>{{trans('frontend::frontend.back')}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop