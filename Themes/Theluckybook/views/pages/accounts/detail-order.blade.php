@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_order')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_order')}} #{{$invoice->code}}</span>
                                <span class="status-invoice">{{config('asgard.shopping.config.status_invoice')[$invoice->status]}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-addresses">
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="item-order">
                                            <div id="box_main_cart" class="mt-5">
                                                <div class="row">
                                                    <div class="col-md-4 order-md-2">
                                                        <div class="block block-collapsible-nav block-item-nav-cart-first">
                                                            <div class="title block-collapsible-nav-title">
                                                                <strong>Hình thức thanh toán</strong>
                                                            </div>
                                                            <div class="content block-info-cart">
                                                                <div class="custom-controls-stacked">
                                                                    <strong>{{$invoice->method_payment->title}}</strong>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="block block-collapsible-nav">
                                                            <div class="title block-collapsible-nav-title">
                                                                <strong>Thông tin đơn hàng</strong>
                                                            </div>
                                                            <div class="content block-info-cart">
                                                                <div class="d-flex justify-content-between">
                                                                    <div>
                                                                        <strong><span>Mã đơn hàng:</span></strong>
                                                                    </div>
                                                                    <div>
                                                                        <span>{{$invoice->code}}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex justify-content-between">
                                                                    <div>
                                                                        <strong><span>Tạm tính:</span></strong>
                                                                    </div>
                                                                    <div>
                                                                        <span>{{number_format($invoice->sub_total)}}
                                                                            <sup>đ</sup></span>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex justify-content-between">
                                                                    <div>
                                                                        <strong><span>Chi phí vận chuyển:</span></strong>
                                                                    </div>
                                                                    <div>
                                                                        <span>{{number_format($invoice->price_ship)}}
                                                                            <sup>đ</sup></span>
                                                                    </div>
                                                                </div>
                                                                <div class="total-cart">
                                                                    <div class="d-flex justify-content-between">
                                                                        <div>
                                                                            <strong><span>Tổng tiền (Đã bao gồm VAT):</span></strong>
                                                                        </div>
                                                                        <div>
                                                                            <span>{{number_format($invoice->total)}}
                                                                                <sup>đ</sup></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="block block-cart-info">
                                                            <div class="item-theart-cart">
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <h5>Sản phẩm</h5>
                                                                    </div>
                                                                    <div class="col-md-5 d-none d-md-block">
                                                                        <div class="row">
                                                                            <div class="col-md-4">Giá</div>
                                                                            <div class="col-md-4">Số lượng</div>
                                                                            <div class="col-md-4">Tạm tính</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @foreach($invoice->item_invoice as $item)
                                                                <div class="item-cart">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <h3>
                                                                                        <a href="{{route('page.detailProduct',$item->slug)}}">{{$item->title}}</a>
                                                                                    </h3>
                                                                                    @if($item->size !='' ||  $item->color != '' )
                                                                                        <p class="option-item-cart">
                                                                                            <i>( {{$item->size !='' ? 'Kích thước:'.$item->size:''}} {{$item->color !='' ? 'Màu sắc:'.$item->color:''}}
                                                                                                )</i></p>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-5">
                                                                            <div class="row info-price">
                                                                                <div class="col-4">
                                                                                    <span class="d-md-none">Giá:</span>
                                                                                    <span>{{number_format($item->price)}}
                                                                                        <sup>đ</sup></span>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <span class="d-md-none">SL:</span>
                                                                                    <span>{{$item->qty}}</span>
                                                                                </div>
                                                                                <div class="col-4">
                                                                                    <span class="d-md-none">Tạm tính:</span>
                                                                                    <span>{{number_format($item->price*$item->qty)}}
                                                                                        <sup>đ</sup></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <div class="block block-dashboard-addresses">
                                                            <div class="block-content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="box box-shipping-address">
                                                                            <strong class="box-title">
                                                                                <span>Địa chỉ giao hành</span>
                                                                            </strong>
                                                                            <div class="box-content">
                                                                                <address>
                                                                                    <div class="box-info-address">
                                                                                        <p>
                                                                                            <strong>{{$invoice->full_name}}</strong>
                                                                                        </p>
                                                                                        <p>{{$invoice->address}}</p>
                                                                                        <p>
                                                                                            {{$invoice->citys->name}}
                                                                                            , {{$invoice->districts->type}} {{$invoice->districts->name}}
                                                                                            , {{$invoice->ward->type}} {{$invoice->ward->name}}
                                                                                        </p>
                                                                                        <p>{{$invoice->phone}}</p>
                                                                                    </div>
                                                                                </address>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop