@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.login')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper line-border-bottom">
                <div class="row align-items-center">
                    <div class="col-md-9">
                        <h1 class="page-title mb-1">
                            {{trans('frontend::frontend.login')}}
                        </h1>
                        <p>Đăng nhập hoặc đăng ký tại THE LUCKYBOOK có thể phục vụ bạn tốt hơn thông qua các chương trình
                            chăm sóc
                            khách hàng và các khuyến mãi đặc biệt dành cho khách hàng mua online.</p>
                    </div>
                    <div class="col-md-3">
                        <div class="box-info-support">
                            <span>Hỗ trợ</span>
                            <a href="tel:@setting('contact::hotline')"><i class="fa fa-phone"></i></a>
                            <a href="mailto:tel:@setting('contact::email')"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-content-login">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 line-width">
                        <div class="block block-customer-login">
                            <div class="block-title">
                                <h2>BẠN ĐÃ CÓ TÀI KHOẢN ?</h2>
                            </div>
                            <div class="block-content">
                                <div class="box-form-wrapper-control">
                                    <h3>ĐĂNG NHẬP BẰNG EMAIL</h3>
                                    <form class="form form-login" action="{{route('page.postLogin')}}" method="post"
                                          id="login_form">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label for="email">Địa chỉ Email</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                   placeholder="name@example.com" value="{{old('email')}}"
                                                   autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Mật khẩu</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   autocomplete="off" required>
                                        </div>
                                        <div class="box-control-form">
                                            <div class="row align-items-center">
                                                <div class="col-6">
                                                    <a class="action remind "
                                                       title="{{trans('frontend::frontend.forgot_password')}}"
                                                       href="{{route('page.forgotPassword')}}">
                                                        <span>{{trans('frontend::frontend.forgot_password')}}</span>
                                                    </a>
                                                </div>
                                                <div class="col-6">
                                                    <div class="box-wrapper-action">
                                                        <button type="submit" class="action btn-action-form primary"
                                                                name="send"
                                                                id="send2"><i class="fa fa-sign-in"
                                                                              aria-hidden="true"></i><span> {{trans('frontend::frontend.login')}}</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="box-icon-facebook">
                                    <a href="{{route('page.login.facebook')}}" title="Đăng nhập facebook ">
                                        <div class="icon-facebook">
                                            <div class="icon">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                            </div>
                                            <div class="text">
                                                Login With Facebook
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block block-new-customer">
                            <div class="block-title">
                                {{trans('frontend::frontend.new_customer')}}
                            </div>
                            <div class="block-content">
                                <div class="actions-toolbar">
                                    <div class="primary">
                                        <div class="box-control-form">
                                            <a href="{{route('page.register')}}"
                                               title="{{trans('frontend::frontend.new_register')}}"
                                               class="action btn-action-form primary"><i class="fa fa-pencil-square-o"></i><span>{{trans('frontend::frontend.new_register')}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop

