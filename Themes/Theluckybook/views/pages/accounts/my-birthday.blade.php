@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.my_child_birthday')])
@stop
@section('content')
    @php($customer_user = Auth::guard('customer')->user())
    <main class="page-main-content">
        <section class="box-content-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 order-md-2">
                        <div class="page-title-wrapper">
                            <h1 class="page-title">
                                <span class="base">{{trans('frontend::frontend.my_child_birthday')}}</span>
                            </h1>
                        </div>
                        <div class="block block-dashboard-addresses">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                    </button>
                                    {{Session::get('success')}}
                                </div>
                            @endif
                            <div id="status"></div>
                            <div class="block-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="item-order">
                                            <table class="table table-hover">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th scope="col">Họ Tên</th>
                                                    <th scope="col">Ngày Sinh</th>
                                                    <th scope="col">Giới Tính</th>
                                                    <th scope="col">Ghi Chú</th>
                                                    <th scope="col" width="50px"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($customer_user->birthday_customer as $birthday)
                                                    <tr>
                                                        <td>{{$birthday->full_name}}</td>
                                                        <td>{{date('d-m-Y',strtotime($birthday->birthday_child))}}</td>
                                                        <td>
                                                            @if($birthday->sex_child == 1)
                                                                Nam
                                                            @endif
                                                            @if($birthday->sex_child == 2)
                                                                Nữ
                                                            @endif
                                                        </td>
                                                        <td>{{$birthday->note_child}}</td>
                                                        <td>
                                                            <a href="javascript:void(0)"
                                                               data-url="{{route('page.customer.deleteBirthday')}}"
                                                               data-id="{{$birthday->id}}"
                                                               class="btn btn-danger btn-flat delete-birthday">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="box-button-add-new">
                                                <button class="btn btn-add-birthday" data-toggle="modal"
                                                        data-target="#modal_birthday">Thêm mới
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>{{trans('frontend::frontend.my_account')}}</strong>
                            </div>
                            <div class="content block-collapsible-nav-content" id="block-collapsible-nav">
                                @include('partials.menu-account')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <div class="modal fade" id="modal_birthday" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            {!! Form::open(['route' => ['page.customer.postAddBirthday'], 'method' => 'post','id'=>'form_birthday']) !!}
            <div class="modal-content">
                <div class="modal-body">
                    <div class="panel-body">
                        <div class="box-wrapper-birthday box-multi-add">
                            <div class="item-detail box-input-add">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="full_name">Họ và Tên</label>
                                        <input type="text" class="form-control" name="full_name"
                                               placeholder="Họ và Tên" id="full_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="birthday_child">Ngày Sinh </label>
                                        <input type="date" class="form-control" name="birthday_child"
                                               placeholder="Ngày Sinh " id="birthday_child" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="sex_child">Giới Tính </label>
                                        <select name="sex_child" id="sex_child" class="form-control" required>
                                            <option value="">Chọn giới tính
                                            </option>
                                            <option value="1">Nam</option>
                                            <option value="2">Nữ</option>
                                            <option value="3">Khác</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="note_child">Ghi Chú </label>
                                        <textarea class="form-control" name="note_child"
                                                  placeholder="Ghi Chú " id="note_child"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop