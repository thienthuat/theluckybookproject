@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>trans('frontend::frontend.register')])
@stop
@section('content')
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper line-border-bottom">
                <div class="row align-items-center">
                    <div class="col-md-9">
                        <h1 class="page-title mb-1">
                            {{trans('frontend::frontend.register')}}
                        </h1>
                        <p>Đăng nhập hoặc đăng ký tại THE LUCKYBOOK có thể phục vụ bạn tốt hơn thông qua các chương trình
                            chăm sóc
                            khách hàng và các khuyến mãi đặc biệt dành cho khách hàng mua online.</p>
                    </div>
                    <div class="col-md-3">
                        <div class="box-info-support">
                            <span>Hỗ trợ</span>
                            <a href="tel:@setting('contact::hotline')"><i class="fa fa-phone"></i></a>
                            <a href="mailto:tel:@setting('contact::email')"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-content-register">
                <div class="row">
                    <div class="col-md-6 line-width">
                        @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                {{Session::get('error')}}
                            </div>
                        @endif
                        <div class="block block-customer-login">
                            <div class="block-title">
                                <h2>ĐĂNG NHẬP BẰNG TÀI KHOẢN FACEBOOK</h2>
                            </div>
                            <div class="box-icon-facebook">
                                <a href="{{route('page.login.facebook')}}" title="Đăng nhập facebook ">
                                    <div class="icon-facebook">
                                        <div class="icon">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </div>
                                        <div class="text">
                                            Login With Facebook
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="block-title">
                                <h2>HOẶC ĐIỀN NHỮNG THÔNG TIN DƯỚI ĐÂY</h2>
                            </div>
                            <div class="box-form-register">
                                <form class="form" action="{{route('page.postRegister')}}" method="post"
                                      id="form_create_account">
                                    {!! csrf_field() !!}
                                    <div class="form-row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="firstname">{{trans('frontend::form.first_name')}}
                                                    <sup>*</sup></label>
                                                <input type="text" class="form-control" id="firstname" name="firstname"
                                                       value="{{old('firstname')}}"
                                                       autocomplete="off" required>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label for="lastname">{{trans('frontend::form.last_name')}}<sup>*</sup></label>
                                                <input type="text" class="form-control" id="lastname" name="lastname"
                                                       value="{{old('lastname')}}"
                                                       autocomplete="off" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email<sup>*</sup></label>
                                        <input type="email" class="form-control" id="email" name="email"
                                               value="{{old('email')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Số điện thoại<sup>*</sup></label>
                                        <input type="text" class="form-control" id="phone" name="phone"
                                               value="{{old('phone')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="birthday">Ngày sinh <sup>*</sup></label>
                                        <input type="date" class="form-control" id="birthday" name="birthday"
                                               value="{{old('birthday')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="form-group box-sex">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" id="sex_1" value="1"
                                                   checked>
                                            <label class="form-check-label" for="sex_1">Name</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" id="sex_2"
                                                   value="2">
                                            <label class="form-check-label" for="sex_2">Nữ</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Địa chỉ<sup>*</sup></label>
                                        <input type="text" class="form-control" id="address" name="address"
                                               value="{{old('address')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="province_id">Tỉnh/Thành phố <sup>*</sup></label>
                                            <select id="province_id" name="province_id"
                                                    data-url="{{route('page.location.ajaxDistrict')}}"
                                                    class="custom-select-form form-control" required>
                                                <option value="">--Chọn Tỉnh/Thành phố--</option>
                                                @foreach($provinces as $province)
                                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="district_id">Quận/huyện <sup>*</sup></label>
                                            <select id="district_id" name="district_id"
                                                    class="custom-select-form  form-control"
                                                    data-url="{{route('page.location.ajaxWard')}}" required>
                                                <option value="">--Chọn Quận/huyện--</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="ward_id">Phường,xã <sup>*</sup></label>
                                            <select id="ward_id" name="ward_id" class="custom-select-form  form-control"
                                                    required>
                                                <option value="">--Chọn Phường,xã--</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Mật khẩu <sup>*</sup></label>
                                        <input type="password" class="form-control" id="password" name="password"
                                               minlength="6"
                                               value="{{old('password')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Xác nhận mật khẩu <sup>*</sup></label>
                                        <input type="password" class="form-control" id="password_confirmation"
                                               name="password_confirmation" minlength="6"
                                               value="{{old('password_confirmation')}}"
                                               autocomplete="off" required>
                                    </div>
                                    <div class="actions-toolbar">
                                        <div class="box-control-form">
                                            <div class="row align-items-center">
                                                <div class="col-12">
                                                    <div class="box-wrapper-action">
                                                        <button type="submit" class="action btn-action-form primary"
                                                                name="send"
                                                                id="send2">
                                                            <i class="fa fa-pencil-square-o"></i><span> {{trans('frontend::frontend.register')}}</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block block-new-customer">
                            <div class="block-title">
                                <h2>BẠN ĐÃ CÓ TÀI KHOẢN ?</h2>
                            </div>
                            <div class="block-content">
                                <div class="actions-toolbar">
                                    <div class="primary">
                                        <div class="box-control-form">
                                            <a href="{{route('page.login')}}"
                                               title="{{trans('frontend::frontend.login')}}"
                                               class="action btn-action-form primary"><i class="fa fa-sign-in"
                                                                                         aria-hidden="true"></i><span>{{trans('frontend::frontend.login')}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@stop