@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>$page->meta_title])
@stop
@section('content')
    <section class="container">
        @php($banner = $page->getImageAttribute() !='' ? $page->getImageAttribute()->path:'')
        @if($banner != '')
            <div class="box-banner-for-page">
                <img src="{{url($banner)}}" alt="{{$page->title}}" class="img-fluid">
            </div>
        @endif
        <div class="breadcrumbs">
            <ul class="items">
                <li class="item home">
                    <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                        {{trans('frontend::frontend.homepage')}}
                    </a>
                </li>
                <li class="item">
                    <strong>{{$page->title}}</strong>
                </li>
            </ul>
        </div>
    </section>
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{$page->title}}
                </h1>
            </div>
            <div class="box-content-contact">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-content">
                            {!! $page->body !!}
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    </main>
@stop

