@extends('layouts.master')
@section('meta')
@include('partials.meta',['title'=>$product->meta_title,'keywords'=>$product->meta_keyword,'description'=>$product->meta_description])
@stop
@section('content')
<section class="container">
    <div class="breadcrumbs">
        <ul class="items">
            <li class="item home">
                <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                    {{trans('frontend::frontend.homepage')}}
                </a>
            </li>
            @foreach ($breadcrumbs as $breadcrumb)
            <li class="item home">
                <a href="{{route('page.product.getProductCategory', $breadcrumb->slug)}}"
                    title="{{$breadcrumb->title}}">
                    {{$breadcrumb->title}}
                </a>
            </li>
            @endforeach
            <li class="item blog">
                <strong>{{$product->title}}</strong>
            </li>
        </ul>
    </div>
</section>
<main class="page-main-content">
    <section class="container">
        <div class="row content-product-detail">
            <div class="col-md-5 left-content-product-detail">
                <div class="box-slider-product">
                    <div id="product_page_carousel">
                        @php($multi_images= $product->getMultipleThumbnailAttribute())
                        @if($multi_images->count() >0)
                        @foreach($multi_images as $image)
                        <a href="{{url($image->path)}}" data-fancybox="images">
                            <img src="{{url($image->path)}}" alt="" class="img-fluid">
                        </a>
                        @endforeach
                        @endif
                    </div>
                    <div id="thumb_product_page_carousel">
                        @if($multi_images->count() >0)
                        @foreach($multi_images as $image)
                        <div>
                            <img src="{{Imagy::getThumbnail($image->path, 'smallThumb')}}" alt="" class="img-fluid">
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="row des-product-detail">
                    <div class="col-md-12">
                        <div class="box-content">
                            {!! $product->description !!}
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-7 right-content-product-detail">
                <div id="status"></div>
                <div class="page-title-wrapper">
                    <h1 class="page-title">
                        {{$product->title}}
                    </h1>
                </div>
                <div class="product-info-price">
                    <div class="price-box price-final_price">
                        @if($product->price_sale > 0)
                        <div class="special-price">
                            <div class="price-container">
                                <div class="price-label">Special Price</div>
                                <div class="price-wrapper ">
                                    <div class="price">
                                        <sup>đ</sup>
                                        <span id="price_change">{{number_format($product->price_sale)}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="old-price">
                            <div class="price-container">
                                <span class="price-label">Regular Price</span>
                                <div class="price-wrapper ">
                                    <div class="price"><sup>đ</sup>{{number_format($product->price)}}</div>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="special-price">
                            <div class="price-container">
                                <span class="price-label">Special Price</span>
                                <div class="price-wrapper ">
                                    <div class="price"><sup>đ</sup>
                                        <span id="price_change">
                                            {{number_format($product->price)}}
                                        </span></div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="box-product-add-form">
                    <div class="box-add-to-cart">
                        @if($product->quanlity<=0) <div class="actions">
                            <strong>Sản phẩm hết hàng</strong>
                    </div>
                    @else
                    <div class="box-qty">
                        <label class="label" for="qty"><span>Số lượng</span></label>
                        <div class="control">
                            <input type="number" name="qty" id="qty" minlength="1" maxlength="12" value="1" title="Qty"
                                class="input-text qty">
                        </div>
                    </div>
                    <div class="actions">
                        <button type="button" title="Thêm vào giỏ hàng"
                            class="action primary tocart product_addtocart_button_detail"
                            data-url="{{route('page.cart.addToCart')}}" data-id="{{$product->id}}"
                            data-price="{{$product->price_sale >0 ?$product->price_sale:$product->price}}">
                            <span>Thêm vào giỏ hàng</span>
                        </button>
                    </div>
                    @endif
                </div>
                @if($product->check_excluded)
                <p style="font-size: 20px; color:red; padding:10px;font-weigth:bold;"> EXCLUDED FROM PROMOTIONS</p>
                @endif
            </div>
            <div class="product-social-links">
                <div class="product-addto-links">
                    <a href="javascript:void(0)" data-url="{{route('page.addToWishList',$product->id)}}"
                        class="action towishlist add-to-wishlist"><span>Thêm vào yêu thích</span></a>
                </div>
                <div class="product-share-fb">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="box-sumary">
                <div class="value" itemprop="description">
                    <p>
                        {!! $product->sumary !!}
                    </p>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="box-comment-fb">
                    <div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5"></div>
                </div>
            </div>
        </div>
        @if($product_relateds->count() >0)
        <div class="row">
            <div class="col-md-12">
                <div class="box-product-related">
                    <div class="block-title title">
                        <strong id="block-related-heading" role="heading" aria-level="2">Có thể bạn sẽ
                            thích</strong>
                    </div>
                    <div class="box-related">
                        <div class="owl-carousel owl-theme box-slider-category-product">
                            @foreach($product_relateds as $product_related)
                            @php($image = $product_related->getThumbnailAttribute() != '' ?
                            Imagy::getThumbnail($product_related->getThumbnailAttribute()->path , 'mediumThumb')
                            : Theme::url('images/img404.png'))
                            <div class="item">
                                <div class="products-grid">
                                    <div class="product-item">
                                        <div class="product-item-info">
                                            <div class=" product-item-images">
                                                <a href="{{route('page.detailProduct',$product_related->slug)}}"
                                                    title="{{$product_related->title}}" class="product-item-photo">
                                                    <div class="product-image-container">
                                                        <div class="product-image-wrapper">
                                                            <img class="product-image-photo" src="{{url($image)}}"
                                                                alt="{{$product_related->title}}">
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product-item-details">
                                                <div class="content-name">
                                                    <strong class="product-item-name">
                                                        <a title="{{$product_related->title}}"
                                                            href="{{route('page.detailProduct',$product_related->slug)}}"
                                                            class="product-item-link">
                                                            {{$product_related->title}} </a>
                                                    </strong>
                                                    <a href="javascript:void(0)" class="action wishlist add-to-wishlist"
                                                        data-url="{{route('page.addToWishList',$product_related->id)}}"
                                                        data-action="add-to-wishlist" title="Add to Wishlist">
                                                    </a>
                                                </div>
                                                <div class="price-box price-final_price">
                                                    @if($product_related->price_sale > 0)
                                                    <span class="price-container price-final_price tax weee">
                                                        <span class="price-wrapper ">
                                                            <span
                                                                class="price"><sup>đ</sup>{{number_format($product_related->price_sale)}}</span>
                                                        </span>
                                                        <span class="price-wrapper old-price">
                                                            <span
                                                                class="price"><sup>đ</sup>{{number_format($product_related->price)}}</span>
                                                        </span>
                                                    </span>
                                                    @else
                                                    <span class="price-container price-final_price tax weee">
                                                        <span class="price-wrapper ">
                                                            <span
                                                                class="price"><sup>đ</sup>{{number_format($product_related->price)}}</span>
                                                        </span>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </section>
</main>
@stop