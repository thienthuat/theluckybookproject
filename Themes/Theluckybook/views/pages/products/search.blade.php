@extends('layouts.master')
@section('meta')
    @include('partials.meta',[])
@stop
@section('content')
    <div class="category-view-top-wrapper">
        <div class="container">
            <div class="category-view-top">
                <div class="page-title-wrapper">
                    <h1 class="page-title" id="page-title-heading" >
                        <span class="base">Kết quả tìm kiếm cho : {{$key}}</span></h1>
                </div>
                <div class="breadcrumbs">
                    <ul class="items">
                        <li class="item home">
                            <a href="{{route('homepage')}}" title="Trang Chủ">
                                Trang Chủ </a>
                        </li>
                        <li class="item category118">
                            <strong>Kết quả tìm kiếm cho : {{$key}}</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <main id="maincontent" class="page-product-main">
        <div class="container">
            <div class="row">
                <div class="col-md-9 order-md-2">
                    <h2 class="product-page-title">Kết quả tìm kiếm cho : {{$key}}</h2>
                    <div class="box-content-product-page">
                        <div class="row">
                            @foreach($products as $product)
                                @php($image = $product->getThumbnailAttribute() != '' ?
                                                    Imagy::getThumbnail($product->getThumbnailAttribute()->path , 'mediumThumb')
                                                    : Theme::url('images/img404.png'))
                                <div class="item col-md-4 col-sm-6 col-12">
                                    <div class="products-grid">
                                        <div class="product-item">
                                            <div class="product-item-info">
                                                <div class=" product-item-images">
                                                    <a href="{{route('page.detailProduct',$product->slug)}}"
                                                       title="{{$product->title}}"
                                                       class="product-item-photo">
                                                        <div class="product-image-container">
                                                            <div class="product-image-wrapper">
                                                                <img class="product-image-photo"
                                                                     src="{{url($image)}}"
                                                                     alt="{{$product->title}}">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="product-item-details">
                                                    <div class="content-name">
                                                        <strong class="product-item-name">
                                                            <a title="{{$product->title}}"
                                                               href="{{route('page.detailProduct',$product->slug)}}"
                                                               class="product-item-link">
                                                                {{$product->title}} </a>
                                                        </strong>
                                                        <a href="javascript:void(0)"
                                                           class="action wishlist add-to-wishlist"
                                                           data-url="{{route('page.addToWishList',$product->id)}}"
                                                           data-action="add-to-wishlist"
                                                           title="Add to Wishlist">
                                                        </a>
                                                    </div>
                                                    @if($product->quanlity==0)
														<div class="box-dont-has-product">
															<span>Sản phẩm hết hàng</span>
														</div>
														@else
														<div class="price-box price-final_price">
                                                            @if($product->price_sale > 0)
                                                                <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price_sale)}}</span>
                                                                                </span>
                                                                                <span class="price-wrapper old-price">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                                            @else
                                                                <span class="price-container price-final_price tax weee">
                                                                                <span class="price-wrapper ">
                                                                                    <span class="price"><sup>đ</sup>{{number_format($product->price)}}</span>
                                                                                </span>
                                                                            </span>
                                                            @endif
                                                        </div>
														@endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="toolbar toolbar-products">
                                    {!! $products->links('partials.pagination.default') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-short-product">
                        <div class="box-widget">
                            <div class="block-title">
                                <strong>Sản Phẩm Mới</strong>
                            </div>
                            <div class="block-content-product">
                                {!! app('\Modules\Product\Http\Controllers\PublicController')->getProductNew() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop