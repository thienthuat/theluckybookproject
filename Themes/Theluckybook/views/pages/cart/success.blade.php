@extends('layouts.master')
@section('meta')
@include('partials.meta',['title'=>$page->meta_title])
@stop
@section('content')
<section class="container">
    @php($banner = $page->getBannerForPage() !='' ? $page->getBannerForPage()->path:'')
    @if($banner != '')
    <div class="box-banner-for-page">
        <img src="{{url($banner)}}" alt="{{$page->title}}" class="img-fluid">
    </div>
    @endif
    <div class="breadcrumbs">
        <ul class="items">
            <li class="item home">
                <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                    {{trans('frontend::frontend.homepage')}}
                </a>
            </li>
            <li class="item">
                <strong>{{$page->title}}</strong>
            </li>
        </ul>
    </div>
</section>
<main class="page-main-content">
    <section class="container">
        <div class="page-title-wrapper">
            <h1 class="page-title">
                {{$page->title}}
            </h1>
        </div>
        <div class="box-content-contact">
            @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                {{Session::get('error')}}
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                {{Session::get('success')}}
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    {!! $page->body !!}
                </div>
            </div>
            <div id="box_main_cart" class="mt-5">
                <div class="row">
                    <div class="col-md-4 order-md-2">
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>Thông tin đơn hàng</strong>
                            </div>
                            <div class="content block-info-cart">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Mã đơn hàng:</span></strong>
                                    </div>
                                    <div>
                                        <span>{{$invoice->code}}</span>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Tạm tính:</span></strong>
                                    </div>
                                    <div>
                                        <span>{{number_format($invoice->sub_total)}} <sup>đ</sup></span>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Chi phí vận chuyển:</span></strong>
                                    </div>
                                    <div>
                                        <span>{{number_format($invoice->price_ship)}} <sup>đ</sup></span>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Giảm giá:</span></strong>
                                    </div>
                                    <div>
                                        <span>{{$totalPromo}} <sup>đ</sup></span>
                                    </div>
                                </div>
                                <div class="total-cart">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <strong><span>Tổng tiền (Đã bao gồm VAT):</span></strong>
                                        </div>
                                        <div>
                                            <span>{{number_format($invoice->total)}} <sup>đ</sup></span>
                                        </div>
                                    </div>
                                </div>
                                @if((isset($invoice->method_payment->description)?
                                $invoice->method_payment->description: null) != null)
                                <div class="info-payment">
                                    @php
                                    $des = strtr($invoice->method_payment->description, [
                                                        '%invoice_number%' => $invoice->code ?? ''
                                                    ]);    
                                    @endphp
                                    {!! $des !!}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="block block-cart-info">
                            <div class="item-theart-cart">
                                <div class="row">
                                    <div class="col-md-7">
                                        <h5>Sản phẩm</h5>
                                    </div>
                                    <div class="col-md-5 d-none d-md-block">
                                        <div class="row">
                                            <div class="col-md-4">Giá</div>
                                            <div class="col-md-4">Số lượng</div>
                                            <div class="col-md-4">Tạm tính</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach($invoice->item_invoice as $item)
                            <div class="item-cart">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="row">
                                            <div class="col-12">
                                                <h3><a
                                                        href="{{route('page.detailProduct',$item->slug)}}">{{$item->title}}</a>
                                                </h3>
                                                @if($item->size !='' || $item->color != '' )
                                                <p class="option-item-cart">
                                                    <i>( {{$item->size !='' ? 'Kích thước:'.$item->size:''}}
                                                        {{$item->color !='' ? 'Màu sắc:'.$item->color:''}}
                                                        )</i></p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="row info-price">
                                            <div class="col-4">
                                                <span class="d-md-none">Giá:</span>
                                                <span>{{number_format($item->price)}}
                                                    <sup>đ</sup></span>
                                            </div>
                                            <div class="col-4">
                                                <span class="d-md-none">SL:</span>
                                                <span>{{$item->qty}}</span>
                                            </div>
                                            <div class="col-4">
                                                <span class="d-md-none">Tạm tính:</span>
                                                <span>{{number_format($item->price*$item->qty)}}
                                                    <sup>đ</sup></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="action-cart">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="actions">
                                            <div class="primary">
                                                <a id="top-cart-btn-view-cart" href="{{route('homepage')}}"
                                                    class="action primary checkout" title="Tiếp tục mua hàng">
                                                    <span><i class="fa fa-angle-left" aria-hidden="true"></i> Tiếp tục
                                                        mua hàng</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop