@extends('layouts.master')
@section('meta')
@include('partials.meta',['title'=>trans('frontend::frontend.page_checkout')])
@stop
@section('content')
@php($customer_user = Auth::guard('customer')->user())
<main class="page-main-content">
    <section class="box-content-cart">
        <div class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    <span class="base">{{trans('frontend::frontend.page_checkout')}}</span>
                </h1>
            </div>
            <form class="form form-checkout-no-login" action="{{route('page.cart.checkoutNoAuth')}}" method="post">
                <div class="row">

                    <div class="col-md-8">
                        <div class="box-checkout-no-login">
                            <div class="checkout-box_heading">
                                <h3 class="checkout-box_title">
                                    Địa chỉ giao hàng của quý khách
                                </h3>
                                @if($customer_user == null)
                                <a class="checkout-box_link-speedy-login" data-toggle="modal" data-target="#modalLogin"
                                    href="javascript:void(0)">Đăng nhập để đặt hàng</a>
                                @endif
                            </div>
                            <div class="block-content">
                                @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span>
                                    </button>
                                    {{Session::get('error')}}
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span>
                                    </button>
                                    {{Session::get('success')}}
                                </div>
                                @endif
                                <div class="block block-dashboard-addresses">
                                    <div class="block-content">
                                        {!! csrf_field() !!}
                                        @if($customer_user == null)
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-3 col-form-label">Email*</label>
                                            <div class="col-sm-9 col-md-7">
                                                <input type="email"
                                                    class="form-control {{$errors->has('email') ?'error':''}}"
                                                    id="email" name="email" value="{{old('email')}}" required>
                                                @if ($errors->has('email'))
                                                <label id="email-error" class="error"
                                                    for="email">{{ $errors->first('email') }}</label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname"
                                                class="col-sm-3 col-form-label">{{trans('frontend::form.first_name')}}
                                                *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <input type="text" class="form-control" id="fullname" name="fullname"
                                                    value="{{old('fullname')}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="address" class="col-sm-3 col-form-label">Địa chỉ nhận
                                                hàng
                                                *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <input type="text" class="form-control" id="address" name="address"
                                                    required value="{{old('address')}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="province_id" class="col-sm-3 col-form-label">Tỉnh/Thành
                                                phố
                                                *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <select name="province_id" class="custom-select" id="province_id"
                                                    data-url="{{route('page.location.ajaxDistrict')}}" required>
                                                    <option value="">Lựa chọn</option>
                                                    @foreach($provinces as $province)
                                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="district_id" class="col-sm-3 col-form-label">Quận/huyện
                                                *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <select name="district_id" class="custom-select" id="district_id"
                                                    data-url="{{route('page.location.ajaxWard')}}" required>
                                                    <option value="">Lựa chọn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ward_id" class="col-sm-3 col-form-label">Phường,
                                                xã *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <select name="ward_id" class="custom-select" id="ward_id" required>
                                                    <option value="">Lựa chọn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-sm-3 col-form-label">Số điện thoại
                                                *</label>
                                            <div class="col-sm-9 col-md-7">
                                                <input type="text" class="form-control" id="phone" name="phone"
                                                    value="{{old('phone')}}" required>
                                            </div>
                                        </div>
                                        <div class="box-note">
                                            <div class="form-group row">
                                                <label for="note" class="col-sm-3 col-form-label">
                                                    Ghi Chú
                                                </label>
                                                <div class="col-sm-9 col-md-7">
                                                    <textarea class="form-control" id="note" rows="5"
                                                        name="note">{{old('note')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-sm-3 col-form-label"></label>
                                            <div class="col-sm-9 col-md-7">
                                                <input type="checkbox" name="add_new_password"
                                                    id="box_check_change_password" value="1" class="checkbox"
                                                    {{ (is_array(old('add_new_password')) && in_array(1, old('add_new_password'))) ? ' checked' : '' }}>
                                                <label class="label" for="box_check_change_password"><span>Tạo một tài
                                                        khoản để sử dụng sau</span></label>
                                            </div>
                                        </div>
                                        <fieldset class="fieldset password" id="box_show_change_password"
                                            style="display: {{ (is_array(old('change_password')) && in_array(1, old('change_password'))) ? ' block' : 'none' }};">
                                            <div class="form-group row">
                                                <label for="password" class="col-sm-3 col-form-label">Mật
                                                    Khẩu</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="password" class="input-text" name="password"
                                                        id="password">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password_confirmation"
                                                    class="col-sm-3 col-form-label">{{trans('frontend::form.confirm_new_password')}}</label>
                                                <div class="col-sm-9 col-md-7">
                                                    <input type="password" class="input-text"
                                                        name="password_confirmation" id="password_confirmation">
                                                </div>
                                            </div>
                                        </fieldset>
                                        @else
                                        <div id="existing-addresses">
                                            @foreach($address as $item)
                                            <div
                                                class="wrapper-address-option {{$item->default_shipping_address ==1 ?'current':''}}">
                                                <label class="address-option">
                                                    <input id="inputAddressType{{$item->id}}"
                                                        class="option-address-checkout" value="{{$item->id}}"
                                                        {{$item->default_shipping_address ==1 ?'checked':''}}
                                                        {{ old('shippingAddressId')==$item->id ? 'checked' : '' }}
                                                        type="radio" name="shippingAddressId"
                                                        data_price_ship="{{$item->districts->price_ship}}">
                                                    <div class="checkout-address">
                                                        <p class="ch-head checkout-address _name">
                                                            {{$item->full_name}}
                                                        </p>
                                                        <p>{{$item->address}}</p>
                                                        <p>
                                                            {{$item->citys->name}}
                                                            , {{$item->districts->type}} {{$item->districts->name}}
                                                            , {{$item->ward->type}} {{$item->ward->name}}
                                                        </p>
                                                        <p>Điện thoại di động: {{$item->phone}}</p>
                                                    </div>
                                                </label>
                                            </div>
                                            @endforeach
                                            <div class="wrapper-address-option">
                                                <label class="address-option">
                                                    <input value="0" type="radio" class="option-address-checkout"
                                                        id="new-address-option" name="shippingAddressId"
                                                        {{ old('shippingAddressId')=="0" ? 'checked' : '' }}>
                                                    <div class="text">
                                                        Thêm địa chỉ khác
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="box-form-add-address">
                                                <div class="form-group row">
                                                    <label for="fullname"
                                                        class="col-sm-3 col-form-label">{{trans('frontend::form.first_name')}}</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <input type="text" class="form-control" id="fullname"
                                                            name="fullname" value="{{old('fullname')}}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="address" class="col-sm-3 col-form-label">Địa chỉ
                                                        nhận
                                                        hàng
                                                        *</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <input type="text" class="form-control" id="address"
                                                            name="address" required value="{{old('address')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="province_id" class="col-sm-3 col-form-label">Tỉnh/Thành
                                                        phố
                                                        *</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <select name="province_id" class="custom-select"
                                                            id="province_id"
                                                            data-url="{{route('page.location.ajaxDistrict')}}" required>
                                                            <option value="">Lựa chọn</option>
                                                            @foreach($provinces as $province)
                                                            <option value="{{$province->id}}">{{$province->name}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="district_id" class="col-sm-3 col-form-label">Quận/huyện
                                                        *</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <select name="district_id" class="custom-select"
                                                            id="district_id"
                                                            data-url="{{route('page.location.ajaxWard')}}" required>
                                                            <option value="">Lựa chọn</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="ward_id" class="col-sm-3 col-form-label">Phường,
                                                        xã *</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <select name="ward_id" class="custom-select" id="ward_id"
                                                            required>
                                                            <option value="">Lựa chọn</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-3 col-form-label">Số điện
                                                        thoại
                                                        *</label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <input type="text" class="form-control" id="phone" name="phone"
                                                            value="{{old('phone')}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-note mt-3">
                                                <div class="form-group row">
                                                    <label for="note" class="col-sm-3 col-form-label">
                                                        Ghi Chú
                                                    </label>
                                                    <div class="col-sm-9 col-md-7">
                                                        <textarea class="form-control" id="note" rows="5"
                                                            name="note">{{old('note')}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="actions">
                                                    <div class="primary text-center">
                                                        <button type="submit" class="action primary checkout"
                                                            title="Tiến hành thanh toán">
                                                            <span>{{trans('frontend::frontend.page_checkout')}}
                                                                »</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block block-collapsible-nav block-item-nav-cart-first">
                            <div class="title block-collapsible-nav-title">
                                <strong>Hình thức thanh toán</strong>
                            </div>
                            <div class="content block-info-cart">
                                <div class="custom-controls-stacked">
                                    @foreach($payment_methods as $key=>$method)
                                    <label class="custom-control custom-radio">
                                        <input id="payment_method{{$method->id}}" value="{{$method->id}}"
                                            name="payment_method" type="radio" class="custom-control-input"
                                            {{$key==0 ?'checked':''}}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{$method->title}}</span>
                                    </label>
                                    @endforeach
                                    {{-- <label class="custom-control custom-radio" for="payment_method_zalo_atm">
                                        <input id="payment_method_zalo_atm" value="ZALO_ATM"
                                            name="payment_method" type="radio" class="custom-control-input"
                                            {{$key==0 ?'checked':''}}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{$method->title}}</span>
                                    </label> --}}
                                </div>
                            </div>
                        </div>
                        <div class="block block-collapsible-nav">
                            <div class="title block-collapsible-nav-title">
                                <strong>Thông tin đơn hàng</strong>
                            </div>
                            <div class="content block-info-cart">
                                <div class="cart-product">
                                    <div class="item header-box-cart">
                                        <div class="d-flex justify-content-between">
                                            <div class="cart-product-item-name">
                                                Sản phẩm
                                            </div>
                                            <div class="cart-product-item-qty">Số lượng</div>
                                            <div class="cart-product-item-price">Thành tiền</div>
                                        </div>
                                    </div>
                                    @if(Cart::content()->count() > 0)
                                    @foreach(Cart::content() as $item)
                                    <div class="item">
                                        <div class="d-flex justify-content-between">
                                            <div class="cart-product-item-name">
                                                <strong><span>{{$item->name}}</span></strong>
                                            </div>
                                            <div class="cart-product-item-qty">
                                                <span>{{$item->qty}}</span>
                                            </div>
                                            <div class="cart-product-item-price">
                                                <span>{{number_format($item->price*$item->qty)}}
                                                    <sup>đ</sup></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Tạm tính:</span></strong>
                                    </div>
                                    <div>
                                        <span id="checkout_sub_total_price">{{$totalCart}}</span><sup>đ</sup>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <strong><span>Chi Phí Vận Chuyển:</span></strong>
                                    </div>
                                    <div>

                                        <span id="checkout_ship_price">{{$price_ship}}</span><sup>đ</sup>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" id="box-show-promo">
                                    <div>
                                        <strong><span>Giảm giá:</span></strong>
                                    </div>
                                    <div>

                                        <span id="checkout_promo">{{$promo}}</span><sup>đ</sup>
                                    </div>
                                </div>
                                @if($customer_user)
                                <div class="box-promotion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Điểm tích lũy</h4>
                                        </div>
                                        <div class="panel-body">
                                            <p>Số điểm tích lũy của bạn: <span id="point_aftter_cart">{{$customer_user->point - (int)session('cart_point')}}</span></p>
                                            <div class="input-group">
                                                <input id="input_point" placeholder="Nhập ở đây.." type="number"
                                                    class="form-control" value="{{old('point',session('cart_point'))}}"
                                                    name="point">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-coupon" id="btn_coupon_point"
                                                        data-url="{{route('page.cart.addCodePoint')}}" data-type="point"
                                                        type="button">Đồng ý</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="total-cart">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <strong><span>Tổng tiền (Đã bao gồm VAT):</span></strong>
                                        </div>
                                        <div>
                                            <span id="checkout_total_price">{{$totalCartAfter}}</span><sup>đ</sup>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</main>
<!-- Modal -->
@if(!$customer_user)
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{trans('frontend::frontend.login')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form form-login" action="{{route('page.postLoginModal')}}" method="post"
                    id="login_form_modal">
                    {!! csrf_field() !!}
                    <div id="statusLogin"></div>
                    <fieldset class="fieldset login">
                        <div class="field note">{{trans('frontend::frontend.note_registed_customer')}}</div>
                        <div class="form-group">
                            <label for="email_login">Email</label>
                            <input name="email_login" value="{{old('email')}}" autocomplete="off" id="email_login"
                                type="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="password_modal">Password</label>
                            <input name="password" type="password" autocomplete="off" class="form-control"
                                id="password_modal" required>

                        </div>
                        <div class="actions-toolbar">
                            <div class="primary">
                                <button type="submit" class="action login primary" name="send" id="send2"><span>
                                        {{trans('frontend::frontend.login')}}</span>
                                </button>
                            </div>
                            <div class="secondary">
                                <a class="action remind" title="{{trans('frontend::frontend.forgot_password')}}"
                                    href="{{route('page.forgotPassword')}}">
                                    <span>{{trans('frontend::frontend.forgot_password')}}</span>
                                </a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@stop
@section('scripts')
<script>
    window.addEventListener("load", function(){
        $('#btn_coupon_point').click();
    });
</script>
@endsection