@extends('layouts.master')
@section('meta')
@include('partials.meta')
@stop
@section('content')
<main>
    <section class="container">
        @if(Block::get('hot-promotion-cap') != "")
        <div class="caution-banner">{!! Block::get('hot-promotion-cap') !!}</div>
        @else
        <div class="caution-banner">5% cashback. 1 bookpoint = 1.000 VND</div>
        @endif
        <section class="box-home-slider">
            <div class="row">
                <div class="col-sm-8">
                    <div class="box-slider">
                        <div class="owl-carousel owl-theme" id="home_slider_carousel">
                            @foreach($sliders as $slider)
                            @php($image = $slider->getThumbnailAttribute() != ''
                            ?$slider->getThumbnailAttribute()->path:'')
                            <div class="item">
                                <div class="box-img-slider">
                                    <img src="{{url($image)}}" alt="{{$slider->title}}">
                                </div>
                                <div class="slide-caption">
                                    <h2 class="slide-caption__title">{{$slider->title}}</h2>
                                    <p class="slide-caption__desc">
                                        {{$slider->sumary}}
                                    </p>
                                    <a href="{{$slider->link}}" class="btn"
                                        title="{{$slider->title}}">{{$slider->text_link}}</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-banner-image">
                    <div class="image-banner image-banner-top">
                        {!! Block::get('image-banner-top') !!}
                    </div>
                    <div class="image-banner image-banner-bottom">
                        {!! Block::get('image-banner-bottom') !!}
                    </div>
                </div>
            </div>
        </section>
        @include('partials.products.new-home',['products'=>$product_news])
        @include('partials.products.selling',['products'=>$product_sellings])
        <section class="featureproduct-slider th-featureproduct-slider">
            <div class="row">
                @foreach ($ads as $key => $value)
                @php($image = $value->getThumbnailAttribute() != '' ?$value->getThumbnailAttribute()->path:'')
                <div class="item col-lg-4 col-sm-6 col-md-6 col-xs-12 item-home-special">
                    <div class="products-grid">
                        <div class="product-item">
                            <div class="product-item-info">
                                <div class=" product-item-images">
                                    <a href="{{ $value->link }}" title="{{ $value->title }}" class="product-item-photo">
                                        <div class="product-image-container">
                                            <div class="product-image-wrapper">
                                                <img class="product-image-photo" src="{{ $image }}"
                                                    alt="{{ $value->title }}" />
                                            </div>
                                        </div>
                                    </a>
                                    <div class="th-wrapper-view-more">
                                        <a title="{{ $value->title }}" href="{{ $value->link }}"
                                            class="th-product-title">
                                            {{ $value->title }}

                                        </a>
                                        <a href="{{ $value->link }}" title="{{ $value->title }}" class="btn-see-more">
                                            See DeTail
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
        </section>
        @include('partials.products.sale',['products'=>$product_sales])
        <section class="box-home-info mt-3">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="info info1">
                        <div class="static_info">
                            {!! Block::get('block-info-1') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="info info2">
                        <div class="static_info">
                            {!! Block::get('block-info-2') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="info info3">
                        <div class="static_info">
                            {!! Block::get('block-info-3') !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</main>
@stop