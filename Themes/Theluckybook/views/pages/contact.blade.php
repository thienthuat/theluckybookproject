@extends('layouts.master')
@section('meta')
    @include('partials.meta',['title'=>$page->meta_title])
@stop
@section('content')
    <section class="container">
        @php($banner = $page->getImageAttribute() !='' ? $page->getImageAttribute()->path:'')
        @if($banner != '')
            <div class="box-banner-for-page">
                <img src="{{url($banner)}}" alt="{{$page->title}}" class="img-fluid">
            </div>
        @endif
        <div class="breadcrumbs">
            <ul class="items">
                <li class="item home">
                    <a href="{{route('homepage')}}" title="@setting('core::site-name')">
                        {{trans('frontend::frontend.homepage')}}
                    </a>
                </li>
                <li class="item">
                    <strong>{{$page->title}}</strong>
                </li>
            </ul>
        </div>
    </section>
    <main class="page-main-content">
        <section class="container">
            <div class="page-title-wrapper">
                <h1 class="page-title">
                    {{$page->title}}
                </h1>
            </div>
            <div class="box-content-contact">
                @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-content">
                            {!! $page->body !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.1256561203004!2d105.97605761451459!3d10.251448292679044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310a9d2e302b26bf%3A0xb87fa4531a758cde!2zNjQsIDE3IFRy4bqnbiBQaMO6LCBQaMaw4budbmcgNCwgVsSpbmggTG9uZywgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1561637192488!5m2!1sen!2s" width="600" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>

                    </div>
                </div>
                <div class="box-form-contact">
                    <form method="post" action="{{route('public.contact.submit')}}" id="frmContact">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name">Họ và Tên <sup>*</sup></label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Nhập họ và tên" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email <sup>*</sup></label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           aria-describedby="emailHelp" placeholder="Nhập email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone">Điện Thoại</label>
                                    <input type="text" class="form-control" id="phone" name="phone"
                                           placeholder="Nhập số điện thoại">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="company">Công Ty</label>
                                    <input type="text" class="form-control" id="company" name="company"
                                           placeholder="Nhập công ty">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="message">Nội dung <sup>*</sup></label>
                                    <textarea class="form-control" required rows="4" id="message" name="message"></textarea>
                                </div>
                            </div>
                            <div class="box-btn-contact">
                                <button type="submit" class="btn btn-send-contact">Gửi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>
@stop

