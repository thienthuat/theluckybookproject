var csrf_token = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function () {
    $('body').on('click', '.box-size-attr .choose-size', function () {
        $('.box-size-attr').removeClass('error');
        var price = $(this).attr('data-price')
        var price_format = $(this).attr('data-price-format')
        $("#price_change").text(price_format);
        $('.product_addtocart_button_detail').attr('data-price', price);
    });
    $('body').on('click', '.box-color-attr .choose', function () {
        $('.box-color-attr').removeClass('error');
    });
    $('body').on('click', '.product_addtocart_button_detail', function () {
        var product_id = $(this).attr('data-id')
        var price = $(this).attr('data-price')
        var url = $(this).attr('data-url')
        var qty = $('#qty').val()
        var type_color = $('input[name=type_color]:checked').val();
        var type_size = $('input[name=type_size]:checked').val();
        if($('.box-size-attr').length > 0 && type_size === undefined) {
            $('.box-size-attr').addClass('error');
        } else if(qty <= 0) {
            var html = '';
            html += '<div class="alert alert-danger alert-dismissible" role="alert">';
            html += '<button type="button" class="close" data-dismiss="alert"';
            html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
            html += '</button><span>Số lượng sản phẩm phải lớn hơn 0.</span></div>';
            $("#status").html(html);
            $('html,body').animate({
                scrollTop: ($("#status").offset().top) - 100
            });
        } else if($('.box-color-attr').length > 0 && type_color === undefined) {
            $('.box-color-attr').addClass('error');
        } else {
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    _token: csrf_token,
                    product_id: product_id,
                    price: price,
                    qty: qty,
                    type_color: type_color,
                    type_size: type_size
                },
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (data) {
                    $('body').removeClass('loading');
                    if(data.status === '200') {
                        changeDataCart(data);
                        var html = '';
                        html += '<div class="alert alert-success alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert"';
                        html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                        html += '</button><span>Sản phẩm đã được thêm vào giỏ hàng.</span></div>';
                        $("#status").html(html);
                    } else {
                        var html = '';
                        html += '<div class="alert alert-danger alert-dismissible" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert"';
                        html += 'aria-label="Close"><span aria-hidden="true">&times;</span>';
                        html += '</button><span>' + data.message + '</span></div>';
                        $("#status").html(html);
                    }
                    $('html,body').animate({
                        scrollTop: ($("#status").offset().top) - 100
                    });
                },
                error: function (xhr) { // if error occured
                    alert("Error occured.please try again");
                    $('body').removeClass('loading');
                },
                complete: function () {
                    $('body').removeClass('loading');
                }
            });
        }
    });
    $('body').on('click', '.header-icon-cart', function () {
        var parent = $(this).parents('.header-box-cart');
        if(parent.hasClass('active')) {
            parent.removeClass('active');
        } else {
            parent.addClass('active');
        }
    });
    $('body').on('click', '#btn-minicart-close', function () {
        var parent = $(this).parents('.header-box-cart');
        parent.removeClass('active');
    });
    $('body').on('click', '.btn-coupon', function () {
        var code = $("#input_coupon").val()
        var url = $(this).data('url');
        if(code != '') {
            $.ajax({
                url: url,
                type: 'get',
                data: {code: code},
                beforeSend: function () {
                    $('body').addClass('loading');
                },
                success: function (data) {
                    console.log(data)
                    $('body').removeClass('loading');
                    if(data.status === 200) {
                        $("#checkout_total_price").text(data.pricePromotion)
                    }
                },
                error: function (xhr) { // if error occured
                    alert("Error occured.please try again");
                    $('body').removeClass('loading');
                },
                complete: function () {
                    $('body').removeClass('loading');
                }
            });
        }
    });
    
    $('body').on('click', '.delete-header-cart', function () {
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'get',
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (data) {
                $('body').removeClass('loading');
                if(data.status === '200') {
                    changeDataCart(data);
                }
            },
            error: function (xhr) { // if error occured
                alert("Error occured.please try again");
                $('body').removeClass('loading');
            },
            complete: function () {
                $('body').removeClass('loading');
            }
        });
    });
    $('body').on('click', '.update-header-qty-cart', function () {
        var url = $(this).attr('data-url');
        var id = $(this).attr('data-id');
        var qty = $(this).parents('.actions-cart-header').find('input').val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                _token: csrf_token,
                cart_id: id,
                qty: qty
            },
            beforeSend: function () {
                $('body').addClass('loading');
            },
            success: function (data) {
                $('body').removeClass('loading');
                if(data.status === '200') {
                    changeDataCart(data);
                } else {
                    alert(data.message);
                    location.reload(true);
                }
            },
            error: function (xhr) { // if error occured
                alert("Error occured.please try again");
                $('body').removeClass('loading');
            },
            complete: function () {
                $('body').removeClass('loading');
            }
        });
    });
    
    function changeDataCart (data) {
        $('.header-icon-cart span').text(data.count_cart);
        $('.count_cart').text(data.count_cart);
        $('.subtotal_cart').text(data.subtotal_cart);
        $('.minicart-items-wrapper').html(data.view_header_cart);
        $('#box_main_cart').html(data.view_page_cart);
    }
});
