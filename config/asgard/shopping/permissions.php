<?php

return [
    'shopping.invoices' => [
        'index' => 'shopping::invoices.list resource',
        'create' => 'shopping::invoices.create resource',
        'edit' => 'shopping::invoices.edit resource',
        'destroy' => 'shopping::invoices.destroy resource',
    ],
    'shopping.invoiceitems' => [
        'index' => 'shopping::invoiceitems.list resource',
        'create' => 'shopping::invoiceitems.create resource',
        'edit' => 'shopping::invoiceitems.edit resource',
        'destroy' => 'shopping::invoiceitems.destroy resource',
    ],
    'shopping.methods' => [
        'index' => 'shopping::methods.list resource',
        'create' => 'shopping::methods.create resource',
        'edit' => 'shopping::methods.edit resource',
        'destroy' => 'shopping::methods.destroy resource',
    ],
// append




];
