<?php

return [
    'site-name' => [
        'description' => 'core::settings.site-name',
        'view' => 'text',
        'translatable' => true,
    ],
    'site-name-mini' => [
        'description' => 'core::settings.site-name-mini',
        'view' => 'text',
        'translatable' => true,
    ],
    'keywords' => [
        'description' => 'Keywords',
        'view' => 'text',
        'translatable' => true,
    ],
    'sumary' => [
        'description' => 'Sumary',
        'view' => 'text',
        'translatable' => true,
    ],'time_open' => [
        'description' => 'Time Open',
        'view' => 'text',
        'translatable' => false,
    ],
    'site-description' => [
        'description' => 'core::settings.site-description',
        'view' => 'textarea',
        'translatable' => true,
    ],
	'per_cent_point' => [
        'description' => 'percent points accumulated',
        'view' => 'number',
        'translatable' => false,
    ],
//    'template' => [
//        'description' => 'core::settings.template',
//        'view' => 'core::fields.select-theme',
//    ],
    'analytics-script' => [
        'description' => 'core::settings.analytics-script',
        'view' => 'textarea',
        'translatable' => false,
    ],
//    'locales' => [
//        'description' => 'core::settings.locales',
//        'view' => 'core::fields.select-locales',
//        'translatable' => false,
//    ],
];
